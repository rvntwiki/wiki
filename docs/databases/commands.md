---
title: SQL Commands
Summary: Commonly used SQL Commands
Author: rvnt
Dataset: 2024-01-28
base_url: https://sanctum.spctr.uk/
---

# SQL Commands



---

``` mermaid
graph TD
    
    A[SQL Commands]
    B[<a href="#data-query-language-dql" style="text-decoration: none;">D.Q.L</a>]
    C[<a href="#data-definition-language-ddl" style="text-decoration: none;">D.D.L</a>]
    D[<a href="#data-manipulation-language-dml" style="text-decoration: none;">D.M.L</a>]
    E[<a href="#data-control-language-dcl" style="text-decoration: none;">D.C.L</a>]
    F[<a href="#transaction-control-language-tcl" style="text-decoration: none;">T.C.L</a>]
    G[<a href="#select" style="text-decoration: none;">SELECT</a>]
    H[<a href="#create" style="text-decoration: none;">CREATE</a>
        <a href="#drop" style="text-decoration: none;">DROP</a>
        <a href="#alter" style="text-decoration: none;">ALTER</a>
        <a href="#truncate" style="text-decoration: none;">TRUNCATE</a>]
    I[<a href="#insert" style="text-decoration: none;">INSERT</a>
        <a href="#update" style="text-decoration: none;">UPDATE</a>
        <a href="#delete" style="text-decoration: none;">DELETE</a>
        <a href="#merge" style="text-decoration: none;">MERGE</a>]
    J[<a href="#grant" style="text-decoration: none;">GRANT</a>
        <a href="#revoke" style="text-decoration: none;">REVOKE</a>]
    K[<a href="#commit" style="text-decoration: none;">COMMIT</a>
        <a href="#rollback" style="text-decoration: none;">ROLLBACK</a>
        <a href="#savepoint" style="text-decoration: none;">SAVEPOINT</a>]

    A:::shadingClass --- B:::shadingClass
    A --- C:::shadingClass
    A --- D:::shadingClass
    A --- E:::shadingClass
    A --- F:::shadingClass
    B --- G:::shadingClass
    C --- H:::shadingClass
    D --- I:::shadingClass
    E --- J:::shadingClass
    F --- K:::shadingClass
```

---

### Data Query Language
#### SELECT
Used to select which data to return within a query.

``` sql linenums="0"
SELECT column_names FROM table_name;
```

##### All columns
=== "SQL Server"
    ``` sql
    SELECT * FROM heroes;
    ```
=== "MySQL"
    ``` sql
    SELECT * FROM heroes;
    ```
=== "Oracle"
    ``` sql
    SELECT * FROM heroes;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT * FROM heroes;
    ```
=== "SQLite"
    ``` sql
    SELECT * FROM heroes;
    ```
=== "MariaDB"
    ``` sql
    SELECT * FROM heroes;
    ```

##### Specific Columns
=== "SQL Server"
    ``` sql
    SELECT first_name, hero_name FROM heroes;
    ```
=== "MySQL"
    ``` sql
    SELECT first_name, hero_name FROM heroes;
    ```
=== "Oracle"
    ``` sql
    SELECT first_name, hero_name FROM heroes;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT first_name, hero_name FROM heroes;
    ```
=== "SQLite"
    ``` sql
    SELECT first_name, hero_name FROM heroes;
    ```
=== "MariaDB"
    ``` sql
    SELECT first_name, hero_name FROM heroes;
    ```

---

### Data Definition Language
#### CREATE
Used to create database objects; tables, views, functions, procedures, triggers, etc.

``` sql linenums="0"
CREATE OBJECT object_name;
```

##### Database
=== "SQL Server"
    ``` sql
    CREATE DATABASE superverse;
    ```
=== "MySQL"
    ``` sql
    CREATE DATABASE superverse;
    ```
=== "Oracle"
    ``` sql
    CREATE USER superverse
    IDENTIFIED BY password;
    GRANT CONNECT, RESOURCE TO superverse;
    ```
=== "PostgreSQL"
    ``` sql
    CREATE DATABASE superverse;
    ```
=== "SQLite"
    `SQLite does not use the concept of a database in the same way as other systems.
    Instead, a database is created by simply saving a file with the .sqlite extension.`
=== "MariaDB"
    ``` sql
    CREATE DATABASE superverse;
    ```

##### Table
=== "SQL Server"
    ``` sql
    CREATE TABLE powers (
        power_id INT PRIMARY KEY,
        power_name VARCHAR(50),
        power_description TEXT,
        power_range NUMERIC(4,2)
    );
    ```
=== "MySQL"
    ``` sql
    CREATE TABLE powers (
        power_id INT PRIMARY KEY,
        power_name VARCHAR(50),
        power_description TEXT,
        power_range DECIMAL(4,2)
    );
    ```
=== "Oracle"
    ``` sql
    CREATE TABLE powers (
        power_id NUMBER PRIMARY KEY,
        power_name VARCHAR2(255),
        power_description CLOB,
        power_range NUMBER(1,2)
    );
    ```

    - Requires VARCHAR2 in place of VARCHAR
=== "PostgreSQL"
    ``` sql
    CREATE TABLE powers (
        power_id SERIAL PRIMARY KEY,
        power_name VARCHAR(255),
        power_description TEXT,
        power_range NUMERIC(1,2)
    );
    ```
=== "SQLite"
    ``` sql
    CREATE TABLE powers (
        power_id INTEGER PRIMARY KEY,
        power_name TEXT,
        power_description TEXT,
        power_range REAL
    );
    ```
    
    - Will accept VARCHAR but ignore the assigned length
    - Does not support NUMBER, NUMERIC, DECIMAL data types
=== "MariaDB"
    ``` sql
    CREATE TABLE powers (
        power_id INT PRIMARY KEY,
        power_name VARCHAR(255),
        power_description TEXT,
        power_range DECIMAL
    );
    ```

##### Derived Table
=== "SQL Server"
    ``` sql
    SELECT * INTO heroes
    FROM villians;
    ```
=== "MySQL"
    ``` sql
    CREATE TABLE villians AS
    SELECT * FROM heroes;
    ```
=== "Oracle"
    ``` sql
    CREATE TABLE villians AS
    SELECT * FROM heroes;
    ```
=== "PostgreSQL"
    ``` sql
    CREATE TABLE villians AS
    TABLE heroes;
    ```
=== "SQLite"
    ``` sql    
    CREATE TABLE villians AS
    SELECT * FROM heroes;
    ```
=== "MariaDB"
    ``` sql
    CREATE TABLE villians AS
    SELECT * FROM heroes;
    ```

##### Index
=== "SQL Server"
    ``` sql
    CREATE INDEX index_hero_name
    ON heroes (hero_name);
    ```
=== "MySQL"
    ``` sql
    CREATE INDEX index_hero_name
    ON heroes (hero_name);
    ```
=== "Oracle"
    ``` sql
    CREATE INDEX index_hero_name
    ON heroes (hero_name);
    ```
=== "PostgreSQL"
    ``` sql
    CREATE INDEX index_hero_name
    ON heroes (hero_name);
    ```
=== "SQLite"
    ``` sql
    CREATE INDEX index_hero_name
    ON heroes (hero_name);
    ```
=== "MariaDB"
    ``` sql
    CREATE INDEX index_hero_name
    ON heroes (hero_name);
    ```

##### Unique Index
=== "SQL Server"
    ``` sql
    CREATE UNIQUE INDEX index_hero_name
    ON heroes (hero_name);
    ```
=== "MySQL"
    ``` sql
    CREATE UNIQUE INDEX index_hero_name
    ON heroes (hero_name);
    ```
=== "Oracle"
    ``` sql
    CREATE UNIQUE INDEX index_hero_name
    ON heroes (hero_name);
    ```
=== "PostgreSQL"
    ``` sql
    CREATE UNIQUE INDEX index_hero_name
    ON heroes (hero_name);
    ```
=== "SQLite"
    ``` sql
    CREATE UNIQUE INDEX index_hero_name
    ON heroes (hero_name);
    ```
=== "MariaDB"
    ``` sql
    CREATE UNIQUE INDEX index_hero_name
    ON heroes (hero_name);
    ```

##### View
=== "SQL Server"
    ``` sql
    CREATE VIEW adult_heroes AS
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```
=== "MySQL"
    ``` sql
    CREATE VIEW adult_heroes AS
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```
=== "Oracle"
    ``` sql
    CREATE VIEW adult_heroes AS
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```
=== "PostgreSQL"
    ``` sql
    CREATE VIEW adult_heroes AS
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```
=== "SQLite"
    ``` sql
    CREATE VIEW adult_heroes AS
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```
=== "MariaDB"
    ``` sql
    CREATE VIEW adult_heroes AS
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```

##### Update View
=== "SQL Server"
    ``` sql
    CREATE OR UPDATE VIEW adult_heroes AS
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```
=== "MySQL"
    ``` sql
    CREATE OR REPLACE VIEW adult_heroes AS
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```
=== "Oracle"
    ``` sql
    CREATE OR REPLACE VIEW adult_heroes AS
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```
=== "PostgreSQL"
    ``` sql
    CREATE OR REPLACE VIEW adult_heroes AS
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```
=== "SQLite"
    ``` sql
    CREATE VIEW IF NOT EXISTS adult_heroes AS            
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```
=== "MariaDB"
    ``` sql
    CREATE OR REPLACE VIEW adult_heroes AS
    SELECT first_name, second_name, hero_name
    FROM heroes
    WHERE hero_age >= 18;
    ```

##### Procedure
=== "SQL Server"
    ``` sql
    CREATE PROCEDURE procedure_heroes
    AS
    BEGIN
        SELECT * FROM heroes
    END;
    ```
=== "MySQL"
    ``` sql
    DELIMITER //
    CREATE PROCEDURE procedure_heroes()
    BEGIN
        SELECT * FROM heroes
    END //
    DELIMITER ;
    ```
=== "Oracle"
    ``` sql
    CREATE OR REPLACE PROCEDURE procedure_heroes
    IS
    BEGIN
        SELECT * FROM heroes
    END;
    ```
=== "PostgreSQL"
    ``` sql
    CREATE OR REPLACE PROCEDURE procedure_heroes()
    LANGUAGE plpgsql
    AS $procedure_heroes$
    BEGIN
        SELECT * FROM heroes
    END $procedure_heroes$;
    ```
=== "SQLite"
    `SQLite does not support stored procedures in the same way as other systems. However, you can use triggers, views, and other database features to achieve similar functionality.`
=== "MariaDB"
    ``` sql
    DELIMITER //
    CREATE PROCEDURE procedure_heroes()
    BEGIN
        SELECT * FROM heroes
    END //
    DELIMITER;
    ```

##### Run Procedure
=== "SQL Server"
    ``` sql
    EXEC procedure_heroes;
    ```
=== "MySQL"
    ``` sql
    CALL procedure_heroes();
    ```
=== "Oracle"
    ``` sql
    BEGIN
        procedure_heroes();
    END;
    ```
=== "PostgreSQL"
    ``` sql
    CALL procedure_heroes();
    ```
=== "SQLite"
     SQLite does not support stored procedures in the same way as other systems.
=== "MariaDB"
    ``` sql
    CALL procedure_heroes();
    ```

---

#### DROP
Used to remove database objects; tables, views, functions, procedures, triggers, etc.

``` sql linenums="0"
DROP OBJECT object_name;
```

##### Database
=== "SQL Server"
    ``` sql
    DROP DATABASE superverse;
    ```
=== "MySQL"
    ``` sql
    DROP DATABASE superverse;
    ```
=== "Oracle"
    ``` sql
    DROP USER superverse CASCADE;
    ```
=== "PostgreSQL"
    ``` sql
    DROP DATABASE superverse;
    ```
=== "SQLite"
     `SQLite does not support this operation directly. You would typically delete the database file from the file system.`
=== "MariaDB"
    ``` sql
    DROP DATABASE superverse;
    ```

##### Table
=== "SQL Server"
    ``` sql
    DROP TABLE heroes;
    ```
=== "MySQL"
    ``` sql
    DROP TABLE heroes;
    ```
=== "Oracle"
    ``` sql
    DROP TABLE heroes;
    ```
=== "PostgreSQL"
    ``` sql
    DROP TABLE heroes;
    ```
=== "SQLite"
    ``` sql
    DROP TABLE heroes;
    ```
=== "MariaDB"
    ``` sql
    DROP TABLE heroes;
    ```

##### Column
=== "SQL Server"
    ``` sql
    ALTER TABLE heroes
    DROP COLUMN age;
    ```
=== "MySQL"
    ``` sql
    ALTER TABLE heroes
    DROP COLUMN age;    
    ```
=== "Oracle"
    ``` sql
    ALTER TABLE heroes
    DROP COLUMN age;
    ```
=== "PostgreSQL"
    ``` sql
    ALTER TABLE heroes
    DROP COLUMN age;
    ```
=== "SQLite"
    `SQLite does not support this operation directly. You would need to create a new table without the column, copy the data over, and then rename the new table.`
=== "MariaDB"
    ``` sql
    ALTER TABLE heroes
    DROP COLUMN age;
    ```
    
---

#### ALTER
Used to modify the structure of database objects.

``` sql
ALTER OBJECT object_name;
```

##### Add Column
=== "SQL Server"
    ``` sql
    ALTER TABLE heroes
    ADD hero_power VARCHAR(255);
    ```
=== "MySQL"
    ``` sql
    ALTER TABLE heroes
    ADD hero_power VARCHAR(255);
    ```
=== "Oracle"
    ``` sql
    ALTER TABLE heroes
    ADD (hero_power VARCHAR2(255));
    ```
=== "PostgreSQL"
    ``` sql
    ALTER TABLE heroes
    ADD COLUMN hero_power VARCHAR(255);
    ```
=== "SQLite"
    `SQLite does not support adding columns with a specific data type directly. You would need to create a new table with the desired column, copy the data over, and then rename the new table.`
=== "MariaDB"
    ``` sql
    ALTER TABLE heroes
    ADD COLUMN hero_power VARCHAR(255);
    ```

##### Modify Column
=== "SQL Server"
    ``` sql
    ALTER TABLE heroes
    ALTER COLUMN hero_age INT;
    ```
=== "MySQL"
    ``` sql
    ALTER TABLE heroes
    CHANGE COLUMN hero_age INT;
    ```
=== "Oracle"
    ``` sql
    ALTER TABLE heroes
    MODIFY (hero_age INT);
    ```
=== "PostgreSQL"
    ``` sql
    ALTER TABLE heroes
    ALTER COLUMN hero_age TYPE INT;
    ```
=== "SQLite"
    `SQLite does not support directly changing a column type. You would have to recreate the table with the desired column type.`
=== "MariaDB"
    ``` sql
    ALTER TABLE heroes
    MODIFY hero_age INT;
    ``` 

##### Rename Column
=== "SQL Server"
    ``` sql
    EXEC sp_rename 'heroes.second_name', 'surname', 'COLUMN';
    ```
=== "MySQL"
    ``` sql
    ALTER TABLE heroes
    CHANGE second_name surname VARCHAR(255) NOT NULL;
    ```
=== "Oracle"
    ``` sql
    ALTER TABLE heroes 
    RENAME COLUMN second_name
    TO surname;    
    ```
=== "PostgreSQL"
    ``` sql
    ALTER TABLE heroes
    RENAME COLUMN second_name
    TO surname;
    ```
=== "SQLite"
    `SQLite does not support the direct renaming of a column. You would typically have to create a new table with the desired column name and copy the data.`
=== "MariaDB"
    ``` sql
    ALTER TABLE heroes
    CHANGE second_name surname VARCHAR(255) NOT NULL;
    ```

##### Add Constraint
=== "SQL Server"
    ``` sql
    ALTER TABLE heroes
    ADD CONSTRAINT fk_hero_name FOREIGN KEY (power_id)
    REFERENCES powers(power_id);
    ```
=== "MySQL"
    ``` sql
    ALTER TABLE heroes
    ADD CONSTRAINT fk_hero_name FOREIGN KEY (power_id)
    REFERENCES powers(power_id);
    ```
=== "Oracle"
    ``` sql
    ALTER TABLE heroes
    ADD CONSTRAINT fk_hero_name FOREIGN KEY (power_id)
    REFERENCES powers(power_id);
    ```
=== "PostgreSQL"
    ``` sql
    ALTER TABLE heroes
    ADD CONSTRAINT fk_hero_name FOREIGN KEY (power_id)
    REFERENCES powers(power_id);
    ```
=== "SQLite"
    `SQLite does not support adding constraints to existing tables. You would need to create a new table with the desired constraints and copy the data.`
=== "MariaDB"
    ``` sql
    ALTER TABLE heroes
    ADD CONSTRAINT fk_hero_name FOREIGN KEY (power_id)
    REFERENCES powers(power_id);
    ```

##### Remove Constraint
=== "SQL Server"
    ``` sql
    ALTER TABLE heroes
    DROP CONSTRAINT fk_power_id;
    ```
=== "MySQL"
    ``` sql
    ALTER TABLE heroes
    DROP FOREIGN KEY fk_power_id;
    ```
=== "Oracle"
    ``` sql
    ALTER TABLE heroes
    DROP CONSTRAINT fk_power_id;
    ```
=== "PostgreSQL"
    ``` sql
    ALTER TABLE heroes
    DROP CONSTRAINT fk_power_id;
    ```
=== "SQLite"
    `SQLite does not support the direct removal of constraints. Instead, you would need to create a new table without the constraint and copy the data.`
=== "MariaDB"
    ``` sql
    ALTER TABLE heroes
    DROP FOREIGN KEY fk_power_id;
    ```

##### Rename Table
=== "SQL Server"
    ``` sql
    EXEC sp_rename 'heroes', 'superheroes';
    ```
=== "MySQL"
    ``` sql
    RENAME TABLE heroes TO superheroes;
    ```
=== "Oracle"
    ``` sql
    RENAME heroes TO superheroes;
    ```
=== "PostgreSQL"
    ``` sql
    ALTER TABLE heroes RENAME TO superheroes;
    ```
=== "SQLite"
    ``` sql
    ALTER TABLE heroes RENAME TO superheroes;
    ```
=== "MariaDB"
    ``` sql
    RENAME TABLE heroes TO superheroes;
    ```

---

#### TRUNCATE
Used to remove all data from a table.

``` sql
TRUNCATE OBJECT object_name;
```

##### Table
=== "SQL Server"
    ``` sql
    TRUNCATE TABLE heroes;
    ```
=== "MySQL"
    ``` sql
    TRUNCATE TABLE heroes;
    ```
=== "Oracle"
    ``` sql
    TRUNCATE TABLE heroes;
    ```
=== "PostgreSQL"
    ``` sql    
    TRUNCATE TABLE heroes;
    ```
=== "SQLite"
    ``` sql
    DELETE FROM heroes;
    ```
=== "MariaDB"
    ``` sql
    TRUNCATE TABLE heroes;
    ```

---

### Data Manipulation Language
#### INSERT
Used to insert new records into a table.

``` sql

INSERT INTO table_name (column1, column2, column3)
VALUES (value1, value2, value3);
```

=== "SQL Server"
    ``` sql
    INSERT INTO Heroes (first_name, second_name, hero_name, hero_age)
    VALUES ('Bruce', 'Wayne', 'Batman', 'Gotham');
    ```
=== "MySQL"
    ``` sql
    INSERT INTO Heroes (first_name, second_name, hero_name, hero_age)
    VALUES ('Bruce', 'Wayne', 'Batman', 'Gotham');
    ```
=== "Oracle"
    ``` sql
    INSERT INTO Heroes (first_name, second_name, hero_name, hero_age)
    VALUES ('Bruce', 'Wayne', 'Batman', 'Gotham');
    ```
=== "PostgreSQL"
    ``` sql
    INSERT INTO Heroes (first_name, second_name, hero_name, hero_age)
    VALUES ('Bruce', 'Wayne', 'Batman', 'Gotham');
    ```
=== "SQLite"
    ``` sql
    INSERT INTO Heroes (first_name, second_name, hero_name, hero_age)
    VALUES ('Bruce', 'Wayne', 'Batman', 'Gotham');
    ```
=== "MariaDB"
    ``` sql
    INSERT INTO Heroes (first_name, second_name, hero_name, hero_age)
    VALUES ('Bruce', 'Wayne', 'Batman', 'Gotham');
    ```

#### UPDATE
Used to modify existing records in a table.

``` sql
UPDATE table_name
SET column_name_1 = value_1, column_name_2 = value_2
WHERE condition;
```

=== "SQL Server"
    ``` sql
    UPDATE Heroes
    SET first_name = 'Bruce', second_name = 'Wayne'
    WHERE hero_name = 'Batman'
    ```
=== "MySQL"
    ``` sql
    UPDATE Heroes
    SET first_name = 'Bruce', second_name = 'Wayne'
    WHERE hero_name = 'Batman'
    ```
=== "Oracle"
    ``` sql
    UPDATE Heroes
    SET first_name = 'Bruce', second_name = 'Wayne'
    WHERE hero_name = 'Batman'
    ```
=== "PostgreSQL"
    ``` sql
    UPDATE Heroes
    SET first_name = 'Bruce', second_name = 'Wayne'
    WHERE hero_name = 'Batman'
    ```
=== "SQLite"
    ``` sql
    UPDATE Heroes
    SET first_name = 'Bruce', second_name = 'Wayne'
    WHERE hero_name = 'Batman'
    ```
=== "MariaDB"
    ``` sql
    UPDATE Heroes
    SET first_name = 'Bruce', second_name = 'Wayne'
    WHERE hero_name = 'Batman'
    ```

#### DELETE
Used to delete existing records from a table.

``` sql
DELETE FROM table_name WHERE condition;
```

=== "SQL Server"
    ``` sql
    DELETE FROM Heroes WHERE hero_name = 'Batman';
    ```
=== "MySQL"
    ``` sql
    DELETE FROM Heroes WHERE hero_name = 'Batman';
    ```
=== "Oracle"
    ``` sql
    DELETE FROM Heroes WHERE hero_name = 'Batman';
    ```
=== "PostgreSQL"
    ``` sql
    DELETE FROM Heroes WHERE hero_name = 'Batman';
    ```
=== "SQLite"
    ``` sql
    DELETE FROM Heroes WHERE hero_name = 'Batman';
    ```
=== "MariaDB"
    ``` sql
    DELETE FROM Heroes WHERE hero_name = 'Batman';
    ```

#### MERGE
Used to merge existing records that exist within tables. It will update existing records and insert missing records.

``` sql
MERGE INTO target_table AS target
USING source_table AS source
ON target.key_column = source.key_column
WHEN MATCHED THEN
    UPDATE SET target.column = source.column
WHEN NOT MATCHED BY TARGET THEN
    INSERT (key_column, column) VALUES (source.key_column, source.column)
WHEN NOT MATCHED BY SOURCE THEN
    DELETE;

```

=== "SQL Server"
    ``` sql
    MERGE INTO heroes_new AS target
    USING heroes_old AS source
    ON target.hero_id = source.hero_id
    WHEN MATCHED THEN
        UPDATE SET target.column_name = source.column_name
    WHEN NOT MATCHED BY TARGET THEN
        INSERT (hero_id, column_name) VALUES (source.hero_id, source.column_name)
    WHEN NOT MATCHED BY SOURCE THEN
        DELETE;
    ```
=== "MySQL"
    `MySQL does not have a MERGE statement but offers the INSERT ... ON DUPLICATE KEY UPDATE syntax for upsert operations`
=== "Oracle"
    ``` sql
    MERGE INTO heroes_new target
    USING (SELECT hero_id, column_name FROM heroes_old) source
    ON (target.hero_id = source.hero_id)
    WHEN MATCHED THEN
        UPDATE SET target.column_name = source.column_name
    WHEN NOT MATCHED THEN
        INSERT (hero_id, column_name) VALUES (source.hero_id, source.column_name);
    ```
=== "PostgreSQL"
    ``` sql
    MERGE INTO heroes_new AS target
    USING heroes_old AS source
    ON target.hero_id = source.hero_id
    WHEN MATCHED THEN
        UPDATE SET column_name = source.column_name
    WHEN NOT MATCHED THEN
        INSERT (hero_id, column_name) VALUES (source.hero_id, source.column_name);
    ```
=== "SQLite"
    `SQLite does not support the MERGE statement. Instead, you can use the INSERT ON CONFLICT clause to achieve a similar upsert effect`
=== "MariaDB"
    `MariaDB does not support the MERGE statement. However, you can use the INSERT ... ON DUPLICATE KEY UPDATE syntax to perform a similar operation`
    
---

### Data Control Language
??? example "Privileges"
    <div class="tbl-privileges">

    | **Privilege**    | **Description**                                        |
    | :--------------- | :----------------------------------------------------- |
    | ALTER            | Alter the structure of a database object.              |
    | BACKUP DATABASE  | Back up the database.                                  |
    | BACKUP LOG       | Back up the transaction log of the database.           |
    | CREATE           | Create new database objects, such as tables and views. |
    | CREATE TABLE     | Create new tables.                                     |
    | CREATE VIEW      | Create new views.                                      |
    | CREATE PROCEDURE | Create stored procedures.                              |
    | CREATE FUNCTION  | Create user-defined functions.                         |
    | CREATE RULE      | Create rules for data validation.                      |
    | CREATE DEFAULT   | Create default values for columns.                     |
    | DELETE           | Delete rows from a table.                              |
    | DROP             | Delete database objects.                               |
    | EXECUTE          | Run stored procedures and functions.                   |
    | INSERT           | Insert rows into a table.                              |
    | REFERENCES       | Create a foreign key constraint.                       |
    | SELECT           | Retrieve rows from a table or view.                    |
    | UPDATE           | Update rows in a table.                                |
    | VIEW DEFINITION  | View the metadata of database objects.                 |
    | CONNECT          | Connect to the database.                               |
    | GRANT OPTION     | Grant permissions they possess to other users.         |
    | CONTROL          | Full control over an object.                           |
    | TAKE OWNERSHIP   | Take ownership of an object.                           |
    | TRIGGER          | Create or manage triggers on a table.                  |
    | IMPERSONATE      | Impersonate another user.                              |

    </div>

#### GRANT
Used to give privileges to users.

``` sql
GRANT privileges database_name.table_name TO 'user_name'@'host_name';
```

##### Specific Privileges
=== "SQL Server"
    ``` sql
    GRANT INSERT, UPDATE ON superverse.heroes TO 'user_1';
    ```
=== "MySQL"
    ``` sql
    GRANT INSERT, UPDATE ON superverse.heroes TO 'user_1'@'hostname';
    ```
=== "Oracle"
    ``` sql
    GRANT INSERT, UPDATE ON superverse.heroes TO user_1;
    ```
=== "PostgreSQL"
    ``` sql
    GRANT INSERT, UPDATE ON ALL TABLES IN SCHEMA superverse.heroes TO user_1;
    ```
=== "SQLite"
    `SQLite does not support the GRANT statement as it handles permissions at the file-system level.`
=== "MariaDB"
    ``` sql
    GRANT INSERT, UPDATE ON superverse.heroes TO 'user_1'@'hostname';
    ```

##### All Privileges
=== "SQL Server"
    ``` sql
    EXEC sp_addrolemember 'db_owner', 'user_1';
    ```
=== "MySQL"
    ``` sql
    GRANT ALL PRIVILEGES ON *.* TO 'user_1'@'hostname' WITH GRANT OPTION;
    ```
=== "Oracle"
    ``` sql
    GRANT ALL PRIVILEGES TO user_1;
    ```
=== "PostgreSQL"
    ``` sql
    GRANT ALL PRIVILEGES ON DATABASE heroes TO user_1;
    ```
=== "SQLite"
    `SQLite does not support the GRANT statement as it handles permissions at the file-system level.`
=== "MariaDB"
    ``` sql
    GRANT ALL PRIVILEGES ON *.* TO 'user_1'@'hostname' IDENTIFIED BY 'p4ssw0rd';
    ```

#### REVOKE
Used to remove privileges from users.

``` sql
REVOKE privileges database_name.table_name TO 'user_name'@'host_name';
```

###### Specific Privileges
=== "SQL Server"
    ``` sql
    REVOKE INSERT, UPDATE ON superverse.heroes FROM user_1;
    ```
=== "MySQL"
    ``` sql
    REVOKE INSERT, UPDATE ON superverse.heroes FROM 'username'@'hostname';
    ```
=== "Oracle"
    ``` sql
    REVOKE INSERT, UPDATE ON heroes FROM user_1;
    ```
=== "PostgreSQL"
    ``` sql
    REVOKE INSERT, UPDATE ON heroes FROM user_1;
    ```
=== "SQLite"
    `SQLite does not support the REVOKE statement as it handles permissions at the file-system level.`
=== "MariaDB"
    ``` sql
    REVOKE INSERT, UPDATE ON superverse.heroes FROM 'user_1'@'hostname';
    ```

##### All Privileges
=== "SQL Server"
    ``` sql
    REVOKE ALL PRIVILEGES FROM user_1;
    ```
=== "MySQL"
    ``` sql
    REVOKE ALL PRIVILEGES ON *.* FROM 'user_1'@'hostname';
    ```
=== "Oracle"
    ``` sql
    REVOKE ALL PRIVILEGES FROM user_1;
    ```
=== "PostgreSQL"
    ``` sql
    REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA superverse FROM user_1;
    ```
=== "SQLite"
    `SQLite does not support the REVOKE statement as it handles permissions at the file-system level.`
=== "MariaDB"
    ``` sql
    REVOKE ALL PRIVILEGES ON *.* FROM 'user_1'@'hostname';
    ```

---

### Transaction Control Language

#### COMMIT
Used to save all transactions made since the last COMMIT or ROLLBACK statement.

??? example "Valid Transactions"
    - **DML**: INSERT, UPDATE, DELETE
    - **DDL**: CREATE, ALTER, DROP

``` sql
BEGIN TRANSACTION;
    SQL Query
COMMIT;
```

=== "SQL Server"
    ``` sql
    BEGIN TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    COMMIT TRANSACTION;
    ```
=== "MySQL"
    ``` sql
    START TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    COMMIT;
    ```
=== "Oracle"
    ``` sql
    UPDATE heroes SET hero_name = 'Batman'
    WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    COMMIT WORK;
    ```
=== "PostgreSQL"
    ``` sql
    BEGIN;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    COMMIT;
    ```
=== "SQLite"
    ``` sql
    BEGIN TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    COMMIT;
    ```
=== "MariaDB"
    ``` sql
    START TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    COMMIT;
    ```

#### ROLLBACK
Used to undo transactions that have not yet been saved.

``` sql
BEGIN TRANSACTION;
    SQL Query
ROLLBACK;
```

=== "SQL Server"
    ``` sql
    BEGIN TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK TRANSACTION;
    ```
=== "MySQL"
    ``` sql
    START TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK;
    ```
=== "Oracle"
    ``` sql
    UPDATE heroes SET hero_name = 'Batman'
    WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK WORK;
    ```
=== "PostgreSQL"
    ``` sql
    BEGIN;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK;
    ```
=== "SQLite"
    ``` sql
    BEGIN TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK;
    ```
=== "MariaDB"
    ``` sql
    START TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK;
    ```

#### SAVEPOINT
Used to for granular control of tranactions.

``` sql
BEGIN TRANSACTION;
    SQL Query
ROLLBACK TO SAVEPOINT savepoint_name;
```

=== "SQL Server"
    ``` sql
    BEGIN TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK TRANSACTION save_point_1;
    COMMIT;
    ```
=== "MySQL"
    ``` sql
    START TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK TO save_point_1;
    COMMIT;
    ```
=== "Oracle"
    ``` sql
    UPDATE heroes SET hero_name = 'Batman'
    WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK TO save_point_1;
    ```
=== "PostgreSQL"
    ``` sql
    BEGIN;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK TO SAVEPOINT save_point_1;
    COMMIT;
    ```
=== "SQLite"
    ``` sql
    BEGIN TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK TO save_point_1;
    COMMIT;
    ```
=== "MariaDB"
    ``` sql
    START TRANSACTION;
        UPDATE heroes SET hero_name = 'Batman'
        WHERE first_name = 'Bruce' AND second_name = 'Wayne';
    ROLLBACK TO save_point_1;
    COMMIT;
    ```

---

## CASE Statements
A CASE Statement are the equivalent of IF statements in other programming languages.

``` sql
SELECT CASE WHEN column_name = 'T' THEN 'True'
            WHEN column_name = 'F' THEN 'False'
        ELSE 'Error'
        END AS column_alias
FROM table_name;
```

=== "SQL Server"
    ``` sql
    SELECT hero_name,
        CASE
            WHEN hero_name = 'Batman' THEN 'DC'
            WHEN hero_name = 'Ironman' THEN 'Marvel'
            ELSE 'Unknown'
        END AS Universe
    FROM heroes;
    ```
=== "MySQL"
    ``` sql
    SELECT hero_name,
        CASE
            WHEN hero_name = 'Batman' THEN 'DC'
            WHEN hero_name = 'Ironman' THEN 'Marvel'
            ELSE 'Unknown'
        END AS Universe
    FROM heroes;
    ```
=== "Oracle"
    ``` sql
    SELECT hero_name,
        CASE
            WHEN hero_name = 'Batman' THEN 'DC'
            WHEN hero_name = 'Ironman' THEN 'Marvel'
            ELSE 'Unknown'
        END AS Universe
    FROM heroes;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT hero_name,
        CASE
            WHEN hero_name = 'Batman' THEN 'DC'
            WHEN hero_name = 'Ironman' THEN 'Marvel'
            ELSE 'Unknown'
        END AS Universe
    FROM heroes;
    ```
=== "SQLite"
    ``` sql
    SELECT hero_name,
        CASE 
            WHEN hero_name = 'Batman' THEN 'DC'
            WHEN hero_name = 'Ironman' THEN 'Marvel'
            ELSE 'Unknown'
        END AS Universe
    FROM heroes;
    ```
=== "MariaDB"
    ``` sql
    SELECT hero_name,
        CASE 
            WHEN hero_name = 'Batman' THEN 'DC'
            WHEN hero_name = 'Ironman' THEN 'Marvel'
            ELSE 'Unknown'
        END AS Universe
    FROM heroes;
    ```

---

## Keys
Keys are constraints that are created on a table.

### Primary Key

- Unique or distinct values only.
- No NULL values.
- Only one primary key per table.
- A single column or group of columns can be assigned the primary key.

### Unique Key

- Unique or distinct vlaues only.
- NULL values allowed.

### Foreign Key

- Refers to a pimrary key in another table.
- Creates a 'Primary / Replica' relationship.

---

## Data Types
### Numeric
<div class="tbl-data_types"></div>

| Name             | Alias            | Example              | Details                                                                                      |
| :--------------: | :--------------: | :------------------: | :------------------------------------------------------------------------------------------- |
| Integer          | INT              | INT(3)               | A whole number with a display width of 3 digits, such as 525                                 |
| Small Integer    | SMALLINT         | SMALLINT(5)          | A small whole number with a display width of 5 digits, such as 32767                         |
| Tiny Integer     | TINYINT          | TINYINT(3)           | A very small whole number with a display width of 3 digits, such as 127                      |
| Big Integer      | BIGINT           | BIGINT(20)           | A large whole number with a display width of 20 digits, such as 9223372036854775807          |
| Decimal          | DECIMAL          | DECIMAL(5,2)         | A decimal number with a precision of 5 digits and a scale of 2 digits, such as 123.45        |
| Numeric          | NUMERIC          | NUMERIC(10,5)        | A decimal number with a precision of 10 digits and a scale of 5 digits, such as 5.67891      |
| Float            | FLOAT            | FLOAT(7)             | A single-precision floating-point number with a display width of 7 digits, such as 123.4567  |
| Real             | REAL             | REAL(7)              | A single-precision floating-point number with a display width of 7 digits, such as 123.4567  |
| Double Precision | DOUBLE PRECISION | DOUBLE PRECISION(15) | A double-precision floating-point number with a display width of 15 digits, such as 567890.5 |
| Bit              | BIT              | BIT(1)               | A binary digit with a display width of 1 digit, either 0 or 1                                |


### Date & Time
<div class="tbl-data_types"></div>

| Name      | Alias     | Example   | Details                                                           |
| :-------: | :-------: | :-------: | :---------------------------------------------------------------- |
| Date      | DATE      | DATE      | Stores calendar date values, typically 'YYYY-MM-DD'               |
| Time      | TIME      | TIME      | Stores time of day values, typically 'HH:MI:SS'                   |
| Timestamp | TIMESTAMP | TIMESTAMP | Stores both date and time values, typically 'YYYY-MM-DD HH:MI:SS' |
| Datetime  | DATETIME  | DATETIME  | Stores both date and time values, typically 'YYYY-MM-DD HH:MI:SS' |
| Year      | YEAR      | YEAR      | Stores year values, typically 'YYYY'                              |

### Strings
<div class="tbl-data_types"></div>

| Name               | Alias      | Example      | Details                                                                            |
| :----------------: | :--------: | :----------: | :--------------------------------------------------------------------------------- |
| Character          | CHAR       | CHAR(5)      | Fixed length character data type. Maximum: 255. Shorter values padded with spaces. |
| Variable Character | VARCHAR    | VARCHAR(100) | Variable length character data type. Maximum: 65,535.                              |
| Text               | TEXT       | TEXT         | Variable length character data type. Maximum: 65,535.                              |
| Tiny Text          | TINYTEXT   | TINYTEXT     | Variable length character data type. Maximum: 255.                                 |
| Medium Text        | MEDIUMTEXT | MEDIUMTEXT   | Variable length character data type. Maximum: 16,777,215.                          |
| Long Text          | LONGTEXT   | LONGTEXT     | Variable length character data type. Maximum: 4,294,967,295.                       |

### Unicode Strings
<div class="tbl-data_types"></div>

| Name                        | Alias    | Example       | Details                                                                                      |
| :-------------------------: | :------: | :-----------: | :------------------------------------------------------------------------------------------- |
| National Character          | NCHAR    | NCHAR(5)      | Fixed length Unicode character data type. Maximum: 4,000. Shorter values padded with spaces. |
| National Variable Character | NVARCHAR | NVARCHAR(100) | Fixed length Unicode character data type. Maximum: 4,000.                                    |
| National Text               | NTEXT    | NTEXT         | Variable length Unicode data type. Maximum: 1,073,741,823.                                   |

### Binary
<div class="tbl-data_types"></div>

| Name            | Alias      | Example        | Details                                                                                |
| :-------------: | :--------: | :------------: | :------------------------------------------------------------------------------------- |
| Binary          | BINARY     | BINARY(5)      | Fixed length binary string data type. Maximum: 255. Shorter values padded with spaces. |
| Variable Binary | VARBINARY  | VARBINARY(100) | Variable length binary string data type. Maximum: 65,535.                              |
| Blob            | BLOB       | BLOB           | Binary large object. Maximum: System Specific.                                         |
| Tiny Blob       | TINYBLOB   | TINYBLOB       | Binary large object. Maximum: 255 bytes.                                               |
| Medium Blob     | MEDIUMBLOB | MEDIUMBLOB     | Binary large object. Maximum: 16,777,215 bytes.                                        |
| Long Blob       | LONGBLOB   | LONGBLOB       | Binary large object. Maximum: 4,294,967,295 bytes.                                     |

### Misc
<div class="tbl-data_types"></div>

| Name    | Alias   | Example                      | Details                                                                     |
| :-----: | :-----: | :--------------------------: | :-------------------------------------------------------------------------- |
| Boolean | BOOLEAN | BOOLEAN                      | Boolean data type. True or False.                                           |
| Enum    | ENUM    | ENUM('Red', 'Green', 'Blue') | String object data type. A single value chosen from a user specified list.  | 
| Set     | SET     | SET('One', 'Two', 'Three')   | String object data type. Multiple values chosen from a user specified list. | 

---

## JOINS
A JOIN clause is used to combine records from two or more tables based on a common column between them. This allows the user to query multiple tables at once.


??? example "Tables"
    === "superverse"
        ``` sql
        SELECT * FROM superverses;
        ```
        
        <div class="tbl-data_example" markdown>

        | superverse_id | superverse_name |
        | :-----------: | :-------------: |
        | 1             | Marvel          |
        | 2             | DC              |

        </div>

    === "heroes"
        ``` sql
        SELECT * FROM heroes;
        ```
        
        <div class="tbl-data_example" markdown>

        | hero_id | superverse_id | hero_name       | secret_identity  | archetype |
        | :-----: | :-----------: | :-------------: | :--------------: | :-------: |
        | 1       | 1             | Ironman         | Tony Stark       | Hero      |
        | 2       | 1             | Captain America | Steve Rogers     | Hero      |
        | 3       | 1             | Hulk            | Bruce Banner     | Hero      |
        | 4       | 2             | Batman          | Bruce Wayne      | Hero      |
        | 5       | 2             | Constantine     | John Constantine | Hero      |
        | 6       | 2             | Superman        | Clark Kent       | Hero      |

        </div>

    === "villians"
        ``` sql
        SELECT * FROM villians;
        ```

        <div class="tbl-data_example" markdown>

        | villian_id | superverse_id | villian_name  | archetype |
        | :--------: | :-----------: |:------------: | :-------: |
        | 1          | 1             | Justin Hammer | Villian   |
        | 2          | 1             | Red Skull     | Villian   |
        | 3          | 1             | Abomination   | Villian   |
        | 4          | 2             | Joker         | Villian   |
        | 5          | 2             | Lucifer       | Villian   |
        | 6          | 2             | Lex Luthor    | Villian   |

        </div>

    === "powers"
        ``` sql
        SELECT * FROM powers;
        ```

        <div class="tbl-data_example" markdown>

        | power_id | hero_id | power_name  |
        | :------: | :-----: | :---------: |
        | 1        | 1       | Smarts      |
        | 2        | 2       | Super Serum |
        | 3        | 3       | Rage        |
        | 4        | 4       | Stealth     |
        | 5        | 5       | Magic       |
        | 6        | 6       | Flight      |

        </div>

    === "incidents"
        ``` sql
        SELECT * FROM incidents;
        ```

        <div class="tbl-data_example" markdown>

        | incident_id | incident_location | hero_id | villian_id |
        | :---------: | :---------------: | :-----: | :--------: |
        | 1           | New York          | 1       | 1          |
        | 2           | Germany           | 1       | 2          |
        | 3           | Virginia          | 3       | 3          |
        | 4           | Gotham            | 7       | 4          |
        | 5           | London            | 8       | 5          |
        | 6           | Metropolis        | 6       | 6          |

        </div>

---

### INNER JOIN
An INNER JOIN in SQL is a type of join operation that combines rows from two or more tables based on a related column between them. It returns only the rows where there is a match in both tables. If a row from one table does not have a matching row in the other table, that row is not included in the result.

``` sql
SELECT table_name_1.column_name_1, table_name_2.column_name_2
FROM table_name_1
INNER JOIN table_name_1 ON table_name_1.common_column_1 = table_name_2.common_column_2;
```

![Inner Join](img/joins/InnerJoin-dark.svg#only-dark){ class='sql_join' }
![Inner Join](img/joins/InnerJoin-light.svg#only-light){ class='sql_join' }

??? example "Input & Output"
    <div class="tbl-data_example" markdown>

    | hero_id | superverse_id | hero_name       | secret_identity  | archetype |
    | :-----: | :-----------: | :-------------: | :--------------: | :-------: |
    | 1       | 1             | Ironman         | Tony Stark       | Hero      |
    | 2       | 1             | Captain America | Steve Rogers     | Hero      |
    | 3       | 1             | Hulk            | Bruce Banner     | Hero      |
    | 4       | 2             | Batman          | Bruce Wayne      | Hero      |
    | 5       | 2             | Constantine     | John Constantine | Hero      |
    | 6       | 2             | Superman        | Clark Kent       | Hero      |
        
    Table: heroes

    | incident_id | incident_location | hero_id | villian_id |
    | :---------: | :---------------: | :-----: | :--------: |
    | 1           | New York          | 1       | 1          |
    | 2           | Germany           | 1       | 2          |
    | 3           | Virginia          | 3       | 3          |
    | 4           | Gotham            | 7       | 4          |
    | 5           | London            | 8       | 5          |
    | 6           | Metropolis        | 6       | 6          |

    Table: incidents

    </div>

    --- 
    
    <div class='tbl-join_example' markdown>

    | hero_name | incident_location |
    | :-------: | :---------------: |
    | Ironman   | New York          |
    | Ironman   | Germany           |
    | Hulk      | Virginia          |
    | Superman  | Metropolis        |

    Output: INNER JOIN

    </div>

=== "SQL Server"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    INNER JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```
=== "MySQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    INNER JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```
=== "Oracle"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    INNER JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    INNER JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```
=== "SQLite"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    INNER JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```
=== "MariaDB"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    INNER JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```

---

### LEFT JOIN
A LEFT JOIN in SQL returns all records from the left table and the matched records from the right table. If there is no match for a specific record in the right table, the left will be matched with a NULL value.

``` sql
SELECT table_name_1.column_name_1, table_name_2.column_name_2
FROM table_name_1
LEFT JOIN table_name_1 ON table_name_1.common_column_1 = table_name_2.common_column_2;
```

![Left Join](img/joins/LeftOuterJoin-dark.svg#only-dark){ class='sql_join' }
![Left Join](img/joins/LeftOuterJoin-light.svg#only-light){ class='sql_join' }

=== "SQL Server"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```
=== "MySQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```
=== "Oracle"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```
=== "SQLite"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```
=== "MariaDB"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```

??? example "Input & Output"
    <div class="tbl-data_example" markdown>

    | hero_id | superverse_id | hero_name       | secret_identity  | archetype |
    | :-----: | :-----------: | :-------------: | :--------------: | :-------: |
    | 1       | 1             | Ironman         | Tony Stark       | Hero      |
    | 2       | 1             | Captain America | Steve Rogers     | Hero      |
    | 3       | 1             | Hulk            | Bruce Banner     | Hero      |
    | 4       | 2             | Batman          | Bruce Wayne      | Hero      |
    | 5       | 2             | Constantine     | John Constantine | Hero      |
    | 6       | 2             | Superman        | Clark Kent       | Hero      |
        
    Table: heroes

    | incident_id | incident_location | hero_id | villian_id |
    | :---------: | :---------------: | :-----: | :--------: |
    | 1           | New York          | 1       | 1          |
    | 2           | Germany           | 1       | 2          |
    | 3           | Virginia          | 3       | 3          |
    | 4           | Gotham            | 7       | 4          |
    | 5           | London            | 8       | 5          |
    | 6           | Metropolis        | 6       | 6          |

    Table: incidents

    </div>

    ---
    
    <div class='tbl-join_example' markdown>

    | hero_name       | incident_location |
    | :-------------: | :---------------: |
    | Ironman         | New York          |
    | Ironman         | Germany           |
    | Hulk            | Virginia          |
    | Superman        | Metropolis        |
    | Captain America |                   |
    | Constantine     |                   |
    | Batman          |                   |

    Output: LEFT JOIN

    </div>

---

### LEFT EXCLUDING JOIN
A LEFT EXCLUDING JOIN in SQL returns all of the records in left table that don't match any record in right table.

``` sql
SELECT table_name_1.column_name_1, table_name_2.column_name_2
FROM table_name_1
LEFT JOIN table_name_1 ON table_name_1.common_column_1 = table_name_2.common_column_2
WHERE table_name_2.common_column_2 IS NULL;
```

![Left Join](img/joins/LeftOuterJoin-IsNull-dark.svg#only-dark){ class='sql_join' }
![Left Join](img/joins/LeftOuterJoin-IsNull-light.svg#only-light){ class='sql_join' }

=== "SQL Server"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE incidents.hero_id IS NULL;
    ```
=== "MySQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE incidents.hero_id IS NULL;
    ```
=== "Oracle"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE incidents.hero_id IS NULL;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE incidents.hero_id IS NULL;
    ```
=== "SQLite"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE incidents.hero_id IS NULL;
    ```
=== "MariaDB"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE incidents.hero_id IS NULL;
    ```

??? example "Input & Output"
    <div class="tbl-data_example" markdown>

    | hero_id | superverse_id | hero_name       | secret_identity  | archetype |
    | :-----: | :-----------: | :-------------: | :--------------: | :-------: |
    | 1       | 1             | Ironman         | Tony Stark       | Hero      |
    | 2       | 1             | Captain America | Steve Rogers     | Hero      |
    | 3       | 1             | Hulk            | Bruce Banner     | Hero      |
    | 4       | 2             | Batman          | Bruce Wayne      | Hero      |
    | 5       | 2             | Constantine     | John Constantine | Hero      |
    | 6       | 2             | Superman        | Clark Kent       | Hero      |
        
    Table: heroes

    | incident_id | incident_location | hero_id | villian_id |
    | :---------: | :---------------: | :-----: | :--------: |
    | 1           | New York          | 1       | 1          |
    | 2           | Germany           | 1       | 2          |
    | 3           | Virginia          | 3       | 3          |
    | 4           | Gotham            | 7       | 4          |
    | 5           | London            | 8       | 5          |
    | 6           | Metropolis        | 6       | 6          |

    Table: incidents

    </div>

    ---
    
    <div class='tbl-join_example' markdown>

    | hero_name       | incident_location |
    | :-------------: | :---------------: |
    | Captain America |                   |
    | Batman          |                   |
    | Constantine     |                   |

    Output: LEFT EXCLUDING JOIN

    </div>

---

### RIGHT JOIN
A RIGHT JOIN in SQL returns all records from the right table and the matched records from the left table. If there is no match for a specific record in the left table, the left will be matched with a NULL value.

``` sql
SELECT table_name_1.column_name_1, table_name_2.column_name_2
FROM table_name_1
RIGHT JOIN table_name_1 ON table_name_1.common_column_1 = table_name_2.common_column_2;
```

![Right Join](img/joins/RightOuterJoin-dark.svg#only-dark){ class='sql_join' }
![Right Join](img/joins/RightOuterJoin-light.svg#only-light){ class='sql_join' }

??? example "Input & Output"
    <div class="tbl-data_example" markdown>

    | hero_id | superverse_id | hero_name       | secret_identity  | archetype |
    | :-----: | :-----------: | :-------------: | :--------------: | :-------: |
    | 1       | 1             | Ironman         | Tony Stark       | Hero      |
    | 2       | 1             | Captain America | Steve Rogers     | Hero      |
    | 3       | 1             | Hulk            | Bruce Banner     | Hero      |
    | 4       | 2             | Batman          | Bruce Wayne      | Hero      |
    | 5       | 2             | Constantine     | John Constantine | Hero      |
    | 6       | 2             | Superman        | Clark Kent       | Hero      |
        
    Table: heroes

    | incident_id | incident_location | hero_id | villian_id |
    | :---------: | :---------------: | :-----: | :--------: |
    | 1           | New York          | 1       | 1          |
    | 2           | Germany           | 1       | 2          |
    | 3           | Virginia          | 3       | 3          |
    | 4           | Gotham            | 7       | 4          |
    | 5           | London            | 8       | 5          |
    | 6           | Metropolis        | 6       | 6          |

    Table: incidents

    </div>

    ---

    <div class='tbl-join_example' markdown>

    | hero_name | incident_location |
    | :-------: | :---------------: |
    | Ironman   | New York          |
    | Ironman   | Germany           |
    | Hulk      | Virginia          |
    |           | Gotham            |
    |           | London            |
    | Superman  | Metropolis        |

    Output: RIGHT JOIN

    </div>

=== "SQL Server"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id;    
    ```
=== "MySQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id;    
    ```
=== "Oracle"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id;    
    ```
=== "PostgreSQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id;
    ```
=== "SQLite"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id;    
    ```
=== "MariaDB"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id;    
    ```

---

### RIGHT EXCLUDING JOIN
A RIGHT EXCLUDING JOIN in SQL returns all of the records in right table that don't match any record in left table.

``` sql
SELECT table_name_1.column_name_1, table_name_2.column_name_2
FROM table_name_1
RIGHT JOIN table_name_1 ON table_name_1.common_column_1 = table_name_2.common_column_2
WHERE table_name_1.common_column_1 IS NULL;
```

![Right Join](img/joins/RightOuterJoin-IsNull-dark.svg#only-dark){ class='sql_join' }
![Right Join](img/joins/RightOuterJoin-IsNull-light.svg#only-light){ class='sql_join' }


??? example "Input & Output"
    <div class="tbl-data_example" markdown>

    | hero_id | superverse_id | hero_name       | secret_identity  | archetype |
    | :-----: | :-----------: | :-------------: | :--------------: | :-------: |
    | 1       | 1             | Ironman         | Tony Stark       | Hero      |
    | 2       | 1             | Captain America | Steve Rogers     | Hero      |
    | 3       | 1             | Hulk            | Bruce Banner     | Hero      |
    | 4       | 2             | Batman          | Bruce Wayne      | Hero      |
    | 5       | 2             | Constantine     | John Constantine | Hero      |
    | 6       | 2             | Superman        | Clark Kent       | Hero      |
        
    Table: heroes

    | incident_id | incident_location | hero_id | villian_id |
    | :---------: | :---------------: | :-----: | :--------: |
    | 1           | New York          | 1       | 1          |
    | 2           | Germany           | 1       | 2          |
    | 3           | Virginia          | 3       | 3          |
    | 4           | Gotham            | 7       | 4          |
    | 5           | London            | 8       | 5          |
    | 6           | Metropolis        | 6       | 6          |

    Table: incidents

    </div>

    ---

    <div class='tbl-join_example' markdown>

    | hero_name | incident_location |
    | :-------: | :---------------: |
    |           | Gotham            |
    |           | London            |

    Output: RIGHT EXCLUDING JOIN

    </div>

=== "SQL Server"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE ON heroes.hero_id IS NULL;
    ```
=== "MySQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE ON heroes.hero_id IS NULL;
    ```
=== "Oracle"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE ON heroes.hero_id IS NULL;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE ON heroes.hero_id IS NULL;
    ```
=== "SQLite"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE ON heroes.hero_id IS NULL;
    ```
=== "MariaDB"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE ON heroes.hero_id IS NULL;
    ```

---

### FULL JOIN
A FULL OUTER JOIN in SQL retrieves all records from both tables, including matching and non-matching records. If there are no matching records in one or both tables, NULL values are included for those columns. It’s essentially the combination of a LEFT JOIN and a RIGHT JOIN.

``` sql
SELECT table_name_1.column_name_1, table_name_2.column_name_2
FROM table_name_1
FULL JOIN table_name_1 ON table_name_1.column_name_1 = table_name_2.column_name_2;
```

![Full Outer Join](img/joins/FullOuterJoin-dark.svg#only-dark){ class='sql_join' }
![Full Outer Join](img/joins/FullOuterJoin-light.svg#only-light){ class='sql_join' }

??? example "Input & Output"
    <div class="tbl-data_example" markdown>

    | hero_id | superverse_id | hero_name       | secret_identity  | archetype |
    | :-----: | :-----------: | :-------------: | :--------------: | :-------: |
    | 1       | 1             | Ironman         | Tony Stark       | Hero      |
    | 2       | 1             | Captain America | Steve Rogers     | Hero      |
    | 3       | 1             | Hulk            | Bruce Banner     | Hero      |
    | 4       | 2             | Batman          | Bruce Wayne      | Hero      |
    | 5       | 2             | Constantine     | John Constantine | Hero      |
    | 6       | 2             | Superman        | Clark Kent       | Hero      |
        
    Table: heroes

    | incident_id | incident_location | hero_id | villian_id |
    | :---------: | :---------------: | :-----: | :--------: |
    | 1           | New York          | 1       | 1          |
    | 2           | Germany           | 1       | 2          |
    | 3           | Virginia          | 3       | 3          |
    | 4           | Gotham            | 7       | 4          |
    | 5           | London            | 8       | 5          |
    | 6           | Metropolis        | 6       | 6          |

    Table: incidents

    </div>

    ---

    <div class='tbl-join_example' markdown>

    | hero_name       | incident_location |
    | :-------------: | :---------------: |
    | Ironman         | New York          |
    | Ironman         | Germany           |
    | Hulk            | Virginia          |
    |                 | Gotham            |
    |                 | London            |
    | Superman        | Metropolis        |
    | Captain America |                   |
    | Constantine     |                   |
    | Batman          |                   |

    Output: FULL JOIN

    </div>

=== "SQL Server"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    FULL JOIN incidents ON heroes.hero_id = incidents.hero_id; 
    ```
=== "MySQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id; 
    UNION
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id; 
    ```
    !!! warning "Caveat"    
        - MySQL does not support FULL JOIN directly.
        - Use a combination of LEFT JOIN and RIGHT JOIN with a UNION.
=== "Oracle"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    FULL JOIN incidents ON heroes.hero_id = incidents.hero_id; 
    ```
=== "PostgreSQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    FULL JOIN incidents ON heroes.hero_id = incidents.hero_id;    
    ```
=== "SQLite"
    ``` sql    
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id; 
    UNION ALL
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id; 
    WHERE heroes.hero_id IS NULL;
    ```
    !!! warning "Caveat"
        - SQLite does not support FULL JOIN directly.
        - Use a combination of LEFT JOIN, a UNION ALL, and a WHERE.
=== "MariaDB"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id; 
    UNION
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id; 
    ```
    !!! warning "Caveat"    
        - MariaDB does not support FULL JOIN directly.
        - Use a combination of LEFT JOIN and RIGHT JOIN with a UNION.

---

### FULL EXCLUDING JOIN
An OUTER EXCLUDING JOIN in SQL returns all of the records in the left table and all of the records in the right table that do not match.

``` sql
SELECT table_name_1.column_name_1, table_name_2.column_name_2
FROM table_name_1
FULL JOIN table_name_1 ON table_name_1.common_column_1 = table_name_2.common_column_2
WHERE table_name_1.common_column_1 IS NULL OR table_name_2.common_column_2 IS NULL;
```

![Full Outer Join](img/joins/FullOuterJoin-IsNull-dark.svg#only-dark){ class='sql_join' }
![Full Outer Join](img/joins/FullOuterJoin-IsNull-light.svg#only-light){ class='sql_join' }

??? example "Input & Output"
    <div class="tbl-data_example" markdown>

    | hero_id | superverse_id | hero_name       | secret_identity  | archetype |
    | :-----: | :-----------: | :-------------: | :--------------: | :-------: |
    | 1       | 1             | Ironman         | Tony Stark       | Hero      |
    | 2       | 1             | Captain America | Steve Rogers     | Hero      |
    | 3       | 1             | Hulk            | Bruce Banner     | Hero      |
    | 4       | 2             | Batman          | Bruce Wayne      | Hero      |
    | 5       | 2             | Constantine     | John Constantine | Hero      |
    | 6       | 2             | Superman        | Clark Kent       | Hero      |
        
    Table: heroes

    | incident_id | incident_location | hero_id | villian_id |
    | :---------: | :---------------: | :-----: | :--------: |
    | 1           | New York          | 1       | 1          |
    | 2           | Germany           | 1       | 2          |
    | 3           | Virginia          | 3       | 3          |
    | 4           | Gotham            | 7       | 4          |
    | 5           | London            | 8       | 5          |
    | 6           | Metropolis        | 6       | 6          |

    Table: incidents

    </div>

    ---

    <div class='tbl-join_example' markdown>

    | hero_name       | incident_location |
    | :-------------: | :---------------: |
    | Ironman         | New York          |
    | Ironman         | Germany           |
    | Hulk            | Virginia          |
    |                 | Gotham            |
    |                 | London            |
    | Superman        | Metropolis        |
    | Captain America |                   |
    | Constantine     |                   |
    | Batman          |                   |

    Output: FULL JOIN

    </div>

=== "SQL Server"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    FULL OUTER JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE heroes.hero_id IS NULL OR incidents.hero_id IS NULL;
    ```
=== "MySQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE incidents.hero_id IS NULL
    UNION ALL
    SELECT heroes.hero_name, incidents.incident_location
    FROM incidents
    LEFT JOIN heroes ON heroes.hero_id = incidents.hero_id
    WHERE heroes.hero_id IS NULL;
    ```
    !!! warning "Caveat"    
        - MySQL does not support FULL JOIN directly.
        - Use a combination of LEFT JOIN and RIGHT JOIN with a UNION.
=== "Oracle"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    FULL OUTER JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE heroes.hero_id IS NULL OR incidents.hero_id IS NULL;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    FULL JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE heroes.hero_id IS NULL OR incidents.hero_id IS NULL;
    ```
=== "SQLite"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE incidents.hero_id IS NULL
    UNION ALL
    SELECT heroes.hero_name, incidents.incident_location
    FROM incidents
    LEFT JOIN heroes ON heroes.hero_id = incidents.hero_id
    WHERE heroes.hero_id IS NULL;
    ```
    !!! warning "Caveat"
        - SQLite does not support FULL JOIN directly.
        - Use a combination of LEFT JOIN, a UNION ALL, and a WHERE.
=== "MariaDB"
    ``` sql
    SELECT heroes.hero_name, incidents.incident_location
    FROM heroes
    LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id
    WHERE incidents.hero_id IS NULL
    UNION ALL
    SELECT heroes.hero_name, incidents.incident_location
    FROM incidents
    LEFT JOIN heroes ON heroes.hero_id = incidents.hero_id
    WHERE heroes.hero_id IS NULL;
    ```
    !!! warning "Caveat"    
        - MariaDB does not support FULL JOIN directly.
        - Use a combination of LEFT JOIN and RIGHT JOIN with a UNION.

---

### SELF JOIN
A SELF JOIN is a regular join where a table is joined to itself. Self joins are often used to combine and compare rows within a table.

``` sql
SELECT table_name_1.column_name_1, table_name_2.column_name_2
FROM table_name_1 AS alias_1
JOIN table_name_1 AS alias_1 ON alias_1.column_name_1 = alias_2.column_name_2;
```

??? example "Input & Output"
    <div class="tbl-data_example" markdown>

    | hero_id | superverse_id | hero_name       | secret_identity  | archetype |
    | :-----: | :-----------: | :-------------: | :--------------: | :-------: |
    | 1       | 1             | Ironman         | Tony Stark       | Hero      |
    | 2       | 1             | Captain America | Steve Rogers     | Hero      |
    | 3       | 1             | Hulk            | Bruce Banner     | Hero      |
    | 4       | 2             | Batman          | Bruce Wayne      | Hero      |
    | 5       | 2             | Constantine     | John Constantine | Hero      |
    | 6       | 2             | Superman        | Clark Kent       | Hero      |
        
    Table: heroes

    </div>

    ---
    
    <div class='tbl-join_example' markdown>

    | hero_name       | hero_name       |
    | :-------------: |:-------:------- |
    | Ironman         | Captain America |
    | Ironman         | Hulk            |
    | Ironman         | Batman          |
    | Ironman         | Constantine     |
    | Ironman         | Superman        |
    | Captain America | Ironman         |
    | Captain America | Hulk            |
    | Captain America | Batman          |
    | Captain America | Constantine     |
    | Captain America | Superman        |
    | Hulk            | Ironman         |
    | Hulk            | Captain America |
    | Hulk            | Batman          |
    | Hulk            | Constantine     |
    | Hulk            | Superman        |
    | Batman          | Ironman         |
    | Batman          | Captain America |
    | Batman          | Hulk            |
    | Batman          | Constantine     |
    | Batman          | Superman        |
    | Constantine     | Ironman         |
    | Constantine     | Captain America |
    | Constantine     | Hulk            |
    | Constantine     | Batman          |
    | Constantine     | Superman        |
    | Superman        | Ironman         |
    | Superman        | Captain America |
    | Superman        | Hulk            |
    | Superman        | Batman          |
    | Superman        | Constantine     |

    Output: SELF JOIN

    </div>

=== "SQL Server"
    ``` sql
    SELECT h1.hero_name, h2.hero_name
    FROM heroes AS h1, heroes AS h2
    WHERE h1.hero_id = h2.hero_id;
    ```
=== "MySQL"
    ``` sql
    SELECT h1.hero_name, h2.hero_name
    FROM heroes AS h1
    JOIN heroes AS h2 ON h1.hero_id = h2.hero_id;
    ```
=== "Oracle"
    ``` sql
    SELECT h1.hero_name, h2.hero_name
    FROM heroes AS h1
    JOIN heroes AS h2 ON h1.hero_id = h2.hero_id;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT h1.hero_name, h2.hero_name
    FROM heroes AS h1
    JOIN heroes AS h2 ON h1.hero_id = h2.hero_id;
    ```
=== "SQLite"
    ``` sql
    SELECT h1.hero_name, h2.hero_name
    FROM heroes AS h1
    JOIN heroes AS h2 ON h1.hero_id = h2.hero_id;
    ```
=== "MariaDB"
    ``` sql
    SELECT h1.hero_name, h2.hero_name
    FROM heroes AS h1
    JOIN heroes AS h2 ON h1.hero_id = h2.hero_id;
    ```

---

### NATURAL JOIN
A NATURAL JOIN in SQL is a type of join that creates a new result table by combining column values of two tables.

``` sql
SELECT table_name_1.column_name_1, table_name_2.column_name_2
FROM table_name_1
NATURAL JOIN table_name_2;
```

??? example "Input & Output"
    <div class="tbl-data_example" markdown>

    | superverse_id | superverse_name |
    | :-----------: | :-------------: |
    | 1             | Marvel          |
    | 2             | DC              |
        
    Table: superverse

    | hero_id | superverse_id | hero_name       | secret_identity  | archetype |
    | :-----: | :-----------: | :-------------: | :--------------: | :-------: |
    | 1       | 1             | Ironman         | Tony Stark       | Hero      |
    | 2       | 1             | Captain America | Steve Rogers     | Hero      |
    | 3       | 1             | Hulk            | Bruce Banner     | Hero      |
    | 4       | 2             | Batman          | Bruce Wayne      | Hero      |
    | 5       | 2             | Constantine     | John Constantine | Hero      |
    | 6       | 2             | Superman        | Clark Kent       | Hero      |
        
    Table: heroes

    </div>

    ---

    <div class='tbl-join_example' markdown>

    | hero_name       | superverse_name |
    | :-------------: |:-------:------- |
    | Ironman         | Marvel          |
    | Captain America | Marvel          |
    | Hulk            | Marvel          |
    | Batman          | DC              |
    | Constantine     | DC              |
    | Superman        | DC              |

    Output: NATURAL JOIN

    </div>

=== "SQL Server"
    ``` sql
    SELECT heroes.hero_name, superverses.superverse_name
    FROM heroes
    JOIN superverses
    USING (hero_id);
    ```
=== "MySQL"
    ``` sql
    SELECT heroes.hero_name, superverses.superverse_name
    FROM heroes
    NATURAL JOIN superverses;
    ```
=== "Oracle"
    ``` sql
    SELECT heroes.hero_name, superverses.superverse_name
    FROM heroes
    NATURAL JOIN superverses;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT heroes.hero_name, superverses.superverse_name
    FROM heroes
    NATURAL JOIN superverses;
    ```
=== "SQLite"
    ``` sql
    SELECT heroes.hero_name, superverses.superverse_name
    FROM heroes
    NATURAL JOIN superverses;
    ```
=== "MariaDB"
    ``` sql
    SELECT heroes.hero_name, superverses.superverse_name
    FROM heroes
    NATURAL JOIN superverses;
    ```

---

### CROSS JOIN
The CROSS JOIN (CARTESIAN PRODUCT) produces a result with every combination of the rows from the two tables. Each table has 4 rows so this produces 16 rows in the result. It is the set of all ordered pairs where each pair includes one element from each of the original sets. In other words, it matches each row of one table to every row of another table.

``` sql
SELECT table_name_1.column_name_1, table_name_2.column_name_2
FROM table_name_1
CROSS JOIN table_name_2;
```

??? example "Input & Output"
    <div class="tbl-data_example" markdown>

    | hero_id | superverse_id | hero_name       | secret_identity  | archetype |
    | :-----: | :-----------: | :-------------: | :--------------: | :-------: |
    | 1       | 1             | Ironman         | Tony Stark       | Hero      |
    | 2       | 1             | Captain America | Steve Rogers     | Hero      |
    | 3       | 1             | Hulk            | Bruce Banner     | Hero      |
    | 4       | 2             | Batman          | Bruce Wayne      | Hero      |
    | 5       | 2             | Constantine     | John Constantine | Hero      |
    | 6       | 2             | Superman        | Clark Kent       | Hero      |
        
    Table: heroes

    | power_id | hero_id | power_name  |
    |:-------: | :-----: | :---------: |
    | 1        | 1       | Smarts      |
    | 2        | 2       | Super Serum |
    | 3        | 3       | Rage        |
    | 4        | 4       | Stealth     |
    | 5        | 5       | Magic       |
    | 6        | 6       | Flight      |

    Table: powers

    </div>

    ---

    <div class='tbl-join_example' markdown>

    | hero_name       | power_name  |
    | :-------------: | :---------: |
    | Ironman         | Smarts      |
    | Captain America | Smarts      |
    | Hulk            | Smarts      |
    | Batman          | Smarts      |
    | Constantine     | Smarts      |
    | Superman        | Smarts      |
    | Ironman         | Super Serum |
    | Captain America | Super Serum |
    | Hulk            | Super Serum |
    | Batman          | Super Serum |
    | Constantine     | Super Serum |
    | Superman        | Super Serum |
    | Ironman         | Rage        |
    | Captain America | Rage        |
    | Hulk            | Rage        |
    | Batman          | Rage        |
    | Constantine     | Rage        |
    | Superman        | Rage        |
    | Ironman         | Stealth     |
    | Captain America | Stealth     |
    | Hulk            | Stealth     |
    | Batman          | Stealth     |
    | Constantine     | Stealth     |
    | Superman        | Stealth     |
    | Ironman         | Magic       |
    | Captain America | Magic       |
    | Hulk            | Magic       |
    | Batman          | Magic       |
    | Constantine     | Magic       |
    | Superman        | Magic       |
    | Ironman         | Flight      |
    | Captain America | Flight      |
    | Hulk            | Flight      |
    | Batman          | Flight      |
    | Constantine     | Flight      |
    | Superman        | Flight      |

    Output: CROSS JOIN

    </div>

=== "SQL Server"
    ``` sql
    SELECT heroes.hero_name, powers.power_name
    FROM heroes
    CROSS JOIN powers; 
    ```
=== "MySQL"
    ``` sql
    SELECT heroes.hero_name, powers.power_name
    FROM heroes
    CROSS JOIN powers; 
    ```
=== "Oracle"
    ``` sql
    SELECT heroes.hero_name, powers.power_name
    FROM heroes
    CROSS JOIN powers; 
    ```
=== "PostgreSQL"
    ``` sql
    SELECT heroes.hero_name, powers.power_name
    FROM heroes
    CROSS JOIN powers;    
    ```
=== "SQLite"
    ``` sql
    SELECT heroes.hero_name, powers.power_name
    FROM heroes
    CROSS JOIN powers; 
    ```
=== "MariaDB"
    ``` sql
    SELECT heroes.hero_name, powers.power_name
    FROM heroes
    CROSS JOIN powers; 
    ```

---

## UNION
The UNION operator is used to combine two different SQL queries, outputting the combined result set.

The requirements of using a UNION are:

- Both queries must return the same number of columns.
- Both queries must return columns in the same order.
- Both queries must use the data data types for all columns.

---

## DELETE vs TRUNCATE
### DELETE
 - Can remove specific or all records
 - Requires a `COMMIT;`
 - Can use `WHERE` to filter affected records
 - Slower

=== "SQL Server"
    ``` sql
    DELETE FROM heroes WHERE hero_id = 1;
    COMMIT;
    ```
=== "MySQL"
    ``` sql
    DELETE FROM heroes WHERE hero_id = 1;
    COMMIT;
    ```
=== "Oracle"
    ``` sql
    DELETE FROM heroes WHERE hero_id = 1;
    COMMIT;
    ```
=== "PostgreSQL"
    ``` sql
    DELETE FROM heroes WHERE hero_id = 1;
    COMMIT;
    ```
=== "SQLite"
    ``` sql
    DELETE FROM heroes WHERE hero_id = 1;
    COMMIT;
    ```
=== "MariaDB"
    ``` sql
    DELETE FROM heroes WHERE hero_id = 1;
    COMMIT;
    ```

### TRUNCATE

 - Will remove all records
 - Does not require a `COMMIT;`
 - Unable to use `WHERE`
 - Faster

=== "SQL Server"
    ``` sql
    TRUNCATE TABLE heroes;
    ```
=== "MySQL"
    ``` sql
    TRUNCATE TABLE heroes;
    ```
=== "Oracle"
    ``` sql
    TRUNCATE TABLE heroes;
    ```
=== "PostgreSQL"
    ``` sql
    TRUNCATE TABLE heroes;
    ```
=== "SQLite"
    ``` sql
    TRUNCATE TABLE heroes;
    ```
=== "MariaDB"
    ``` sql
    TRUNCATE TABLE heroes;
    ```

---

## DISTINCT vs GROUP BY
### DISTINCT
- Will return unique column values
- Can be used on a single or multiple columns

=== "SQL Server"
    ``` sql
    SELECT DISTINCT hero_name FROM heroes;    
    ```
=== "MySQL"
    ``` sql
    SELECT DISTINCT hero_name FROM heroes;    
    ```
=== "Oracle"
    ``` sql    
    SELECT DISTINCT hero_name FROM heroes;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT DISTINCT hero_name FROM heroes;
    ```
=== "SQLite"
    ``` sql
    SELECT DISTINCT hero_name FROM heroes;    
    ```
=== "MariaDB"
    ``` sql
    SELECT DISTINCT hero_name FROM heroes;    
    ```

### GROUP BY
- Will group together data based on the selected columns
- Can be used with aggregate functions

=== "SQL Server"
    ``` sql
    SELECT hero_name, COUNT(1) FROM heroes GROUP BY hero_name;
    ```
=== "MySQL"
    ``` sql
    SELECT hero_name, COUNT(1) FROM heroes GROUP BY hero_name;
    ```
=== "Oracle"
    ``` sql
    SELECT hero_name, COUNT(1) FROM heroes GROUP BY hero_name;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT hero_name, COUNT(1) FROM heroes GROUP BY hero_name;
    ```
=== "SQLite"
    ``` sql
    SELECT hero_name, COUNT(1) FROM heroes GROUP BY hero_name;
    ```
=== "MariaDB"
    ``` sql
    SELECT hero_name, COUNT(1) FROM heroes GROUP BY hero_name;
    ```

---

## WHERE vs HAVING
### WHERE
- Filters records from the table
- Can specify JOIN conditions
- If combined with a GROUP BY, results are filtered then grouped

=== "SQL Server"
    ``` sql
    SELECT hero_name FROM heroes WHERE hero_id = 1;
    ```
=== "MySQL"
    ``` sql
    SELECT hero_name FROM heroes WHERE hero_id = 1;
    ```
=== "Oracle"
    ``` sql
    SELECT hero_name FROM heroes WHERE hero_id = 1;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT hero_name FROM heroes WHERE hero_id = 1;
    ```
=== "SQLite"
    ``` sql
    SELECT hero_name FROM heroes WHERE hero_id = 1;
    ```
=== "MariaDB"
    ``` sql
    SELECT hero_name FROM heroes WHERE hero_id = 1;
    ```

### HAVING
- Filters froms from the GROUP BY clause.
- Executes after WHERE and GROUP BY.

=== "SQL Server"
    ``` sql
    SELECT hero_id, COUNT(*) 
    FROM incidents 
    GROUP BY hero_id 
    HAVING COUNT(*) > 1;
    ```
=== "MySQL"
    ``` sql
    SELECT hero_id, COUNT(*) 
    FROM incidents 
    GROUP BY hero_id 
    HAVING COUNT(*) > 1;
    ```
=== "Oracle"
    ``` sql
    SELECT hero_id, COUNT(*) 
    FROM incidents 
    GROUP BY hero_id 
    HAVING COUNT(*) > 1;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT hero_id, COUNT(*) 
    FROM incidents 
    GROUP BY hero_id 
    HAVING COUNT(*) > 1;
    ```
=== "SQLite"
    ``` sql
    SELECT hero_id, COUNT(*) 
    FROM incidents 
    GROUP BY hero_id 
    HAVING COUNT(*) > 1;
    ```
=== "MariaDB"
    ``` sql
    SELECT hero_id, COUNT(*) 
    FROM incidents 
    GROUP BY hero_id 
    HAVING COUNT(*) > 1;
    ```

---

## Views vs Synonym
### View

- A database object based on a query.
- Essentially naming the results returned from a query.
- Directly linked with the query. Modifying the query will change the results.

### Synonym

- Alias for any database object.
- Single objects only.

---

## Functions vs Procedures

### Functions

- Should always return a value when executed.
- Can be called from a SELECT query.
- Generally used to calculate and return a result.

### Procedures

- Not required to return a value when executed.
- Cannot be called from a SELECT query.
- Generally used to implement business logic.

---

## Views vs Materialised Views
### View

- A database object based on a query.
- Essentially naming the results returned from a query.
- Directly linked with the query. Modifying the query will change the results.

### Materialised View

- A database object based on a query.
- Can be configured to refresh automatically or periodically.
- Useful for performance tuning.

---

## Aggregate Functions

### SUM()
The SUM function will calculate the SUM of all given values.
``` sql
SELECT SUM(hero_salary) FROM hero_finances;
```

The following will return the SUM of values for each group.
``` sql
SELECT SUM(hero_salary) FROM hero_finances GROUP BY superverse_id;
```

### AVG()
The AVG function will calculate the average of all given values.
``` sql
SELECT AVG(hero_salary) FROM hero_finances;
```

The following will return the average of all values for each group.
``` sql
SELECT AVG(hero_salary) FROM hero_finances GROUP BY superverse_id;
```

### MIN()
The MIN function will find the smallest value from all given values.
``` sql
SELECT MIN(hero_salary) FROM hero_finances;
```

The following will return the smallest value for each group.
``` sql
SELECT MIN(hero_salary) FROM hero_finances GROUP BY superverse_id;
```

### MAX()
The MAX function will find the largest value from all given values.
``` sql
SELECT MAX(hero_salary) FROM hero_finances;
```

The following will return the largest value for each group.
``` sql
SELECT MAX(hero_salary) FROM hero_finances GROUP BY superverse_id;
```

### COUNT()
The COUNT function will count the total number of records for the specified criteria.
``` sql
SELECT COUNT(hero_id) AS No_of_Heroes FROM hero_finances;
```

The following will return the total number of values for each group.
``` sql
SELECT COUNT(hero_id) AS No_of_Heroes FROM hero_finances GROUP BY superverse_id;
```

---

## String Functions

### CONCAT()
Concatenates two or more strings

=== "SQL Server"
    ``` sql
    SELECT CONCAT('first_name', ' ', 'second_name') AS secret_identity;
    ```
=== "MySQL"
    ``` sql
    SELECT CONCAT('first_name', ' ', 'second_name') AS secret_identity;
    ```
=== "Oracle"
    ``` sql    
    SELECT CONCAT('first_name', ' ', 'second_name') AS secret_identity FROM DUAL;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT CONCAT('first_name', ' ', 'second_name') AS secret_identity;
    ```
=== "SQLite"
    ``` sql
    SELECT 'first_name' || ' ' || 'second_name' AS secret_identity;
    ```
=== "MariaDB"
    ``` sql
    SELECT CONCAT('first_name', ' ', 'second_name') AS secret_identity;
    ```

### LENGTH()
Returns the length of a string

=== "SQL Server"
    ``` sql
    SELECT LEN(secret_identity) AS StringLength;
    ```
=== "MySQL"
    ``` sql
    SELECT LENGTH(secret_identity) AS StringLength;
    ```
=== "Oracle"
    ``` sql
    SELECT LENGTH(secret_identity) FROM dual;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT LENGTH(secret_identity) AS StringLength;
    ```
=== "SQLite"
    ``` sql
    SELECT LENGTH(secret_identity) AS StringLength;
    ```
=== "MariaDB"
    ``` sql
    SELECT LENGTH(secret_identity) AS StringLength;
    ```

### LOWER()
Converts a string to lowercase

=== "SQL Server"
    ``` sql
    SELECT LOWER(secret_identity) AS LowercaseString;
    ```
=== "MySQL"
    ``` sql
    SELECT LOWER(secret_identity) AS LowercaseString;
    ```
=== "Oracle"
    ``` sql
    SELECT LOWER(secret_identity) FROM dual;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT LOWER(secret_identity) AS LowercaseString;
    ```
=== "SQLite"
    ``` sql
    SELECT LOWER(secret_identity) AS LowercaseString;
    ```
=== "MariaDB"
    ``` sql
    SELECT LOWER(secret_identity) AS LowercaseString;
    ```

### UPPER()
Converts a string to uppercase

=== "SQL Server"
    ``` sql
    SELECT UPPER(secret_identity) AS UppercaseString;
    ```
=== "MySQL"
    ``` sql
    SELECT UPPER(secret_identity) AS UppercaseString;
    ```
=== "Oracle"
    ``` sql
    SELECT UPPER(secret_identity) FROM dual;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT UPPER(secret_identity) AS UppercaseString;
    ```
=== "SQLite"
    ``` sql
    SELECT UPPER(secret_identity) AS UppercaseString;
    ```
=== "MariaDB"
    ``` sql
    SELECT UPPER(secret_identity) AS UppercaseString;
    ```

### TRIM()
Removes leading and trailing spaces from a string

=== "SQL Server"
    ``` sql
    SELECT TRIM('secret_identity') AS TrimmedString;
    ```
=== "MySQL"
    ``` sql
    SELECT TRIM('secret_identity') AS TrimmedString;
    ```
=== "Oracle"
    ``` sql
    SELECT TRIM('secret_identity') AS TrimmedString;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT TRIM('secret_identity') AS TrimmedString;
    ```
=== "SQLite"
    ``` sql
    SELECT TRIM('secret_identity') AS TrimmedString;
    ```
=== "MariaDB"
    ``` sql
    SELECT TRIM('secret_identity') AS TrimmedString;
    ```

### SUBSTR()
Extracts a substring from a string

=== "SQL Server"
    ``` sql
    SELECT SUBSTRING(secret_identity, 1, CHARINDEX(' ',secret_identity)-1) AS first_name;
    ```
=== "MySQL"
    ``` sql
    SELECT SUBSTRING(secret_identity, 1, INSTR(secret_identity,' ', 1, 1) -1), AS first_name FROM Dual;
    ```
=== "Oracle"
    ``` sql
    SELECT SUBSTR(secret_identity, 1, INSTR(secret_identity,' ', 1, 1)-1), AS first_name FROM Dual;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT SUBSTR(secret_identity, 1, POSITION(' ' IN secret_identity) -1) AS first_name;
    ```
=== "SQLite"
    ``` sql
    SELECT SUBSTR(secret_identity, 1, INSTR(secret_identity, ' ') -1) AS first_name;
    ```
=== "MariaDB"
    ``` sql
    SELECT SUBSTRING(secret_identity, 1, INSTR(secret_identity, ' ') -1) AS first_name;
    ```

---

## Date & Time Functions

### CURRENT_DATE()
Returns the current date

=== "SQL Server"
    ``` sql
    SELECT CAST(GETDATE() AS DATE) AS CurrentDate;
    ```
=== "MySQL"
    ``` sql
    SELECT CURRENT_DATE();
    ```
=== "Oracle"
    ``` sql    
    SELECT CURRENT_DATE FROM dual;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT CURRENT_DATE;
    ```
=== "SQLite"
    ``` sql
    SELECT CURRENT_DATE;
    ```
=== "MariaDB"
    ``` sql
    SELECT CURRENT_DATE();
    ```

### CURRENT_TIME()
Returns the current time

=== "SQL Server"
    ``` sql
    SELECT CAST(GETDATE() AS TIME) AS CurrentTime;
    ```
=== "MySQL"
    ``` sql
    SELECT CURRENT_TIME();
    ```
=== "Oracle"
    ``` sql
    SELECT TO_CHAR(CURRENT_TIMESTAMP, 'HH24:MI:SS') FROM dual;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT CURRENT_TIME;
    ```
=== "SQLite"
    ``` sql
    SELECT time('now');
    ```
=== "MariaDB"
    ``` sql
    SELECT CURRENT_TIME();
    ```

### DATE()
Extracts the date part of a date or date/time expression

=== "SQL Server"
    ``` sql
    SELECT CAST(GETDATE() AS DATE) AS CurrentDate;
    ```
=== "MySQL"
    ``` sql
    SELECT DATE(NOW());
    ```
=== "Oracle"
    ``` sql
    SELECT TO_DATE('August 01, 2017', 'MONTH DD, YYYY') FROM dual;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT CURRENT_DATE;
    ```
=== "SQLite"
    ``` sql
    SELECT DATE('now');
    ```
=== "MariaDB"
    ``` sql
    SELECT CURDATE();
    ```

### TIME()
Extracts the time part of a date or date/time expression

=== "SQL Server"
    ``` sql
    SELECT CAST(GETDATE() AS TIME) AS CurrentTime;
    ```
=== "MySQL"
    ``` sql
    SELECT CAST(GETDATE() AS TIME) AS CurrentTime;
    ```
=== "Oracle"
    ``` sql
    SELECT TO_CHAR(CURRENT_TIMESTAMP, 'HH24:MI:SS') FROM dual;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT CURRENT_TIME;
    ```
=== "SQLite"
    ``` sql
    SELECT time('now');
    ```
=== "MariaDB"
    ``` sql
    SELECT TIME('10:20:30') AS result;
    ```

### DATEFDIFF()
Returns the difference between two dates

=== "SQL Server"
    ``` sql
    SELECT DATEDIFF(day, '2022-01-01', '2022-02-01') AS DateDifference;
    ```
=== "MySQL"
    ``` sql
    SELECT DATEDIFF('2022-01-01', '2022-02-01');
    ```
=== "Oracle"
    ``` sql
    SELECT TO_DATE('2022-01-02', 'YYYY-MM-DD') - TO_DATE('2022-01-01', 'YYYY-MM-DD') AS DateDifference FROM dual;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT DATE('2022-01-02') - DATE('2022-01-01') AS DateDifference;
    ```
=== "SQLite"
    ``` sql
    SELECT julianday('2022-01-02') - julianday('2022-01-01') AS DateDifference;
    ```
=== "MariaDB"
    ``` sql
    SELECT DATEDIFF('2022-01-01', '2022-02-01');
    ```

---

## Window Functions
A window function, also known as an analytic function, is a function that operates on a set of rows, referred to as a window, and returns a value for each row.

### OVER()
The OVER() clause is used with window functions to perform calculations across a group of rows, referred to as a window.

<!-- - Allows you to perform calculations on the current row from the table with other rows of the table.
- This is different from GROUP BY which aggregates the entire result set.
- Allows for aggregate functions to be used as window functions. -->

=== "SQL Server"
    ``` sql
    SELECT hero_id, hero_salary, AVG(hero_salary) OVER() AS average_salary
    FROM hero_finances;
    ```
=== "MySQL"
    ``` sql
    SELECT hero_id, hero_salary, (SELECT AVG(hero_salary) FROM hero_finances) as average_salary
    FROM hero_finances;
    ```
=== "Oracle"
    ``` sql
    SELECT hero_id, hero_salary, AVG(hero_salary) OVER() AS average_salary
    FROM hero_finances;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT hero_id, hero_salary, AVG(hero_salary) OVER() AS average_salary
    FROM hero_finances;
    ```
=== "SQLite"
    ``` sql
    SELECT hero_id, hero_salary, (SELECT AVG(hero_salary) FROM hero_finances) as average_salary
    FROM hero_finances;
    ```
=== "MariaDB"
    ``` sql
    SELECT hero_id, hero_salary, (SELECT AVG(hero_salary) FROM hero_finances) as average_salary
    FROM hero_finances;
    ```

??? example "Example: OVER()"
    ``` sql
    SELECT hero_id, hero_salary, AVG(hero_salary) OVER() AS average_salary
    FROM hero_finances;
    ```
    <div class="tbl-data_example"></div>

    | hero_id | hero_salary | average_salary     |
    | ------- | ----------- | ------------------ |
    | 1       | 30000       | 38333.333333333333 |
    | 2       | 25000       | 38333.333333333333 |
    | 3       | 35000       | 38333.333333333333 |
    | 4       | 22000       | 38333.333333333333 |
    | 5       | 62000       | 38333.333333333333 |
    | 6       | 56000       | 38333.333333333333 |

### RANK()
It will assign a rank to each row.
Multiple records with the same values will share the same rank.
For each duplicate row, the following rank **is** skipped.

=== "SQL Server"
    ``` sql
    SELECT hero_id, hero_salary, RANK() OVER (ORDER BY hero_salary DESC) AS r_hero_salary
    FROM hero_finances;
    ```
=== "MySQL"
    ``` sql
    SELECT hero_id, hero_salary, RANK() OVER (ORDER BY hero_salary DESC) AS r_hero_salary
    FROM hero_finances;
    ```
=== "Oracle"
    ``` sql
    SELECT hero_id, hero_salary, RANK() OVER (ORDER BY hero_salary DESC) AS r_hero_salary
    FROM hero_finances;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT hero_id, hero_salary, RANK() OVER (ORDER BY hero_salary DESC) AS r_hero_salary
    FROM hero_finances;
    ```
=== "SQLite"
    ``` sql
    SELECT hero_id, hero_salary, RANK() OVER (ORDER BY hero_salary DESC) AS r_hero_salary
    FROM hero_finances;
    ```
=== "MariaDB"
    ``` sql
    SELECT hero_id, hero_salary, RANK() OVER (ORDER BY hero_salary DESC) AS r_hero_salary
    FROM hero_finances;
    ```

??? example "Example: RANK()"
    ``` sql
    SELECT hero_id, hero_salary, RANK() OVER (ORDER BY hero_salary DESC) AS r_hero_salary
    FROM hero_finances;
    ```
    <div class="tbl-data_example"></div>

    | hero_id | hero_salary | r_hero_salary |
    | :-----: | :---------: | :-----------: |
    | 5       | 62000       | 1             |
    | 6       | 56000       | 2             |
    | 2       | 35000       | 3             |
    | 3       | 35000       | 3             |
    | 1       | 30000       | 5             |
    | 4       | 22000       | 6             |

### DENSE_RANK()
It will assign a rank to each row.
Multiple rows with the same value will share the same rank.
For each duplicate row, the following rank **is not** skipped.

=== "SQL Server"
    ``` sql
    SELECT hero_id, hero_salary, DENSE_RANK() OVER (ORDER BY hero_salary) AS DR_hero_salary
    FROM hero_finances;
    ```
=== "MySQL"
    ``` sql
    SELECT hero_id, hero_salary, DENSE_RANK() OVER (ORDER BY hero_salary) AS DR_hero_salary
    FROM hero_finances;    
    ```
=== "Oracle"
    ``` sql
    SELECT hero_id, hero_salary, DENSE_RANK() OVER (ORDER BY hero_salary) AS DR_hero_salary
    FROM hero_finances;    
    ```
=== "PostgreSQL"
    ``` sql
    SELECT hero_id, hero_salary, DENSE_RANK() OVER (ORDER BY hero_salary) AS DR_hero_salary
    FROM hero_finances;    
    ```
=== "SQLite"
    ``` sql
    SELECT hero_id, hero_salary, DENSE_RANK() OVER (ORDER BY hero_salary) AS DR_hero_salary
    FROM hero_finances;    
    ```
=== "MariaDB"
    ``` sql
    SELECT hero_id, hero_salary, DENSE_RANK() OVER (ORDER BY hero_salary) AS DR_hero_salary
    FROM hero_finances;    
    ```
    
??? example "Example: DENSE_RANK()"
    ``` sql
    SELECT hero_id, hero_salary, DENSE_RANK() OVER (ORDER BY hero_salary DESC) AS r_hero_salary
    FROM hero_finances;
    ```
    <div class="tbl-data_example"></div>

    | hero_id | hero_salary | r_hero_salary |
    | :-----: | :---------: | :-----------: |
    | 5       | 62000       | 1             |
    | 6       | 56000       | 2             |
    | 2       | 35000       | 3             |
    | 3       | 35000       | 3             |
    | 1       | 30000       | 4             |
    | 4       | 22000       | 5             |

### ROW_NUMBER()
Assigns a unique sequential integer number to each row in the query’s result set.

=== "SQL Server"
    ``` sql
    SELECT ROW_NUMBER() OVER (ORDER BY hero_name) AS row_number, secret_identity
    FROM heroes;
    ```
=== "MySQL"
    ``` sql
    SELECT ROW_NUMBER() OVER (ORDER BY hero_name) AS row_number, secret_identity
    FROM heroes;
    ```
=== "Oracle"
    ``` sql
    SELECT ROW_NUMBER() OVER (ORDER BY hero_name) AS row_number, secret_identity
    FROM heroes;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT ROW_NUMBER() OVER (ORDER BY hero_name) AS row_number, secret_identity
    FROM heroes;
    ```
=== "SQLite"
    ``` sql
    SELECT ROW_NUMBER() OVER (ORDER BY hero_name) AS row_number, secret_identity
    FROM heroes;
    ```
=== "MariaDB"
    ``` sql
    SELECT ROW_NUMBER() OVER (ORDER BY hero_name) AS row_number, secret_identity
    FROM heroes;
    ```

??? example "Example: ROW_NUMBER()"
    ``` sql
    SELECT ROW_NUMBER() OVER (ORDER BY hero_name) AS row_number, secret_identity
    FROM heroes;
    ```
    <div class="tbl-data_example"></div>

    | row_number | secret_identity  |
    | :--------: | :--------------: |
    | 1          | Bruce Wayne      |
    | 2          | Steve Rogers     |
    | 3          | John Constantine |
    | 4          | Bruce Banner     |
    | 5          | Tony Stark       |
    | 6          | Clark Kent       |

### LEAD()
Provides access to a row at a specified physical offset that follows the current row.

=== "SQL Server"
    ``` sql
    SELECT hero_id, superverse_id, hero_salary, 
    LEAD(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS next_hero_salary_in_superverse 
    FROM hero_finances;
    ```
=== "MySQL"
    ``` sql
    SELECT hero_id, superverse_id, hero_salary, 
    LEAD(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS next_hero_salary_in_superverse 
    FROM hero_finances;
    ```
=== "Oracle"
    ``` sql
        SELECT hero_id, superverse_id, hero_salary, 
    LEAD(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS next_hero_salary_in_superverse 
    FROM hero_finances;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT hero_id, superverse_id, hero_salary, 
    LEAD(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS next_hero_salary_in_superverse 
    FROM hero_finances;
    ```
=== "SQLite"
    `SQLite does not support the LEAD() function`
=== "MariaDB"
    ``` sql
    SELECT hero_id, superverse_id, hero_salary, 
    LEAD(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS next_hero_salary_in_superverse 
    FROM hero_finances;
    ```
    
??? example "Example: LEAD()"
    ``` sql
    SELECT hero_id, superverse_id, hero_salary, 
    LEAD(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS next_hero_salary_in_superverse 
    FROM hero_finances;
    ```
    <div class="tbl-data_example"></div>

    | hero_id | superverse_id | hero_salary | next_hero_salary_in_superverse |
    | :-----: | :-----------: | :---------: | :----------------------------: |
    | 2       | 1             | 25000       | 30000                          |
    | 1       | 1             | 30000       | 35000                          |
    | 3       | 1             | 35000       |                                |
    | 4       | 2             | 22000       | 56000                          |
    | 6       | 2             | 56000       | 62000                          |
    | 5       | 2             | 62000       |                                |

### LAG()
Provides access to a row at a specified physical offset that comes before the current row.

=== "SQL Server"
    ``` sql
    SELECT hero_id, superverse_id, hero_salary, 
    LAG(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS prev_hero_salary_in_superverse 
    FROM hero_finances;
    ```
=== "MySQL"
    ``` sql
    SELECT hero_id, superverse_id, hero_salary, 
    LAG(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS prev_hero_salary_in_superverse 
    FROM hero_finances;
    ```
=== "Oracle"
    ``` sql
    SELECT hero_id, superverse_id, hero_salary, 
    LAG(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS prev_hero_salary_in_superverse 
    FROM hero_finances;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT hero_id, superverse_id, hero_salary, 
    LAG(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS prev_hero_salary_in_superverse 
    FROM hero_finances;
    ```
=== "SQLite"
    `SQLite: SQLite does not support the LAG() function`
=== "MariaDB"
    ``` sql
    SELECT hero_id, superverse_id, hero_salary, 
    LAG(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS prev_hero_salary_in_superverse 
    FROM hero_finances;
    ```
    
??? example "Example: LAG()"
    ``` sql
    SELECT hero_id, superverse_id, hero_salary, 
    LAG(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) AS prev_hero_salary_in_superverse 
    FROM hero_finances;
    ```
    <div class="tbl-data_example"></div>

    | hero_id | superverse_id | hero_salary | prev_hero_salary_in_superverse |
    | :-----: | :-----------: | :---------: | :----------------------------: |
    | 2       | 1             | 25000       |                                |
    | 1       | 1             | 30000       | 25000                          |
    | 3       | 1             | 35000       | 30000                          |
    | 4       | 2             | 22000       |                                |
    | 6       | 2             | 56000       | 22000                          |
    | 5       | 2             | 62000       | 56000                          |

---

## Common Examples

### Text to DateTime

=== "SQL Server"
    ``` sql
    SELECT CAST('31-01-2021' AS DATE) AS date_value; 
    ```
=== "MySQL"
    ``` sql
    SELECT DATE_FORMAT('31-01-1999', '%d-%m-%Y') AS date_value;
    ```
=== "Oracle"
    ``` sql
    SELECT TO_DATE('31-01-1999') AS date_value FROM Dual;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT TO_Date('31-01-1999', 'DD-MM-YYYY') AS date_value;
    ```
=== "SQLite"
    ``` sql
    SELECT substr('31-01-1999', 7, 4) || '-' || substr('31-01-1999', 4, 2) || '-' || substr('31-01-1999', 1, 2) AS date_value;
    ```
=== "MariaDB"
    ``` sql
    SELECT STR_TO_DATE('31-01-1999', '%d-%m-%Y') AS date_value;
    ```

### Subqueries

=== "SQL Server"
    ``` sql
    SELECT hero_id, hero_salary, (SELECT AVG(hero_salary) FROM hero_finances) as average_salary
    FROM hero_finances;
    ```
=== "MySQL"
    ``` sql
    SELECT hero_id, hero_salary, (SELECT AVG(hero_salary) FROM hero_finances) as average_salary
    FROM hero_finances;
    ```
=== "Oracle"
    ``` sql
    SELECT hero_id, hero_salary, (SELECT AVG(hero_salary) FROM hero_finances) as average_salary
    FROM hero_finances;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT hero_id, hero_salary, (SELECT AVG(hero_salary) FROM hero_finances) AS average_salary
    FROM hero_finances;
    ```
=== "SQLite"
    ``` sql
    SELECT hero_id, hero_salary, (SELECT AVG(hero_salary) FROM hero_finances) as average_salary
    FROM hero_finances;
    ```
=== "MariaDB"
    ``` sql
    SELECT hero_id, hero_salary, (SELECT AVG(hero_salary) FROM hero_finances) as average_salary
    FROM hero_finances;
    ```

### Yesterday's Date
Different RDBMS will utilise different functions to achieve the same result.

=== "SQL Server"
    ``` sql
    SELECT DATEADD(day, -1, GETDATE()) AS Yesterday;
    ```
=== "MySQL"
    ``` sql
    SELECT DATE_SUB(CURDATE(), INTERVAL 1 DAY) AS Yesterday;
    ```
=== "Oracle"
    ``` sql
    SELECT SYSDATE - 1 AS Yesterday FROM dual;
    ```
=== "PostgreSQL"
    ``` sql
    SELECT CURRENT_DATE - INTERVAL '1 day' AS Yesterday;
    ```
=== "SQLite"
    ``` sql
    SELECT DATE('now','-1 day') AS Yesterday;
    ```
=== "MariaDB"
    ``` sql
    SELECT DATE_SUB(CURDATE(), INTERVAL 1 DAY) AS Yesterday;
    ```

---

## WITH
If the same subquery is to be used more than once within a query then it is standard practice to use a WITH clause instead. Similar to functions in other programming languages.

=== "SQL Server"
    ``` sql
    SELECT hero_id, hero_salary,
        (SELECT AVG(hero_salary) FROM hero_finances) AS average_salary
    FROM hero_finances;
    ```
=== "MySQL"
    ``` sql
    SELECT hero_id, hero_salary,
        (SELECT AVG(hero_salary) FROM hero_finances) AS average_salary
    FROM hero_finances;
    ```
=== "Oracle"
    ``` sql
    SELECT hero_id, hero_salary,
        (SELECT AVG(hero_salary) FROM hero_finances) AS average_salary
    FROM hero_finances;
    ```
=== "PostgreSQL"
    ``` sql
    WITH hero_salaries AS (
        SELECT hero_id, AVG(hero_salary) AS average_salary 
        FROM hero_finances 
        GROUP BY hero_id
    )
    SELECT * FROM hero_salaries;
    ```
=== "SQLite"
    ``` sql
    SELECT hero_id, hero_salary, 
       (SELECT AVG(hero_salary) FROM hero_finances) AS average_salary
    FROM hero_finances;
    ```
=== "MariaDB"
    ``` sql
    SELECT hero_id, hero_salary, 
       (SELECT AVG(hero_salary) FROM hero_finances) AS average_salary
    FROM hero_finances;
    ```

---

## INDEXES
- An index is a database object that applied to one or more columns within a table.
- They improve execution times within queries.
- The tables primary key is an example of an index.
- Indexes can have different functionalities and behaviours.


=== "SQL Server"
    ``` sql
    CREATE INDEX idx_hero_name ON heroes (hero_name);
    ```
=== "MySQL"
    ``` sql
    CREATE INDEX idx_hero_name ON heroes (hero_name);
    ```
=== "Oracle"
    ``` sql    
    CREATE INDEX idx_hero_name ON heroes (hero_name);
    ```
=== "PostgreSQL"
    ``` sql
    CREATE INDEX idx_hero_name ON heroes (hero_name);
    ```
=== "SQLite"
    ``` sql
    CREATE INDEX idx_hero_name ON heroes (hero_name);
    ```
=== "MariaDB"
    ``` sql
    CREATE INDEX idx_hero_name ON heroes (hero_name);
    ```

---

## SQL Optimiser
An SQL optimiser is responsible for analysing queries and determining the most efficient execution mechanisms. The optimiser uses costing methods or internal rules to determine the most efficient way of producing the result of the query. It generates one or more query plans for each query, each of which may be a mechanism used to run a query. It is not used directly but instead runs 'behind the scenes' whenever a query is executed.A

## SQL Statistics
Database statistics are a form of dynamic metadata that assist the query optimizer in making better decisions. They are used by the query optimiser to track the distribution of values in indexes and columns.

For example, if there are only a dozen rows in a table, then there’s no point going to an index to do a lookup; you will always be better off doing a full table scan. But if that same table grows to a million rows, then you will probably be better off using the index.

---

## Query Tuning
The following tips can help reduce execution times and resource costs of running queries.

### Check SQL Query
Ensure the SQL Query has been written in the best way possible.

- Ensure table joins are correct.
- Ensure filters are correct by using `WHERE`.
- Ensure cross/cartesian joins will not occur.
- Avoid repeated subqueries by using `WITH`.
- Select only required and relevant columns.
- Make use of the optimiser by making joins in a similar fashion to creating indexes.

### Check Indexes
Ensure the indexes have been created for the desired columns.

- Ensure the correct type of indexes Are used.
- Ensure correct columns are indexed.
- Avoid unnecessary indexes.

=== "SQL Server"
    ``` sql
    SELECT * 
    FROM sys.indexes 
    WHERE object_id = OBJECT_ID('heroes');
    ```
=== "MySQL"
    ``` sql
    SELECT * 
    FROM information_schema.statistics 
    WHERE table_name = 'heroes';
    ```
=== "Oracle"
    ``` sql
    SELECT * 
    FROM ALL_INDEXES 
    WHERE table_name = 'HEROES';
    ```
=== "PostgreSQL"
    ``` sql
    SELECT indexname, indexdef 
    FROM pg_indexes 
    WHERE tablename = 'heroes';
    ```
=== "SQLite"
    ``` sql
    .indexes heroes
    ```
=== "MariaDB"
    ``` sql
    SELECT * 
    FROM information_schema.statistics 
    WHERE table_name = 'heroes';
    ```

### Check Statistics
Check if table statistics are up to date.

- Ensure statistics are generated for used tables.
- Regenerate statisctics if data within table has been modified.

=== "SQL Server"
    ``` sql
    SELECT * 
    FROM sys.dm_db_stats_properties (OBJECT_ID('heroes'));
    ```
=== "MySQL"
    ``` sql
    SELECT * 
    FROM information_schema.statistics 
    WHERE table_name = 'heroes';
    ```
=== "Oracle"
    ``` sql
    SELECT * 
    FROM DBA_TAB_COL_STATISTICS 
    WHERE table_name = 'HEROES';
    ```
=== "PostgreSQL"
    ``` sql
    SELECT * 
    FROM pg_stats
    WHERE tablename = 'heroes';
    ```
=== "SQLite"
    ``` sql
    ANALYZE;
    SELECT * 
    FROM sqlite_stat1 
    WHERE tbl = 'heroes';
    ```
=== "MariaDB"
    ``` sql
    SELECT * 
    FROM information_schema.statistics 
    WHERE table_name = 'heroes';
    ```

### Check Explain Plan
Check the database's explain plan for the current query.

- Ensure correct JOINs, INDEXes, etc are used during the execution of the query.

=== "SQL Server"
    ``` sql
    SET SHOWPLAN_ALL ON;
    GO
    SELECT * FROM heroes;
    GO
    SET SHOWPLAN_ALL OFF;
    GO
    ```
=== "MySQL"
    ``` sql
    EXPLAIN SELECT * FROM heroes;
    ```
=== "Oracle"
    ``` sql
    EXPLAIN PLAN FOR SELECT * FROM heroes;
    SELECT * FROM table(DBMS_XPLAN.DISPLAY);
    ```
=== "PostgreSQL"
    ``` sql
    EXPLAIN SELECT * FROM heroes;
    ```
=== "SQLite"
    ``` sql
    EXPLAIN QUERY PLAN SELECT * FROM heroes;
    ```
=== "MariaDB"
    ``` sql
    EXPLAIN SELECT * FROM heroes;
    ```

---

## Triggers

Triggers are database objects that execute queries or stored procedures at specified system events.

### DML Triggers

- Executed when DML operations are used.
- `INSERT`, `UPDATE`, `DELETE`, `MERGE`
- Can be set to execute before or after the DML operation.

### DDL Triggers

- Executed when DDL operations are used.
- `CREATE`, `ALTER`, `DROP`, `TRUNCATE`

### Database Triggers

- Executed when a database session is started or ended.

---
