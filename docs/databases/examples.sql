CREATE TABLE superverses (
    superverse_id SERIAL PRIMARY KEY,
    superverse_name VARCHAR(50)
);

INSERT INTO superverses (superverse_name) VALUES ('Marvel'), ('DC');

CREATE TABLE heroes (
    hero_id SERIAL PRIMARY KEY,
    superverse_id INT,
    hero_name VARCHAR(100),
    secret_identity VARCHAR(100),
    archetype VARCHAR(50),
    CONSTRAINT fk_superverse_id FOREIGN KEY (superverse_id) REFERENCES superverses (superverse_id)
);

INSERT INTO heroes (hero_name, secret_identity, archetype, superverse_id) 
VALUES ('Ironman', 'Tony Stark','Hero', 1),
('Captain America', 'Steve Rogers','Hero', 1),
('Hulk', 'Bruce Banner','Hero', 1),
('Batman', 'Bruce Wayne','Hero', 2),
('Constantine', 'John Constantine','Hero', 2),
('Superman', 'Clark Kent','Hero', 2);

CREATE TABLE villians (
    villian_id SERIAL PRIMARY KEY,
    superverse_id INT,
    villian_name VARCHAR(100),
    archetype VARCHAR(50),
    CONSTRAINT fk_superverse_id FOREIGN KEY (superverse_id) REFERENCES superverses (superverse_id)
);

INSERT INTO villians (villian_name, archetype, superverse_id) 
VALUES ('Justin Hammer','Villian', 1),
('Red Skull','Villian', 1),
('Abomination','Villian', 1),
('Joker','Villian', 2),
('Lucifer','Villian', 2),
('Lex Luthor','Villian', 2);

CREATE TABLE powers (
    power_id SERIAL PRIMARY KEY,
    hero_id INT,
    power_name VARCHAR(255),
    CONSTRAINT fk_hero_id FOREIGN KEY (hero_id) REFERENCES heroes (hero_id)
);

INSERT INTO powers (hero_id, power_name)
VALUES (1,'Smarts'),
(2,'Super Serum'),
(3,'Rage'),
(4,'Stealth'),
(5,'Magic'),
(6,'Flight');


CREATE TABLE incidents (
    incident_id SERIAL PRIMARY KEY,
    incident_location VARCHAR(100),
    hero_id INT,
    villian_id INT
    -- CONSTRAINT fk_hero_id FOREIGN KEY (hero_id) REFERENCES heroes (hero_id),
    -- CONSTRAINT fk_villian_id FOREIGN KEY (villian_id) REFERENCES villians (villian_id)
);

INSERT INTO incidents (incident_location, hero_id, villian_id)
VALUES ('New York', 1, 1),
('Germany', 1, 2),
('Virginia', 3, 3),
('Gotham', 7, 4),
('London', 8, 5),
('Metropolis', 6, 6);

CREATE TABLE hero_finances (
  finance_id SERIAL PRIMARY KEY,
  hero_id INT,
  superverse_id INT,
  hero_salary INT
 );

INSERT INTO hero_finances (hero_id, superverse_id, hero_salary)
VALUES (1, 1, 30000),
(2, 1, 25000),
(3, 1, 35000),
(4, 2, 22000),
(5, 2, 62000),
(6, 2, 56000);

---

SELECT * FROM superverses;

SELECT * FROM heroes;

SELECT * FROM villians;

SELECT * FROM powers;

SELECT * FROM incidents;

SELECT * FROM hero_finances;

SELECT heroes.hero_name, incidents.incident_location
FROM heroes
INNER JOIN incidents ON heroes.hero_id = incidents.hero_id;

SELECT heroes.hero_name, incidents.incident_location
FROM heroes
LEFT JOIN incidents ON heroes.hero_id = incidents.hero_id;

SELECT heroes.hero_name, incidents.incident_location
FROM heroes
RIGHT JOIN incidents ON heroes.hero_id = incidents.hero_id;

SELECT heroes.hero_name, incidents.incident_location
FROM heroes
FULL OUTER JOIN incidents ON heroes.hero_id = incidents.hero_id;

SELECT h1.hero_name, h2.hero_name
FROM heroes AS h1
JOIN heroes AS h2 ON h1.hero_id != h2.hero_id;

SELECT heroes.hero_name, superverses.superverse_name
FROM heroes
NATURAL JOIN superverses;

SELECT heroes.hero_name, powers.power_name
FROM heroes
CROSS JOIN powers;




SELECT hero_id, hero_salary, RANK() OVER (ORDER BY hero_salary DESC) dsds
FROM hero_finances;

SELECT hero_id, hero_salary, DENSE_RANK() OVER (ORDER BY hero_salary) AS TEST
FROM hero_finances;

SELECT ROW_NUMBER() OVER (ORDER BY hero_name) row_number, secret_identity
FROM heroes;

SELECT hero_id, superverse_id, hero_salary, 
LEAD(hero_salary) OVER (PARTITION BY superverse_id ORDER BY hero_salary) as next_hero_salary_in_superverse 
FROM hero_finances;