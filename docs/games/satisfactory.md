# Satisfactory

## Statistics

### Ore's 

The following tables show ore production per minute for each miner:


<div id='tbl-Satisfactory'></div>
| **Miner**   | **Impure** | **Normal** | **Pure** |
|:------------|:----------:|:----------:|:--------:|
| Mk 1        | 30         | 60         | 120      |
| Mk 2        | 60         | 120        | 240      |
| Mk 3        | 120        | 240        | 480      |
| Mk.3 (150%) | 180        | 360        | 720      |
| Mk.3 (200%) | 240        | 480        | 960      |
| Mk.3 (250%) | 300        | 600        | 1200     |
<p id=caption>Iron Ore, Copper Ore, Caterium Ore, Limestone, Coal, Quartz, Sulfur</p>

<div id='tbl-Satisfactory'></div>
| **Miner**   | **Impure** | **Normal** | **Pure** |
|:------------|:----------:|:----------:|:--------:|
| Mk 1        | 60         | 120        | 240      |
| Mk.1 (150%) | 90         | 180        | 360      |
| Mk.1 (200%) | 120        | 240        | 480      |
| Mk.1 (250%) | 150        | 300        | 600      |
<p id=caption>Crude Oil</p>

## Colour Palette's

<div id='tbl-Satisfactory'></div>

| **#** | **Item** | **Colour 1** | **Colour 2** | **Category** |
| :---: | :------- | :----------: | :----------: | :----------: |
| 1 | Iron Ore | <div style='background-color: #663633' id='colourCode'>#663633</div>  | <div style='background-color: #656F8C' id='colourCode'>#656F8C</div>  | <img src='../img/satisfactory/Iron_Ore.png' width='50'> |
| 2 | Copper Ore | <div style='background-color: #9D5A47' id='colourCode'>#9D5A47</div>  | <div style='background-color: #405A61' id='colourCode'>#405A61</div>  | <img src='../img/satisfactory/Copper_Ore.png' width='50'> |
| 3 | Caterium Ore | <div style='background-color: #E9D2A0' id='colourCode'>#E9D2A0</div>  | <div style='background-color: #A27A40' id='colourCode'>#A27A40</div>  | <img src='../img/satisfactory/Caterium_Ore.png' width='50'> |
| 4 | Limestone | <div style='background-color: #C6B5AB' id='colourCode'>#C6B5AB</div>  | <div style='background-color: #89765F' id='colourCode'>#89765F</div>  | <img src='../img/satisfactory/Limestone.png' width='50'> |
| 5 | Coal | <div style='background-color: #050807' id='colourCode'>#050807</div>  | <div style='background-color: #0B0F15' id='colourCode'>#0B0F15</div>  | <img src='../img/satisfactory/Coal.png' width='50'> |
| 6 | Sulfur | <div style='background-color: #DED267' id='colourCode'>#DED267</div>  | <div style='background-color: #876421' id='colourCode'>#876421</div>  | <img src='../img/satisfactory/Sulfur.png' width='50'> |
| 7 | Raw Quartz | <div style='background-color: #ED82BC' id='colourCode'>#ED82BC</div>  | <div style='background-color: #9D2A68' id='colourCode'>#9D2A68</div>  | <img src='../img/satisfactory/Raw_Quartz.png' width='50'> |
| 8 | Bauxite | <div style='background-color: #D29A80' id='colourCode'>#D29A80</div>  | <div style='background-color: #9C5548' id='colourCode'>#9C5548</div>  | <img src='../img/satisfactory/Bauxite.png' width='50'> |
| 9 | Uranium | <div style='background-color: #448A3A' id='colourCode'>#448A3A</div>  | <div style='background-color: #173717' id='colourCode'>#173717</div>  | <img src='../img/satisfactory/Uranium.png' width='50'> |
| 10 | SAM Ore | <div style='background-color: #124354' id='colourCode'>#124354</div>  | <div style='background-color: #2C0436' id='colourCode'>#2C0436</div>  | <img src='../img/satisfactory/SAM_Ore.png' width='50'> |
| 11 | Leaves | <div style='background-color: #4D6F43' id='colourCode'>#4D6F43</div>  | <div style='background-color: #142C12' id='colourCode'>#142C12</div>  | <img src='../img/satisfactory/Leaves.png' width='50'> |
| 12 | Wood | <div style='background-color: #735E43' id='colourCode'>#735E43</div>  | <div style='background-color: #2A1E14' id='colourCode'>#2A1E14</div>  | <img src='../img/satisfactory/Wood.png' width='50'> |
| 13 | Biomass | <div style='background-color: #6B5F35' id='colourCode'>#6B5F35</div>  | <div style='background-color: #3E3118' id='colourCode'>#3E3118</div>  | <img src='../img/satisfactory/Biomass.png' width='50'> |
| 14 | Solid Biofuel | <div style='background-color: #847161' id='colourCode'>#847161</div>  | <div style='background-color: #3C2F25' id='colourCode'>#3C2F25</div>  | <img src='../img/satisfactory/Solid_Biofuel.png' width='50'> |
| 15 | Mycelia | <div style='background-color: #8B8186' id='colourCode'>#8B8186</div>  | <div style='background-color: #3E3C49' id='colourCode'>#3E3C49</div>  | <img src='../img/satisfactory/Mycelia.png' width='50'> |
| 16 | Flower Petals | <div style='background-color: #A94066' id='colourCode'>#A94066</div>  | <div style='background-color: #7D8398' id='colourCode'>#7D8398</div>  | <img src='../img/satisfactory/Flower_Petals.png' width='50'> |
| 17 | Fabric | <div style='background-color: #978A85' id='colourCode'>#978A85</div>  | <div style='background-color: #000003' id='colourCode'>#000003</div>  | <img src='../img/satisfactory/Fabric.png' width='50'> |
| 18 | Concrete | <div style='background-color: #83755F' id='colourCode'>#83755F</div>  | <div style='background-color: #C7C3C8' id='colourCode'>#C7C3C8</div>  | <img src='../img/satisfactory/Concrete.png' width='50'> |
| 19 | Iron Ingot | <div style='background-color: #8E9298' id='colourCode'>#8E9298</div>  | <div style='background-color: #484948' id='colourCode'>#484948</div>  | <img src='../img/satisfactory/Iron_Ingot.png' width='50'> |
| 20 | Iron Plate | <div style='background-color: #AFB2B6' id='colourCode'>#AFB2B6</div>  | <div style='background-color: #4C4F54' id='colourCode'>#4C4F54</div>  | <img src='../img/satisfactory/Iron_Plate.png' width='50'> |
| 21 | Iron Rod | <div style='background-color: #595B63' id='colourCode'>#595B63</div>  | <div style='background-color: #1C1E22' id='colourCode'>#1C1E22</div>  | <img src='../img/satisfactory/Iron_Rod.png' width='50'> |
| 22 | Screw | <div style='background-color: #99A2AA' id='colourCode'>#99A2AA</div>  | <div style='background-color: #1164CE' id='colourCode'>#1164CE</div>  | <img src='../img/satisfactory/Screw.png' width='50'> |
| 23 | Reinforced Iron Plate | <div style='background-color: #9AA2B5' id='colourCode'>#9AA2B5</div>  | <div style='background-color: #59657E' id='colourCode'>#59657E</div>  | <img src='../img/satisfactory/Reinforced_Iron_Plate.png' width='50'> |
| 24 | Modular Frame | <div style='background-color: #838BAA' id='colourCode'>#838BAA</div>  | <div style='background-color: #5D5D5D' id='colourCode'>#5D5D5D</div>  | <img src='../img/satisfactory/Modular_Frame.png' width='50'> |
| 25 | Copper Ingot | <div style='background-color: #B07E7C' id='colourCode'>#B07E7C</div>  | <div style='background-color: #553329' id='colourCode'>#553329</div>  | <img src='../img/satisfactory/Copper_Ingot.png' width='50'> |
| 26 | Copper Sheet | <div style='background-color: #9E6D63' id='colourCode'>#9E6D63</div>  | <div style='background-color: #325A8D' id='colourCode'>#325A8D</div>  | <img src='../img/satisfactory/Copper_Sheet.png' width='50'> |
| 27 | Wire | <div style='background-color: #AB6C47' id='colourCode'>#AB6C47</div>  | <div style='background-color: #846B4F' id='colourCode'>#846B4F</div>  | <img src='../img/satisfactory/Wire.png' width='50'> |
| 28 | Cable | <div style='background-color: #282A32' id='colourCode'>#282A32</div>  | <div style='background-color: #6B6862' id='colourCode'>#6B6862</div>  | <img src='../img/satisfactory/Cable.png' width='50'> |
| 29 | Copper Powder | <div style='background-color: #7D5664' id='colourCode'>#7D5664</div>  | <div style='background-color: #AFA4B6' id='colourCode'>#AFA4B6</div>  | <img src='../img/satisfactory/Copper_Powder.png' width='50'> |
| 30 | Stator | <div style='background-color: #444351' id='colourCode'>#444351</div>  | <div style='background-color: #59352C' id='colourCode'>#59352C</div>  | <img src='../img/satisfactory/Stator.png' width='50'> |
| 31 | Motor | <div style='background-color: #47526F' id='colourCode'>#47526F</div>  | <div style='background-color: #BF971F' id='colourCode'>#BF971F</div>  | <img src='../img/satisfactory/Motor.png' width='50'> |
| 32 | Heavy Modular Frame | <div style='background-color: #818896' id='colourCode'>#818896</div>  | <div style='background-color: #1B1B1B' id='colourCode'>#1B1B1B</div>  | <img src='../img/satisfactory/Heavy_Modular_Frame.png' width='50'> |
| 33 | Smart Plating | <div style='background-color: #464B73' id='colourCode'>#464B73</div>  | <div style='background-color: #A34033' id='colourCode'>#A34033</div>  | <img src='../img/satisfactory/Smart_Plating.png' width='50'> |
| 34 | Automated Wiring | <div style='background-color: #26292E' id='colourCode'>#26292E</div>  | <div style='background-color: #A34033' id='colourCode'>#A34033</div>  | <img src='../img/satisfactory/Automated_Wiring.png' width='50'> |
| 35 | Caterium Ingot | <div style='background-color: #CDBA89' id='colourCode'>#CDBA89</div>  | <div style='background-color: #927844' id='colourCode'>#927844</div>  | <img src='../img/satisfactory/Caterium_Ingot.png' width='50'> |
| 36 | Quickwire | <div style='background-color: #CDBA89' id='colourCode'>#CDBA89</div>  | <div style='background-color: #2A2E3F' id='colourCode'>#2A2E3F</div>  | <img src='../img/satisfactory/Quickwire.png' width='50'> |
| 37 | Rotor | <div style='background-color: #364364' id='colourCode'>#364364</div>  | <div style='background-color: #744029' id='colourCode'>#744029</div>  | <img src='../img/satisfactory/Rotor.png' width='50'> |
| 38 | Quartz Crystal | <div style='background-color: #CB86C9' id='colourCode'>#CB86C9</div>  | <div style='background-color: #3150B4' id='colourCode'>#3150B4</div>  | <img src='../img/satisfactory/Quartz_Crystal.png' width='50'> |
| 39 | Silica | <div style='background-color: #D3D4DD' id='colourCode'>#D3D4DD</div>  | <div style='background-color: #B2CEDF' id='colourCode'>#B2CEDF</div>  | <img src='../img/satisfactory/Silica.png' width='50'> |
| 40 | Crystal Oscillator | <div style='background-color: #F7CF4E' id='colourCode'>#F7CF4E</div>  | <div style='background-color: #999198' id='colourCode'>#999198</div>  | <img src='../img/satisfactory/Crystal_Oscillator.png' width='50'> |
| 41 | Circuit Board | <div style='background-color: #2B5533' id='colourCode'>#2B5533</div>  | <div style='background-color: #FFB85F' id='colourCode'>#FFB85F</div>  | <img src='../img/satisfactory/Circuit_Board.png' width='50'> |
| 42 | High-Speed Connector | <div style='background-color: #2B5533' id='colourCode'>#2B5533</div>  | <div style='background-color: #415355' id='colourCode'>#415355</div>  | <img src='../img/satisfactory/High-Speed_Connector.png' width='50'> |
| 43 | Steel Ingot | <div style='background-color: #23272C' id='colourCode'>#23272C</div>  | <div style='background-color: #0F1011' id='colourCode'>#0F1011</div>  | <img src='../img/satisfactory/Steel_Ingot.png' width='50'> |
| 44 | Steel Beam | <div style='background-color: #292A2E' id='colourCode'>#292A2E</div>  | <div style='background-color: #703D30' id='colourCode'>#703D30</div>  | <img src='../img/satisfactory/Steel_Beam.png' width='50'> |
| 45 | Steel Pipe | <div style='background-color: #323232' id='colourCode'>#323232</div>  | <div style='background-color: #703D30' id='colourCode'>#703D30</div>  | <img src='../img/satisfactory/Steel_Pipe.png' width='50'> |
| 46 | Encased Industrial Beam | <div style='background-color: #B3A891' id='colourCode'>#B3A891</div>  | <div style='background-color: #863F40' id='colourCode'>#863F40</div>  | <img src='../img/satisfactory/Encased_Industrial_Beam.png' width='50'> |
| 47 | Versatile Framework | <div style='background-color: #292A2D' id='colourCode'>#292A2D</div>  | <div style='background-color: #8B2819' id='colourCode'>#8B2819</div>  | <img src='../img/satisfactory/Versatile_Framework.png' width='50'> |
| 48 | Computer | <div style='background-color: #C08E5A' id='colourCode'>#C08E5A</div>  | <div style='background-color: #2E3531' id='colourCode'>#2E3531</div>  | <img src='../img/satisfactory/Computer.png' width='50'> |
| 49 | Modular Engine | <div style='background-color: #45444B' id='colourCode'>#45444B</div>  | <div style='background-color: #A34033' id='colourCode'>#A34033</div>  | <img src='../img/satisfactory/Modular_Engine.png' width='50'> |
| 50 | Adaptive Control Unit | <div style='background-color: #6B686C' id='colourCode'>#6B686C</div>  | <div style='background-color: #DFA05A' id='colourCode'>#DFA05A</div>  | <img src='../img/satisfactory/Adaptive_Control_Unit.png' width='50'> |
| 51 | Assembly Director System | <div style='background-color: #303032' id='colourCode'>#303032</div>  | <div style='background-color: #A34033' id='colourCode'>#A34033</div>  | <img src='../img/satisfactory/Assembly_Director_System.png' width='50'> |
| 52 | Magnetic Field Generator | <div style='background-color: #303032' id='colourCode'>#303032</div>  | <div style='background-color: #9AD7B1' id='colourCode'>#9AD7B1</div>  | <img src='../img/satisfactory/Magnetic_Field_Generator.png' width='50'> |
| 53 | Nuclear Pasta | <div style='background-color: #C08E5A' id='colourCode'>#C08E5A</div>  | <div style='background-color: #93F8F7' id='colourCode'>#93F8F7</div>  | <img src='../img/satisfactory/Nuclear_Pasta.png' width='50'> |
| 54 | Thermal Propulsion Rocket | <div style='background-color: #303032' id='colourCode'>#303032</div>  | <div style='background-color: #A34033' id='colourCode'>#A34033</div>  | <img src='../img/satisfactory/Thermal_Propulsion_Rocket.png' width='50'> |
| 55 | Water | <div style='background-color: #1F7CA1' id='colourCode'>#1F7CA1</div>  | <div style='background-color: #0D0D0E' id='colourCode'>#0D0D0E</div>  | <img src='../img/satisfactory/Water.png' width='50'> |
| 56 | Packaged Water | <div style='background-color: #66A5C6' id='colourCode'>#66A5C6</div>  | <div style='background-color: #0251B8' id='colourCode'>#0251B8</div>  | <img src='../img/satisfactory/Packaged_Water.png' width='50'> |
| 57 | Crude Oil | <div style='background-color: #111112' id='colourCode'>#111112</div>  | <div style='background-color: #0251B8' id='colourCode'>#0251B8</div>  | <img src='../img/satisfactory/Crude_Oil.png' width='50'> |
| 58 | Packaged Oil | <div style='background-color: #0D0D0E' id='colourCode'>#0D0D0E</div>  | <div style='background-color: #D5D4D2' id='colourCode'>#D5D4D2</div>  | <img src='../img/satisfactory/Packaged_Oil.png' width='50'> |
| 59 | Heavy Oil Residue | <div style='background-color: #85019A' id='colourCode'>#85019A</div>  | <div style='background-color: #0D0D0E' id='colourCode'>#0D0D0E</div>  | <img src='../img/satisfactory/Heavy_Oil_Residue.png' width='50'> |
| 60 | Packaged Heavy Oil Residue | <div style='background-color: #85019A' id='colourCode'>#85019A</div>  | <div style='background-color: #D5D4D2' id='colourCode'>#D5D4D2</div>  | <img src='../img/satisfactory/Packaged_Heavy_Oil_Residue.png' width='50'> |
| 61 | Fuel | <div style='background-color: #CD812B' id='colourCode'>#CD812B</div>  | <div style='background-color: #0D0D0E' id='colourCode'>#0D0D0E</div>  | <img src='../img/satisfactory/Fuel.png' width='50'> |
| 62 | Packaged Fuel | <div style='background-color: #F9A320' id='colourCode'>#F9A320</div>  | <div style='background-color: #10100E' id='colourCode'>#10100E</div>  | <img src='../img/satisfactory/Packaged_Fuel.png' width='50'> |
| 63 | Liquid Biofuel | <div style='background-color: #263E1A' id='colourCode'>#263E1A</div>  | <div style='background-color: #0D0D0E' id='colourCode'>#0D0D0E</div>  | <img src='../img/satisfactory/Liquid_Biofuel.png' width='50'> |
| 64 | Packaged Liquid Biofuel | <div style='background-color: #195200' id='colourCode'>#195200</div>  | <div style='background-color: #10100E' id='colourCode'>#10100E</div>  | <img src='../img/satisfactory/Packaged_Liquid_Biofuel.png' width='50'> |
| 65 | Turbofuel | <div style='background-color: #810006' id='colourCode'>#810006</div>  | <div style='background-color: #0D0D0E' id='colourCode'>#0D0D0E</div>  | <img src='../img/satisfactory/Turbofuel.png' width='50'> |
| 66 | Packaged Turbofuel | <div style='background-color: #D60710' id='colourCode'>#D60710</div>  | <div style='background-color: #10100E' id='colourCode'>#10100E</div>  | <img src='../img/satisfactory/Packaged_Turbofuel.png' width='50'> |
| 67 | Empty Canister | <div style='background-color: #64606A' id='colourCode'>#64606A</div>  | <div style='background-color: #10100E' id='colourCode'>#10100E</div>  | <img src='../img/satisfactory/Empty_Canister.png' width='50'> |
| 68 | Alumina Solution | <div style='background-color: #B4B1B7' id='colourCode'>#B4B1B7</div>  | <div style='background-color: #0D0D0E' id='colourCode'>#0D0D0E</div>  | <img src='../img/satisfactory/Alumina_Solution.png' width='50'> |
| 69 | Packaged Alumina Solution | <div style='background-color: #BDC0CC' id='colourCode'>#BDC0CC</div>  | <div style='background-color: #DCAF4C' id='colourCode'>#DCAF4C</div>  | <img src='../img/satisfactory/Packaged_Alumina_Solution.png' width='50'> |
| 70 | Sulfuric Acid | <div style='background-color: #D1C83A' id='colourCode'>#D1C83A</div>  | <div style='background-color: #0D0D0E' id='colourCode'>#0D0D0E</div>  | <img src='../img/satisfactory/Sulfuric_Acid.png' width='50'> |
| 71 | Packaged Sulfuric Acid | <div style='background-color: #B2AE4F' id='colourCode'>#B2AE4F</div>  | <div style='background-color: #42434F' id='colourCode'>#42434F</div>  | <img src='../img/satisfactory/Packaged_Sulfuric_Acid.png' width='50'> |
| 72 | Nitric Acid | <div style='background-color: #D8D8BC' id='colourCode'>#D8D8BC</div>  | <div style='background-color: #0D0D0E' id='colourCode'>#0D0D0E</div>  | <img src='../img/satisfactory/Nitric_Acid.png' width='50'> |
| 73 | Nitrogen Gas | <div style='background-color: #B0ACB4' id='colourCode'>#B0ACB4</div>  | <div style='background-color: #0D0D0E' id='colourCode'>#0D0D0E</div>  | <img src='../img/satisfactory/Nitrogen_Gas.png' width='50'> |
| 74 | Packaged Nitrogen Gas | <div style='background-color: #23273B' id='colourCode'>#23273B</div>  | <div style='background-color: #B7B9C6' id='colourCode'>#B7B9C6</div>  | <img src='../img/satisfactory/Packaged_Nitrogen_Gas.png' width='50'> |
| 75 | Packaged Nitric Acid | <div style='background-color: #CFCFDB' id='colourCode'>#CFCFDB</div>  | <div style='background-color: #454550' id='colourCode'>#454550</div>  | <img src='../img/satisfactory/Packaged_Nitric_Acid.png' width='50'> |
| 76 | Plastic | <div style='background-color: #4DACF2' id='colourCode'>#4DACF2</div>  | <div style='background-color: #221F24' id='colourCode'>#221F24</div>  | <img src='../img/satisfactory/Plastic.png' width='50'> |
| 77 | Polymer Resin | <div style='background-color: #252FD4' id='colourCode'>#252FD4</div>  | <div style='background-color: #060330' id='colourCode'>#060330</div>  | <img src='../img/satisfactory/Polymer_Resin.png' width='50'> |
| 78 | Rubber | <div style='background-color: #1E1D1D' id='colourCode'>#1E1D1D</div>  | <div style='background-color: #CFB65E' id='colourCode'>#CFB65E</div>  | <img src='../img/satisfactory/Rubber.png' width='50'> |
| 79 | Petroleum Coke | <div style='background-color: #2B2C34' id='colourCode'>#2B2C34</div>  | <div style='background-color: #060602' id='colourCode'>#060602</div>  | <img src='../img/satisfactory/Petroleum_Coke.png' width='50'> |
| 80 | Empty Fluid Tank | <div style='background-color: #7B95BD' id='colourCode'>#7B95BD</div>  | <div style='background-color: #99969B' id='colourCode'>#99969B</div>  | <img src='../img/satisfactory/Empty_Fluid_Tank.png' width='50'> |
| 81 | Compacted Coal | <div style='background-color: #171A24' id='colourCode'>#171A24</div>  | <div style='background-color: #0D0C09' id='colourCode'>#0D0C09</div>  | <img src='../img/satisfactory/Compacted_Coal.png' width='50'> |
| 82 | Black Powder | <div style='background-color: #1C1D23' id='colourCode'>#1C1D23</div>  | <div style='background-color: #C8000A' id='colourCode'>#C8000A</div>  | <img src='../img/satisfactory/Black_Powder.png' width='50'> |
| 83 | Smokeless Powder | <div style='background-color: #0C0C0C' id='colourCode'>#0C0C0C</div>  | <div style='background-color: #DF1720' id='colourCode'>#DF1720</div>  | <img src='../img/satisfactory/Smokeless_Powder.png' width='50'> |
| 84 | AI Limiter | <div style='background-color: #1C1C1E' id='colourCode'>#1C1C1E</div>  | <div style='background-color: #648D8C' id='colourCode'>#648D8C</div>  | <img src='../img/satisfactory/AI_Limiter.png' width='50'> |
| 85 | Supercomputer | <div style='background-color: #222327' id='colourCode'>#222327</div>  | <div style='background-color: #F1BF74' id='colourCode'>#F1BF74</div>  | <img src='../img/satisfactory/Supercomputer.png' width='50'> |
| 86 | Radio Control Unit | <div style='background-color: #2B2B2B' id='colourCode'>#2B2B2B</div>  | <div style='background-color: #E2BC26' id='colourCode'>#E2BC26</div>  | <img src='../img/satisfactory/Radio_Control_Unit.png' width='50'> |
| 87 | Heat Sink | <div style='background-color: #949796' id='colourCode'>#949796</div>  | <div style='background-color: #A1663E' id='colourCode'>#A1663E</div>  | <img src='../img/satisfactory/Heat_Sink.png' width='50'> |
| 88 | Battery | <div style='background-color: #2D3146' id='colourCode'>#2D3146</div>  | <div style='background-color: #ADADBC' id='colourCode'>#ADADBC</div>  | <img src='../img/satisfactory/Battery.png' width='50'> |
| 89 | Fused Modular Frame | <div style='background-color: #A5A5A6' id='colourCode'>#A5A5A6</div>  | <div style='background-color: #F4AA53' id='colourCode'>#F4AA53</div>  | <img src='../img/satisfactory/Fused_Modular_Frame.png' width='50'> |
| 90 | Cooling System | <div style='background-color: #DE9860' id='colourCode'>#DE9860</div>  | <div style='background-color: #999DA1' id='colourCode'>#999DA1</div>  | <img src='../img/satisfactory/Cooling_System.png' width='50'> |
| 91 | Turbo Motor | <div style='background-color: #2C3034' id='colourCode'>#2C3034</div>  | <div style='background-color: #CFA011' id='colourCode'>#CFA011</div>  | <img src='../img/satisfactory/Turbo_Motor.png' width='50'> |
| 92 | Thermal Propulsion Rocket | <div style='background-color: #30313A' id='colourCode'>#30313A</div>  | <div style='background-color: #A84636' id='colourCode'>#A84636</div>  | <img src='../img/satisfactory/Thermal_Propulsion_Rocket.png' width='50'> |
| 93 | Quantum Computer | <div style='background-color: #1F1F1F' id='colourCode'>#1F1F1F</div>  | <div style='background-color: #E6B16D' id='colourCode'>#E6B16D</div>  | <img src='../img/satisfactory/Quantum_Computer.png' width='50'> |
| 94 | Pressure Conversion Cube | <div style='background-color: #D49058' id='colourCode'>#D49058</div>  | <div style='background-color: #1F2133' id='colourCode'>#1F2133</div>  | <img src='../img/satisfactory/Pressure_Conversion_Cube.png' width='50'> |
| 95 | Superposition Oscillator | <div style='background-color: #ADAEA0' id='colourCode'>#ADAEA0</div>  | <div style='background-color: #EDC888' id='colourCode'>#EDC888</div>  | <img src='../img/satisfactory/Superposition_Oscillator.png' width='50'> |
| 96 | Aluminum Scrap | <div style='background-color: #ABAAAE' id='colourCode'>#ABAAAE</div>  | <div style='background-color: #C6841C' id='colourCode'>#C6841C</div>  | <img src='../img/satisfactory/Aluminum_Scrap.png' width='50'> |
| 97 | Aluminum Ingot | <div style='background-color: #B9BCC3' id='colourCode'>#B9BCC3</div>  | <div style='background-color: #666A6C' id='colourCode'>#666A6C</div>  | <img src='../img/satisfactory/Aluminum_Ingot.png' width='50'> |
| 98 | Alclad Aluminum Sheet | <div style='background-color: #AFB2BC' id='colourCode'>#AFB2BC</div>  | <div style='background-color: #818073' id='colourCode'>#818073</div>  | <img src='../img/satisfactory/Alclad_Aluminum_Sheet.png' width='50'> |
| 99 | Aluminum Casing | <div style='background-color: #AAA9AE' id='colourCode'>#AAA9AE</div>  | <div style='background-color: #1F2228' id='colourCode'>#1F2228</div>  | <img src='../img/satisfactory/Aluminum_Casing.png' width='50'> |
| 100 | Encased Uranium Cell | <div style='background-color: #D8D11B' id='colourCode'>#D8D11B</div>  | <div style='background-color: #8BEAC3' id='colourCode'>#8BEAC3</div>  | <img src='../img/satisfactory/Encased_Uranium_Cell.png' width='50'> |
| 101 | Non-fissile Uranium | <div style='background-color: #E7E334' id='colourCode'>#E7E334</div>  | <div style='background-color: #8BEAC3' id='colourCode'>#8BEAC3</div>  | <img src='../img/satisfactory/Non-fissile_Uranium.png' width='50'> |
| 102 | Uranium Fuel Rod | <div style='background-color: #1E1E15' id='colourCode'>#1E1E15</div>  | <div style='background-color: #64C987' id='colourCode'>#64C987</div>  | <img src='../img/satisfactory/Uranium_Fuel_Rod.png' width='50'> |
| 103 | Uranium Waste | <div style='background-color: #D9D423' id='colourCode'>#D9D423</div>  | <div style='background-color: #1CB83B' id='colourCode'>#1CB83B</div>  | <img src='../img/satisfactory/Uranium_Waste.png' width='50'> |
| 104 | Plutonium Pellet | <div style='background-color: #758B8F' id='colourCode'>#758B8F</div>  | <div style='background-color: #72BACD' id='colourCode'>#72BACD</div>  | <img src='../img/satisfactory/Plutonium_Pellet.png' width='50'> |
| 105 | Encased Plutonium Cell | <div style='background-color: #2B3230' id='colourCode'>#2B3230</div>  | <div style='background-color: #A8E5E9' id='colourCode'>#A8E5E9</div>  | <img src='../img/satisfactory/Encased_Plutonium_Cell.png' width='50'> |
| 106 | Plutonium Fuel Rod | <div style='background-color: #CEBE40' id='colourCode'>#CEBE40</div>  | <div style='background-color: #A8E5E9' id='colourCode'>#A8E5E9</div>  | <img src='../img/satisfactory/Plutonium_Fuel_Rod.png' width='50'> |
| 107 | Plutonium Waste | <div style='background-color: #B6AC23' id='colourCode'>#B6AC23</div>  | <div style='background-color: #71CAE3' id='colourCode'>#71CAE3</div>  | <img src='../img/satisfactory/Plutonium_Waste.png' width='50'> |
| 108 | Electromagnetic Control Rod | <div style='background-color: #383941' id='colourCode'>#383941</div>  | <div style='background-color: #EDE954' id='colourCode'>#EDE954</div>  | <img src='../img/satisfactory/Electromagnetic_Control_Rod.png' width='50'> |
| 109 | Beryl Nut | <div style='background-color: #BBA775' id='colourCode'>#BBA775</div>  | <div style='background-color: #B8CACE' id='colourCode'>#B8CACE</div>  | <img src='../img/satisfactory/Beryl_Nut.png' width='50'> |
| 110 | Paleberry | <div style='background-color: #F27954' id='colourCode'>#F27954</div>  | <div style='background-color: #B14244' id='colourCode'>#B14244</div>  | <img src='../img/satisfactory/Paleberry.png' width='50'> |
| 111 | Bacon Agaric | <div style='background-color: #F2E2E2' id='colourCode'>#F2E2E2</div>  | <div style='background-color: #D7577C' id='colourCode'>#D7577C</div>  | <img src='../img/satisfactory/Bacon_Agaric.png' width='50'> |
| 112 | Medicinal Inhaler | <div style='background-color: #EDEDEC' id='colourCode'>#EDEDEC</div>  | <div style='background-color: #47AEE1' id='colourCode'>#47AEE1</div>  | <img src='../img/satisfactory/Medicinal_Inhaler.png' width='50'> |
| 113 | Hog Remains | <div style='background-color: #A38DA8' id='colourCode'>#A38DA8</div>  | <div style='background-color: #554041' id='colourCode'>#554041</div>  | <img src='../img/satisfactory/Hog_Remains.png' width='50'> |
| 114 | Plasma Spitter Remains | <div style='background-color: #6C5982' id='colourCode'>#6C5982</div>  | <div style='background-color: #972E37' id='colourCode'>#972E37</div>  | <img src='../img/satisfactory/Plasma_Spitter_Remains.png' width='50'> |
| 115 | Hatcher Remains | <div style='background-color: #454792' id='colourCode'>#454792</div>  | <div style='background-color: #E394AE' id='colourCode'>#E394AE</div>  | <img src='../img/satisfactory/Hatcher_Remains.png' width='50'> |
| 116 | Stinger Remains | <div style='background-color: #1E1C20' id='colourCode'>#1E1C20</div>  | <div style='background-color: #A07277' id='colourCode'>#A07277</div>  | <img src='../img/satisfactory/Stinger_Remains.png' width='50'> |
| 117 | Alien Protein | <div style='background-color: #654368' id='colourCode'>#654368</div>  | <div style='background-color: #C9CBCE' id='colourCode'>#C9CBCE</div>  | <img src='../img/satisfactory/Alien_Protein.png' width='50'> |
| 118 | Alien DNA Capsule | <div style='background-color: #96949D' id='colourCode'>#96949D</div>  | <div style='background-color: #49343C' id='colourCode'>#49343C</div>  | <img src='../img/satisfactory/Alien_DNA_Capsule.png' width='50'> |
| 119 | Hard Drive | <div style='background-color: #994F29' id='colourCode'>#994F29</div>  | <div style='background-color: #313130' id='colourCode'>#313130</div>  | <img src='../img/satisfactory/Hard_Drive.png' width='50'> |
| 120 | FICSIT Coupon | <div style='background-color: #E49246' id='colourCode'>#E49246</div>  | <div style='background-color: #E9A86A' id='colourCode'>#E9A86A</div>  | <img src='../img/satisfactory/FICSIT_Coupon.png' width='50'> |
| 121 | Blade Runners | <div style='background-color: #092E57' id='colourCode'>#092E57</div>  | <div style='background-color: #050505' id='colourCode'>#050505</div>  | <img src='../img/satisfactory/Blade_Runners.png' width='50'> |
| 122 | Hazmat Suit | <div style='background-color: #D2B763' id='colourCode'>#D2B763</div>  | <div style='background-color: #424462' id='colourCode'>#424462</div>  | <img src='../img/satisfactory/Hazmat_Suit.png' width='50'> |
| 123 | Hover Pack | <div style='background-color: #FFB85C' id='colourCode'>#FFB85C</div>  | <div style='background-color: #373938' id='colourCode'>#373938</div>  | <img src='../img/satisfactory/Hover_Pack.png' width='50'> |
| 124 | Jetpack | <div style='background-color: #FFB85C' id='colourCode'>#FFB85C</div>  | <div style='background-color: #373938' id='colourCode'>#373938</div>  | <img src='../img/satisfactory/Jetpack.png' width='50'> |
| 125 | Gas Mask | <div style='background-color: #EDA048' id='colourCode'>#EDA048</div>  | <div style='background-color: #242122' id='colourCode'>#242122</div>  | <img src='../img/satisfactory/Gas_Mask.png' width='50'> |
| 126 | Gas Filter | <div style='background-color: #8E859C' id='colourCode'>#8E859C</div>  | <div style='background-color: #131211' id='colourCode'>#131211</div>  | <img src='../img/satisfactory/Gas_Filter.png' width='50'> |
| 127 | Iodine Infused Filter | <div style='background-color: #B6881F' id='colourCode'>#B6881F</div>  | <div style='background-color: #121311' id='colourCode'>#121311</div>  | <img src='../img/satisfactory/Iodine_Infused_Filter.png' width='50'> |
| 128 | Build Gun | <div style='background-color: #D99427' id='colourCode'>#D99427</div>  | <div style='background-color: #24282B' id='colourCode'>#24282B</div>  | <img src='../img/satisfactory/Build_Gun.png' width='50'> |
| 129 | Color Gun | <div style='background-color: #E38F3F' id='colourCode'>#E38F3F</div>  | <div style='background-color: #1C1B1D' id='colourCode'>#1C1B1D</div>  | <img src='../img/satisfactory/Color_Gun.png' width='50'> |
| 130 | Object Scanner | <div style='background-color: #CD7F24' id='colourCode'>#CD7F24</div>  | <div style='background-color: #151517' id='colourCode'>#151517</div>  | <img src='../img/satisfactory/Object_Scanner.png' width='50'> |
| 131 | Xeno-Zapper | <div style='background-color: #E38532' id='colourCode'>#E38532</div>  | <div style='background-color: #383F52' id='colourCode'>#383F52</div>  | <img src='../img/satisfactory/Xeno-Zapper.png' width='50'> |
| 132 | Xeno-Basher | <div style='background-color: #E2872F' id='colourCode'>#E2872F</div>  | <div style='background-color: #51C1C8' id='colourCode'>#51C1C8</div>  | <img src='../img/satisfactory/Xeno-Basher.png' width='50'> |
| 133 | Rebar Gun | <div style='background-color: #2C2D30' id='colourCode'>#2C2D30</div>  | <div style='background-color: #0D0D0E' id='colourCode'>#0D0D0E</div>  | <img src='../img/satisfactory/Rebar_Gun.png' width='50'> |
| 134 | Iron Rebar | <div style='background-color: #D27E13' id='colourCode'>#D27E13</div>  | <div style='background-color: #222025' id='colourCode'>#222025</div>  | <img src='../img/satisfactory/Iron_Rebar.png' width='50'> |
| 135 | Shatter Rebar | <div style='background-color: #772E6E' id='colourCode'>#772E6E</div>  | <div style='background-color: #222025' id='colourCode'>#222025</div>  | <img src='../img/satisfactory/Shatter_Rebar.png' width='50'> |
| 136 | Stun Rebar | <div style='background-color: #017BB2' id='colourCode'>#017BB2</div>  | <div style='background-color: #222025' id='colourCode'>#222025</div>  | <img src='../img/satisfactory/Stun_Rebar.png' width='50'> |
| 137 | Explosive Rebar | <div style='background-color: #A50810' id='colourCode'>#A50810</div>  | <div style='background-color: #222025' id='colourCode'>#222025</div>  | <img src='../img/satisfactory/Explosive_Rebar.png' width='50'> |
| 138 | Nobelisk Detonator | <div style='background-color: #DCC109' id='colourCode'>#DCC109</div>  | <div style='background-color: #AA0003' id='colourCode'>#AA0003</div>  | <img src='../img/satisfactory/Nobelisk_Detonator.png' width='50'> |
| 139 | Nobelisk | <div style='background-color: #A00509' id='colourCode'>#A00509</div>  | <div style='background-color: #F7F420' id='colourCode'>#F7F420</div>  | <img src='../img/satisfactory/Nobelisk.png' width='50'> |
| 140 | Gas Nobelisk | <div style='background-color: #5B7A29' id='colourCode'>#5B7A29</div>  | <div style='background-color: #FCDA40' id='colourCode'>#FCDA40</div>  | <img src='../img/satisfactory/Gas_Nobelisk.png' width='50'> |
| 141 | Pulse Nobelisk | <div style='background-color: #8EABB6' id='colourCode'>#8EABB6</div>  | <div style='background-color: #FCDA40' id='colourCode'>#FCDA40</div>  | <img src='../img/satisfactory/Pulse_Nobelisk.png' width='50'> |
| 142 | Cluster Nobelisk | <div style='background-color: #F39F26' id='colourCode'>#F39F26</div>  | <div style='background-color: #FB6F58' id='colourCode'>#FB6F58</div>  | <img src='../img/satisfactory/Cluster_Nobelisk.png' width='50'> |
| 143 | Nuke Nobelisk | <div style='background-color: #424D49' id='colourCode'>#424D49</div>  | <div style='background-color: #72C48A' id='colourCode'>#72C48A</div>  | <img src='../img/satisfactory/Nuke_Nobelisk.png' width='50'> |
| 144 | Rifle | <div style='background-color: #FACA42' id='colourCode'>#FACA42</div>  | <div style='background-color: #272728' id='colourCode'>#272728</div>  | <img src='../img/satisfactory/Rifle.png' width='50'> |
| 145 | Rifle Ammo | <div style='background-color: #CC6927' id='colourCode'>#CC6927</div>  | <div style='background-color: #050504' id='colourCode'>#050504</div>  | <img src='../img/satisfactory/Rifle_Ammo.png' width='50'> |
| 146 | Homing Rifle Ammo | <div style='background-color: #272A2D' id='colourCode'>#272A2D</div>  | <div style='background-color: #423A1B' id='colourCode'>#423A1B</div>  | <img src='../img/satisfactory/Homing_Rifle_Ammo.png' width='50'> |
| 147 | Turbo Rifle Ammo | <div style='background-color: #C12B31' id='colourCode'>#C12B31</div>  | <div style='background-color: #090807' id='colourCode'>#090807</div>  | <img src='../img/satisfactory/Turbo_Rifle_Ammo.png' width='50'> |
| 148 | Boom Box | <div style='background-color: #D6953C' id='colourCode'>#D6953C</div>  | <div style='background-color: #1F2122' id='colourCode'>#1F2122</div>  | <img src='../img/satisfactory/Boom_Box.png' width='50'> |
| 149 | Chainsaw | <div style='background-color: #DD832C' id='colourCode'>#DD832C</div>  | <div style='background-color: #1D202B' id='colourCode'>#1D202B</div>  | <img src='../img/satisfactory/Chainsaw.png' width='50'> |
| 150 | Zipline | <div style='background-color: #D48020' id='colourCode'>#D48020</div>  | <div style='background-color: #C8B684' id='colourCode'>#C8B684</div>  | <img src='../img/satisfactory/Zipline.png' width='50'> |
| 151 | FICSMAS Gift | <div style='background-color: #F0C07A' id='colourCode'>#F0C07A</div>  | <div style='background-color: #D92020' id='colourCode'>#D92020</div>  | <img src='../img/satisfactory/FICSMAS_Gift.png' width='50'> |
| 152 | Red FICSMAS Ornament | <div style='background-color: #DB0108' id='colourCode'>#DB0108</div>  | <div style='background-color: #7F0000' id='colourCode'>#7F0000</div>  | <img src='../img/satisfactory/Red_FICSMAS_Ornament.png' width='50'> |
| 153 | Copper FICSMAS Ornament | <div style='background-color: #FEA318' id='colourCode'>#FEA318</div>  | <div style='background-color: #844E01' id='colourCode'>#844E01</div>  | <img src='../img/satisfactory/Copper_FICSMAS_Ornament.png' width='50'> |
| 154 | Iron FICSMAS Ornament | <div style='background-color: #FFFFFF' id='colourCode'>#FFFFFF</div>  | <div style='background-color: #9F9B98' id='colourCode'>#9F9B98</div>  | <img src='../img/satisfactory/Iron_FICSMAS_Ornament.png' width='50'> |
| 155 | Blue FICSMAS Ornament | <div style='background-color: #1DC0E7' id='colourCode'>#1DC0E7</div>  | <div style='background-color: #004F78' id='colourCode'>#004F78</div>  | <img src='../img/satisfactory/Blue_FICSMAS_Ornament.png' width='50'> |
| 156 | Candy Cane | <div style='background-color: #AD0000' id='colourCode'>#AD0000</div>  | <div style='background-color: #ECDCCC' id='colourCode'>#ECDCCC</div>  | <img src='../img/satisfactory/Candy_Cane.png' width='50'> |
| 157 | FICSMAS Wonder Star | <div style='background-color: #F9DD2C' id='colourCode'>#F9DD2C</div>  | <div style='background-color: #D79103' id='colourCode'>#D79103</div>  | <img src='../img/satisfactory/FICSMAS_Wonder_Star.png' width='50'> |
| 158 | FICSMAS Bow | <div style='background-color: #F13F2C' id='colourCode'>#F13F2C</div>  | <div style='background-color: #C6080C' id='colourCode'>#C6080C</div>  | <img src='../img/satisfactory/FICSMAS_Bow.png' width='50'> |
| 159 | FICSMAS Decoration | <div style='background-color: #404124' id='colourCode'>#404124</div>  | <div style='background-color: #C6080C' id='colourCode'>#C6080C</div>  | <img src='../img/satisfactory/FICSMAS_Decoration.png' width='50'> |
| 160 | Actual Snow | <div style='background-color: #FDE9D0' id='colourCode'>#FDE9D0</div>  | <div style='background-color: #CBC4E0' id='colourCode'>#CBC4E0</div>  | <img src='../img/satisfactory/Actual_Snow.png' width='50'> |
| 161 | Snowball | <div style='background-color: #D9EBF7' id='colourCode'>#D9EBF7</div>  | <div style='background-color: #DCD6E1' id='colourCode'>#DCD6E1</div>  | <img src='../img/satisfactory/Snowball.png' width='50'> |
| 162 | Snowball Pile | <div style='background-color: #F9EAF9' id='colourCode'>#F9EAF9</div>  | <div style='background-color: #E3CBBB' id='colourCode'>#E3CBBB</div>  | <img src='../img/satisfactory/Snowball_Pile.png' width='50'> |
| 163 | Snowman | <div style='background-color: #FBEBE2' id='colourCode'>#FBEBE2</div>  | <div style='background-color: #CF0C13' id='colourCode'>#CF0C13</div>  | <img src='../img/satisfactory/Snowman.png' width='50'> |
| 164 | Giant FICSMAS Tree | <div style='background-color: #322B12' id='colourCode'>#322B12</div>  | <div style='background-color: #121607' id='colourCode'>#121607</div>  | <img src='../img/satisfactory/Giant_FICSMAS_Tree.png' width='50'> |
| 165 | FICSMAS Gift Tree | <div style='background-color: #5D4B28' id='colourCode'>#5D4B28</div>  | <div style='background-color: #910002' id='colourCode'>#910002</div>  | <img src='../img/satisfactory/FICSMAS_Gift_Tree.png' width='50'> |
| 166 | FICSMAS Wreath | <div style='background-color: #704B15' id='colourCode'>#704B15</div>  | <div style='background-color: #910002' id='colourCode'>#910002</div>  | <img src='../img/satisfactory/FICSMAS_Wreath.png' width='50'> |
| 167 | FICSMAS Tree Branch | <div style='background-color: #8487A3' id='colourCode'>#8487A3</div>  | <div style='background-color: #6E5B5B' id='colourCode'>#6E5B5B</div>  | <img src='../img/satisfactory/FICSMAS_Tree_Branch.png' width='50'> |
| 168 | Power Shard | <div style='background-color: #C6712F' id='colourCode'>#C6712F</div>  | <div style='background-color: #86C0E9' id='colourCode'>#86C0E9</div>  | <img src='../img/satisfactory/Power_Shard.png' width='50'> |
| 169 | Blue Power Slug | <div style='background-color: #7BBFDD' id='colourCode'>#7BBFDD</div>  | <div style='background-color: #6B74AD' id='colourCode'>#6B74AD</div>  | <img src='../img/satisfactory/Blue_Power_Slug.png' width='50'> |
| 170 | Yellow Power Slug | <div style='background-color: #E7B55A' id='colourCode'>#E7B55A</div>  | <div style='background-color: #E79763' id='colourCode'>#E79763</div>  | <img src='../img/satisfactory/Yellow_Power_Slug.png' width='50'> |
| 171 | Purple Power Slug | <div style='background-color: #DF88E5' id='colourCode'>#DF88E5</div>  | <div style='background-color: #B250D0' id='colourCode'>#B250D0</div>  | <img src='../img/satisfactory/Purple_Power_Slug.png' width='50'> |
| 172 | SAM Ingot | <div style='background-color: #124354' id='colourCode'>#124354</div>  | <div style='background-color: #2C0436' id='colourCode'>#2C0436</div>  | <img src='../img/satisfactory/SAM_Ingot.png' width='50'> |
| 173 | Dark Matter | <div style='background-color: #C08E5A' id='colourCode'>#C08E5A</div>  | <div style='background-color: #1A151B' id='colourCode'>#1A151B</div>  | <img src='../img/satisfactory/Dark_Matter.png' width='50'> |
| 174 | Quantum Crystal | <div style='background-color: #C08E5A' id='colourCode'>#C08E5A</div>  | <div style='background-color: #1A151B' id='colourCode'>#1A151B</div>  | <img src='../img/satisfactory/Quantum_Crystal.png' width='50'> |
| 175 | Cup | <div style='background-color: #BB7C51' id='colourCode'>#BB7C51</div>  | <div style='background-color: #3EA2D3' id='colourCode'>#3EA2D3</div>  | <img src='../img/satisfactory/Cup.png' width='50'> |
| 176 | Employee of the Planet Cup | <div style='background-color: #CDA44E' id='colourCode'>#CDA44E</div>  | <div style='background-color: #2A2930' id='colourCode'>#2A2930</div>  | <img src='../img/satisfactory/Employee_of_the_Planet_Cup.png' width='50'> |
| 177 | Mercer Sphere | <div style='background-color: #010101' id='colourCode'>#010101</div>  | <div style='background-color: #407246' id='colourCode'>#407246</div>  | <img src='../img/satisfactory/Mercer_Sphere.png' width='50'> |
| 178 | Somersloop | <div style='background-color: #5E0311' id='colourCode'>#5E0311</div>  | <div style='background-color: #440D39' id='colourCode'>#440D39</div>  | <img src='../img/satisfactory/Somersloop.png' width='50'> |
| 179 | FICSIT | <div style='background-color: #FD9426' id='colourCode'>#FD9426</div>  | <div style='background-color: #4CA8BD' id='colourCode'>#4CA8BD</div>  | <img src='../img/satisfactory/FICSIT.png' width='50'> |
| 180 | Pioneer | <div style='background-color: #CCB69D' id='colourCode'>#CCB69D</div>  | <div style='background-color: #DD9A5A' id='colourCode'>#DD9A5A</div>  | <img src='../img/satisfactory/Pioneer.png' width='50'> |
