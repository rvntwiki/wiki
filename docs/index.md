---
title: Home
Summary: Welcome to The Sanctum
Author: rvnt
Date: 2021-04-11
base_url: https://sanctum.spctr.uk/
---

Welcome
===

A collection of snippets and tidbits I've found useful.

[Free Media Heck Yeah](https://fmhy.pages.dev/)

[Awesome Self Hosted](https://github.com/awesome-selfhosted/awesome-selfhosted)