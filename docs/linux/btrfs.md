---
title: BtrFS
Summary: Using BTRFS
Author: rvnt
Dataset: 2024-11-12
base_url: https://sanctum.spctr.uk/
---

# BrtFS

## Install
`sudo apt-get install btrfs-progs`

## Create the file system:
`sudo mkfs.btrfs /dev/sdX`

Replace /dev/sdX with your actual device name.

## Mount the file system:
`sudo mount /dev/sdX /mountPoint`

## Create a storage pool
`sudo btrfs device add /dev/sdY /mountPoint`

??? info "Windows Alternative"
    Microsoft Windows has similar funtionality named Storage Spaces.