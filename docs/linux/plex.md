---
title: Plex
Summary: Tips for configuring the Apache2 web server
Author: rvnt
Date: 2022-05-22
base_url: https://sanctum.rvnt.uk/
---

## Generate pfx Certificate

    ``` shell
    openssl pkcs12 -export -out /etc/letsencrypt/live/domain.com/pkcs12.pfx -inkey /etc/letsencrypt/live/domain.com/privkey.pem -in /etc/letsencrypt/live/domain.com/cert.pem -certfile /etc/letsencrypt/live/domain.com/fullchain.pem
    ```


## Custom SSL Certificate

    ``` shell
    Custom certificate location: /etc/letsencrypt/live/domain.com/pkcs12.pfx
    Custom certificate encryption key: Password for certificate.
    Custom certificate domain: https://sub.domain.com
    ```