---
title: Free Disk Space
Summary: Tips for freeing up disk space
Author: rvnt
Date: 2021-09-20
base_url: https://sanctum.rvnt.uk/
---

# Free Disk Space

## Locations

| **Location** | **Description** |
| --- | --- |
| `/tmp/` | Directory containing temporary files |
| `/var/tmp/` | Directory containing temporary files |

## Commands

| **Command** | **Description** |
| --- | --- |
| `sudo apt-get clean` | Delete unused packages |
| `sudo apt-get autoclean` | Delete outdated packages |
| `sudo apt-get autoremove` | Delete unnecessary packages |
| `sudo apt install localepurge` | Delete unnecessary translations |
| `sudo journalctl --vacuum-size 10M` | Delete old journal files until they take less than 10MB |
| `df -h` | Show file system information, in human readable format |
| `du -sh` | Show file space usage, for each file, in human readable format |
| ` find / -type f -size +1024k` | Search for files in '/' that are at least 1MB in size |

## Tools

| **Command** | **Description** |
| --- | --- |
| `ncdu` | Graphical disk usage app |