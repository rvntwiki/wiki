---
title: Fail2Ban
Summary: Using Fail2Ban to secure machine
Author: rvnt
Date: 2021-04-11
base_url: https://sanctum.rvnt.uk/
---

Fail2Ban
===

Unban
---

1. Locate rule:

    ``` shell 
    sudo iptables -L --line-numbers
    ```

    Output

    ``` shell hl_lines="16"
    Chain INPUT (policy ACCEPT)
    num  target                    prot  opt  source                    destination
    1    f2b-apache-fakegooglebot  tcp   --   anywhere                  anywhere  multiport dports http,https
    2    ACCEPT                    all   --   anywhere                  anywhere
    3    f2b-apache-auth           tcp   --   anywhere                  anywhere  multiport dports http,https
    4    f2b-sshd                  tcp   --   anywhere                  anywhere  multiport dports ssh

    Chain FORWARD (policy ACCEPT)
    num  target                    prot  opt  source                    destination

    Chain OUTPUT (policy ACCEPT)
    num  target                    prot  opt  source                    destination

    Chain f2b-apache-auth (1 references)
    num  target                    prot  opt  source                    destination
    1    RETURN                    all   --   anywhere                  anywhere

    Chain f2b-apache-fakegooglebot (1 references)
    num  target                    prot  opt  source                    destination
    1    REJECT                    all   --   162.158.63.149            anywhere  reject-with icmp-port-unreachable
    2    REJECT                    all   --   162.158.75.163            anywhere  reject-with icmp-port-unreachable
    3    RETURN                    all   --   anywhere                  anywhere

    Chain f2b-sshd (1 references)
    num  target                    prot  opt  source                    destination
    1    REJECT                    all   --   123.256.289.012           anywhere  reject-with icmp-port-unreachable
    2    REJECT                    all   --   ns853234.ip-21-252-74.eu  anywhere  reject-with icmp-port-unreachable
    3    REJECT                    all   --   201.122.221.43            anywhere  reject-with icmp-port-unreachable
    4    REJECT                    all   --   thomsai.com               anywhere  reject-with icmp-port-unreachable
    5    REJECT                    all   --   193.169.252.19            anywhere  reject-with icmp-port-unreachable
    6    REJECT                    all   --   211.136.149.117           anywhere  reject-with icmp-port-unreachable
    7    RETURN                    all   --   anywhere                  anywhere
    ```

1. Delete rule:

    ``` shell 
	sudo iptables -D f2b-apache-auth 1
    ```