---
title: EncFS & SSHFS
Summary: Setting up a remote encrypted file system
Author: rvnt
Date: 2021-04-11
base_url: https://sanctum.rvnt.uk/
---

EncFS & SSHFS
===

??? warning "User and group ID's - :material-server-network: [All servers]"

    User ID *(UID)* and group ID *(GID)* much match on all servers.

    1. Print user and group ID's

        ``` shell
        id
        ```

        Output

        ``` shell
        uid=1000(**userName**) gid=1000(**groupName**) groups=1000(**groupName**)
        ```

Update Hosts File
---

:material-server-network: [All servers]

``` shell
echo "**serverIP**  **serverName** **serverName**" >> /etc/hosts
```

Prepare Directories
---

:material-server-network: [All servers]

1. Create directories:

    ``` shell
    mkdir –p /homes/storage/**serverName**/**mediaType**/
    ```

1. Take ownership:

    ``` shell
    sudo chown –R **userName**:**groupName** /home/storage/
    ```

:material-server: [Main server]

1. Create directories:

    ``` shell
    mkdir –p /home/media/**serverName**/**mediaType**/
    ```

1. Take ownership:

    ``` bash
    chown –R **userName**:**groupName** /home/media/
    ```

Mount Directories
---

:material-server: [Main server]

1. Mount remote media directories 

    ``` shell
    sshfs –o uid=**userID**,gid=**groupID** –o allow_otherreconnect jarvis@**targetServerName**:/home/storage/**targetServerName**/**mediaType**/ /home/storage/**targetServerName**/**mediaType**/
    ```

1. Verify mounted directories

    ``` shell
    df –h /home/storage/**targetServerName**/*
    ```

Create EncFS Paritions
---

:material-server: [Main server]

!!! warning "Run user"
    
    Partitions must be mounted via common user. eg. User **userName**.
    
1. On client machine run:

    ``` shell
    encfs --public /home/storage/**targetServerName**/**mediaType**/ /home/media/**mediaType**/**targetServerName**
    ```

1. When prompted:

    1. Type ++x++ for expert
    1. Type ++1++ for AES
    1. Type ++"256"++
    1. Type ++"1024"++
    1. Type ++2++ for Block32
    1. Type ++y++ for filename initialization vector chaining
    1. Type ++y++ for per-file initialization vectors
    1. Type ++n++ for IV header chaining
    1. Type ++n++ for block authentication code headers
    1. Type ++0++ for adding random bytes to each block header
    1. Type ++y++ for file-hole pass-through
    1. Enter password for EncFS. eg. **targetServer-NamemediaType**

Manual Mount
---

??? warning "User permissions"

    By default the mount points will only be accessible by the user running the commmand.

    To get around this run the commands as root or use sudo.

    The EncFS flag '--public' allows other users to access the mount point but only when run as root or with sudo.

1. Connect to remote storage:

    ``` shell
    sudo sshfs -o uid=**userID**,gid=**groupID** -o allow_other,reconnect **userName**@**serverName**:/home/storage/**serverName**/**mediaType** /home/storage/**serverName**/**mediaType**
    ```

1. Decrypt remote storage:

    ``` shell
    encfs --public /home/storage/**serverName**/**mediaType** /home/media/**mediaType**/**serverName**
    ```

Unmount Directories
---

1. Unmount encfs:

    ``` shell
    encfs -u /home/media/**mediaType**/**serverName**
    ```

1. Unmount sshfs:

    ``` shell
    fusermount -u /home/storage/**mediaType**
    ```

If that fails

1. Get PID for mount

    ``` shell
    ps aux | grep "encfs --public /home/storage/**targetServerName**/**mediaType**"
    ```

    Output

    ``` shell
    **userName**   123456  0.0  0.0 654321   811 ?        Ssl  Jan01   0:00 encfs --public /home/storage/**targetServerName**/**mediaType**/ /home/media/**mediaType**/**targetServerName**/
    ```

1. Manually kill PID

    ``` shell
    kill -9 "123456"
    ```