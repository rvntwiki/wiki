# Docker

??? warning "Warning: Sudo"
    Docker on Linux requires root access.

## Useful

`sudo docker container list`

`sudo docker container stats`

## Docker Compose
### [PlexTraktSync](https://github.com/linuxserver-labs/docker-plextraktsync)

/opt/Docker/PlexSyncTrakt/docker-compose.yml

    version: "2.1"
    services:
    plextraktsync:
        image: lscr.io/linuxserver-labs/plextraktsync:latest
        container_name: plextraktsync
    environment:
        - PUID=1001
        - PGID=1001
        - TZ=Europe/London
    volumes:
        - /opt/Docker/PlexTraktSync/config:/config
    restart: unless-stopped
    deploy:
      resources:
        limits:
          memory: 2G
        reservations:
          memory: 1G


`sudo docker compose up -d`
