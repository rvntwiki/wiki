---
title: Let's Encrypt
Summary: Create SSL certificates with Let's Encrypt
Author: rvnt
Date: 2024-11-12
base_url: https://sanctum.spctr.uk/
---

Let's Encrypt
===


Todo
---

_Create guide for setting up lets encrypt..._

Convert to PFX key
---

1. Generate Key
    
    ``` shell
    openssl pkcs12 -export -out /etc/letsencrypt/live/domain.com/domain.pfx -inkey /etc/letsencrypt/live/domain.com/privkey.pem -in /etc/letsencrypt/live/domain.com/cert.pem -certfile /etc/letsencrypt/live/domain.com/fullchain.pem -passout pass:P@55w0rd
    ```