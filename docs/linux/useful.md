---
title: Useful Commands
Summary: Useful shell commands
Author: rvnt
Date: 2021-04-11
base_url: https://sanctum.rvnt.uk/
---

# Useful Commands
## SSH Tunnel
```shell
 ssh -L 8888:127.0.0.1:32400 ip.address.of.server
```

## Find Files Recursively

``` shell
find . -type f -name '*.jpg'
```

To delete all found files

``` shell
find . -type f -name '*.jpg' -delete
```

Delete all jpeg/jpg/png files located in the current directory, 3 dirs deep
``` shell
find . -maxdepth 3 \( -iname '*.jp*g' -o -iname '*.png' \) -type f -delete -print
```


List All partitions
---

``` shell
lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL
```

Output

``` shell
NAME FSTYPE  SIZE MOUNTPOINT LABEL
sda          256G
sdb          256G /
```

Find PID for daemon
---

``` shell
ps aux | grep 1234
```

Output

``` shell
**userName**    1234  0.1  0.3 1676828 81892 pts/4   Sl+  16:11   0:07 /usr/bin/python3 /home/**userName**/.local/bin/mkdocs serve
```

Find PID for port
---

``` shell
netstat -plnt | grep ':80'
```

Output

``` shell
tcp6   0    0 :::80    :::*    LISTEN    643522/apache2
```

Rsync with progress bar
---

``` shell
rsync -ahP file1 [,fileN] destination
```

Output

``` shell
sending incremental file list
file.ext
	10.00M 100%  250.00MB/s    0:00:00 (xfr#1, to-chk=0/1)
```

## Hide Disk
Identify the Drive:

List all connected drives using lsblk to find the drive you want to hide:

```bash
lsblk
```


Get Device Path:

Note the device path of the drive you want to hide (e.g., /dev/sdb1).


Create a Custom Udev Rule:

Use a custom udev rule to hide the drive from the desktop environment:

```bash
sudo nano /etc/udev/rules.d/99-hide-disks.rules
```


Add the following line, replacing sdb1 with your actual device name:

```
KERNEL=="sdb1", ENV{UDISKS_IGNORE}="1"
Reload Udev Rules:
```

Save the file and reload the udev rules:

```bash
sudo udevadm control --reload
sudo udevadm 
```