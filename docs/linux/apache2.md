---
title: Apache2
Summary: Tips for configuring the Apache2 web server
Author: rvnt
Date: 2021-10-11
base_url: https://sanctum.spctr.uk/
---

## Creating a subdomain (HTTPS)

1. Navigate to apache2 installation directory: `cd /etc/apache2`
1. Navigate to *sites-available* directory: `cd sites-available`
1. Copy the HTTPS template: `cp 000-default-le-ssl.conf subdomain.domain.com.conf`
1. Edit *subdomain.domain.com.conf*: `nano subdomain.domain.com.conf`