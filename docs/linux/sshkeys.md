---
title: SSH Keys
Summary: Setting up SSH keys
Author: rvnt
Date: 2021-04-12
base_url: https://sanctum.spctr.uk/
---

Setting up SSH Keys
===

Generating Keys
---

1. On client machine run:

    ``` shell
    ssh-keygen -t rsa -b 4096 -C "**clientName-serverName-YYYYMMDD**"
    ```

1. When prompted:

    1. `Enter file in which to save the key: /home/userName/.ssh/**clientName-serverName-YYYYMMDD**`
    1. `Enter passphrase (empty for no passphrase): **optional**`
    1. `Enter same passphrase again: **optional**`

Copy Public Key [Login/Key]
---

If username and password access is enabled or existing key is installed:

1. On client machine run:

    ``` shell
    ssh-copy-id -i **clientName-serverName-YYYYMMDD**.pub **userName@**serverName**
    ```

Copy Public Key [Manual]
---

If username and password access is disabled and no existing key is installed:

1. On client machine run:

    ``` shell
    cat /home/**userName**/.ssh/**clientName-serverName-YYYYMMDD**.pub
    ```

1. Copy entire output of command:

    ``` shell    
    ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAgEAhH0kJGPdwAQv3GH3HR1yJS3s5+A4LoasJVwU3q/n3DlGMS+CD8MQFV2FbWrwjXTZgzv7u8Ncd66nmPdjqNxvt36OUKF8V5cGJoypsgllBMCygIAv3nHPkYAsuh/dsyFqWCXpiMu/9UEQjm3ZVtimwye7D3BMDabJgfdoX1iUoPVqMhhew2yEELln/YJRf7GYpmwOmIqynOZvegGSkKsQt7VDjQ4w6SXM7rKVv7sefQXWuh9tO1y2987YX58sRK/oe7SS8rjkNeTJ/zynpc0zXnuf5oA4I/NOuVoOCOt6wSlcPQTDH5mtec3ruVmHHHNndHgJiR2muuqnVBBod3Y1HhurZ3rG5rlgolnAhxU+LmDQwinHKhS9ItgbGR3Blk6QGSApIxg8m/011sR/x9nZL00J+1yuxLuq4gI8xfn8c2LOpr936ZBMIib1/IJKh33PZ06Z5iTaZWDQ8gTWjtHTm06jlxTmysP0JhSD0k3VvzxwX/owhrlkj+OJl0adwC3VMA900fK5jaVvKyJW4FrxXIsBitCCmGKajnFr/Lq8Oeh+r/gNcx58LzAADI9LOzGjUvmHkwn/uacoVAUQl44C1ME9To/6xoDBMpZQ+kt7xLUz0VhvS1vJtiELWDaatWvnqPKq/kqQ9IcnVnZtYZ+RIp/YGfERwaOpDt6oN5iEv1s= **clientName-serverName-YYYYMMDD**
    ```

1. Connect to server and run:

    ``` shell
    nano /home/**userName**/.ssh/authorized_keys
    ```

1. Paste public key on next blank line:

    ++rbutton++

1. Save and exit:

    1. ++ctrl+o++
    1. ++enter++
    1. ++ctrl+x++

1. Reload SSH daemon:

    ``` shell
    sudo systemctl restart ssh
    ```

Connecting with keys
---

1. Define custom host:

    ``` shell
    nano ~/.ssh/config
    ```

1. Paste and modify:

    <!-- sshd -->
    ``` apache
    Host **hostName**
        User **userName**
        Hostname **serverAddress**
        IdentityFile ~/.ssh/**clientName-serverName-YYYYMMDD**
        TCPKeepAlive yes
        IdentitiesOnly yes
    ```

1. Save and exit:

    1. ++ctrl+o++
    1. ++enter++
    1. ++ctrl+x++

1. Verify:

    ``` shell
    ssh **userName**@**hostName**
    ```

Disable Password Authentication
---

!!! warning
    With `PasswordAuthentication no` set, if ssh keys are lost or corrupted no remote access will be possible.

1. Open connection to server and run:

    ``` shell
    sudo nano /etc/ssh/sshd_config
    ```

1. Find and update the variable:

    ``` shell
    PasswordAuthentication no
    ```

1. Save and exit:

    1. ++ctrl+o++
    1. ++enter++
    1. ++ctrl+x++

1. Reload SSHd daemon:

    ``` shell
    sudo systemctl restart sshd
    ```

