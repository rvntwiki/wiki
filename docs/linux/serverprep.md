---
title: Server Preparation
Summary: Initial steps to be run on a clean install
Author: rvnt
Date: 2021-04-11
base_url: https://sanctum.rvnt.uk/
---

Users & Groups
===

Create common user and group
---

:material-server-network: [All Servers]

1. Create common group:

    ``` shell
    addgroup **groupName**
    ```

1. Create common user:

    ``` shell
    addusers **userName** -ingroup **groupName**
    ```

Create required directories
---

:material-server-network: [All Servers]

1. Create required directories:

    ``` shell
    mkdir -p /home/storage/**mediaType**
    ```

1. Take ownership:

    ``` shell
    sudo chown -R **userName**:**groupName** /home/storage/**mediaType**
    ```

Install required tools
---

:material-server-network: [All Servers]

1. Install required tools:

    ``` shell
    sudo apt-get install encfs sshfs
    ```

Update fuse.conf
---

:material-server-network: [All Servers]

1. Update fuse.conf:

    ``` shell
    echo "user_allow_other" >> /etc/fuse.conf
    ```

Update SSH config files
---

:material-server-network: [All Servers]

!!! warning
    With `PasswordAuthentication no` set, if ssh keys are lost or corrupted no remote access will be possible.

1. Open sshd_config and add private keys for each server:
        
    ``` shell
    sudo nano /etc/ssh/sshd_config
    ```

    `Hostkey /home/**userName**/.ssh/**clientName-serverName-YYYYMMDD**`

1. Ensure the following variables are set correctly:

    <!-- sshd -->
    ``` apache
    PubkeyAuthentication yes
    ChallengeResponseAuthentication no
    PasswordAuthentication no
    ```

1. Save and exit:

    1. ++ctrl+o++
    1. ++enter++
    1. ++ctrl+x++

1. Reload SSHd daemon:

    ``` shell
    sudo systemctl restart sshd
    ```

1. Create or edit ~/.ssh/config:

    ``` shell
    nano ~/.ssh/config
    ```

1. Add host for each server:

    <!-- sshd -->
    ``` apache
    Host **hostName**
        User **userName**
        Hostname **serverAddress**
        IdentityFile ~/.ssh/**clientName-serverName-YYYYMMDD**
        TCPKeepAlive yes
        IdentitiesOnly yes
    ```

1. Save and exit:

    1. ++ctrl+o++
    1. ++enter++
    1. ++ctrl+x++

1. Reload SSH daemon:

    ``` shell
    sudo systemctl restart ssh
    ```

1. Verify:

    ``` shell
    ssh **userName**@**hostName**
    ```