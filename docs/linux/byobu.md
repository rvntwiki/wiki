---
title: Byobu
Summary: Text-based window manager and terminal multiplexer
Author: rvnt
Date: 2021-08-17
base_url: https://sanctum.rvnt.uk/
---

Byobu
===

[Byobu](https://www.byobu.org/home) is a suite of enhancements to tmux, as a command line tool providing live system status, dynamic window management, and some convenient keybindings:

Cheatsheet
---

| **Key** | **Effect** |
| ------- | ---------- |
| ++f1++ | <o50>*Used by X11*</o50> |
| <i30>++shift+f1++</i30> | Display this help |
| ++f2++ | Create a new window |
| <i30>++shift+f2++</i30> | Create a horizontal split |
| <i30>++ctrl+f2++</i30> | Create a vertical split |
| <i30>++ctrl+shift+f2++</i30> | Create a new session |
| ++f3++ / ++f4++ | Move focus among windows |
| <i30>++alt+left++ / ++right++</i30> | Move focus among windows |
| <i30>++alt+up++ / ++down++</i30> | Move focus among sessions |
| <i30>++shift+left++ / ++right++ / ++up++ / ++down++</i30> | Move focus among splits |
| <i30>++shift+f3++ / ++f4++</i30>  | Move focus among splits |
| <i30>++ctrl+f3++ / ++f4++</i30>  | Move a split |
| <i30>++ctrl+shift+f3++ / ++f4++</i30>  | Move a window |
| <i30>++shift+alt+left++ / ++right++ / ++up++ /++down++</i30> | Resize a split |
| ++f5++ | Reload profile, refresh status |
| <i30>++alt+f5++</i30> | Toggle UTF-8 support, refresh status
| <i30>++shift+f5++</i30> | Toggle through status lines
| <i30>++ctrl+f5++</i30> | Reconnect ssh/gpg/dbus sockets
| <i30>++ctrl+shift+f5++</i30> | Change status bar's color randomly
| ++f6++ | Detach session and then logout |
| <i30>++shift+f6++</i30> | Detach session and do not logout |
| <i30>++alt+f6++</i30> | Detach all clients but yourself |
| <i30>++ctrl+f6++</i30> | Kill split in focus |
| ++f7++ | Enter scrollback history |
| <i30>++alt+page-up++ / ++page-down++</i30> | Enter and move through scrollback |
| <i30>++shift+f7++</i30> | Save history to $BYOBU_RUN_DIR/printscreen |
| ++f8++ | Rename the current window |
| <i30>++ctrl+f8++</i30> | Rename the current session
| <i30>++shift+f8++</i30> | Toggle through split arrangements
| <i30>++alt+shift+f8++</i30> | Restore a split-pane layout
| <i30>++ctrl+shift+f8++</i30> | Save the current split-pane layout
| ++f9++ | Launch byobu-config window |
| <i30>++ctrl+f9++</i30> | Enter command and run in all windows
| <i30>++shift+f9++</i30> | Enter command and run in all splits
| <i30>++alt+f9++</i30> | Toggle sending keyboard input to all splits
| ++f10++ | <o50>*Used by X11*</o50> |
| ++f11++ | <o50>*Used by X11*</o50> |
| <i30>++alt+f11++</i30> | Expand split to a full window |
| <i30>++shift+f11++</i30> | Zoom into a split, zoom out of a split |
| <i30>++ctrl+f11++</i30> | Join window into a vertical split |
| ++f12++ | Escape sequence |
| <i30>++shift+f12++</i30> | Toggle on/off Byobu's keybindings |
| <i30>++alt+f12++</i30> | Toggle on/off Byobu's mouse support |
| <i30>++ctrl+shift+f12++</i30> | Mondrian squares |
