---
title: Windows Cheat Sheet
Summary: Shortcuts and useful info
Author: rvnt
Date: 2021-07-21
base_url: https://sanctum.rvnt.uk
---

# Windows Cheat Sheet

??? info "Note"
    Please note that the information provided applies mainly to Windows Vista onwards. i.e. Windows Vista, Windows 7, Windows 8, and Windows 10.

    The majority of the information contained below was sourced from [13Cubed](https://nullreferer.com/?https://www.13cubed.com/).

## Registry Hives

There are five main [registry hives](https://nullreferer.com/?https://docs.microsoft.com/en-us/windows/win32/sysinfo/registry-hives) that contain system information as well as a sixth registry hive file that contains user specific information:


| **Hive**   | **Disk Location** | **Registry Location** |
| ---------- | ----------------- | --------------------- 
| DEFAULT    | %SystemRoot%\System32\config | HKEY_USERS\\.DEFAULT
| SAM        | %SystemRoot%\System32\config | HKEY_LOCAL_MACHINE\SAM
| SECURITY   | %SystemRoot%\System32\config | HKEY_LOCAL_MACHINE\SECURITY
| SOFTWARE   | %SystemRoot%\System32\config | HKEY_LOCAL_MACHINE\SOFTWARE
| SYSTEM     | %SystemRoot%\System32\config | HKEY_LOCAL_MACHINE\SYSTEM
| NTUSER.DAT | C:\Users\%username% | HKEY_CURRENT_USER

The operating system protects these hive files preventing them from being opened, edited, or copied on a live system. A forensic tool such as FTKi would be needed to be used to extract these hive files for analysis.

The operating system creates automatic backups of system hive files within %SystemRoot%\System32\config\RegBack which may contain evidence of deleted registry keys.

Other registry hives such as HARDWARE are generated at system boot and not stored on disk.

---

## Recent Files

The registry key `HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer` contains information relating to files recently accessed, viewed, or modified:

| **Location** | **Relevance** |
| ------------ | ------------- |
| \ComDlg32\LastVisitedPidlMRU | Binaries that were used to open or save files. |
| \ComDlg32\OpenSavePidlMRU | Paths to the files themselves that were opened or saved. |
| \RecentDocs | Files that have recently been interacted with. |
| \RunMRU | Commands that have recently been used in the run tool. ++win+r++ |
| \TypedPaths | Paths that have been explicitly entered in the Explorer address bar. ++win+e++ |
| \UserAssist | When an applicaiton was executed and how many times it was executed.  (ROT13 Obfuscated) |

---

## Startup Apps

Applications that are set to automatically launch at startup for the current user are stored in `HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\` and for all users in `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\`.

| **Location** | **Relevance** |
| ------------ | ------------- |
| \Run | Will launch at every login. |
| \RunOnce | Will launch for next login only. |

---

## Shellbags

Stores information relating to a folders size, position, icon, sorting method, etc. Shellbags persist for deleted folders and are stored in `HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\Shell`.

| **Location** | **Relevance** |
| ------------ | ------------- |
| \Bags | Contains Shellbags |
| \BagMRU | Database of folders which are currently stored. |

---

## UsrClass.dat

Stores shellbag information as well as recording configuraiton information from user processes that do not have permission to write to standard registry hives. Can be found on disk at ` %USERPROFILE%\AppData\Local\Microsoft\Windows` and connects to the registry at `HKEY_CURRENT_USER\SOFTWARE\Classes`

---

## USB Devices

### Class ID and Serial Number

??? info "CurrentControlSet vs ControlSet001"
    `CurrentControlSet` will only be found on a live system.

    `ControlSet001` will be used when analysing an image. The operating system may create additional ControlSets if there are issues with the previous version.

    `HKEY_LOCAL_MACHINE\SYSTEM\Select\Current` will show the last used ControlSetXXX.

Looking in the relevant ControlSet location it is possible to determine the Class ID and serial number of a USB device by looking at the values stored in `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Enum\USBSTOR`.

??? info "USB Serial Numbers"
    If the serial number stored in `\Enum\USBSTOR` has an & at the second to last character then it is most likely a globaly unique serial number. e.g. 01234567898765432&0
    
    Whereas if it was the second character from the start then it would imply that the operating system has generated a unique serial for that machine only as the manufacturer has not adhered to Microsoft's guidelines regarding serial numbers. e.g. 0&0123456789

### VID and PID

The same can also be done to determine the USB device's VID and PID by looking at the values stored in `HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Enum\USB`. e.g. `VID_090A&PID_1100`

??? info "VID and PID"
    USB VID and PID lookup databases exist online to allow for more information about a specific USB device to be determined. 

### Volume Name

The key `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Portable Devices\Devices` will contain serial numbers of USB devices previously connected. By cross referencing serials numbers found in `CurrentControlSet` or `ControlSet001` you can determine the Volume Name of the device, which could be seen in explorer when the device was connected. e.g. `USB Drive`

### Drive Letters and Volume GUID

The key `HKEY_LOCAL_MACHINE\SYSTEM\MountedDevices` will contain a list of previosly mounted devices which can also be cross referenced with the serial number to determine the drive letter used as well as the volume GUID (Globally Unique Identifier) for that specific USB device. e.g. `G:\` and `\??\Volume{91eeb585-4ef4-11ab-af32-806e6f6e6763}`

### Volume Serial Number

The key `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\EMDMgmt` was originally used for ReadyBoost in Windows Vista and contains a complete history of Volume Serial Numbers. The USB device serial can be used to determine Volume Serial Numbers even if a device has been formatted multiple times. The device serial will match to several Volume Serial Numbers if this is the case. It will be in decimal format and will have to be converted to hexadecimal.

Please note this key ***does not*** exist on SSD/NMVE system drives.

The Volume Serial Number can also be found using command prompt and the command `dir`. It will be shown in hexadecimal format. e.g.  ` Volume Serial Number is DACC-FA3D`

### Determing the User

It is possible to determine which user mounted a specific USB device by looking at the values stored in `HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Mountpoints2`. It will contain a list of Volume GUID's of the USB devices that the current user has mounted/connected while logged on.

### First and Last Connected

It is also possible to determine when a USB device was first connected, the last time it was connected, and the last time it was disconnected. The keys stored within `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Enum\USBSTOR\{Ven_Prod_Version}\{USB Serial Number}\Properties\{Volume GUID}` will tell us the following:

- 0064 - First time it was connected. (Windows 7+)
- 0066 - Last time it was connected. (Windows 8+)
- 0067 - Last time it was disconnected. (Windows 8+)

This information is also stored in logs which can be found at `%SystemRoot%\inf\setupapi.dev.log`.

### Software

| **Developer** | **Software** |
| -------- | ------------- |
| Jason Hale | [USB Detective](https://nullreferer.com/?https://usbdetective.com/) |
| NirSoft | [USBDeview](https://nullreferer.com/?https://www.nirsoft.net/utils/usb_devices_view.html) |
| WoanWare | [USB Device Forensics](https://nullreferer.com/?https://github.com/woanware/usbdeviceforensics) |

---

## Time Zone Information

We can determine the timezone which will aid when creating a timeline of events, especially when multiple devices from multiple time zones are involved in a investigation. This can be found under the key `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation`.

---

## Computer Name

The computer name can also be determined by looking within the key `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName`.

---

## Active Shares

Any folders that are being actively shared can be seen within the key `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\LanmanServer\Shares`.

---

## Last Accessed

We can determine if the operating system is tracking last accessed time and dates by looking at the value `NtfsDisableLastAccessUpdate` within the key `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\FileSystem`.

By default it is set to 1 which means the operating system is ***not*** keeping track of last accessed time and dates.

---

## Network Interfaces

Information related to network connections can be viewed within the key `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\Tcpip\Parameters\Interfaces`. Information such as IP address, Lease time, DHCP server, etc. 

We can determine the last time the machine connected to a specific network by looking at the last write time for that networks GUID registry key.

### Network Location Awareness (NLA)

NLA aggregates network information for a machine and generates a GUID to identify each network. These can be seen at `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\NetworkList` under the keys `\Signatures`, `\NlIa`, and `Profiles`.

In some cases additional NLA information can be found in `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\HomeGroup`, but not always.

The value `NameType` under the key `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\NetworkList\Profiles\{GUID}` will indicate one of the following:

- 0x06 - Wired Connection
- 0x17 - Broadband Connection (e.g. Tethered Mobile Phone)
- 0x47 - Wireless Connection

Other useful values such as `DateCreated` and `DateLastConnected` can also be found under the same registry key. These values are stored as 128-bit Windows System Time Structure.

---

## LNK Files

.lnk files are metadata files that link to other files on disk, typicall used as shortcuts. It contain significant information relating to its target file including but not limited to

- Creation time
- Modification time
- Access time
- Original path
- Size last time it was accessed
- Network Share info
- File attributes (Read Only / Hidden / etc)

### Jump Lists

Another form of .lnk files Located in `C:\Users\%username%\AppData\Roaming\Microsoft\Windows\Recent\AutomaticDestinations` and `C:\Users\%username%\AppData\Roaming\Microsoft\Windows\Recent\CustomDestinations`, they 
provide an easy way to access files recently accessed via the task bar. e.g. Microsoft Word pinned to the taskbar will show recently used documents when hovering over the icon.

---

## Prefetch

Prefetch pre-loads the files an application uses as it starts, improving performance by avoiding unnecessary disk reads. Located in `%SystemRoot%\Prefetch` files will be stored in the format `filename-hash.pf` with the hash representing the file path. e.g. `CALCULATOR.EXE-DD323BEE`. Multiple identical filenames with different hashes would indicate the same file existing in multiple locations.

Prefetch files contain useful metadata such as:

- File name
- File size
- Created time
- Modified time
- Executable name
- Executable path
- Run counter
- Last run time

We can determine the status of the prefetcher by looking at the value `EnablePrefetcher` under the key `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management\PrefetchParameters`.

- 0 - Disabled
- 1 - Application prefetching enabled
- 2 - Boot prefetching enabled
- 3 - Application and boot prefetching enabled

### SuperFetch

SuperFetch is designed to identify the way the user uses applications and to look for patterns. SuperFetch records usage patterns and uses the Prefetch tool to pre-load RAM with the files for the applications that it expects the user to open next. The benefits are SuperFetch are greatly reduced on SSD's and NMVE's.

---

## AppCompatCache / ShimCache

| **OS**   | **Location** |
| -------- | ------------ |
| Windows XP | HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\AppCompatibility |
| Windows Vista + | HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\AppCompatCache |

Introduced in Windows 95 it is designed to ensure backwards compatibility with legacy software in newer versions of Windows. 
It is only updated and written to the registry at system shutdown, however a Volatility plugin named [shimcachemem.py](https://nullreferer.com/?https://github.com/fireeye/Volatility-Plugins/tree/master/shimcachemem) is capable of parsing this directly from memory on a live system.

The time and dates listed are the last modification date for that binrary. ***M***ACB.

Although PreFetch/SuperFetch is disabled on Windows Server OS's, ShimCache is available for forensic analysis.

---

## RecentFileCache.bcf

| **OS**   | **Location** |
| -------- | ------------ |
| Windows 7 | %SystemRoot%\AppCompat\Programs |

Contains additional information, complimenting PreFetch/SuperFetch, about application execution. It stores the full name and file path of executables that have been run since the last time the Windows Application Experience service was run. This was replaced by Amcache.hve in Windows 8 onwards.

### Windows Application Experience

Introduced in Windows Server 2003 SP1 it automatically applies software updates to ensure they can run when new updates are released, aka backward compatibility.

??? note "Amcache.hve"
    Although Amcache.hve is not typically available on Windows 7, if the compatibility update KB2952664 is installed it will also be found on a Windows 7 machine.

---

## Amcache.hve

| **OS**   | **Location** |
| -------- | ------------ |
| Windows 8 + | %SystemRoot%\AppCompat\Programs |

A registry hive which contains even more information and metadata about application execution, including a SHA1 hash of the binary that was executed.

---

## System Resource Utilization Monitor (SRUM)

| **OS**   | **Location** |
| -------- | ------------ |
| Windows 8 + | %SystemRoot%\System32\sru\SRUDB.dat |

SRUM tracks system resource utitlization; CPU cycles, network activity, power consumption, etc. It is available to view by the user in Task Manager under the App History tab. 

The SRUM database itself is stored in the ESE (Extensible Storage Engine) format and contains a more in depth view of a users activity and therefore forensic artifacts. Forensic software will be required to extract/copy the SRUM database on a live system. It is only written to disk once an hour or at system shutdown, until then it is stored in the registry at HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\WindowsNT\CurrentVersion\SRUM\Extensions.

The SRUM database can also be extracted from Shadow Volume Copies available on a device or image.

The free utitlity [srum-dump](https://nullreferer.com/?https://github.com/MarkBaggett/srum-dump) can be used to extract the data stored in SRUDB.dat into a spreadsheet.

---

## Timestamps

Timestamps are typically referred to as MCAB or MACE.

### MCAB

| **Initial** | **Meaning** |
| ----------- | ----------- |
| **M** | **M**odified |
| **A** | **A**ccessed |
| **C** | MFT Entry **C**hanged |
| **B** | **B**irth (Created) |

### MACE

| **Initial** | **Meaning** |
| ----------- | ----------- |
| **M** | **M**odified |
| **A** | **A**ccessed |
| **C** | **C**reated |
| **E** | MFT **E**ntry Changed |

Timestamps are stored in `$MFT`, a Windows NTFS metadata file, and located at the root of a NTFS volume. In turn it contains multiple copies of the MCAB/MACE timestamps;

 - `$STANDARD_INFORMATION` (`$SI`)
 - `$FILE_NAME` (`$FN`)

The majority of applications (cmd.exe, explorer.exe, etc.) use `$SI` which is user modifiable, whereas `$FN` can only be modified by the Windows kernel. Comparing timestamps stored in `$SI` and `$FN` can be done to deterimine if any timestamps have been artificially modified.

| **Operation** | **Modified** | **Accessed** | **Changed** | **Birth (Created)** |
| ------------- | :----------: | :----------: | :---------: | :-----------------: |
| File Create | Yes | Yes | <o50>N/A^α^</o50> | Yes |
| File Modify | Yes | No | <o50>N/A^α^</o50> | No |
| File Copy | No (Inherited) | Yes | <o50>N/A^α^</o50> | Yes |
| File Access |  No | No* | <o50>N/A^α^</o50> | No |

<o50>*α: Windows Explorer will only display Modified, Accessed, and Birth timestamps.*</o50>

[AnalyzeMFT](https://nullreferer.com/?https://github.com/dkovar/analyzeMFT) can be used to analyse `$MFT` files and compare `$SI` and `$FN` timestamps.

#### Timestamp Notes

1. If the birth timestamp is later than the modified timestamp then it is likely that it is a copied file.

1. Files copied using Windows Subsystem for Linux (WSL) will show the timestamp for when its copied for all four fields in MACB/MACE.

1. As [previously mentioned](#last-accessed), the registry key `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\FileSystem\NtfsDisableLastAccessUpdate` will disable the updating of access times in most circumstances if set to 1. This is set to 0 by default in Windows XP and to 1 by default in Windows Vista onwards.

---

## Index Attribute

`$I30` index files contain an index of files and directories for a given directory. For each file listed within an `$I30` file it contains:

1. The full filename.
1. The parent directory.
1. MACB timestamps (`$FN`).
1. Physical size.
1. Logical size.

It's possible that evidence of deleted or overwritten files may exists within the slack of the `$I30` index files.

[INDXParse](https://nullreferer.com/?https://github.com/williballenthin/INDXParse) can be used to analyse `$I30` index files.

---

## LNK Files / Shortcuts

LNK files can contain the following relevant metadata:

1. Original path of the target file.
1. Timestamps for the target and LNK files. (Modified, Accessed, Created)
1. Size of the target file.
1. Attributes of target file. (Read Only, Hidden, System, etc.)
1. System name, volume name, volume serial number, and possibly the MAC address.
1. Whether or not the target file is a local file or located on a remote computer.
1. The creation time of the LNK file will usually indicate the time and date that the target file was first opened.
1. The modification time of the LNK file will usually indicate the time and date the target file was last opened.

LNK files may still exist and offer the above information even if the target file has been deleted.

---

## Jump Lists

Featured in Windows 7 and onwards, Jump Lists give the user quick access to recently accessed applications and actions via the taskbar.


### AutomaticDestinations

Location: C:\Users\%USERNAME%\AppData\Roaming\Microsoft\Windows\Recent\AutomaticDestinations

File extension: .automaticDestinations-ms

Provides common features across all applications due to using the Windows API.

AutomaticDestions are OLECF Files:
*(Object Linking and Embedding = OLE) + (Compound Files = CF)*

Object Linking and Embedding Compound Files:

1. Also known as "Compound Binary Files" or "Composite Document File V2".
1. Multiple data streams within a single file
1. Each data stream contains an embedded LNK file.
1. "DestList" stream acts as a "Most Recently Used" list.

### CustomDestinations
Location: C:\Users\%USERNAME%\AppData\Roaming\Microsoft\Windows\Recent\CustomDestinations
File extension: .customDestinations-ms

Provides application specific features that can vary depending on the software or developer.

1. Created when a user manually pins an item to the taskbar or start menu.
1. Are not OLECF files.
1. Do not contain multiple data streams.
1. Instead contain multiple LNK files appended to each other.
1. Which allows LNK files to be carved out.
1. Hex editors can also be used for manual parsing.

---

## Shell Bags

Contains the folder view settings of a directory. E.g. Icon layout and size, Window size, etc.

The registry stores the *last write time* of keys and subkeys which will show when a folder was first visited or last updated.

Shellbags are also created for .zip files.

Shellbags demonstrate whether a specific user has accessed a folder, which may indicate prior knowledge about the existence of that data to the user.

Shellbag data persists after deletion of a folder and can be used to show that a folder existed, even for removeable drives.

Windows Vista Onwards

### Hive Locations

1. C:\\Users\\*username*\\**NTUSER.DAT**
1. C:\\Users\\*username*\\AppData\\Local\\Microsoft\\Windows\\**UsrClass.dat**

### Shellbag Locations

1. **NTUSER.DAT**\\Software\\Microsoft\\Windows\\Shell\\BagMRU
1. **NTUSER.DAT**\\Software\\Microsoft\\Windows\\Shell\\Bags
1. **UsrClass.dat**\\Local Settings\\Software\\Microsoft\\Windows\\Shell\\BagMRU
1. **UsrClass.dat**\\Local Settings\\Software\\Microsoft\\Windows\\Shell\\Bags

### Registry Locations

1. **UsrClass.dat** = HKEY_CURRENT_USER\\Software\\Classes

??? note "Windows XP"
    Shellbag Locations:
    
    Local Folders: **NTUSER.DAT**\\Software\\Microsoft\\Windows\\ShellNoRoam

    Network Folders: **NTUSER.DAT**\\Software\\Microsoft\\Windows\\Shell

    Removable Device Folders: **NTUSER.DAT**\\Software\\Microsoft\\Windows\\StreamMRU

| **Key** | **Subkey** | **Description** |
| ------- | ---------- | --------------- |
| BagMRU | | Stores actual directory structure of folders accessed |
| | MRUListEx | 4-Byte value indicating the order in which folders were accessed with the most recent access listed first. |
| | NodeSlot | Points to the Bags subkey containing customisation data. |
| | NodeSlots | Found in the root BagMRU subkey. Updated with new Shellbag creation. |
| Bags | | Stores actual folder customisation data. (Windows size, layout type, sort order etc.) |

### Software

| **Developer** | **Software** |
| ---- | --------- |
| TZWorks | [Windows ShellBag Parser](https://nullreferer.com/?https://tzworks.com/prototype_page.php?proto_id=14) |
| Willi Ballenthin | [Shellbags.py](https://nullreferer.com/?https://github.com/williballenthin/shellbags/blob/master/shellbags.py) |
| Eric Zimmerman | [Shellbags Explorer](https://nullreferer.com/?https://ericzimmerman.github.io/) |
| Magnet Forensics | [Internet Evidence Finder (IEF)](https://nullreferer.com/?https://www.magnetforensics.com/) |

---

## Recycle Bin

| **Location** | **OS** | **Metadata** |
| ------------ | ------ | --------- |
| C:\RECYCLED | Windows 95/98/ME | **INFO2**: "Shared" Recycle Bin<br>- File name and full path of deleted file.<br>- Size of the deleted file.<br>- Date and time at which the file was deleted. |
| C:\RECYCLER | Windows NT/2K/XP | **\SID*\INFO2**: Per User Recycle Bin<br>- File name and full path of deleted file.<br>- Size of the deleted file.<br>- Date and time at which the file was deleted. |
| C:\$Recycle.Bin | Windows Vista+ | **\SID*\$I**XXXXXX.ext: Per User Recycle Bin<br>- File name and full path of deleted file.<br>- Size of the deleted file.<br>- Date and time at which the file was deleted.<br>**\SID*\$R**XXXXXX.ext: Per User Recycle Bin<br>- Contents of the deleted file. |

??? note "Windows 95 to XP"
    Prior to Windows Vista original file contents were:

    1. Renamed with a "D"
    2. Followed by the drive letter on which the file was deleted, e.g. "c".
    3. Followed by an index number starting at 0, incrementing in order of deletion.
    4. Followed by the original file extension.

    e.g. "C:\Pictures\Potato.jpg" would be renamed to "Dc0.jpg"

### Software

| **Developer** | **Software** |
| -------- | ------------- |
| Digital Forensics Stream | [$I Parse ](https://nullreferer.com/?https://df-stream.com/recycle-bin-i-parser/) |

---

## Remote Desktop Protocol (RDP)

### RDP Events

!!! warning
    Due to the nature of Microsoft Windows, some logs may be missing or vary slightly from what is expected.

RDP log files can be found at `C:\Windows\System32\winevt\Logs`.

The events mentioned below refer to target machine/server, and **NOT** the machine used to connect to it.

#### RDP Successful Logon

| **#** | **Event ID** | **Description** | **Log File** | **Action** |
| ----- | ------------ | --------------- | ------------ | ---------- |
| 1 | 1149 | User authentication succeeded | Microsoft-Windows-<br>TerminalServices-<br>RemoteConnectionManager<br>%4Operational.evtx | Network Connection |
| 2 | 4624<br><br>*Logon Type:<br>10=Connect*<br>*7=Reconnect* | An account was succesffuly logged on | Security.evtx | Authentication |
| 3 | 21 | Remote Desktop Services: Session logon succeeded | Microsoft-Windows-<br>TerminalServices-<br>LocalSessionManager<br>%4Operational.evtx | Logon |
| 4 | 22 | Remote Desktop Services: Shell start notification receieved | Microsoft-Windows-<br>TerminalServices-<br>LocalSessionManager<br>%4Operational.evtx | Logon |

#### RDP Unsuccessful Logon

| **#** | **Event ID** | **Description** | **Log File** | **Action** |
| ----- | ------------ | --------------- | ------------ | ---------- |
| 1 | 1149 | User authentication succeeded | Microsoft-Windows-<br>TerminalServices-<br>RemoteConnectionManager<br>%4Operational.evtx | Network Connection |
| 2 | 4625<br><br>*Logon Type:<br>10=Connect*<br>*7=Reconnect* | An account failed to log on | Security.evtx | Authentication |

#### RDP Session Disconnect (Window Close)

| **#** | **Event ID** | **Description** | **Log File** | **Action** |
| ----- | ------------ | --------------- | ------------ | ---------- |
| 1 | 24 | Remote Desktop Services: Session has been disconnected | Microsoft-Windows-<br>TerminalServices-<br>LocalSessionManager<br>%4Operational.evtx | Session Disconnect / Reconnect |
| 2 | 40 | Session *X* has been disconnected, reason code *Z* | Microsoft-Windows-<br>TerminalServices-<br>LocalSessionManager<br>%4Operational.evtx | Session Disconnect / Reconnect |
| 3 | 4779 | A session was disconnected from a Window Station | Security.evtx | Session Disconnect / Reconnect |
| 4 | 4634<br><br>*Logon Type:<br>10=Connect*<br>*7=Reconnect* | An account was logged off | Security.evtx | Session Disconnect / Reconnect |

#### RDP Session Disconnect (Start > Disconnect)

| **#** | **Event ID** | **Description** | **Log File** | **Action** |
| -- | ------------ | --------------- | ------------ | ---------- |
| 1 | 24 | Remote Desktop Services: Session has been disconnected | Microsoft-Windows-<br>TerminalServices-<br>LocalSessionManager<br>%4Operational.evtx | Session Disconnect / Reconnect |
| 2 | 39 | Session *X* has been disconnected by session *Y* | Microsoft-Windows-<br>TerminalServices-<br>LocalSessionManager<br>%4Operational.evtx | Session Disconnect / Reconnect |
| 3 | 40 | Session *X* has been disconnected, reason code *Z* | Microsoft-Windows-<br>TerminalServices-<br>LocalSessionManager<br>%4Operational.evtx | Session Disconnect / Reconnect |
| 4 | 4779 | A session was disconnected from a Window Station | Security.evtx | Session Disconnect / Reconnect |
| 5 | 4634<br><br>*Logon Type:<br>10=Connect*<br>*7=Reconnect* | An account was logged off | Security.evtx | Session Disconnect / Reconnect |

#### RDP Session Reconnect

| **#** | **Event ID** | **Description** | **Log File** | **Action** |
| ----- | ------------ | --------------- | ------------ | ---------- |
| 1 | 1149 | User authentication succeeded | Microsoft-Windows-<br>TerminalServices-<br>RemoteConnectionManager<br>%4Operational.evtx | Network Connection |
| 2 | 4624<br><br>*Logon Type:*<br>*7=Reconnect* | An account was successfully logged on | Security.evtx | Authentication |
| 3 | 25 | Remote Desktop Services: Session reconnection succeeded | Microsoft-Windows-<br>TerminalServices-<br>LocalSessionManager<br>%4Operational.evtx | Session Disconnect / Reconnect |
| 4 | 40^✣^ | Session *X* has been disconnected, reason code *Z* | Microsoft-Windows-<br>TerminalServices-<br>LocalSessionManager<br>%4Operational.evtx | Session Disconnect / Reconnect |
| 5 | 4778 | A session was reconnected to a Window Station | Security.evtx | Session Disconnect / Reconnect |

^✣^ *Events also inidcate/correlate to reconnections.*

#### RDP Session Logoff

| **#** | **Event ID** | **Description** | **Log File** | **Action** |
| ----- | ------------ | --------------- | ------------ | ---------- |
| 1 | 23 | Remote Desktop Services: Session logoff succeeded | Microsoft-Windows-<br>TerminalServices-<br>LocalSessionManager<br>%4Operational.evtx | Logoff |
| 2 | 4634<br><br>*Logon Type:<br>10=Connect*<br>*7=Reconnect* | An account was logged off | Security.evtx | Logoff |
| 3 | 4647 | User initiated logoff | Security.evtx | Logoff |
| 4 | 9009 | The Desktop Windows Manager has exited with code (*X*) | System.evtx | Logoff |

### RDP Cache

The RDP cache is located at `C:\Users\%username%\AppData\Local\Microsoft\Terminal Server Client\Cache\`.

It will contain bin files relating to RDP sessions, e.g. `Cache0000.bin, Cache0001.bin, ..., Cache9999.bin`, etc.

### Software

| **Developer** | **Software** |
| ---- | --------- |
| ANSSI | [BMC-Tools](https://nullreferer.com/?https://github.com/ANSSI-FR/bmc-tools/) |

---

## Log Parsing

*Use Log Parser and SQL commands to search through system logs.* 

### Software

| **Developer** | **Software** |
| ---- | --------- |
| Microsoft | [Log Parser](https://nullreferer.com/?https://www.microsoft.com/en-gb/download/details.aspx?id=24659) |

---

## Persistence Mechanisms

### Run and RunOnce

| **Registry Keys** | **Target** |
| ----------------- | ---------- |
| HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\\**Run** | Single User |
| HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\\**RunOnce** | Single User |
| HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\\**Run** | All Users |
| HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\\**RunOnce** | All Users |

### Software

| **Developer** | **Software** |
| ------------- | ------------ |
| Microsoft | [SysInternals: Autoruns](https://nullreferer.com/?https://docs.microsoft.com/en-us/sysinternals/downloads/autoruns) |

---