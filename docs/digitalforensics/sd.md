---
title: SD Cards
Summary: Breakdown of SD Cards
Author: rvnt
Date: 2024-03-13
base_url: https://sanctum.spctr.uk/
---

# SD Cards

<div id='SANCTUM-table'></div>


| **Marking** | **Type** | **Text** | **Minimum Write Speed** |
| :---------: | :------: | :---------: | :---------------------: |
| ![SDC-Class 2](img/C2-L.svg#only-light) ![SDC-Class 2](img/C2-D.svg#only-dark) | Speed Class | C2 | 2MB/s |
| ![SDC-Class 4](img/C4-L.svg#only-light) ![SDC-Class 4](img/C4-D.svg#only-dark) | Speed Class | C4 | 4MB/s |
| ![SDC-Class 6](img/C6-L.svg#only-light) ![SDC-Class 6](img/C6-D.svg#only-dark) | Speed Class | C6 | 6MB/s |
| ![SDC-Class 10](img/C10-L.svg#only-light) ![SDC-Class 10](img/C10-D.svg#only-dark) | Speed Class | C10| 10MB/s |
| ![SDC-UHS Speed Class U1](img/U1-L.svg#only-light) ![SDC-UHS Speed Class U1](img/U1-D.svg#only-dark) | UHS Speed Class | U1 | 10MB/s |
| ![SDC-UHS Speed Class U3](img/U3-L.svg#only-light) ![SDC-UHS Speed Class U3](img/U3-D.svg#only-dark) | UHS Speed Class | U3 | 30MB/s |
| ![SDC-Video Speed Class 6](img/V6-L.svg#only-light) ![SDC-Video Speed Class 6](img/V6-D.svg#only-dark) | Video Speed Class | V6 | 6 MB/s |
| ![SDC-Video Speed Class 10](img/V10-L.svg#only-light) ![SDC-Video Speed Class 10](img/V10-D.svg#only-dark) | Video Speed Class | V10 | 10 MB/s |
| ![SDC-Video Speed Class 30](img/V30-L.svg#only-light) ![SDC-Video Speed Class 30](img/V30-D.svg#only-dark) | Video Speed Class | V30 | 30 MB/s |
| ![SDC-Video Speed Class 60](img/V60-L.svg#only-light) ![SDC-Video Speed Class 60](img/V60-D.svg#only-dark) | Video Speed Class | V60 | 60 MB/s |
| ![SDC-Video Speed Class 90](img/V90-L.svg#only-light) ![SDC-Video Speed Class 90](img/V90-D.svg#only-dark) | Video Speed Class | V90 | 90 MB/s |

<div id='SANCTUM-table'></div>

| **Marking** | **Type** | **Min Capacity** | **Max Capacity** | **File System** |
| :---------: | :------: | :--------------: | :--------------: | :-------------: |
| ![SDC-SD](img/SD-L.svg#only-light) ![SDC-SD](img/SD-D.svg#only-dark) | SD / SDSC | 128 MB | 2 GB | FAT12 / FAT16 |
| ![SDC-SDHC](img/SDHC-L.svg#only-light) ![SDC-SDHC](img/SDHC-D.svg#only-dark) | SDHC | 2 GB | 32 GB | FAT32 |
| ![SDC-SDXC](img/SDXC-L.svg#only-light) ![SDC-SDXC](img/SDXC-D.svg#only-dark) | SDXC | 32 GB | 2 TB | exFAT |
| ![SDC-SDUC](img/SDUC-L.svg#only-light) ![SDC-SDUC](img/SDUC-D.svg#only-dark) | SDUC | 2 TB | 128 TB | exFAT |

<div id='SANCTUM-table'></div>

| **Marking** | **UHS Bus Interface** | **Theoretical Maximum Transfer Speeds** |
| :---------: | :-------------------: | :-------------------------------------: |
| ![SDC-UHS Speed Class U1](img/U1-L.svg#only-light) ![SDC-UHS Speed Class U1](img/U1-D.svg#only-dark) | UHS-I | Up to 104MB/s |
| ![SDC-UHS Speed Class U3](img/U3-L.svg#only-light) ![SDC-UHS Speed Class U3](img/U3-D.svg#only-dark) | UHS-II | Up to 312MB/s |

<div id='SANCTUM-table'></div>

| **Marking** | **Performance Class** | **Minimum Random Read Speed** | **Minimum Write Speed** |
| :---------: | :-------------------: | :---------------------------: | :---------------------: |
| ![SDC-A1](img/A1-L.svg#only-light) ![SDC-A1](img/A1-D.svg#only-dark) | A1 | 1500 IO/s | 500 IO/s |
| ![SDC-A2](img/A2-L.svg#only-light) ![SDC-A2](img/A2-D.svg#only-dark) | A2 | 4000 IO/s | 2000 IO/s |


<div id='SANCTUM-table'></div>

| **Marking** | **Bus Interface** | **Min Speed** | **Max Speed** |
| :---------: | :---------------: | :-----------: | :-----------: |
| ![SDC-UHS-1](img/UHSI-L.svg#only-light) ![SDC-UHS-1](img/UHSI-D.svg#only-dark) | UHS-1 | 50 MB/s | 104 MB/s |
| ![SDC-UHS-2](img/UHSII-L.svg#only-light) ![SDC-UHS-2](img/UHSII-D.svg#only-dark) | UHS-2 | 156 MB/s | 312 MB/s |
| ![SDC-UHS-3](img/UHSIII-L.svg#only-light) ![SDC-UHS-3](img/UHSIII-D.svg#only-dark) | UHS-3 | 312 MB/s | 624 MB/s |
| ![SDC-EX](img/EX-L.svg#only-light) ![SDC-EX](img/EX-D.svg#only-dark) | SD Express | 985 MB/s | 3938 MB/s |