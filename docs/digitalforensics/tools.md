---
title: Tools
Summary: Digital Forensic Tools
Author: rvnt
Date: 2024-02-19
base_url: https://sanctum.spctr.uk/
---

# Tools

Information taken from [Hacking Articles](https://github.com/ignitetechnologies).

## Open Source Tools

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------- |
| [CAINE](https://www.caine-live.net/) | A complete forensic environment that is organized to integrate existing software tools as software modules and to provide a friendly graphical interface. |
| [Binwalk](https://github.com/ReFirmLabs/binwalk) | A fast, easy to use tool for analyzing, reverse engineering, and extracting firmware images. |
| [Magicrescue](https://github.com/jbj/magicrescue) ^†^ | A tool to recover files by searching for them on the disk, independent of any filesystem metadata. |
| [Scalpel](https://github.com/nolaforensix/scalpel-2.02) ^†^  | A file carving and indexing application that's used to recover files from disk images. |
| [Scrounge-ntfs](http://thewalter.net/stef/software/scrounge/) | A tool to recover data from NTFS partitions. |
| [Autopsy](https://sleuthkit.org/autopsy/) | A digital forensics platform and graphical interface to The Sleuth Kit and other digital forensics tools. |
| [The Sleuth Kit](https://sleuthkit.org/sleuthkit/) | A library and collection of command-line digital forensics tools that allow you to investigate disk images. |
| [Wireshark](https://www.wireshark.org/) | A network protocol analyzer that lets you capture and interactively browse the traffic running on a computer network. |
| [Volativity](https://www.volatilityfoundation.org/) | An open-source memory forensics framework for incident response and malware analysis. |
| [ddrescue](https://www.gnu.org/software/ddrescue/) | A data recovery tool that copies data from one file or block device to another, trying hard to rescue data in case of read errors. |
| [Mobile Verification Toolkit](https://docs.mvt.re/en/latest/) | A tool to simplify the process of gathering forensic traces helpful to identify a potential compromise of Android and iOS devices. |

^†^ *^Deprecated^*

## Proprietary Tools

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------- |
| [Forensic Toolkit](https://www.exterro.com/digital-forensics-software/forensic-toolkit) | A comprehensive digital forensics application that provides a wide range of features to analyze data recovered during a forensic investigation. |
| Exterro [FTK Imager](https://www.exterro.com/ftk-product-downloads/ftk-imager-version-4-7-1) | A disk imaging and memory previewing tool used in digital forensics. |
| [Axiom](https://www.magnetforensics.com/products/magnet-axiom/) | A comprehensive digital forensics tool that can extract evidence from multiple sources. |
| [ProDiscover](https://prodiscover.com/) | A computer forensics tool that allows you to thoroughly examine a hard drive for evidence of wrongdoing. |
| [OSForensics](https://www.osforensics.com/) | A suite of forensic tools that allows you to extract digital artifacts from computers quickly with high performance. |
| OpenText [EnCase](https://www.opentext.com/products/encase-forensic) | A comprehensive digital investigations platform used to uncover, analyze, and report on digital evidence. |
| X-Ways [Forensics](https://www.x-ways.net/forensics/index-m.html) | An advanced forensic data recovery solution that provides a wide range of features to analyze data recovered during a forensic investigation. |
| Oxygen [Forensics](https://oxygenforensics.com/en/) | A comprehensive mobile forensics tool that allows extraction and analysis of data from a wide range of mobile devices. |

## Mobile Forensic Tools

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------- |
| Magnet [Acquire](https://www.magnetforensics.com/resources/magnet-acquire/) | A tool that allows you to acquire forensic images of computer and mobile devices. |
| Oxygen [Forensics](https://oxygenforensics.com/en/) | A comprehensive mobile forensics tool that allows extraction and analysis of data from a wide range of mobile devices. |
| [Mobile Verification Toolkit](https://github.com/mvt-project/mvt) | A tool to simplify the process of gathering forensic traces helpful to identify a potential compromise of Android and iOS devices. |
| Elcomsoft [iOS Forensic Toolkit](https://www.elcomsoft.com/eift.html) | A toolkit for performing physical and logical acquisition of iPhone, iPad and iPod touch devices. |
| Cellebrite [UFED](https://cellebrite.com/en/ufed/) | A comprehensive mobile data extraction and analysis tool used in digital forensics investigations. |
| [MOBILedit Forensic Express](https://www.mobiledit.com/forensic-express-personal) | A phone content analyzer and data recovery tool that extracts vital information from a wide range of mobile devices. |
| [Autopsy](https://sleuthkit.org/autopsy/) | A digital forensics platform and graphical interface to The Sleuth Kit and other digital forensics tools. |
| [Andriller](https://github.com/den4uk/andriller) | An Android forensic tool that provides a wealth of data extraction capabilities. |

## Network Forensic Tools

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------- |
| [Wireshark](https://www.wireshark.org/) | A network protocol analyzer that lets you capture and interactively browse the traffic running on a computer network. |
| [TCPDump](https://www.tcpdump.org/) | A command-line packet capture and analysis tool widely used in the networking and security industry. |
| [Tshark](https://tshark.dev/setup/install/) | The command-line version of Wireshark used to capture and analyze network traffic. |
| [Xplico](https://www.xplico.org/) | A network forensic analysis tool (NFAT) that supports the reconstruction of various Internet services. |
| [Security Onion](https://securityonionsolutions.com/) | A free and open-source Linux distribution for threat hunting, enterprise security monitoring, and log management. |
| [Snort](https://www.snort.org/) | An open-source network intrusion detection system (NIDS) and intrusion prevention system (IPS). |
| [Zeek](https://zeek.org/) ^‡^ | A powerful network analysis framework that is much different from the typical IDS you may know (formerly known as Bro). |
| [NetworkMiner](https://www.netresec.com/?page=NetworkMiner) | A network forensic analysis tool (NFAT) for Windows that can detect the OS, hostname, and open ports of network hosts through packet sniffing. |

^‡^ *^Formally^* *^Bro^*

## Memory Forensic Tools

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------- |
| Exterro [FTK Imager](https://www.exterro.com/ftk-product-downloads/ftk-imager-version-4-7-1) | A disk imaging and memory previewing tool used in digital forensics. |
| Belkasoft [RAM Capturer]() | A free tool to dump the data from a computer's volatile memory. |
| [memdump](https://www.kali.org/tools/memdump/) | A tool for dumping memory contents to a file for further analysis. |
| Arsenal Recon [Hibernation Recon](https://arsenalrecon.com/products/hibernation-recon) | A tool used to extract and reconstruct forensic evidence from Windows hibernation files. |
| [WindowsScope](https://www.windowsscope.com/) | A tool for live system analysis and reverse engineering of the Windows operating system. |
| Volatility Foundation [Volatility Framework](https://www.volatilityfoundation.org/) | An open-source memory forensics framework for incident response and malware analysis. |
| PassMark [Volatility Workbench](https://www.osforensics.com/tools/volatility-workbench.html) | A graphical user interface (GUI) for the Volatility tool. |
| Trellix [Redline](https://fireeye.market/apps/211364) | A tool for triage, live memory analysis, and incident response. |
| Magnet [DumpIt](https://www.magnetforensics.com/resources/magnet-dumpit-for-windows/) | A utility for dumping system memory to a file for analysis. |
| Magnet [RAM Capture](https://www.magnetforensics.com/resources/magnet-ram-capture/) | A free imaging tool designed to capture the physical memory of a suspect's computer. |

## Live Forensic Tools

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------- |
| PassMark [OS Forensics](https://www.osforensics.com/) | A suite of forensic tools that allows you to extract digital artifacts from computers quickly with high performance. |
| Kali Linux [Forensics Mode](https://www.kali.org/) | A special mode in Kali Linux that includes forensic tools and prevents the system from touching the hard drive or swap space. |
| [F-Response](https://www.f-response.com/) | A tool that provides read-only access to the full contents of a computer, server, or network device over a network. |
| OpenText [EnCase Live](https://www.opentext.com/products/encase-forensic) | A digital investigations platform for forensic professionals that uncovers forensic evidence rapidly and allows its review without the need to image. |


## Disk Imaging Tools

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------- |
| [dc3dd](https://www.kali.org/tools/dc3dd/) | A patched version of dd that includes a number of features useful for computer forensics. |
| X-Ways [Imager](https://www.x-ways.net/imager/) | A powerful disk imaging tool part of the X-Ways forensics suite. |
| [dd](https://ss64.com/bash/dd.html) | A command-line utility for Unix and Unix-like operating systems to convert and copy files. |
| [Guymager](https://www.kali.org/tools/guymager/) | A free and open-source forensic disk imager for Linux. |
| Exterro [FTK Imager](https://www.exterro.com/ftk-product-downloads/ftk-imager-version-4-7-1) | A disk imaging and memory previewing tool used in digital forensics. |
| PassMark [OSFClone](https://www.osforensics.com/tools/create-disk-images.html) | A free and open-source tool that allows you to create disk images of hard drives, partitions, and USB devices. |
| OpenText [EnCase Imager](https://www.opentext.com/products/encase-forensic) | A comprehensive forensic acquisition tool that produces exact copies of data without altering the original evidence. |
| GetData [FEX Imager](https://getdataforensics.com/product/fex-imager/) | A tool that creates a forensic image of a drive preserving every bit. |
| X-Ways [WinHex](https://www.x-ways.net/winhex/) | A universal hexadecimal editor particularly helpful in the realm of computer forensics, data recovery, low-level data processing, and IT security. |

## File Analysis Tools

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------- |
| [AnalyzePESig](https://blog.didierstevens.com/2021/08/21/update-analyzepesig-version-0-0-0-8/) | A tool for analyzing PE file signatures. |
| [Pdfid](https://www.kali.org/tools/pdfid/) | A tool to scan PDFs for certain interesting elements. |
| [Pdf-parser](https://www.kali.org/tools/pdf-parser/) | A tool to parse PDF files and identify fundamental elements. |
| [TrID](https://www.mark0.net/soft-trid-e.html) | A tool to identify file types from their binary signatures. |
| [ExifTool](https://exiftool.org/) | A platform-independent Perl library plus a command-line application for reading, writing and editing meta information in a wide variety of files. |
| [OfficeMalScanner](https://github.com/fboldewin/reconstructer.org) ^†^ | A tool for scanning Microsoft Office documents for malicious scripts or exploits. |
| [PDFStreamDumper](https://github.com/zha0/pdfstreamdumper) | A tool for security researchers to examine and understand the internals of PDF documents. |

^†^ *^Deprecated^*

## Registry Forensics Forensics

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------: |
| Zimmerman's [Tools](https://ericzimmerman.github.io/) | A collection of standalone tools for digital forensics analysis and incident response. |
| Zimmerman's [RecentFileCacheParser](https://ericzimmerman.github.io/) | A tool that parses the RecentFileCache.bcf file used by the Windows operating system. |
| Zimmerman's [AmcacheParser](https://ericzimmerman.github.io/) | A tool that parses the Amcache.hve registry file used by the Windows operating system. |
| Zimmerman's [ShellBags Explorer](https://ericzimmerman.github.io/) | A tool that provides a graphical interface to the Windows Shell Bags. |
| [regshot](https://github.com/Seabreg/Regshot) | A tool that takes a snapshot of the registry and compares it with a second one, done after doing system changes or installing a new software product. |
| [RegRipper](https://github.com/keydet89/RegRipper3.0) | An open-source tool for extracting, correlating, and analyzing information from Windows registry. |

## Steganography Tools

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------- |
| Rbcafe [Outguess](https://www.rbcafe.com/software/outguess/) | A universal steganographic tool that allows the insertion of hidden information into the redundant bits of data sources. |
| [SilentEye](https://github.com/achorein/silenteye/) | A steganography tool that provides a friendly GUI allowing you to hide files inside images or audio files. |
| [Stegdetect](https://github.com/BionicSwash/Stegdetect) | A tool used to detect steganographic content in images. |
| [StegoSuite](https://github.com/osde8info/stegosuite) | A free and open-source steganography software written in Java. |
| [OpenStego](https://github.com/syvaidya/openstego) | A software for hiding secret data in various kinds of files. |

## Data Recovery Tools

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------: |
| [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) | A file data recovery software designed to recover lost files from digital camera memory or hard disks. |
| [TestDisk](https://www.cgsecurity.org/wiki/Testdisk) | A powerful free data recovery software designed to recover lost partitions and/or make non-booting disks bootable again. |
| Stellar [Data Recovery](https://www.stellarinfo.com/) | A comprehensive data recovery software to recover files, folders, documents, photos, videos, and more from both internal and external hard disks, USB flash drives, and other storage media. |
| [Recuva](https://www.ccleaner.com/recuva) | A superior file recovery tool that can help you recover your deleted files quickly and easily. |
| Runtime [GetDataBack](https://www.runtime.org/data-recovery-software.htm) | A highly advanced data recovery software that can help you get your data back when your drive's partition table, boot record, or FAT/MFT has been damaged or lost. |
| EaseUS [Data Recovery Wizard](https://www.easeus.com/datarecoverywizard/free-data-recovery-software.htm) | A complete data recovery software that can recover files from hard drive, USB drive, memory card, mobile phone, digital camera, iPod, and other media. |

## Cloud Forensics Tools

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------: |
| [Docker Explorer](https://github.com/google/docker-explorer) | A tool for exploring Docker images, containers, and registries. |
| Magnet [AXIOM Cloud](https://www.magnetforensics.com/resources/axiom-cloud/) | A comprehensive digital forensics tool that can extract evidence from multiple cloud sources. |
| Cellebrite [UFED Cloud Analyzer](https://cellebrite.com/en/ufed-cloud-analyzer-5/) | A tool that extracts and collects data from social media, cloud, and collaborative services. |
| MSAB [XRY Cloud](https://www.msab.com/product/xry-extract/xry-cloud/) | A cloud data extraction tool that retrieves data from cloud-based apps and services. |
| Belkasoft [Cloud Extractor]() | A tool that allows investigators to extract and analyze data from cloud services. |
| Amazon [AWS](https://aws.amazon.com/mp/scenarios/security/forensics/) | A comprehensive cloud services platform provided by Amazon that offers computing power, database storage, content delivery, and other functionalities. |
| Microsoft [Azure CLI](https://learn.microsoft.com/en-us/cli/azure/) | A command-line tool that provides commands for managing Azure resources. |
| Microsoft [Office 365 eDiscovery Export Tool](https://learn.microsoft.com/en-us/purview/ediscovery-configure-edge-to-export-search-results) | A tool used to export content searches results to a local computer from Office 365. |
| Google [Cloud SDK](https://cloud.google.com/sdk/) | A set of tools that allows for the management of resources and applications hosted on Google Cloud. |

## Browser Forensics

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------: |
| Nirsoft [ChromeCacheView](https://www.nirsoft.net/utils/chrome_cache_view.html) | A small utility that reads the cache folder of Google Chrome Web browser. |
| Nirsoft [MZCacheView](https://www.nirsoft.net/utils/mozilla_cache_viewer.html) | A small utility that reads the cache folder of Firefox/Mozilla/Netscape Web browsers. |
| Nirsoft [WebCacheImageInfo](https://www.nirsoft.net/utils/web_cache_image_info.html) | A tool that allows you to display the images stored in the cache of your Web browser. |
| Nirsoft [MyLastSearch](https://www.nirsoft.net/utils/my_last_search.html) | A utility that reveals the searches made on the most popular search engines. |
| Nirsoft [Web Browser Tools](https://www.nirsoft.net/web_browser_tools.html) | A collection of utilities aimed at providing insights into how browsers store data. |
| Nirsoft [BrowsingHistoryView](https://www.nirsoft.net/utils/browsing_history_view.html) | A tool that displays the details of all Web browser histories in one table. |
| Sysinternals [Strings](https://learn.microsoft.com/en-us/sysinternals/downloads/strings) | A Sysinternals tool used to extract strings of text from files. |
| Magnet [AXIOM](https://www.magnetforensics.com/products/magnet-axiom/) | A comprehensive digital forensics tool that can extract evidence from multiple sources. |
| PassMark [OS Forensics](https://www.osforensics.com/) | A suite of forensic tools that allows you to extract digital artifacts from computers quickly with high performance. |

## Email Forensics

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------: |
| [MailXaminer](https://www.mailxaminer.com/) | An advanced email investigation tool that supports over 20+ email formats and applications. |
| SysTools [MailPro+]https://www.systoolsgroup.com/mail-pro-plus.html() | A professional tool for email conversion, search, and forensics. |
| [Autopsy](https://www.autopsy.com/) | A digital forensics platform and graphical interface to The Sleuth Kit and other digital forensics tools. |
| [Aid4Mail](https://www.aid4mail.com/) | A tool specialized in processing, converting, and migrating email and mailbox files. |
| [Xtraxtor](https://xtraxtor.com/) | An email extraction tool that allows you to extract and save emails from various sources. |

## Malware Analysis

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------: |
| hex-rays [IDA Pro](https://hex-rays.com/IDA-pro/) | A disassembler for computer software which generates assembly language source code from machine-executable code. |
| Sysinternals [Process Monitor](https://learn.microsoft.com/en-us/sysinternals/downloads/procmon) | An advanced monitoring tool for Windows that shows real-time file system, Registry and process/thread activity. |
| [Yara](https://virustotal.github.io/yara/) | A tool used in malware research and detection to identify files that meet certain conditions. |
| [Cuckoo Sandbox](https://cuckoosandbox.org) ^†^ | An open-source automated malware analysis system used for analyzing suspicious files. |
| MIT [PANDA](https://panda-re.mit.edu/) | An open-source Platform for Architecture-Neutral Dynamic Analysis. It is built upon the QEMU whole system emulator, and so analyses have access to all code executing in the guest and all data. |
| [CAPE](https://github.com/kevoreilly/CAPEv2) | An Open Source software for automating analysis of suspicious files. |
| [rkhunter](https://www.kali.org/tools/rkhunter/) | A Unix-based tool that scans for rootkits, backdoors and possible local exploits. |
| [Qu1cksc0pe](https://github.com/CYB3RMX/Qu1cksc0pe) | A tool that assists in the quick scope of malicious files during malware analysis. |
| [VirusTotal](https://www.virustotal.com/gui/home/upload) | A website that provides free checking of files for viruses, and is often used for detecting false positives. |
| [Hybrid Analysis](https://www.hybrid-analysis.com/) | A free online service for malware analysis. |

^†^ *^Deprecated^*

## Indicators of Compromise

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------: |
| [Maltego](https://www.maltego.com) | An open-source intelligence and forensics application used for gathering information about individuals, companies, websites, etc. |
| [ThreatConnect](https://threatconnect.com/) | A threat intelligence platform that allows organizations to aggregate and act upon threat intelligence data. |
| Cortex [AutoFocus](https://www.paloaltonetworks.com/cortex/autofocus) | A threat intelligence service that focuses on providing detailed analysis of malware and threats. |

## Pentesting Search Engines

<div id='tbl-Tools'></div>

| **Tool** | **Description** |
| :------: | :-------------: |
| [shodan.io](https://shodan.io) | Servers |
| [google.com](https://google.com) | Dorks |
| [wigle.net](https://wigle.net) | WiFi Networks |
| [grep.app](https://grep.app) | Codes Search |
| [app.binaryedge.io](https://app.binaryedge.io) | Threat Intelligence |
| [onyphe.io](https://onyphe.io) | Servers |
| [viz.greynoise.io](https://viz.greynoise.io) | Threat Intelligence |
| [censys.io](https://censys.io) | Servers |
| [hunter.io](https://hunter.io) | Email Addresses |
| [fofa.info](https://fofa.info) | Threat Intelligence |
| [zoomeye.org](https://zoomeye.org) | Threat Intelligence |
| [leakix.net](https://leakix.net) | Threat Intelligence |
| [intelx.io](https://intelx.io) | OSINT |
| [app.netlas.io](https://app.netlas.io) | Attack Surface |
| [searchcode.com](https://searchcode.com) | Codes Search |
| [urlscan.io](https://urlscan.io) | Threat Intelligence |
| [publicww.com](https://publicww.com) | Codes Search |
| [fullhunt.io](https://fullhunt.io) | Attack Surface |
| [socradar.io](https://socradar.io) | Threat Intelligence |
| [binaryedge.io](https://binaryedge.io) | Attack Surface |
| [ivre.rocks](https://ivre.rocks) | Servers |
| [crt.sh](https://crt.sh) | Certificate Search |
| [vulners.com](https://vulners.com) | Vulnerabilities |
| [pulsedive.com](https://pulsedive.com) | Threat Intelligence |

## Hacking Practice

<div id='tbl-Tools'></div>

| **Tool** |
| :------: |
| [TryHackMe](https://TryHackMe) |
| [HackTheBox](https://HackTheBox) |
| [Pentester Lab](https://Pentester Lab) |
| [TCM-Security](https://TCM-Security) |
| [Vulnhub](https://Vulnhub) |
| [Offensive Security](https://Offensive Security) |
| [VulnMachines](https://VulnMachines) |
| [Portswigger Web Security Academy](https://Portswigger Web Security Academy) |
| [Hacker101](https://Hacker101) |
| [PicoCTF](https://PicoCTF) |
| [HackMyVM](https://HackMyVM) |
| [Try2Hack](https://Try2Hack) |
| [Cybrary](https://Cybrary) |
| [RangeForce](https://RangeForce) |
| [LetsDefend](https://LetsDefend) |
| [BugBountyHunt3r](https://BugBountyHunt3r) |
| [CertifedSecure](https://CertifedSecure) |
| [CTFTime](https://CTFTime) |

