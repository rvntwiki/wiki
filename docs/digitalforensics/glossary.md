---
title: Glossary
Summary: Common terms and phrases used within digital forensics
Author: rvnt
Date: 2021-07-17
base_url: https://sanctum.rvnt.uk/
---

# Glossary
## General
### ACPO Guidelines
The Association of Chief Police Officers (ACPO) Good Practice Guide for Computer Based Electronic Evidence.

| **#** | **Principle** |
| ----- | ------------- |
| 1 | No action taken by law enforcement agencies, persons employed within those agencies or their agents should change data which may subsequently be relied upon in court. |
| 2 | In circumstances where a person finds it necessary to access original data, that person must be competent to do so and be able to give evidence explaining the relevance and the implications of their actions. |
| 3 | An audit trail or other record of all processes applied to digital evidence should be created and preserved. An independent third party should be able to examine those processes and achieve the same result. |
| 4 | The person in charge of the investigation has overall responsibility for ensuring that  the law and these principles are adhered to. |

### Digital Forensics
The application of investigative and analytical techniques to identify, preserve, extract, and document digital evidence in a way that’s suitable for presentation in a court of law.

### Data Corruption 
Referring to errors in digital data when unintended changes are introduced during transmission, storing, processing, or reading, making the information unusable. 

### Encryption
The process of encoding information or data in such a way that only authorized users can access it.

### Decryption
The process of converting encrypted data back into its unencrypted form. If there is a large amount of data a symmetric key is used.

### Metadata
The data embedded within a file that describes the characteristics of the document. Although some metadata, such as modification dates and file sizes, can be seen by the user, other hidden or embedded information requires a technical expert to locate it.

### Steganography 
A means of hiding information within a seemingly ordinary message so that only the intended recipient knows of its existence. Although this art dates back more than 2000 years, digital steganography works by replacing bits in files, such as images or audio files, with secret data.

### Hash Value
The process of summarizing the contents of a file with an numerical unique value. Used to identify and verify digital evidence.

| **Hash** | **File** | **Value** |
| -------- | -------- | --------- |
| MD5 | Image.jpg | ca64f1ee77e6ebcbd1a5ab942362c157 |
| SHA-1 | Image.jpg | ee02a43ef5365b31eafe54adfbd68bcedf9beef0 |
| SHA-256 | Image.jpg | 7bcfb187b1b6d6a3f402889fa5fb16caf08d10ab1416873f896a3880a45e5712 |

### Hash Collision
A hash collision or clash is when two different files produce the same hash value.

| **Hash** | **File** | **Value** |
| -------- | -------- | --------- |
| MD5 | Image1.jpg | ca64f1ee77e6ebcbd1a5ab942362c157 |
| MD5 | Image2.jpg | ca64f1ee77e6ebcbd1a5ab942362c157 |

## Basic

### Deletion
The removal of data from a file system.

### File
Data stored on disk.

### Path
The folder hierarchy within a file system that indicates the logical storage location of a file. Also referred to as directories, files can be organized within different folders for efficiency or security’s sake.

### Program / Application
A set of computer instructions designed to perform a specific task.

### R.A.I.D.
A Redundant Array of Independent Disks, wherein individual storage devices are grouped to act as a single device. Disks can be grouped to act as a gestalt for either redundancy, increased bandwidth, or a combination of both. Common RAID levels include 0, 1, 5, and 6, as well as combinations of 1 and 0, known as 10, or 1+0.

### R.A.M.
Random-access memory is the temporary storage component of a computer. RAM is not intended to store data long-term, but rather cache it in a location that can be quickly accessed by the processor. RAM can be reallocated on the fly as either the operating system or individual applications require more or less of it. By design, the contents of RAM cannot be recovered after an extended power off or reboot.

### Removable Storage
Storage devices designed for transport between computer systems. e.g. A removable USB storage device.

### Wipe
The process of systematically overwriting any data resident in sectors upon a storage device. Wiping is used to eliminate the possibility of recovering any previously allocated or deleted data.

### O.S.
Operating System.

---

## Forensic Process
### Preservation
#### Acquisition / Imaging
Using forensic tools to create a bit for bit image of the original device or machine.

#### Bit-Stream Image
Used during the preservation process. A sector by sector or bit by bit copy of the original media that verifies each bit is a true and accurate copy.

#### Verification
A term used to refer to the hashing of both source media and acquired image to verify the accuracy of the copy

#### Write Blocker
A hardware device or software application designed to prevent an operating system from making any changes to the contents of a connected storage device. Typically used for imaging or triage purposes, write blockers exist because modern operating systems routinely make changes to connected storage devices

### Analysis
#### Live Analysis
The opposite of dead box analysis. Analysis is preformed on site, on a running machine rather than being sent to a forensic lab for imaging and analysis. Allows for volatile data to be acquired.

#### Keyword Search 
An examination technique used to identify relevant evidence on a digital device by searching the acquired content using a pre-determined list of keywords imported into the forensic tool, even if the word or phrase occurs in an unallocated space or deleted file.

#### Hash Matching
The process of comparing a list of hashes generated from an exhibit to a list of hashes of known illegal files.

#### File Carving
Searching for deleted or unallocated files based upon their header, footer, or other content. Used primary in recovering complete or partial files when their file system entries are no longer present.

### Admin
#### Chain of Custody
The chronological and documented order and paper trail that records the sequence of custody, transfer, analysis, and disposition of physical and electronic evidence.

---

## Files
### Header
The beginning bytes of a file that indicate its type and the means by which it can be decoded.

### Footer
The last bytes of a file that may also be used to indicate type of file.

### File Signature
The internal structure of the file, typically the header and footer that indicates the type of file.

---

## Units of Measurement
### Bit
The smallest unit of data in a computer, has a single binary value of 1 or 0.

### Byte
A unit of data used by a computer that consists of a group of 8 bits; and can be used to represent an alphanumeric character.

### Byte Conversion

| | | |
| - | - | - |
| 1 Bit | = | 0 / 1 (Binary Digit) |
| 8 Bits | = | 1 Byte |
| 1000 Bytes | = | 1 Kilobyte |
| 1000 Kilobytes | = | 1 Megabyte |
| 1000 Megabytes | = | 1 Gigabyte |
| 1000 Gigabytes | = | 1 Terabyte |
| 1000 Terabytes | = | 1 Petabyte |
| 1000 Petabytes | = | 1 Exabyte |
| 1000 Exabytes | = | 1 Zettabyte |
| 1000 Zettabytes | = | 1 Yottabyte |
| 1000 Yottabytes | = | 1 Brontobyte |
| 1000 Brontobytes | = | 1 Geopbyte |

---

## Storage Media
### U.S.B.
Universal Serial Bus

### H.D.D.
Hard Disk Drive, or hard drive. This is the physical disk that holds information.

### Fixed Storage
Also known as primary storage, fixed storage refers to the storage devices within a computer that hosts its operating system, installed applications, and user data. Typically either hard disks or solid state disks, fixed storage is not intended to be disconnected from a computer and transported to another computer for subsequent connection. Although possible, fixed storage is typically only removed for an upgrade or replacement due to failure. Fixed storage may lack external buses or protections for transport, such as weather sealing or shock absorption. 

### Active/Live Files
Data that is readily available and not deleted.  

### Allocated Data
Data that is present on a drive or storage media, not deleted or written over. 

### Allocated Space
The area of a hard drive or storage media used by a file system to track and store files and their associated metadata.

### Ambient Data
Data or information that is located in areas not normally accessible to the user. Usually is stored in the unallocated clusters, file slack, virtual memory, or other areas.

### Cluster
It is the smallest managed section of a HDD that contains a file. They vary in size depending on OS and HDD capacity.

### Cluster Sector
A group of sectors that make up the smallest unit of disk allocation for a file within a file system. It’s the smallest amount of space a file system can track.

### Sector
A grouping of bytes for addressing purposes within a storage device. Just as 8 bits are grouped into 1 byte, traditionally 512 bytes are grouped into a sector. Due to increased storage capacities, sectors may be grouped into larger byte allocations, such as 4096 bytes. Devices using sectors larger than 512 bytes are referred to as Advanced Format devices. Sectors are configured and assigned during the manufacturing process. Sectors are the smallest of data that can be written or read from a storage device. If only 1 byte within a file is changed, the entire sector containing the change must be rewritten. Sectors are not grouped linearly upon a storage device, but rather spaced to match the read/write capabilities of the device.

### Slack Space / File Slack
The remaining space within a cluster, starting from the end of the file until the end of the cluster.

 ![Glossary-SlackSpace](img/G-SlackSpace.png)

### Unallocated Space
The free space on a hard drive that can be used to store data. Sometimes files that may be of interest to an investigator are concealed here.

### Partition
This is the logical division of a storage device for a file system or Operating System installation.

### File System
The organizational system by which files are stored, modified, and accessed within most digital storage devices.

### Free Space
Space waiting to be used by a file system. Typically containing files that have been deleted and are waiting to be replaced with new allocation or files.

### Master Boot Record
A special type of sector at the very beginning of partitioned storage devices that holds information on how logical partitions are organized. It also contains an executable code to function as a loader for the operating system.

### GUID Partition Table
The successor to the master boot record, it allows for both more partitions and larger partitions to be created; as well as more robust Operating System support.

### Host Protected Area
A portion of a storage device reserved for maintenance and startup that is inaccessible to the end user.

### Logical Block Address (LBA)
A modern form of addressing the sectors of a storage device that replaced the Cylinder/Head/Sector system. Sectors are addressed using numbers, starting at zero and incrementing by one. Although flash memory is organized differently than magnetic media, the translation layer converts addressing into LBA format as well. 

### Physical Address
Physical address commonly refers to the sector in which data resides. The physical sector could be represented by either C/H/S or LBA addressing depending upon the age and design of the media.

### Swap File / Virtual Memory
The swap file is a temporary space used by the operating system in place of RAM, typically when RAM is occupied by large amounts of data. 

---

## File Systems

### FAT
The File Allocation Table file system was designed in the 1970s. A rudimentary file system, newer versions of FAT have been routinely released to make use of larger storage devices and newer features, such as long file names. Because it is supported by most operating systems, FAT32 and its successor exFAT are routinely utilized for removable media.

### FAT16
The file system used for most older systems, used a 16-bit binary number to hold cluster numbers. A volume using FAT16 can hold a maximum of 65,526 clusters, FAT16 was used for hard disk volumes ranging in size from 16 MB to 2,048 MB. VFAT is a variant of FAT16.

### FAT32
FAT32 is a version of the FAT file system that was introduced in 1996. It is an extension of Microsoft’s FAT16 file system. The purpose of FAT32 was to overcome the limitations of FAT16 and add support for larger media. FAT32 uses a 28-bit binary cluster number (not 32, because 4 of the 32 bits are "reserved"). FAT32 can theoretically handle volumes with over 268 million clusters, and will support (theoretically) drives up to 2 TB in size.

### exFAT
The Extensible File Allocation Table file system was introduced by Microsoft in 2006 and optimized for flash memory such as USB flash drives and SD cards. Designed to be a lightweight file system like FAT32, but without the extra features and overhead of NTFS and without the limitations of FAT32.

### NTFS
The New Technology File System is Microsoft’s proprietary replacement for the FAT file systems utilized by default with Windows Operating Systems. NTFS incorporates many modern file system features, such as journaling, delayed writes, and access control lists.

### HFS
Hierarchical File System is a proprietary file system developed by Apple Inc. for use in computer systems running MacOS. Originally designed for use on floppy and hard disks, it can also be found on read-only media such as CD-ROMs.

### HFS+
HFS+ is a journaling file system developed by Apple Inc. It replaced the Hierarchical File System (HFS) as the primary file system of Apple computers in 1998.

### AFS
Apple File System is a proprietary file system developed and deployed by Apple Inc. for MacOS. It aims to fix core problems of HFS+/

### EXT
The Extended File System, was implemented in 1992 as the first file system created specifically for the Linux kernel. It has metadata structure inspired by traditional Unix filesystem principles, and was designed to overcome certain limitations of the MINIX file system.

### EXT2
The ext2 or second extended file system is a file system for the Linux kernel. It was initially designed as a replacement for the extended file system (EXT).

### EXT3
EXT3 is a journaled file system that is commonly used by the Linux kernel.

### EXT4
The ext4 journaling file system or fourth extended filesystem is a journaling file system for Linux, developed as the successor to ext3. 

---

## Time

### MACB
A timestamp;

**M**odified, **A**ccess, MFT Entry **C**hanged, **B**irth (Created).

### MACE
A timestamp;

**M**odified, **A**ccessed, **C**reated, MFT **E**ntry Changed.

---
## Computer Forensics
### :fontawesome-brands-windows: Microsoft Windows 

#### Windows Registry
The Windows registry is a hierarchically structured database that is used to store data related to configuration settings, software and user preferences in a Microsoft Windows operating system. It contains entries and values that control the behavior of certain configurations and user preferences, as well as information for OS components and applications that operate at a low level.

#### Registry Hive
A hive in the Windows Registry is the name given to a major section of the registry that contains registry keys, registry subkeys, and registry values.

#### Shellbags
Stores information relating to a folders size, position, icon, sorting method, etc. Shellbags persist for deleted folders.

#### LNK Files
.lnk files are metadata files that link to other files on disk, typicall used as shortcuts.

#### Jump Lists
Another form of .lnk files, they provide an easy way to access files recently accessed via the task bar.

#### Prefetch
Prefetch pre-loads the files an application uses as it starts, improving performance by avoiding unnecessary disk reads. 

#### SuperFetch
SuperFetch is designed to identify the way the user uses applications and to look for patterns. SuperFetch records usage patterns and uses the Prefetch tool to pre-load RAM with the files for the applications that it expects the user to open next.

#### Amcache
The Amcache.hve file is a registry file that stores the information of executed applications.

#### Shimcache
Also known as AppCompatCache, is used by the operating system to identify application compatibility issues. Amcache and Shimcache can provide a timeline of which program was executed and when it was first run and last modified

#### Shell Bags
Contains the folder view settings of a directory. E.g. Icon layout and size, Window size, etc.

#### Compound Files
Compound File Binary Format (CFBF) is a compound document file format for storing numerous files and streams within a single file on a disk. It is a container, with little restriction on what can be stored within it. The file structure loosely resembles a FAT filesystem. The file is partitioned into Sectors which are chained together with a File Allocation Table (not to be mistaken with the file system of the same name) which contains chains of sectors related to each file, a Directory holds information for contained files with a Sector ID (SID) for the starting sector of a chain and so on. 

---

### :fontawesome-brands-linux: Linux

---

### :fontawesome-brands-apple: Apple MacOS

---

## Mobile Phone Forensics

### :fontawesome-brands-android: Android

| | **Logical** | **File System**  | **Physical** |
|-| ----------- | ---------------- | ------------ |
| SMS | Yes | Yes | Yes |
| Contacts | Yes | Yes | Yes |
| Call Logs | Yes | Yes | Yes |
| Media | Yes | Yes | Yes |
| App Data | Yes | Yes | Yes |
| Files | No | Yes | Yes |
| Hidden Files | No | Yes | Yes |
| Deleted Data | No | No | Yes |

#### Logical Extraction
Logical extraction of data is performed through a designated API available from the device vendor. Rhe API allows commercial third-party apps to communicate with the device OS, it also enables forensically sound data extraction.

#### File System Extraction
A file system extraction uses different device-specific methods to copy the file system. While these are comparable to the API used in logical methods, they use different sets of built-in protocols, depending on the OS. The mix of protocols often differs from device family to device family.

#### Physical Extraction
The most comprehensive and detailed extraction available accesses the additional data layers, in both allocated and unallocated space, that constructs the phones physical memory. These layers include three different groups of content pertinent to investigators: 

 - Logical content unavailable through API.
 - Deleted content.
 - Content that the phone collects without any user action.

### :fontawesome-brands-apple: iOS