---
title: Introduction to Cyber Security
Summary: Unit 1 - Introduction to Cyber Security
Author: rvnt
Date: 2023-07-24
base_url: https://sanctum.spctr.uk/
---

## Lesson 1: What is 'Cyber Crime'?
**(Q1)**

Cyber crime can be classed as any criminal activity which involves a computer, networked device or a network. Most cyber crimes are undertaken in order to generate profit for the people behind the attacks – the cybercriminals.

The majority of cyber crimes are carried out against computers or devices such as printers and routers directly. Cybercriminals also use computers or networks to spread malware (software designed to damage or gain unauthorised access to a computer system), images and illegal information.

Cyber crime can be carried out by individuals or small groups. However, recently there have been a significant number of attacks carried out by highly organised criminal groups. 

These groups usually include skilled software developers and others with relevant expertise. One reason that cyber crime is difficult to stop is that cybercriminals are able to carry out attacks from anywhere in the world.

### Types of cyber crime
There are many different types of cyber crime, and most cyber crimes are carried out with the expectation of making substantial amounts of money for the people carrying out the attacks.

For legal purposes, there are three broad types of cyber crime:
#### Cyber Dependent Crimes
Where criminals use digital systems to attack digital targets. These include attacks on computer systems to disrupt IT infrastructure, and stealing data over a network using malware. 

The purpose of the data theft is usually to commit further crime.

#### Cyber Enabled Crimes
Which are ‘existing’ crimes, such as fraud, extortion and identity theft, that have been transformed by the use of the Internet. In many cases, the use of the Internet allows these crimes to be carried out on a very large scale.

#### Internet Use
The use of the Internet to enable crimes such as drug dealing, people smuggling and other ‘real world’ types of crime.

---
## Lesson 2: Motives Behind Cyber Crime
**(Q2)**

### Common Motives for Cyber Crime
Cyber crime has increased greatly, partly because committing crimes online makes it easy for the criminals to hide their identity and location, which can be anywhere in the world that has an Internet connection. This makes it challenging for police and other law enforcement agencies to locate them, but to further complicate this, different countries have different laws and bringing cyber criminals to justice often involves cooperation between countries.

The key motivations for cyber criminals are money and information. According to a Verizon Enterprise report, around 93% of attacks are motivated by reasons related to financial motivation and corporate or government espionage (spying). Another less frequent but broader set of motives is often categorised as ‘FIG’ (Fun, Ideology, and Grudges).

The graph below shows the main motives for cyber criminals between 2010 and 2016.

![Cyber Criminal Motives](img/Motives.png)**

#### Money
This can be the motive for many types of attacks and data theft. The cyber criminal will make money either by stealing from the victim directly, or by selling stolen data in underground marketplaces.

#### Competition
Gaining access to a business competitor’s data can be very valuable. Many attacks are aimed at stealing IP (intellectual property) such as blueprints, designs for new technology or research. However, competitors also carry out attacks for blackmail, to gain the upper hand in a specific market or to create bad PR for a competitor. 

Organisations that have intellectual property at the core of their business, such as pharmaceuticals, high tech manufacturing, mining or utility companies, are especially vulnerable to attacks from competitors.

#### Political Motivation
In recent years, cyber crime has also been used for political reasons. This may take the form of actions such as attempting to manipulate elections, inserting malware or ‘spyware’ (software that allows a user to gain information from another computer by transferring data secretly from their hard drive) into government systems or hacking the personal accounts of government members to learn information.

#### Personal Reasons
There are a wide range of personal reasons why people may attempt cyber crime. Some criminals just enjoy the challenge of finding and exploiting weaknesses in systems and software. Other people may do it for ideological reasons, for revenge or to expose sensitive information which they think is of interest to the public.

### Examples of cyber crime attacks:
#### WannaCry virus attack, 2017
The WannaCry ransomware attack was a global attack where users’ files were held hostage, and a Bitcoin ransom was demanded for their return. 

Cyber criminals took advantage of a weakness in the Microsoft Windows operating system to gain entry to as many as 300,000 computers in 150 countries.

Thousands of NHS hospitals and surgeries across the UK were affected. The attack on the NHS was stopped by a 22-year-old former hacker turned cybersecurity expert who managed to find the ‘kill switch’ in the code and slow down the attack. 

The attack was estimated to have cost the NHS £92 million, with global costs from the attack at around £5 billion. 

A patch that could have stopped the attack had been released by Microsoft about two months earlier, but many users never downloaded it.

#### One billion user accounts stolen from Yahoo, 2013
In one of the largest cases of data theft in history, Yahoo had information from all of its three billion user accounts stolen.

Personal information including names, phone numbers, passwords and email addresses were taken from the Internet giant.

Yahoo claimed at the time that no bank details were taken, and only released information about the breach in 2017.

It was the second time Yahoo had suffered a major breach, after the accounts of nearly 500 million users were accessed in 2014.

---
## Lesson 3: Who Carries Out Cyber Crime?
**(Q3)**

### Definition of a threat actor
In cyber security, a threat actor is a broad term for any individual or group of individuals that attempts to conduct malicious activities against individuals or enterprises, whether intentionally or unintentionally.

Threat actors can be internal or external to the organisation being targeted.

Threat actors with the technical skills to target and breach security networks often fall into the category of hackers, but the term threat actor is broad and also includes security incidents initiated through negligence, mistake or social media.

#### Individual
This can include both insider and outsider actors. Individual cybercriminals may be motivated by gain or revenge. Some are employees who may carry out cyber crime intentionally, or unintentially through negligence, accident or incompetence.

#### Outside Actors
Individuals acting alone, for either money or revenge. These are a much smaller threat than organised crime as they usually lack the resources for mounting wide-spread attacks.

#### Inside Actors
These are people who already have access to sensitive data or processes from their work. They may be acting out of desire for money or revenge, or they may simply be careless or negligent. Insiders are less likely to trigger red flags or to cause alerts until it is too late.

#### Organised Crime
These are often highly sophisticated organisations who are motivated by profit. They usually target data that has a high value on the dark web, such as banking information, and also engage in ransomware attacks. These organisations may be quite large and will invest in technology and automation to improve their reach and scope.

#### Company
These may target other organisations, attempting to steal business secrets in order to gain a market advantage.

#### Nation
Actors sponsored by nation-states tend to be sophisticated and well-funded. They may carry out large-scale attacks or advanced persistent threats – stealthy attacks whose purpose is to gain long-term access to sensitive data. They may be motivated by gain, but are more usually motivated by national security, gaining IP data, political espionage, or attempts to influence the political process.

#### Hacktivist
Hacktivists are politically, socially, or ideologically motivated and target victims for publicity or to effect change, and often plan high profile operations.

#### Script Kiddie
These are actors who lack skills to write their own code, so they rely on scripts they have acquired from other sources. They may be motivated by peer competition, mischief or for gain, and their attacks are not very sophisticated, often being limited to defacing websites or launching denial-of-service attacks (DoS).These are attacks meant to shut down a machine. DoS attacks work by flooding the target with traffic, often from computers that have been ‘hijacked’ using malware.

#### Hacker
A hacker is any skilled computer user that uses their technical knowledge to overcome a problem. While ‘hacker’ can refer to any computer programmer, the term is most often used to refer to someone who uses their technical knowledge to access ‘secure’ systems and networks.

---
## Lesson 4: External and Insider Threats
**(Q4)**

Cyber threats may be internal or external. An external threat may be much trickier to identify as it comes from someone that does not have authorised access to the data and has no formal relationship to the company. These threats could be from someone who is actively trying to carry out a cyber attack on the company or it could have been triggered accidentally from someone who saw an opportunity, perhaps by finding or stealing a company computer or mobile phone. 

Threats coming from outside the company always aim to cause disruption and damage. They are carried out for the purposes of stealing data, for financial gain, or to disrupt the operation of the company.

Internal or insider threats are likely to come from someone who works at the organisation and has access to sensitive corporate data as part of their day-to-day duties. This could be someone working within the company, or who works for subcontractors, external IT support engineers or consultants. 

Insiders have a much greater advantage in being able to carry out cyber attacks because they already have access.

Recent cases highlight that many insider threats come from unhappy or disgruntled employees, such as someone who was informed they will be made redundant. 

These people may not carry out the attacks themselves, but may pass on or sell security or other inside information to those who will carry out the attack.

---
## Lesson 5: The Reasons People Carry Out Cyber Attacks
**(Q5)**

We have already seen that almost any business or organisation can be a target for a cyber attack, so it follows that there are many reasons for these attacks. The most common reason is financial gain, but there may be a host of other motivations. 

Attacks on different industries tend to have different motivations.

### Motivations for outsider/external attacks
Cyber attacks against businesses are often deliberate and are usually motivated by financial gain. However, other motivations may include:

- **Making a social or political point** – e.g. hacktivism.
- **Espionage** – such as spying on competitors to gain unfair advantage.
- **Intellectual challenge** – so-called ‘white hat’ hacking.

Attacks on healthcare providers may be designed to collect medical and personal data that can then be sold on. Stolen medical information can sometimes also be used to gain unauthorised entry to some medical programmes or to obtain prescription drugs for personal use or to sell for profit.

Attacks on food service, accommodation and retail establishments are most often done to collect customer payment information, such as credit card numbers and addresses, which can then be used to make unauthorised purchases and for identity theft. Similarly, attacks on financial services can glean sensitive banking and credit card information.

Public administration bodies are often victims of data breaches where thieves try to steal confidential government records to sell to foreign entities. They may also be made by hackers who want to make a political statement.

Hacktivists are another source of external threat. These people often want to make a political or social statement, or to demonstrate their expertise and earn ‘bragging rights’.

### Motivations for insider/internal attacks
Many insider attacks are also carried out for financial gain. For example, around 26% of internal hackers are based in systems administration. These employees have access to sensitive data and information and they may sell this data on to external sources. Other internal attacks may be motivated by a desire for revenge or to seek retribution for a slight or because they feel undervalued by their employer.

Technically proficient employees may use their systems access to open back doors into computer systems (a back door is any method that allows users or hackers to get around normal security measures to gain access to a computer system, network or software) or leave programmes on the network to steal information or wreak havoc. They may even sell access to these backdoors.

However, the largest number of internal attackers are actually people who have accidentally and unknowingly downloaded malware or leaked information accidentally. Many of these errors take place because an organisation is using outdated software, has failed to install needed updates and patches, or hasn’t invested in raising awareness among its employees. Social engineering attacks are also common. In this case, the employee has unwittingly given out a password or other information and does not know they are part of an attack. Social engineering attacks, like phishing, are also common.

Other insider attacks may be done for different types of personal gain. For example, in 2020, two former eBay employees pleaded guilty to a cyber stalking campaign against a Massachusetts couple whose online newsletter was seen as critical of the e-commerce company.

##### Example 1
An office administrator who stole almost £600,000 from her employers in order to fund her online gambling addiction was jailed for three years at the Crown Court in Londonderry.

Tracey Curran, 44, pleaded guilty to six charges of unlawfully taking the money from the Bank Of Ireland and American Express credit card accounts belonging to her employer.

Source: https://www.belfasttelegraph.co.uk/news/northern-ireland/woman-who-stole-600k-from-employers-to-feed-gambling-addiction-jailed-for-three-years-38850876.html


##### Example 2
The 2013 breach of American retailer Target is an example of a breach caused by employees acting unknowingly. Someone at a partner company opened an email infected with malware and poor internal controls allowed criminals to then gain access to Target. 

On top of this, many of Target’s own staff were using weak or even default passwords, and storing login credentials on servers where the hackers could access them. The company was also using outdated software with inadequate patching, creating more security holes.

---
## Lesson 6: Untargeted and Targeted Attacks
**(Q6)**

#### Untargeted
In untargeted attacks, attackers indiscriminately target as many devices, services or users as possible. They are not targeting a specific individual or business. To do this, attackers use techniques that take advantage of the open nature of the Internet. 

This type of attack includes techniques like:

- **Phishing** – sending emails to random people asking for personal information (such as bank details) or encouraging people to visit a fake website.
- **Water Holing** – setting up a fake website or redirecting people from a real website to order to collect personal information.
- **Ransomware** – a type of malware that encrypts a victim’s files to make them inaccessible. The attacker then demands a ransom from the victim, usually to be paid in crypto-currency, to restore access to the data upon payment.
- **Scanning** – attacking large parts of the Internet at random.

#### Targeted
Targeted cyber attacks

In a targeted attack, a particular industry, organisation or person is singled out for attack. The attackers might spend months laying the groundwork for the attack. This type of attack is often more damaging than an untargeted attack because it has been specifically directed at a particular target. 

Targeted attacks may include:
- Spear-phishing – sending emails to particular individuals which contain an attachment with malicious software, or a link that downloads malware.
- Deploying a botnet – a botnet connects together a number of Internet-connected devices, each of which is running one or more bots – a type of malware, without the owners’ knowledge. Botnets can be used to steal data, send spam, and allow the attacker to access the device and its connection.
- Subverting the supply chain – equipment or software being delivered to the organisation is implanted with malware.

### Differences between targeted and untargeted attacks
Untargeted attacks are far more common than targeted attacks, for two primary reasons. First, untargeted attacks are easier to carry out. Instead of trying to find out how to access a specific system, hackers simply create a generic email with malicious content such as an attachment or link. They will then send this out to every email address they have access to. 

Depending on the form of malware that was used, the goal of the attack might be extortion from ransomware, installing key loggers to track user credentials, or the installation of spyware. Since they are not targeting a specific person or audience, the content in the email is kept very vague. For instance, it may be a fake tracking link for a recent ‘purchase’.

---
## Lesson 7: What is a Cyber Crime Vulnerability?
**(Q7)**

A vulnerability is an oversight by an individual or a flaw or weakness in a system or network that could be exploited by a threat actor to cause damage, or allow an attacker to manipulate the system in some way. Individuals, organisations and nations all have different, but often overlapping, areas of vulnerability.

### Vulnerabilities faced by individuals
In terms of cyber security, one of the biggest vulnerabilities is people themselves. Lack of IT knowledge or lack of training can lead to people becoming an easy target for threat actors. The opening of emails and files which contain malware have led to numerous cyber security incidents in recent years, such as the ‘WannaCry’ malware attack on the NHS in 2017, which also affected computers in a further 149 countries. Individuals may also not have the latest security patches on their computers. Because fraudsters are coming up with new scams all the time, it can be very difficult for individuals to keep up with the latest types of attack and protect themselves.

### Internet of Things (IoT)
The Internet of Things (IoT), a system of interconnected devices is another area of risk. The IoT encompasses many ‘smart’ devices, such as Wi-Fi capable refrigerators, printers, manufacturing robots, webcams and cars.

The issue with these devices is that they can be hijacked to carry out further attacks. Worryingly, many businesses do not even realise just how many IoT devices they have on their networks – meaning that they have unprotected vulnerabilities that they are not aware of. These devices could represent a massive opportunity to attackers and a massive risk for businesses.

### Vulnerabilities Faced by Businesses and Organisations
For any organisation, there are a number of cybersecurity threats and network vulnerabilities that malicious actors can exploit.

These include:

- **Flaws** – These are vulnerabilities in software and applications that include coding errors. A market has grown in software flaws, with detailed information on some fetching hundreds of thousands of pounds. These may also include zero-day vulnerabilities, which are software vulnerabilities where there is no patch yet in place to fix it.
- **Network Vulnerabilities** – These result from insecure operating systems and network architecture. This type of vulnerability includes flaws in servers and hosts, misconfigured wireless network access points and firewalls, and flaws in network protocols.
- **Features** – Common features may be those intended to improve the user’s experience, help diagnose problems or improve management, which can also be exploited by an attacker. For example, JavaScript, which is widely used to develop dynamic web content, can be used by attackers to divert the user’s browser to a malicious website, and for hiding malicious code.
- **User Error** – This is by far the most common vulnerability for all types of companies. Common issues include:
    - Having weak login credentials (e.g. choosing a common or easily guessed password.)**
    - Leaving laptops or mobile phones unattended in a public place.
    - Opening email attachments from an unknown sender.
    - Leaving passwords in the open, such as on sticky notes.
    - Giving all employees access to everything, or having too many people with administrator access.
    - Exposure to social engineering/phishing attacks.
    - Lack of effective employee training.
    - Not updating systems and antivirus software.
- **Charities** – Suffering a data breach is serious for any organisation. Yet for charities, whose success is built upon their reputations and the goodwill of supporters, the loss of any sensitive information can be devastating. Charities often hold sensitive information, such as names, addresses and payment details, and may spend less on training and IT infrastructure.
- **International Organisations and Multi-National Organisations** – In addition to all of the vulnerabilities listed above, these may also be vulnerable to being targeted by government-sponsored hackers trying to keep tabs on the organisation, or hacktivists looking to publicise their own views.

### Vulnerabilities Faced by Nations
The risk of cyber attacks on governments is very high, and growing, as hackers develop more sophisticated tools. Many of the attacks faced by governments are not orchestrated for money, but for information or control – making the stakes for governments very high. This so-called ‘cyber warfare’ may be conducted for a variety of reasons, often to extract information, for money, to test vulnerabilities to a ‘hard’ attack, disrupt the economy, create national trauma or insecurity, or to engage in social engineering, such as influencing an election.

Threat actors who target nations are often well-financed and have support from a particular nation.

One area of vulnerability for nations is that systems for different departments are often linked together, allowing attackers to gain access to sensitive systems by attacking ones that are less well-protected. Because governments tend to have large and unwieldy bureaucracy, this can also make them slow to adopt the latest cyber defences or to upgrade outdated systems.

Governments also have a huge number of employees and people with access to various systems, making them vulnerable to the same type of techniques used against individuals and companies.

### Case study
In 2020, it was reported that implantable cardiac devices made by St Jude Medical were found to have vulnerabilities that could be exploited by hackers. Devices such as pacemakers and defibrillators had a transmitter that read the device’s data and shared it remotely with a physician. 

This transmitter had vulnerabilities which could allow hackers to gain access to the devices and interfere with their functions, such as using up the battery or giving incorrect pacing or shocks.

---
## Lesson 8: Potential Impacts of Cyber Crime
**(Q8)**

According to some estimates, by 2025, cyber crime will cost the world more than $10.5 trillion each year. This will have a significant impact on jobs, research and development, economic growth and investment.

As cyber crime increases, the costs to prevent it also increase. The damage is not limited to finances. There is also a high cost to intangible assets like brand reputation and customer good will.

#### Individuals
Individuals can be severely impacted by cyber crime, not only financially, as attacks may clear out bank accounts and impact credit ratings, but also psychologically. Individuals can end up feeling depressed, embarrassed, shamed or confused following the attack. 

Individuals may not understand why they have been attacked, and embarrassed that they have let something like this happen to them. Recovering from an attack may also be time consuming and involve setting up new accounts or speaking to a wide range of authorities.

#### Organisations
There are obvious financial implications faced by any organisation which has been attacked, potentially including a fall in stock price, regulatory fines and reduced profits. Organisations may lose key data or research to competitors resulting in huge costs and a loss of competitive advantage. Reputational impacts can include a loss of key staff, damaged relationships with customers and intense media scrutiny. Customers may not feel safe shopping with the organisation and so stop visiting their websites.

Companies now have to rethink how they collect and store information to ensure that sensitive information isn’t vulnerable. Many organisations have stopped storing customers’ financial and personal information, such as credit card numbers, home addresses and birth dates.

Cyber crime is also a rapidly growing threat to the charity sector. In the next two years, one in every six large charities will suffer from cyber crime. Many other charities will be the victim of cyber attacks without knowing about it. A charity is four times more likely to discover cyber crime through internal IT controls or by staff raising concerns than all other external sources combined.

Some larger charities believe they experience several thousand attempted cyber attacks every week.

#### Multi-Organisational
A multi-organisation enterprise is where different organisations work together collaboratively to deliver complex products or service systems. For example, the NHS is made up of a number of different organisations (GP surgeries, hospitals, clinics, etc.) that all work together to deliver health care.

For large organisations such as the NHS, their size makes them a high profile target. Digital Health Intelligence has reported that more than 50% of NHS trusts now have electronic patient record systems which hold vast amounts of sensitive personal data.

What is the impact for organisations such as the NHS? It is predominantly financial loss, for example, extortion using ransomware or theft of data such as staff or user bank account details.

An international organisation is one that has branches, or conducts business, in different countries. International organisations face an even greater risk of cyberattack, as it may be necessary to work across borders, or to co-ordinate many different parts of the organisation, in order to fight the attacks. International organisations may also need to adhere to different types of laws, so they may also be at risk of larger fines and legal consequences if there is a breach.

#### Nations
As we have seen, nations are facing a huge increase in cyber espionage, including damage to infrastructure and key services. There is also evidence in some places of election tampering and other types of large-scale social engineering, reducing trust in governments and making it harder for them to act. 

State secrets, personal data on citizens, blueprints to military technology and political manipulation are all key areas in which cyber criminals are attempting to gain access. The impact of cyber attacks on states can be catastrophic, and cyber criminals have attempted to gain access to nuclear power plants, and vital facilities such as dams and electricity supplies.

---
## Lesson 9: Importance of Cyber Security
**(Q9)**

#### What is Cyber Security?
Cyber security is the practice of securing devices, networks, systems and any other digital infrastructure from malicious attacks. The best strategy, whether for individuals, companies or governments, is to have in place a strong security system that includes multiple layers of protection. 

This could include firewalls, antivirus software, antispyware software and password management tools.

#### The Importance of Cyber Security for Individuals
On an individual level, poor cyber security can lead to financial loss, identity theft and even personal safety risks. The best ways to prevent this are to exercise caution, use anti-virus software on all devices, always use strong passwords and never open or respond to suspicious emails.

Mobile phones are at serious risk of cyber attacks. To safeguard phones, users can install tools that lock the phone or enact multi-factor passwords in the event of a loss. 

To combat mobile apps that do not protect against viruses or that leak personal information, it is important to install cyber security tools that will alert or block suspicious activity.

Using public Wi-Fi is another risk for individuals. To secure against man-in-the-middle attacks, where communication is intercepted by a third party, cyber security experts suggest using the most up-to-date software and avoiding password-protected sites that contain personal information. 

One way to guard against a cyber attack on public Wi-Fi is to use a virtual private network (VPN). These create a secure network, where all data sent over a Wi-Fi connection is encrypted.

#### The Importance of Cyber Security for Businesses
Most businesses rely on computer systems to operate. A breach of their systems could result in substantial financial, as well as reputational, loss. 

In addition, General Data Protection Regulation (GDPR) and other data protection laws mean that organisations can suffer from regulatory fines or sanctions as a result of cyber crimes. This is particularly true for international businesses, who must make sure they meet the laws in all of the countries in which they operate.

It is vitally important that businesses invest in training, tools, and technology to reduce the risk of cyber crime. These should include tools that monitor third-party risk and vendor risk, and continuously scan for data exposure and leaks. 

All organisations should also have robust policies and procedures in place, as well as training programmes, so that all staff know how to guard against data leaks and common social engineering scams.

- **Multi-Organisational Businesses** – these include organisations like the NHS. These need to ensure that cyber security is applied uniformly across the entire enterprise – any gaps could allow threat actors to gain access to the entire system.
- **Charities** – organisations like charities, who may have overlooked this type of threat in the past, are now finding cyber security of vital importance. In 2022, 30% of charities experienced cybersecurity breaches.
- **International Organisations** – cyber security is vital for international organisations, which face all of the threats mentioned here, but also need to make sure that they co-ordinate their cyber security with the parts of the organisation in other countries. Some threats, technology and laws may be particular to a specific country, so international organisations may also need to hire experts in each country where they operate.

#### The Importance of Cyber Security for Nations
Here, the risk is often both more nebulous and more high stake. Concrete risks include attacks on infrastructure and defence systems, and theft of data which can lead to financial loss. 

Less concrete but equally important risks are those which impact on public safety, governmental reputation, the overall economy and international relations. 

To combat these risks, all countries should develop a robust national cyber security strategy which should include:

- a national cyber security agency
- a national Critical Infrastructure Protection program
- a national incident response and recovery plan
- clear laws relating to cyber crimes
- cyber security education and training.

---
## Lesson 10: Importance of Reporting Issues Promptly
**(Q10)**

#### For Individuals
Data breaches or cyber attacks should be reported immediately, so that relevant organisations, such as banks, can act quickly to stop the theft and hopefully return the money. In fact, any delay in reporting a financial loss from a data breach will often result in the permanent loss of money.

#### For Businesses and Government Employees
Reporting data breaches is a legal requirement. 

The GDPR introduced a duty on all organisations to report certain types of data breaches to the relevant supervisory authority. Failing to do so can result in heavy fines and penalties, and an investigation by the Information Commissioner’s Office (ICO).

Although the EU GDPR no longer applies to UK residents’ personal data, the Data Protection Act 2018 enacted the EU GDPR’s requirements in UK law, meaning that UK organisations must still comply with its requirements.

The law requires all breaches of personal data, such as customer information, whether they are accidental or the result of cyber crime, to be reported within 72 hours of being made aware of the breach. However, not all breaches need to be reported, only those which “pose a risk to the rights and freedoms of natural living persons”. This usually includes the possibility of affected individuals facing economic or social damage (such as discrimination), reputational damage or financial losses.

Aside from the legal requirements, notification can help others avoid similar risks, and may help the police to track down the criminals. Notification also helps other organisations prepare for similar attacks. Criminals often reuse successful techniques, whether it’s a particular scam method or a network vulnerability, and publicly announcing this threat allows organisations to address the issue.

##### Example 1
Personal data breaches can include:

- access by an unauthorised third party
- deliberate or accidental action (or inaction) by a controller or processor
- sending personal data to an incorrect recipient
- computing devices containing personal data being lost or stolen
- alteration of personal data without permission
- loss of availability of personal data.

##### Example 2
The ICO would need to be notified of the theft of any customer data because the data could be used to commit identity fraud or theft from individuals.

##### Example 3
A hospital suffers a breach that results in an accidental disclosure of patient records. There is likely to be a significant impact on the affected individuals because of the sensitivity of the data and their confidential medical details becoming known to others. This is likely to result in a high risk to their rights and freedoms, so the ICO would need to be informed about the breach.

---
## Lesson 11: The Role of Key Organisations in the Cyber Security Industry
**(Q11)**

#### NCSC (National Cyber Security Centre)
The role of the NCSC is to help to make the UK a safe place to live and work online. Launched in October 2016, the NCSC is headquartered in London and brings together expertise from other UK and European organisations. It carries out its main role by supporting critical organisations in the wider public sector, industry, and business, as well as the general public.

More specifically, the NCSC:

- Understands cyber security, and distils this knowledge into practical guidance that they make available to all.
- Responds to cyber security incidents to reduce the harm they cause to organisations and the wider UK.
- Uses industry and academic expertise to support and grow the UK’s cyber security capability.
- Reduces risks to the UK by securing public and private sector networks.

#### GCHQ (Government Communications Headquarters)
The Government Communications Headquarters, better known as GCHQ, is a world leading intelligence, cyber and security agency with a mission to keep the UK safe. The agency employs highly skilled people who use cutting-edge technology, technical skills and wide-ranging partnerships to identify, analyse and disrupt threats. The priorities of GCHQ are set by the UK’s National Security Strategy and the decisions of the National Security Council, chaired by the Prime Minister, as well as the Joint Intelligence Committee.

The three key mission areas of GCHQ are:

- **Counter terrorism** – Stopping terrorist attacks in the UK and against our interests overseas.
- **Cyber security** – Making the UK the safest place to live and do business online.
- **Strategic advantage** – Managing the threats from hostile states, promoting the UK’s prosperity and shaping the international environment.

#### ICO (Information Commissioner’s Office)**
The ICO is an independent body established to uphold information rights.

The role of the ICO is to uphold information rights in the public interest. More specifically, the agency’s role is “to ensure that personal information is kept secure and safe.”

The ICO is responsible for ensuring organisations follow the requirements of the following three pieces of legislation. We will look at these in more detail later in the online course.

- Data Protection Act (2018).
- General Data Protection Regulation.
- Freedom of Information Act (2000).

#### Scotland
Scotland has its own Information Commissioner. The Scottish Information Commissioner and the UK Information Commissioner’s Office (ICO) have separate roles and responsibilities.

In general, the Scottish Information Commissioner is responsible for the freedom of information compliance of all public authorities in Scotland, while the ICO is responsible for Data Protection rights for the whole of the UK, including Scotland.

The ICO is also responsible for freedom of information compliance for public authorities in England, Wales, and Northern Ireland, and for any agencies that operate in both Scotland and another part of the UK. 

The ICO also has regulatory power under the Freedom of Information Act for UK public authorities based in Scotland. 

These include:

- BBC Scotland (in relation to its public functions)**
- The Northern Lighthouse Board.

---
## Lesson 12: General Job Functions Carried Out by Cyber Security Professionals
**(Q12)**

Cyber security professionals have a wide range of responsibilities, but their main role is to protect electronic data from being compromised. As a cyber security professional, you can expect to safeguard an organisation’s files and network, install firewalls, create security plans and monitor activity.

#### Responsibilities of the Cyber Security Professional
New security threats pop up all the time, and IT security professionals need to stay up to date with the latest tactics employed by hackers. In addition to the high-level responsibilities already mentioned, some specific duties include:

- Set and implement user access controls and identity access management systems.
- Monitor network and application performance to identify any irregular activity.
- Perform regular audits to ensure security practices are compliant with government regulations.
- Use endpoint detection and prevention tools to thwart malicious hacks.
- Set up patch management systems that update applications automatically.
- Implement comprehensive vulnerability management systems across all assets, both on-premises and in the cloud.
- Work with IT teams to set up a shared disaster recovery/business continuity plan.
- Work with HR and/or team leads to educate employees on how to identify suspicious activity.

---
## Lesson 13: Hacking
**(Q13) (Q14)**

#### What is a hacker?
The basic definition of a hacker is someone who uses a computer system to gain illegal access to another system. There are different types of hacker, and they are not all interested in the same thing.

##### Black Hat
A black hat hacker is what people usually think of as the traditional ‘hacker’. They have malicious intent and are looking to break into and exploit vulnerable systems. Most hacks that you see in the news are a result of black hat hackers.

##### White Hat
A white hat hacker is what is known as an ethical hacker. They act to protect businesses and support them against black hat hackers. They are often hired by companies and organisations, and may help install effective protections, find vulnerabilities and provide solutions for them. They typically have authorisation to do what they are doing and there is even a qualification for them – Certified Ethical Hacker – from the EC Council.

##### Grey Hat
A grey hat hacker lives somewhere in the middle. Generally speaking, they are breaking laws and violating ethics, but their intent isn’t usually malicious. Usually, grey hat hackers will not exploit the vulnerabilities they find. However, this type of hacking is still considered illegal because the hacker did not receive permission from the owner prior to attempting to attack the system.

Grey hat hacking is sometimes done in the name of public interest, although quite commonly, if a grey hat identifies a flaw and points it out to a company, the company will want to work with the hacker to help fix the breach. Companies will often reward them just like they would a white hat.

However, the difference between grey hat hackers and white hat hackers is that, if the company decides to ignore a grey hat hacker, the hacker is not bound by ethical hacking rules or an employment contract. They could decide instead to exploit the flaw themselves or sell and share the knowledge online for other hackers to take advantage of.

---
## Lesson 14: Key Skills for Cyber Security Specialists
**(Q15)**

The key skills for cyber security specialists are:

- **Strong IT and digital skills and knowledge of hardware, software and networking systems** – the exact systems used will vary from employer to employer, but you should have a good basic knowledge that you can build on.
- **Communication** – being able to communicate what issues you have found and how to fix them, in a way that anyone can understand, is very important in any positions.
- **Analytical skills** – cyber security requires the ability to think through a problem and work out solutions in a logical manner.
- **Team working** – many issues will be too large for one person to manage, so it is important that you are able to work effectively in a team, and that you can not only get along with others in the team, but can help others and be willing to learn.
- **Project management** – cyber security often involves organising large projects, and being able to create a plan to tackle these effectively and in a timely manner is a vital skill.
- **Problem solving** – cyber security is all about finding problems and then working out how to solve them. This requires the ability to think logically, and also the mindset of not wanting to give up until the problem is solved.

With the pace of development in IT security, you will also need ongoing training and certification in new skills. Cyber security professionals need to continue learning new technology skills to be able to resolve new security issues.

---
## Summary

In this section, we have learned about:

- Motivations for cyber crime
- Different types of threat actors
- External and internal threats 
- Targeted and untargeted attacks
- Cyber crime vulnerabilities of individuals, businesses, and nations
- The impact and importance of cyber crimes on individuals, businesses, and nations
- Key organisations and job functions in cyber security
- What hacking is, and different types of hackers
- Some of the key skills required for a cyber security professional.