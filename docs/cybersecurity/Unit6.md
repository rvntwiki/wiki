---
title: Working with Others
Summary: Unit 6 - Working with Others in Cyber Security
Author: rvnt
Date: 2023-07-24
base_url: https://sanctum.spctr.uk/
---

## Lesson 1: Team Dynamics
**(Q23)**

Team dynamics describes the behaviour and relationships between the people in a group. This could include how they interact, communicate and cooperate with each other. Teams that are able to do these things well can often accomplish more.

Team dynamics involves both psychology (for example, how well do people with different personalities work together) and practical issues, such as what types of written or verbal communication are most effective.

Team dynamics can also be impacted by company culture and structure, and by the senior management’s leadership style, but the strongest influences often come from within the group itself.

A group with a positive dynamic and attitude to teamwork is often easy to identify. Team members trust one another, they work towards a group goal or decision, and they help each other to make things happen.

Recent research has also shown that when a team has a positive dynamic, its members are twice as creative as an average group.

In a team with poor group dynamics, people’s behaviour may disrupt work. Because of this, the group may not come to any final decision, may be inefficient or unproductive, and they might make poor choices because group members did not work together.

### Some Causes of Poor Team Dynamics
Group leaders and team members can contribute to a negative group dynamic. 
Some of the ways in which this can occur are:

- **Weak Leadership** - When a team lacks a strong leader, more dominant members can compete to take charge. This can lead to a lack of direction, infighting, or a focus on the wrong priorities.
- **Too Much Deference to Authority** - This can happen when people want to be seen to agree with a leader, and therefore hold back from expressing their own opinions.
- **Blocking** - This happens when team members behave in a way that disrupts the focus of the group. Blocking behaviour includes:
    - Being aggressive, disagreeable or inappropriately outspoken.
    - Being highly critical of others’ ideas.
    - Not participating in the discussion.
    - Being boastful or dominating the session.
    - Using humour at inappropriate times.
- **Free Riding** - Some group members take it easy, and leave their colleagues to do all the work. Free riders may work hard on their own, but limit their contributions in group situations; this is also known as “social loafing.”
- **Groupthink** - This happens when people place a desire for agreeing above their desire to reach the right decision. This prevents alternative solutions from being fully explored.
- **Evaluation Apprehension** - This happens when people feel that they are being judged very harshly by other group members, and they hold back their opinions as a result.
- **Micromanagement** - When team leaders or managers make all the decisions for the group, causing team members to stop putting in effort as they feel nothing they do will have any influence.

###  Some Strategies for Strengthening Group Dynamics:

- Know your team.
- Tackle problems quickly with good feedback.
- Define clear roles and responsibilities.
- Break down barriers.
- Focus on communication.
- Pay attention.

### Team Dynamics in a Formal Setting Compared to an Informal Setting
In a formal group, the relationship between the members is very professional. Formal groups tend to have stated goals, timelines for achieving these goals and a clear and defined plan for success. For example, a team may be set up to identify future market opportunities. This will be done through a planned process that involves assigned roles and duties. Formal teams may work very efficiently, but there may be fewer opportunities for creativity and to introduce new ideas.

Informal groups tend to be less planned. The lack of formal structure may mean that personalities play a larger part in how work is accomplished. Informal groups may be very creative and innovative, but can sometimes fail to make progress because it may be difficult for them to make decisions.

---
## Lesson 2: Team Workings Versus Working Alone
**(Q24)**
Good teamwork skills can be critical for success in both professional and personal ventures. There will be times where you will have to work with someone else to achieve a goal. When this happens, it is important that you have the skills needed to work effectively with others. 

Some benefits of working with others:

- Group members compensate for each other’s weaknesses and share broad perspectives.
- Greater opportunity to learn from others.
- Make it easier to share ideas and brainstorm, so there is a greater opportunity for creativity and innovation.
- Workloads and large tasks can be shared, often making them go faster.
- Opportunity to improve communication skills and develop better relations and networking with colleagues.

Some benefits of working alone:

- Fewer distractions.
- Greater efficiency.
- No conflicts or personality clashes.
- No need to share responsibility.
- Fewer meetings and other events that can decrease productivity.
- More control over how and when you work and over project management.

---
## Lesson 3: Ways in Which Team Members Can Work Together to Make Use of Individual Strengths
**(Q25)**

Within a team setting, it is still important that team members work to their own strengths. It is the bringing together of these individual strengths which makes a team successful.

Individuals who know their strengths tend to create more effective teams.

When team members value each other’s strengths, they more effectively relate to one another, avoid potential conflicts, boost group working and create positive discussions.

Here are some ways that team members can make use of individual strengths:

- Discuss the strengths and weaknesses of individual team members, so that all team members can help each other and work to their strengths.
- Plan projects around the individual strengths of each team member.
- Pair up team members who are strong in one area with members who are weaker in that area, so the weaker member can learn new skills.
- Analyse project performance to identify the strengths and weaknesses of each team member.
- Team members who are strong in one area can offer training in that area.

---
## Lesson 4: Solving Team Conflicts
**(Q26)**

Conflict is pretty much inevitable when you work with others.

This is because everyone has different views, and sometimes those differences escalate to conflict. How you handle that conflict determines whether it works to the team’s advantage, or ends up damaging the team.

Conflict isn’t necessarily a bad thing. Healthy and constructive differences are often a part of teamwork. Conflict can arise when people have varying views, experiences, skills, and opinions. Team members should be aware of these differences and not let them lead to full-blown arguments.

Here are some steps that can help resolve conflict effectively:

- **Acknowledge the Conflict** – the conflict has to be recognised before it can be managed and resolved. Usually people ignore the first signs of conflict, perhaps because they hope it will just resolve itself. If you are concerned about conflict in the team, discuss it with the other members. Once the team recognises the issue, it will become easier to resolve.
- **Discuss the Impact** – as a team, discuss the impact of the conflict on team dynamics and performance. When everyone understands the damage conflict is having on the team and the project, people may be more willing to work towards resolving the issue.
- **Agree to a Cooperative Process** – everyone involved needs to work together to resolve the conflict. This may mean setting aside individual opinions or ideas for the time being and returning to them later, once things have cooled down.
- **Agree to Communicate** – the most important thing is for communication to remain open. Active listening is an important tool for having constructive conversations, as it helps everyone to understand where the other person is coming from.

Sometimes team members simply need to have their problems heard and discussed by the rest of the team. By looking at the argument together, the team can move forward in agreement.

Resolving conflict when it does arise in a quick and efficient fashion helps maintain a strong and healthy team environment. Remaining open to differing beliefs and ideas is vital, and learning to view conflicts from a co-worker’s perspective will help you become a more effective team member.

---
## Lesson 5: Importance of Reviewing Work Activities
**(Q27)**

Reviewing is the process of going back over the work that has been completed and ensuring it is still fit for purpose and meets the objectives set out at the start.  

This process allows team members to identify whether tasks are on track and whether key targets have been met. It also allows the team to identify what has gone well and what could be improved.

Reviewing is vital in ensuring the best quality work is produced within the timescales set.

Reviewing helps ensure that everyone is working to the same common goal. Work can sometimes veer off-track, and a regular review helps ensure everyone is moving towards the correct end point.

Sometimes a task may be allocated to someone who may not be the most effective. A review of work tasks helps ensure the right people are chosen and work is reallocated when not.

---
## Lesson 6: The Use of Interpersonal Skills for Working in Cyber Security
**(Q28)**

We all use interpersonal skills every day. Interpersonal skills describe how we communicate or interact with other people.

Lots of skills can be defined as interpersonal, including our awareness of ourselves and others; caring about others; collaborating and working with others; having clear communications; conflict management and resolution skills.

Employers often actively look for applicants who can work collaboratively, communicate effectively and have positive relationships with customers and co-workers.

Even if you have a very technical job, such as an IT role, you will need to interact with colleagues or clients regularly, often providing complicated information or having to listen carefully to requirements. Having excellent IT skills on your CV won’t necessarily be enough to get you the job.

IT professionals need to be able to interact successfully with others, as well as manage projects and teams.

### Some Key Interpersonal Skills

1. **Work Ethic** – Having a strong work ethic is viewed favourably by many organisations. This can be split into three distinct aspects:
    - **Professionalism** – this incorporates everything from how you present yourself to your appearance and how you treat others.
    - **Respect** – All workplaces will require you to work under pressure at some time or another, and being able to remain calm and respectful under pressure is vital.
    - **Dependability** – Employers and team members need to know they can count on you. This includes being on time, being prepared and delivering the work when you say you will.
1. **Verbal Communication Skills** – Being able to have clear conversations with other staff, managers and customers is essential to being an effective worker. This could include being a good listener, carrying out excellent presentations to customers, or working with other team members effectively.
1. **Questioning** – Knowing when to ask questions, and what questions are important to ask is an important interpersonal skill. You should not be afraid to ask questions, but you should also make sure that you are asking in an effective way, so that you receive the answers you need.
1. **Giving Information** – Working effectively with others will often include a willingness to help out team members and clients with answers to questions or practical help. This will create a spirit of collaboration.
1. **Clarifying** – This includes not being afraid to ask for clarification, as well as being willing to give clarification when asked for it. This may also include showing someone how to do something.
1. **Giving and Receiving Feedback** – Being open to feedback can help you develop both personally and professionally. View all feedback as a chance to learn. When giving feedback, it is important to focus on being constructive rather than negative.
1. **Listening** – Failure to listen properly can have disastrous consequences, from failing to follow through on a manager’s instructions to not completing a customer’s request. Active listening signals to others that you are taking what they say seriously and are willing to learn from them. It includes giving non-verbal signals, such as nodding and maintaining eye contact. This will build trust as the people you are collaborating with will feel heard.
1. **Collaboration** – Working collaboratively increases productivity and helps deliver better outcomes. Being able to collaborate, particularly in challenging situations, is a real positive for employers and yourself. It shows a clear, positive attitude and an enthusiasm for team working.
1. **Conflict Management** – This is an important skill, especially for those looking at leadership roles. This includes being able to put your views across, or defend the views of others, in a professional and respectful way.
1. **Positive Attitude** – Showing positivity, even in difficult situations, is important. It is best not to say anything negative about your current or past employer or colleagues even if you feel strongly about it. Employees with a positive attitude also tend to treat others positively, which creates a more harmonious and friendly working environment.
1. **Putting Across Your Own Views Clearly and Appropriately** – How you come across to others can speak volumes. Demonstrating kindness and courtesy is a great way of developing long lasting relationships with colleagues and managers.

---
## Lesson 7: Examples of when Interpersonal Skills are Used in Cyber Security
**(Q29)**

### Verbal and non-verbal communication skills
As an IT employee, you often have to explain technical processes in clear, easy-to-understand terms to customers and colleagues. Many of these people may not have the same level of technical knowledge as yourself. You may also need to promote security practices to diverse audiences. All these activities require verbal and written communication.

Having good written and verbal communication skills will make this much easier. Focus on how to explain your ideas both clearly and concisely – without adding un-needed descriptions or jargon words.

#### Collaboration
Solving security problems doesn’t happen in a vacuum, and it is likely that you won’t always have all the answers. There will be many situations, such as an urgent malware attack or the need to develop an entire security system, when you will need to work with others in a team. 

For example, the problem or task may be too large for one person to manage alone, or you may not have all the skills needed to tackle it. In this case, you will need to be able to collaborate with others and work in a team to solve the problem.

#### Questioning
The field of cyber security is always changing, as new threats and new solutions come along. This means that you will always be learning new things in your work. It is important to remember that you may not have all the answers, and you should be willing to ask questions and seek advice from those with more experience. 

The same is true for helping others by giving information. As an IT expert, clients and co-workers will look to you for help and information. A willingness to help others is an important interpersonal skill.

#### A Detail-Oriented Work Ethic
Steve Jobs once said, “Details matter; it’s worth waiting to get it right.” He once left an urgent voicemail on a Sunday morning for Vic Gundotra, Google senior vice president. The second O in Google’s logo on the iPhone was the incorrect shade of yellow, and Jobs wanted it fixed immediately.

In cybersecurity, the wrong logo colour may not be your responsibility, but many other small details might be. You will need to be detail-oriented to find the root of a data breach or analyse thousands of logs after an attack. Sometimes, you will have to do these things while working fast under pressure.

#### Listening
In cyber security, listening is an important part of paying attention. For example, using active listening can help you to understand why someone may be likely to click on a link in an email, and can help you to analyse problems and search for solutions.

#### Giving Information
Technology is always changing, and cybercriminals are always adapting and coming up with new ways of breaking into systems. Because of this, as a cyber security expert, it is very important that you are always learning new things. 

There will be many times when you will rely on someone else to give you information, and so it is very important that you are also willing to give out information that can help others.

#### Respecting Others' Opinions and Views
In cyber security, you never know where a good solution to a problem will come from. So, it is important to always listen to and respect the opinions of others.

#### Giving and Receivng Feedback
This is an excellent way to learn what areas you need to learn more and to help others to improve their knowledge as well. As we said above, cyber security professionals are always learning, so it is important for them to be aware of what they do not know.

#### Clarifying
Any time you are working in a technical field like cyber security, there will be times when you will need clarification or to have something explained in more detail. The same is true for those you work with. Many of your colleagues will not be knowledgeable about technical subjects, so you will need to clarify things for them so they can understand.

#### Putting Across Your Own Views Clearly and Appropriately
You will often be working with managers, clients and colleagues. This makes it important to put across your own views in a way that is appropriate and which other people can understand – no matter what their level of technical knowledge and experience. One way to do this is to always treat others with respect.

---
## Lesson 8: Working with Others Within an Organisation to Support Cyber Security
**(Q30)**

Effective cyber security requires that every individual, and every part of the organisation, actively participates. 

Effective IT departments can establish excellent policies and processes. But it takes only one mistake to give a cybercriminal the opening needed to cause a potentially disastrous breach. 

Clicking on links in phishing emails, opening attachments from unknown senders, using weak passwords – these are just some examples of how individual team members’ actions can create vulnerabilities.

Educating employees and then regularly reviewing their individual responsibility can go a long way towards reducing the organisation’s risk.

True cybersecurity teamwork requires that every individual within the organisation be responsible for ensuring they understand and follow the procedures set out by the cybersecurity team. All individuals and all groups need to work together as a team. This includes not just working with the members of your team, but also with other teams, sections or departments. In order to avoid problems when working with other teams, it will be important for you to find different ways of communicating, to suit different people. For example, you may need to simplify technical information for some people.

Teamwork and communication can ensure cyber threats are identified quickly and potential issues can be acted upon.

---
## Lesson 9: The Purpose of Written Communications in Cyber Security
**(Q31)**

As a cybercrime specialist, writing reports is an important part of your role. These may be viewed by your manager or by clients, and may even be used by the police, legal authorities, and perhaps even as evidence in a court of law, depending on the circumstance. Therefore, it is important that they are written clearly, efficiently and professionally. 

Communicating effectively in writing is essential in the IT industry. Within cyber security, there are numerous different types of written communication used. It is vitally important that the written communication is of a high standard, clear, detailed and fit for purpose.

Common types of written communications include a wide range of reports and fault logs. 

Reports specific to the cyber security profession include:

- Vulnerability reports.
- Penetration testing reports.
- Incident reports.
- Internal policy documents specific to to cyber security.

---
## Lesson 10: The Potential Audience for the Written Communications Used
**(Q32)**

Knowing or anticipating who will be reading what you have written is key to effective written communication. The first question to ask is...

“Who am I writing this for?” 

The answer to this question may not always be obvious.

In an IT role you may produce written communication for a wide range of people including:

- Line managers
- Senior management
- Team members
- External clients or suppliers
- All staff

Within the workplace, it is important to use the correct tone, language and level for your audience. For example, an annual report written on behalf of a large corporate organisation would be extremely professional in terms of its writing style, as it is read by investors. However, an email to a colleague notifying them of when you will install new software on their computer can be short and more informal.

As an IT professional, you also need to be aware of the level of information you need to provide. For example, not everyone will be able to understand complex technical details – you will need to know when to simplify your language and when you can use technical jargon. For example, explaining the inner workings of the security systems may be an information overload for someone requesting a password reset.

For an IT professional, it is important that the level of language used is relevant to the audience. The IT industry has always contained a lot of terminology and abbreviations, and whilst this may make a lot of sense to other IT professionals, non IT experts may struggle to understand the concepts if the language is not adapted to meet their needs. To get a clear message across, always make sure the level of language meets the needs of the audience.

In an IT role, you will have a wide range of audiences and will need to adapt your writing for each.

| Audience | Language | Level of IT Language |
| :------: | :------: | :------------------: |
| Colleagues in your IT Department | Usually informal | You can use technical language. |
| Colleagues in other departments | Informal / formal depending on reason | Use limited jargon and offer clear explanations. |
| Line managers | Formal | This will vary by manager, but try to keep language clear and concise |
| Clients | Formal | Use clear and concise explanation, but technical terms are usually okay. |
| Suppliers | Formal | Technical terms and jargon are fine and often expected. |
| Government bodies & agencies | Formal | This varies by agency, do not assume a high level of knowledge and check first. |
| Other IT professionals in other organisations | Informal / formal depending on reason | Technical language and jargon is expected. |
| Visitors to the organisation | Formal | Explain terms clearly and simply. |

---
## Lesson 11: The Type of Information That May Be Included in Written Communication
**(Q33)**

#### Vulnerability Reports
The purpose of a vulnerability assessment is to review security weaknesses in an information system. It evaluates if the system is susceptible to any known vulnerabilities, assigns severity levels to those vulnerabilities, and recommends solutions, if needed.

The vulnerability report sets out the key findings of the vulnerability assessment. It details the organisation's IT assets, security flaws, and the overall risk with regards to cyber security.

Vulnerability reports are often reviewed by senior management teams and it is important that they are written clearly and set out the details of any problems and recommended solutions in a clear and understandable way.

#### Penetration Testing Report
A penetration test report details the findings of the penetration test, a simulated cyber attack against a computer system which checks for exploitable vulnerabilities.

An effective penetration testing report should include an executive summary, a detailed report, and the raw data from the tests. The executive summary should be a very brief overview of the major findings, of around two pages in length.

The detailed report will include a comprehensive list of the findings and proposed solutions. The final section of the report usually includes the technical details and raw output from each of the tools used in the tests.

The penetration test report will be read by people with some technical skills and understanding, such as IT managers, security experts, and network administrators, so it can be written in more technical language.

#### Incident Report
Many organisations record cyber security incidents in some form of database, ranging from ticketing systems to excel spreadsheets or in-house software.

The cyber security incident database will contain details of any incidents that are investigated by the security team. Each incident will have information about when it was created and closed, who the investigator was, incident information and impact, etc. 

For most incidents, these reports tend to be quite short, so they should be written in a very clear and concise way.

#### Internal Policy Documents
Internal policy documents relate to the day‐to‐day administration of an organisation's internal policies, procedures and rules. 

For example, a cyber security professional may have responsibility for writing a policy document on how employees should report or respond to a suspected phishing attempt. Other types of internal policy documents might include a company email use policy or an IT use policy.

These documents are often read by all employees, including those without a high level of technical knowledge, so they need to be clear, easily read, and provide the right level of information to those who are reading them.

---
## Lesson 12: The Elements That Support Effective Written Communications
**(Q34)**

While written communication skills share many of the same features as verbal communication skills, there are some important differences. Where verbal communication uses body language and tone of voice to express meaning and tone, written communication relies on grammar, punctuation and word choice. Developing effective written communication skills requires practise and attention to detail.

In professional settings, great written communication skills are made up of a number of key elements. 

These include:

- Having a clear purpose.
- Having a clear message.
- Information should be accurate.
- The correct use of spelling, grammar and punctuation.
- Layout. (use of headings, section numbers, bullet points etc.)**
- An attention to detail.
- Using the right tone of voice for the intended audience.

#### Clear Purpose
Having a clear purpose helps your reader understand what you are saying or, at least, understand enough to know what questions they need to ask for further clarification. Writing in simple language and using shorter sentences can convey information with clarity.

Example: “We are implementing a new email policy to ensure that all employees can confidently rely on our email systems. See the details of the new policy below. If you have any questions, you may direct them to the head of IT.”

In this example, the writer sets out the goal of the message right away, then mentions the reason for the policy change and provides simple instructions to follow for further clarification.

#### Clear Message
It is important that your audience can easily understand what you are saying. Make sure that your writing does not contain a lot of ambiguous or confusing statements and that it is clear what you are saying or asking for. One way to do this is to keep sentences and paragraphs short, and to use bullet points.

#### Accuracy
This is especially important in cyber security, where you may be dealing with very important or complicated issues that can have legal consequences. Check your statements for accuracy and make sure that any opinions are clearly labelled as opinions (for example, by writing, “In my opinion…”). It is also easier to be accurate if you keep your sentences short and clear. Try to avoid embellishing factual statements with too much jargon.

#### Attention to Detail
This is vital in every aspect of cyber security, including written communications. In your reports, break problems down into smaller components and write about these individually. Attention to detail also includes elements such as making sure you have checked your spelling, double-checking any facts and figures used, and addressing your writing to the correct audience. It is very easy to get into trouble by sending emails to the wrong address, or by hitting “reply all”.

#### Correct use of Selling, Grammar, and Punctuation
Don’t assume that a spellcheck has caught every error – always read over your written communications before you send them. For longer or formal reports, it can also be helpful to have a colleague look over it and make sure it reads well. Sloppy spelling, grammar and punctuation can make you look unprofessional and change the meaning of your words to something you have not intended.

#### Tone of Voice
This refers to the language that you use, the writer’s attitude toward the reader and the subject of the message.

When writing, it is important to adjust your tone according to the circumstances. For example, in more formal types of communication, you need to know when to be direct, assertive, conversational, encouraging or apologetic, depending on the situation.

To set the right tone for your work-related writing, you should first ask yourself:

- What is the purpose of this document? Why am I writing it?
- Who is my reader, and what do they need to learn from my writing?

For most business messages, it is safest to use a more formal tone, a tone that is confident, courteous, and sincere. Your writing should always use non-discriminatory language, and write at an appropriate level of difficulty for the audience.

Some general guidelines to keep in mind when considering what kind of tone to use include:

- Be confident.
- Be courteous and sincere.
- Use appropriate emphasis and subordination.
- Use non-discriminatory language. (for example, don’t use words like ‘man made’ or assume your reader is a man)**
- Stress the benefits for the reader.
- Write at an appropriate level of difficulty.

#### Layout
This is most important when you are writing longer documents. A huge mass of text can be very off-putting to readers. In order to engage readers and keep their interest, break up your text with headings and sub-heading, section numbers, bullet points and other organising features. 

Make sure you choose a font and size that is simple and easy to read. Overly ornate fonts or very large font sizes do not look professional. When in doubt, keep it simple and use frequent headings and subheadings to organise your work.

---
## Summary
In this section, we have learned about:

- Team dynamics and working with others.
- Ways to resolve conflict in a team.
- Different types of interpersonal skills and how they are used in cyber security.
- Different types of written communication used in cyber security.
- How to create effective written communication.