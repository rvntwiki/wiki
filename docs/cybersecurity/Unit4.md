---
title: Common Threats
Summary: Unit 4 - Understand Common Threats To Cyber Security
Author: rvnt
Date: 2023-07-24
base_url: https://sanctum.spctr.uk/
---

## Lesson 1: Understand the Common and Emerging Threats to Cyber Security (Types of Cyber Attack)
**(Q1)**

To achieve their goals of gaining access or disabling operations, a number of different methods are deployed by cyber criminals. There are always new methods being developed, and some of these categories overlap.

#### Phishing
In phishing, cyber criminals craft emails to fool a target. The recipient might be tricked into downloading malware that’s disguised as an important document, for instance, or convinced to click on a link that takes them to a fake website where they’ll be asked for personal information, like bank user names and passwords. 

Many phishing emails are relatively simple and are sent to thousands of potential victims, but some are specifically crafted to target specific individuals.

#### Spear Phishing
This is a type of phishing attack where, instead of sending out a mass email, a particular individual or individuals are targeted. As with phishing, a spear phishing attack will often involve an email and an attachment, but it will be addressed to a specific person, such as a ranking official or someone involved in confidential operations within an organisation. 

In a spear phishing attack, the attackers usually perform reconnaissance methods before launching their attacks. For example, determining how they format their email addresses and using social media and other publicly available sources to gather information.

#### Vishing
This is another type of phishing attack, but one that uses a phone call. During a vishing call, a scammer uses social engineering to get you to share personal information and financial details. In one popular scenario, the scammer says they are from your bank or law enforcement and your account has been compromised, then they ask you to tell them your account details or direct you to a fake site to enter personal information.

#### Denial of Service
A denial of service attack is a brute force method to try to stop some online services such as websites from working properly. For instance, attackers might send so much traffic to a website that it overwhelms the system’s ability to function, making it unavailable. A distributed denial of service (DDoS) attack uses an army of computers, usually compromised by malware and under the control of cyber criminals, to aim the traffic towards the targets.

#### Zero-day Exploits
Zero-days are vulnerabilities in software that have been announced, but not yet fixed. The name comes from the fact that once a patch is released, each day there are fewer and fewer computers open to attack, as users download their security updates.

Some emerging threats include:

- **Man in the Middle** – This is a type of attack where the attacker intercepts and alters communication between two people or organisations so they can masquerade as one of those communicating and steal information.
- **Smishing** – This is any kind of phishing that involves a text message. The techniques are very similar to phishing, but use text messages instead of emails. It is particularly effective because people tend to be more inclined to trust a text message than an email. While most people are aware of the security risks involved with clicking on links in emails, they may not yet be aware of the risks of clicking on links in text messages.
- **Cryptojacking** – Cryptojacking is an emerging type of specialised attack that involves getting someone else’s computer to do the work of generating cryptocurrency for you (a process which is also called data mining). The attackers will install malware on the victim’s computer to perform the necessary calculations.
- **Watering Hole Attack** – Also called water holing, this works by identifying a website that’s frequented by users within a targeted organisation, or even an entire sector, such as defence, government or healthcare. The attacker identifies weaknesses in the main target’s cyber security, then manipulates the site to deliver malware that will exploit these weaknesses. The computer of anyone visiting or downloading material from that site is then infected with malware.
- **Synthetic Identity Fraud** – In this rapidly-emerging type of fraud, criminals don’t steal an identity, but instead create a new one. To do this, they might combine fake and real information, for example, mixing a fake name and address with a real identification number they have stolen or purchased. They then use this identity to open bank accounts and obtain credit cards. Fraudsters then use these credit cards to make large purchases or obtain bank loans. In the end, the fraudster will vanish without paying off any of the debt.

---
## Lesson 2: What is Meant by Malicious Software?
**(Q2)**

Malicious software, which is more commonly known as malware, is a piece of software that seeks to harm or compromise a computer system. Malware can be in the form of worms, viruses, trojans, spyware, adware, rootkits and more. 

All of these can steal protected data, delete files or install extra software not wanted by a user.

#### Botnet
A botnet is a network of computers that have been intentionally infected with malware so that they will perform automated tasks on the Internet without the permission (or knowledge) of the devices’ owners.

Personal computers, servers, and even webcams and smart devices like WiFi-enabled refrigerators can be used in botnets.

#### Malware
Short for malicious software, malware can refer to any kind of software that is designed to cause damage to a single computer, server or computer network. 

Worms, viruses and trojans are all varieties of malware and are distinguished from one another by the ways that they reproduce and spread. These attacks may be designed to render the computer or network inoperable, or grant the attacker root access so they can control the system remotely.

#### Trojan
A Trojan horse, or Trojan, is a type of malicious code or software that looks like a real piece of software but can then take control of a computer. A Trojan is designed to damage, disrupt, steal, or inflict some other harmful action on data or a network. 

Cybercriminals use forms of social engineering to trick users into loading and executing Trojans on their systems. Once activated, Trojans can give cybercriminals access to a system in order to spy or steal sensitive data.

There are many types of Trojan, including:

- **Backdoor** – this gives attackers remote control over the infected computer.
- **Rootkit** – these Trojans are designed to conceal certain objects or activities in your system. Often their main purpose is to prevent malicious programs from being detected.
- **Trojan-Dropper** – used by hackers in order to install Trojans and/or viruses.
- **Trojan-Game Thief** – these steal user account information from online gamers.

#### Ransomware
Ransomware is a form of malware that encrypts a victim’s files. The attacker then demands a ransom from the victim to restore access to the data. 

Users are given instructions for how to pay a fee to get the decryption key. The costs can range from a few hundred dollars to thousands, and are typically payable in cryptocurrency.

#### Spyware
This is unwanted software that infiltrates a computing device, stealing the Internet usage data and sensitive information. 

Spyware is a type of malware – malicious software designed to gain access to or damage a computer, often without the user’s knowledge. It may gather personal information such as bank account log in details or passwords to email accounts.

#### Virus
A computer virus is a software program that is loaded onto a user’s computer without their knowledge, and that performs malicious actions. 

These range from deleting files to potentially destroying hardware.

#### Worm
This is a type of malware that spreads copies of itself from computer to computer without any human involvement. Worms do not need to attach themselves to a software program in order to cause damage. 

Worms can modify and delete files, or insert malicious software onto a computer. Sometimes a worm’s purpose is just to make copies of itself – depleting system resources and overloading a shared network. They can also steal data, install a backdoor, and allow a hacker to gain control over a computer and its system settings.

---
## Lesson 3: Ways in Which Malicious Software Can Infect a System
**(Q3)**

## How malware can infect your PC
Malware can find its way on to your computer in a number of ways:

#### Spam Emails
The people who create malware often use tricks to try to convince users to download damaging files. This can be an email with a file attached that says it is a receipt for a delivery, a tax refund or an invoice for something you have bought. If the attachment is opened, users could end up installing malware onto their computer without their knowledge. 

A malicious email may sometimes be easy to spot – it could have bad spelling and grammar or come from an email address that people don’t recognise. However, these emails can also look like they come from a real business or someone you know. Some malware can also hack email accounts and use them to send malicious spam to any contacts they find. To prevent a computer from being infected, it’s a good idea to consider the following:

- If you aren’t sure who sent you the email, or something doesn’t look quite right, don’t open it.
- If an email says you have to update your details, don’t click on the link in the email.
- Don’t open an attachment to an email that you weren’t expecting, or that was sent by someone you don’t know.

#### Infected Removable Drives
Many types of malware can be easily spread by infecting removable drives such as USB flash drives or external hard drives. The malware can be automatically installed when you connect the infected drive to your computer. 

Some worms can also spread by infecting computers connected to the same computer network. There are several things you can do to avoid this type of infection:

- Run a security scan of your removable drives every time you plug them in.
- Disable the Autorun function so that it doesn’t automatically start when you plug it in.

#### Bundled with Other Software
Some malware can be installed while you are downloading other programmes. This includes software from websites. Some malware will try to install other software that will be detected as unwanted software. This can include new toolbars on your web browser or programs that show you extra advertisements as you browse the web. 

There will often be an option to opt out of installing this extra software by checking a box during installation. You can avoid installing malware or potentially unwanted software by:

- Only downloading software from the official vendor’s website.
- Taking your time to install the software, making sure you read exactly what you are installing.

#### Compromised Webpages
Malware can enter a computer through infected websites. As soon as you enter the website, the malware will download to your computer. The website might have been created solely for the purpose of trying to get malware installed on as many computers as possible, or it could be a legitimate website that has been compromised or hacked.

---
## Lesson 4: Methods for Removing Malicious Software from an Infected System
**(Q4)**

Every computer should have some form of anti-virus software installed. However, most anti-virus software is not used to detect malware. Computers should also have an anti-malware program to regularly scan, clean and protect them from potential Internet threats: viruses, spyware, trojans, bots, adware and worms.

### How to remove malware from your computer
There are many types of software that will help you to clear an infected system –some is designed for home computers and others specialise in clearing servers and networks. 

In general, clearing an infected system often involves the following:

- **Disconnect from the Internet** - This will prevent more of your data from being sent to a malware server or the malware from spreading further
- **Enter Safe Mode** often referred to as safe boot, this is a way to start your computer so that it allows only the most necessary software and programs to load. This will prevent the malware from loading and make it easier to remove.
- **Check the Activity Monitor for Malicious Applications** – This shows the processes that are running on your computer, and allows you to see how they affect your computer’s activity and performance. For example, you can see which applications are working the hardest: if you do not recognise the app, it may be malware.
- **Run a Malware Scanner** - These can remove most standard infections. You should install and run security software which provides protection against existing and emerging malware.
- **Verify Your Browser’s Homepage** - Malware can modify your web browser’s homepage to re-infect your computer.
- **Clear Your Browser’s Cache** - The cache is a temporary storage location on your computer where data is saved so your browser doesn’t need to download it each time. Clearing it can remove any malware that is still lurking.

If all else fails, it may be necessary to entirely reinstall the operating system and all applications and programs from scratch. This is why it is vital that you always have an up-to-date backup on an external drive.

---
## Lesson 5: The Importance of Perimeter Access Control
**(Q5)**

Physical perimeter access control includes those systems and technologies that protect people and assets in a facility and its grounds by blocking unauthorised physical intrusions across the perimeter. 

In cyber security, this may involve controlling access to an entire building, or to a single room, such as the server room. The goal is to prevent unauthorised access by people who could damage hardware or software, or steal information to use in setting up phishing or other types of attacks.

Physical security perimeters are used to safeguard sensitive data and information systems. These can include fences, barriers, gates, electronic surveillance, physical authentication mechanisms, badges, fingerprint scanners, reception desks, and security patrols. 

It can also include having backup systems in place to ensure that operations can continue if anything should happen.

Security perimeters are often set up in layers. For example, it may be relatively easy to enter a building, but more checks are needed to enter different parts of the building, with the highest level of checks used in the most sensitive areas.

Access control systems are used to make sure that those who are entering each layer are authorised to be there. 

Physical security perimeters may include fences, cameras or CCTV, locked doors, and alarms; access control systems often include badges, sensors, alarms, passwords, fingerprint scanners, etc. 

Essentially, it involves anything that is involved in the control of people, materials and vehicles through entrances and exits of a controlled area.

---
## Lesson 6: The Importance of Check and Challenge on the Premises
**(Q6)**

Check and challenge is the process of confirming someone’s identify and their reason for visiting the business premises or area. It is usually done at reception but can also be carried out by security professionals or through use of key cards or access codes.

Check and challenge is particularly important in areas where there is highly sensitive or classified data or work. Behaviour that may trigger check and challenge could include ‘tailgating’ (waiting until someone with a code or key card opens the door and then entering an area before the door closes and locks again), re-using visitors’ passes, or people who have talked their way through reception, for example, by posing as a delivery worker.

By carrying out check and challenge, organisations can ensure that the people accessing their site are there for the right reasons and not attempting to cause damage to their computer systems or steal data.

### How organisations can protect themselves from unauthorised entry

#### All Staff to Wear and Display Badges
If all permanent staff members do not wear ID badges, it makes it difficult to tell the difference between an intruder and a member of staff. If all staff members display an ID badge and all visitors are wearing a visitor pass, it becomes much easier to spot an intruder.

#### Allocate a Single Point of Entry and Exit for Visitors
One key security measure is for all visitors to enter and leave via the same door. This will allow reception staff to monitor all visitors and ensures that when a visitor leaves the premises they sign out and hand back the visitors’ pass.

#### Visitors Must Supply Valid Picture ID
Visitors must supply valid picture ID such as a driving licence or passport as proof of identification on arrival. This helps confirm the person coming into the building is who they say they are.

#### Visitor Badges Should Not Leave the Premises
How many times have you visited a business and kept the visitors’ badge afterwards? Preventing visitors from removing badges from the premises limits the chances of them being lost, stolen, or reused by an attacker.

#### Education of Employees
It is vital that employees have training in security procedures. It is also important that the employees know what action should be taken should an intruder be identified.

---
## Lesson 7: Identifying Possible Threats to a Business
**(Q7)**

### Personal devices
In addition to employees using their personal devices for company business, many organisations have started using a bring-your-own-device (BYOD) policy, where employees use their own computers, phones, etc. for work, bringing them to the office or using them when working remotely.

However, this policy also has a number of risks. These include the possibility that employees’ personal devices will not be as well protected as company devices. There is also an increased risk of infection from malware, for example, if the employee downloads an app from an untrustworthy source.

Because the employees will be carrying their devices to and from work, there may also be an increased risk of the devices being lost or stolen. Personal devices also tend to lack robust data encryption, or may not be regularly updated, so they may also be easier to hack. If employees are working remotely, such as from Wi-Fi hotspots in public, this could also leave them more vulnerable to hacking and to malware.

### Removable devices
Removable media and devices include:

- Optical discs – such as Blu-Ray discs, DVDs and CD-ROMs.
- Memory cards – such as Compact Flash card, Secure Digital card and Memory Sticks.
- Zip Disks/Floppy disks.
- USB flash drives.

Removable media provide a common route for the introduction of malware and the export of sensitive data. Removable media is very easily lost, which could result in the compromise of large volumes of sensitive information.

### Hardware
Physical hardware theft and loss are also huge risks to an organisation. The loss of a laptop, mobile device, or storage devices such as hard drives and flash drives can result in high costs and loss of reputation. 

Understanding how hardware theft and loss happens, and how to reduce the risks of both physical device and data loss, helps organisations to reduce the risks and the impact of a loss on their business.

Human error is one of the largest risk factors in hardware loss. Employees may accidentally leave laptops, phones and other devices in public places, such as on trains, and these may be found by fraudsters or those who sell them to fraudsters.

While most of us think that we would never lose a laptop, one study by Kensington showed that 84% of businesses surveyed had reported a lost or stolen laptop. In fact, data shows that, in the US, more than 12,000 laptops are left in airports each week, and most of these are never returned to their owners.

### Networking infrastructure
Network infrastructure refers to all of the resources of a network that make network or Internet connectivity possible. Network infrastructure includes both hardware and software, along with elements like servers, network hubs and switches, firewalls, Wi-Fi access points and routers.

Making sure the network infrastructure is robust, secure and protected is critical to cyber security.

Just like a secure physical perimeter can safeguard the material inside a building, a secure networking infrastructure can help safeguard data from hackers. A network perimeter is the secured boundary between the private and locally managed side of a network, such as a company’s intranet, and the public facing side of a network, often the Internet.

Network infrastructure could include:

- **Border Routers** - These direct traffic into, out of, and throughout networks. The border router is the final router under the control of an organisation before traffic appears on an untrusted network, such as the Internet.
- **Firewalls** - This is a set of rules specifying what traffic will be allowed to pass through or denied access. A firewall is usually more thorough at filtering traffic than a border router.
- **Intrusion Detection System (IDS)** - This acts like an alarm system for your network. It is used to detect and alert on suspicious activity. This system can consist of a single device or a collection of sensors placed at strategic points in a network.
- **Intrusion Prevention System (IPS)** - Compared to a traditional IDS which simply notifies administrators of possible threats, an IPS can attempt to automatically defend the target without the administrator’s direct intervention.
- **Demilitarised Zones/Screened Subnets** - DMZ and screened subnets are small networks connected directly to a firewall or other filtering device.
- **Secure open Wi-Fi** - An unsecured, or open, wireless network is one that does not require a Wi-Fi Protected Access (WPA, WPA2 and WPA3) code for access. Organisations should use either WPA2 or the newer WPA3 standard for encryption. Employees who work remotely should also use this security standard and should never connect to an open network.
- **Administration Passwords** - These give access to the router and should be changed regularly to prevent hackers gaining access to the network through the router.
- **Router** - These are used to connect devices to the Internet and to each other, as well as to create LANs. Routers may be vulnerable to unauthorised access from an attacker, such as through a rerouting attack, when the attacker manipulates router updates to cause traffic to flow to unauthorised destinations; or through a masquerade attack, when an attacker manipulates IP packets to falsify IP addresses.

---
## Lesson 8: Threats Presented by Remote Working
**(Q8)**

### Security challenges of remote working
The willingness to work remotely has never been greater than right now. However, companies who offer more remote work opportunities must look at a wide range of security challenges and so must respond accordingly to prevent unauthorised network access.

Allowing employees to access company data from offsite locations raises serious concerns about data encryption, the security of wireless connections, use of removable media and potential loss or theft of devices and data.

The following are some of the security challenges presented by remote working, to both workers and the organisation:

- **Fewer Layers of Security** - Organisations often use perimeter access controls, local area networks and other security tools, such as firewalls and dedicated work computers that come with encryption. Without these multiple layers of security, remote workers are more vulnerable to security threats.
- **Reduced Security on BYOD and Mobile devices** - Using personal devices and open Wi-Fi can expose data to threats from hacking. Many phishing attacks now use text messages or WhatsApp. Using a device that has these apps installed can leave the device, and the data on it, exposed to this type of attack. All employees should be required to use secure passwords and secure Wi-Fi, or a VPN with end-to-end encryption, on any devices they use for work. Organisations can also provide security themselves by installing authorised antivirus and security software.
- **Loss or Theft of Devices** - If employees are working outside their home, for example, in a coffee shop, there is added risk of losing a device or having it stolen.
- **Tracking and Managing Data in the Cloud** - Many businesses, especially those that operate almost entirely remotely, store and share work in the cloud. This can leave the data open to risks in the event that the cloud server is hacked. Cloud-based IT management platforms can be used to allow businesses to connect, monitor and secure data on the cloud, without any geographical boundaries.
- **Inadequate Backup and Recovery Systems** - In case of an accidental data loss, remote employees using their own devices often do not have adequate backups in place. This can be solved by having a centralised data backup and recovery programme for all remote devices used for work, or by using a cloud backup.
- **Damage to Equipment** - This may be more likely to happen when people are working remotely and in a less-controlled environment. For example, they may be working around pets or small children or in a place where food and drink can more easily be spilled on a device.
- **Mixing Up Work and Personal Use** - Remote employees who are using a BYOD device are probably using the same device for work and personal use, and could mix up personal and business data. So, if an employee’s laptop crashes due to malware that was accidentally downloaded on software for personal use, the business data is lost too. This is why having a backup is very important.
- **Legal Compliance** - Small businesses employing freelancers and remote staff can have a harder time ensuring they strictly abide by the rules of GDPR. This is because it is unclear how some of the rules apply to employees using BYOD devices. For example, how would the rules apply to an American citizen working for a US-based company, who is living and working remotely in the EU? All businesses with remote employees should have a clear policy on how they handle business data at all times.

---
## Lesson 9: Best Practice to Minimise Physical Threats to Cyber Security
**(Q9)**

### What is physical security and why it matters
Gaining access to a property can provide criminals with the ability not only to steal physical items from the premises, but also to potentially infect computers with malware or access data through the IT infrastructure.

Physical security includes any security measures that are taken to ensure that only authorised personnel have access to equipment and resources in an office or building. A well-designed physical security system protects the building, resources and equipment against theft, vandalism, natural disaster, sabotage, terrorist attack, cyber attack and other malicious acts.

### Practices to minimise physical threats

Physical security becomes more important in case of critical systems and facilities, for example: server rooms, production bays, data centres, nuclear reactors, electricity power grids, etc.

The following are examples of different system measures which can be used to protect against potential threats:

- **Access Controls** – Organisations need to have some way of controlling access to the premises. If the business is based at a small office with a single door for entry, a simple lock can be enough. But if there are many ways to gain entry, then more advanced methods are needed. This could include high-tech options such as finger-print scanning or a PIN-entry system.
- **Issue ID Cards** – One useful and effective security measure that all organisations should implement is providing ID cards for staff. It’s a simple step, but doing so ensures that anyone who wants to get access to an organisation’s building needs to show an ID card. When combined with a swipe system for access to the building or to different areas of the building, this adds another layer of security.
- **Car Park Security** – The car park for a property is a surprisingly vulnerable area, and it is important that organisations put the proper safety precautions in place. Car parks can be used as an area to conduct surveillance of your property. If there is no access prevention, getaway vehicles can be parked right next to the building to allow criminals to escape in the event of a physical breach. There are a number of different steps that can be taken to make a car park more secure. These include installing a security barrier, having a key card system or using a guard.
- **Automatic Lighting** – Another great physical security feature that can make a big difference to a business is to add automatic lighting. Motion sensing lights will illuminate anyone who is not where they are supposed to be, making it harder for people to sneak around. Often the threat of being seen is more than enough to put off a criminal from attempting to gain access to an area.
- **Surveillance** – This can be a strong deterrent against criminal activity. One common type of surveillance is CCTV. Another option is to hire a professional security firm to conduct patrols. This is expensive and is usually used by large organisations where the risk of a breach is worth the costs involved.
- **Create a Security Culture/Human Firewall** – One of the most important things that a business can do for their physical security is to ensure that all staff take security seriously. If staff are aware of the need to keep an eye out for any suspicious activity and report it as soon as possible, they can be a very valuable line of defence in terms of surveillance. Provide regular training sessions to staff about the importance of good security practice and the things that they can be doing to help the business be more secure.
- **External Barrier** – This can include fences, walls, razor wire, etc. and provides a first line of defence against intruders.

---
## Summary
In this section, we have learned:

- Common and emerging threats in cyber security
- Different types of malicious software
- Ways of identifying and removing malicious software
- Different types of physical threats to cyber security
- Threats from remote working, personal devices, removable devices, hardware and networking infrastructure.