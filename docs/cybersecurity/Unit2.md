---
title: Industry Terminology
Summary: Unit 2 - Understand Industry Terminology Used in Cyber Security
Author: rvnt
Date: 2023-07-24
base_url: https://sanctum.spctr.uk/
---

## Lesson 1: Key Terminology Used Within Cyber Security
**(Q16)**

#### Hardware
Hardware refers to the physical parts of a computer, server and related devices. Internal hardware can include motherboards, hard drives, CPU (central processing unit), power supply, GPU (graphics processing unit) and random access memory (RAM). 

External hardware can include monitors, keyboards, camera, touchpad, mice, printers, and scanners. 

Internal hardware is often referred to as components, while external hardware devices are referred to as peripherals. Together, they are all computer hardware.

#### Software
Software is the instructions that tell a computer what to do. Software includes the entire set of programs, procedures and routines associated with the operation of a computer or network.

There are two main types of software – system software and application software. System software includes the operating system and controls a computer’s internal functioning, as well as peripherals like monitors, printers and storage devices.

Application software tells the computer to execute commands and includes programs that process data, such as word processors, spreadsheets, database management, inventory and payroll programs.

Network software is used to coordinate communication between the computers linked in a network.

Application software may be stored in an internal or external memory device, such as a hard drive. When the program is in use, the computer reads it from the storage device and temporarily places the instructions in random access memory (RAM). This is called ‘running’ or ‘executing’ a program. System software that is permanently stored in the computer’s memory using read-only memory (ROM) technology is called firmware, or ‘hard software’.

#### Data Management
In cyber security, data management refers to the process of protecting data from unauthorised access and data corruption. Managing data securely includes three components: confidentiality, integrity and availability.

**Confidentiality** is about privacy and ensuring information is only accessible to those with a proven reason to see it.

**Integrity** is about making sure that information stored in a database is consistent and un-modified. Systems need to be designed to reduce the chance of human error when information is input and managed, and to make sure that the flow of information does not result in loss or alteration.

**Availability** is about information being available when it’s needed. System design must include appropriate access controls and checks so that the information in the system has consistency and accuracy, can be trusted as correct and can be relied on when providing information to the user.

### Networking in cyber security
A computer network is when two or more computers or virtual resources (such as the cloud) are connected to each other in order to share resources (such as printers), exchange files, or allow electronic communication. Computers may be networked together physically, using cables, or may be connected wirelessly.

Three common types of networks are:

- **A Local Area Network (LAN)** is a private network that connects computers and devices within a small area like a home, an office or a building. LANs enable a number of computers to share a variety of resources like hardware (e.g. printers, scanners, network storage devices), software (e.g. application programs) and data.
- **A Metropolitan Area Network (MAN)** is a computer network that connects computers within a metropolitan area; this could be a single large city, multiple cities and towns, or over a large area with multiple buildings. A MAN is larger than a local area network (LAN) but smaller than a wide area network (WAN).
- **A Wide Area Network (WAN)** is a network that exists over a large geographical area, and connects different smaller networks, including local area networks (LANs) and metro area networks (MANs). It allows computers and users in one location to communicate with computers and users in other locations. A WAN is essentially a network of networks. The Internet is an example of a WAN.

---
## Lesson 2: Differences Between a LAN and a WAN
**(Q17)**

The main difference between a LAN and a WAN is that a LAN connects local devices to each other, while a WAN connects LANs to each other, usually across multiple locations spread over large distances. A WAN may be limited to one corporation or organisation, or it may be accessible to the public, like the Internet. 

#### A LAN is used because:
- Computer resources like hard-disks, DVD-ROM, and printers can share local area networks which reduces the cost of hardware purchases.
- You can use the same software over the entire network instead of purchasing the licensed software for each client in the network.
- Data of all network users can be stored on a single server hard disk.
- It is easy to transfer data and messages over networked computers.
- Managing data from one location improves security.
- LAN users can share a single Internet connection.

#### Drawbacks to using a LAN:
- The LAN administrator can check the data files of every LAN user, which reduces privacy.
- A LAN network can reduce costs by sharing computer resources. However, the- initial cost of installing Local Area Networks may be high.
- Unauthorised users can access critical data of the company in case LAN admin is not able to secure a centralised data repository.
- A LAN needs constant administration as there may be issues related to software setup and hardware failures.

#### A WAN is used because:
- It covers a larger geographical area, which helps facilities located far apart to communicate more easily.
- It can include devices like mobile phones, laptops, tablets, computers, gaming consoles, etc.
- It allows software and resources to be shared by connecting with various workstations.
- Information and files can be shared over a larger area.

#### Drawbacks to using a WAN:
- The initial set up cost of a WAN network is high.
- It can be difficult or expensive to maintain a WAN network as you need skilled technicians and network administrators.
- It can take more time to resolve issues because of the involvement of different types of technologies.
- It offers lower security compared to other types of networks.

---
## Lesson 3: Cyber Security Terms
**(Q18)**

#### Vulnerability
A vulnerability is a flaw or weakness in a system or network that could be exploited by a threat actor to cause damage, or allow an attacker to manipulate the system in some way.

#### Cyber Risk
This is defined as “the potential of loss or harm related to technical infrastructure or the use of technology within an organisation.”

#### Cyber Attack
This occurs when cybercriminals try to gain illegal access to electronic data stored on a computer or a network. The intent might be to inflict reputational damage or harm to a business or person, or the theft of valuable data. Cyber attacks can target individuals, groups, organisations or governments.

#### Protection
While IT security protects both physical and digital data, cyber security protects the digital data on your networks, computers and devices from unauthorised access, attack and destruction. Cyber security includes both network security and computer security.

#### Recovery
This describes the steps taken to recover from a cyber attack. The exact steps will depend on the type of attack, but recovery may include determining what was lost, reinstalling software and data from backups, finding and eliminating viruses and malware, investing in new software to block attacks, developing new procedures to prevent attacks and changing passwords.

### A cyber or cyber security threat
This is an act that seeks to damage or steal data and disrupt digital life in general. They can be against individuals, businesses or nations. Cyber attacks include threats like computer viruses, data breaches, and Denial of Service (DoS) attacks:

- **Denial of Service (DoS)** – A Denial of Service (DoS) attack is an attack meant to shut down a machine or network, making it inaccessible to its intended users. DoS attackers accomplish this by flooding the target with traffic, or by sending it information that triggers a crash. The DoS attack might crash a website or deprive legitimate users, such as employees, account holders or the public, of access to the service or resource.
- **Distributed Denial of Service (DDoS)** – A DDoS attack occurs when attackers synchronise a DoS attack using multiple systems. However, with a DDoS attack, instead of being attacked from one location, the target is attacked from many locations at once. The distribution allows the attacker to make a more disruptive attack, and makes the attack more difficult to track down and stop.
- **Botnet** – A Bot is a type of software application or script that performs tasks on command, allowing an attacker to take remote control of an affected computer or device. A collection of computers infected with Bots is called a ‘botnet’, and is controlled by the hacker or ‘bot-herder’. Botnets are commonly used in DDoS attacks.
- **Trojan** – A Trojan is a type of malicious code or software that looks like a real piece of software but can then be used to take control of your computer. A Trojan is generally designed to disrupt operations, to steal data or to damage a network or system.
- **Ransomware** – A form of malware that deliberately prevents you from accessing files on your computer, holding your data hostage. They normally work by encrypting files and requesting that a ransom be paid in order to have them decrypted or recovered.
- **Spyware** – This is a type of malware that infiltrates your computing device, stealing your data and any sensitive information. Spyware may gather personal information such as your bank account log in details or passwords to email accounts.
- **Virus** – This is a software program that is loaded onto a user’s computer without the user’s knowledge, and performs malicious actions. These range from deleting files to potentially destroying hardware.

### Malware
This is a broad term that describes all forms of malicious software designed to wreak havoc on a computer. 

Common types of malware include:

- Viruses
- Trojans
- Worms
- Ransomware.

---
## Lesson 4: Organisational Strategies
**(Q19)**

An organisational strategy is the sum of the actions a company intends to take to keep its IT infrastructure secure. To keep its systems secure, an organisation (usually large in size) may build two separate functions which work together to meet the organisational strategy. 

These functions are the Security Operations Centre (SOC) and the Network Operations Centre (NOC).

#### SOC
A Security Operations Centre (SOC) is a central function within an organisation employing people, processes, and technology to continuously monitor and improve an organisation’s security, while preventing, detecting, analysing, and responding to cyber security incidents. 

An SOC acts like a central command post, taking in information from across an organisation’s IT infrastructure. The SOC then decides how security events will be managed and acted upon.

The key aims of an SOC are:

- To detect and respond to threats, keeping the information held on systems and networks secure.
- To respond better to threats by learning about the existing and potential threats. The threats from cybercriminals are constantly evolving and the SOC must keep up to date with these threats and adapt the organisation’s security.
- To identify negligent or criminal behaviours. This could be from either internal or external threats.
- To improve business intelligence about user behaviours to shape and prioritise the use of new technologies. For example, do key cards keep getting lost? Should the organisation look at finger print scanners instead?

#### NOC
A Network Operations Centre (NOC) is a centralised location where IT support technicians can supervise, monitor and maintain client networks. These centres may not necessarily be on site, and operations are often carried out by external suppliers. Their role is to ensure 24/7 uptime for all parts of the network.

**NOC services**
An NOC is a central point for software distribution and updating, performance monitoring, coordination with other networks, network troubleshooting and router and domain name management. 

Almost anything related to the network falls under the responsibility of the NOC. 

Key tasks include:

- Performance reporting and improvement recommendations.
- Firewall and intrusion prevention system monitoring and management.
- Network discovery and assessments.
- Optimisation and quality of service reporting.
- Patch management and whitelisting.
- Backup and storage management.
- Email management services.
- Voice and video traffic management.
- Antivirus scanning and remediation.
- Shared threat analysis.
- Policy enforcement.
- Application software installations, troubleshooting and updating.

### What is the difference between an NOC and an SOC?
Both the NOC and SOC serve critical functions – to identify, investigate and resolve issues. While the NOC is focused on network performance and availability, the SOC consists of tools and employees who monitor, detect and analyse an organisation’s full security health every minute of every day.

---
## Lesson 5: Current and Emerging Challenges in Cyber Security
**(Q20)**

The cyber security landscape is constantly changing as it tries to keep pace with the vast amount of cyber attacks. Here, we discuss the key challenges facing cyber security professionals, as well as some emerging threats which organisations and professionals are working hard to protect against.

### Current Challenges in Cyber Security
#### Phishing Attacks
Phishing is a cyber attack that usually uses email as a weapon. The goal of attackers is to trick the email recipient into believing that the message is from a trusted source, and to either click on or download an attachment, or navigate to a fake site and enter their personal details.

This is one of the oldest types of cyber attacks, dating back to the 1990s, and is still one of the most widespread. It is also becoming more sophisticated, with attackers using off-the-shelf tools and templates that very closely mimic real sources, like banks and government institutions. Nearly a third of all data breaches involve phishing.

#### Ransomware
Ransomware is a form of malware that encrypts a victim’s files. The attacker then demands a ransom, usually paid in Bitcoin or another cyber currency, in order to restore access to the data.

Ransomware is often downloaded in email attachments that appear to come from a trusted source – a form of phishing. 

Once the malware is downloaded, it can take over the victim’s computer. Some types of ransomware, like NotPetya, exploit security holes to infect computers without needing to trick users.

#### Increased Data Privacy Regulation
Europe’s GDPR law, which was launched in 2016, and the UK Data Protection Act 2018 and UK GDPR, which apply provisions of the GDPR to the UK, set out the key principles, rights and obligations for most processing of personal data in the UK. Companies operating in both the UK and the EU may need to comply with both the UK GDPR and the EU GDPR.

These laws give individuals more power over their data and enhance the responsibility of organisations in protecting the data they collect and use, including responsibility for data breaches. 

There are tough penalties for those companies and organisations that don’t comply. For example, in 2020 British Airways was fined $26 million for a data breach that occurred in September 2018, and Marriott International was fined £18.4 million for a data breach between 2014 and 2018.

#### Cyber Attacks on Mobile Devices
Research has found that up to 80% of fraudulent transactions may originate on mobile devices. As mobile devices increasingly touch every aspect of our personal and professional lives, they have become the focus for phishing, malware and other types of attacks. As many people now use their phones to conduct online banking, this makes mobile devices even more attractive to threat actors.

#### IoT Ransomware
The Internet of Things is made up of devices which are connected to the Internet and to each other. For example, home appliances, lighting and security cameras. Hackers can use these devices as a gateway to access more sensitive information within the network, as well as information in the devices themselves. Hackers are increasingly targeting IoT devices, especially those that control other devices, for ransomware attacks. Some hackers also aim to ‘brick’ the appliance or device with Brickerbots – rendering it useless.

For example, in 2019, a 14-year-old developed a piece of ransomware called Silex, which targeted IoT devices, for no apparent motive other than to cause financial loss. Silex works by destroying an IoT device’s storage, dropping firewall rules, and removing network configuration, ultimately causing the device to stop functioning.

#### The Dark Web
Websites like Wikipedia, Google, Amazon, YouTube, and Facebook are only a small part of the Internet. Beyond these ‘visible’ websites are others that are not readily available to the general public. That is where the Dark Web and the Deep Web can be found.

The Dark Web is a general term for a collection of websites on encrypted networks with hidden IP addresses – all of which give users strong anonymity. Because they are not indexed by traditional search engines, they can only be accessed with special anonymity browsers.

The Dark Web isn’t just for criminals. There are also legitimate reasons for using the Dark Web. In past years, it has gained popularity as a safe haven for whistle-blowers, activists, journalists, and others who need to share sensitive information, but can’t do so out in the open for fear of political persecution or retribution by their government or employer.

Police and intelligence agencies also monitor the Dark Web to keep tabs on terror groups and cyber criminals. Additionally, corporate IT departments frequently crawl the Dark Web in search of stolen data and compromised accounts, and individuals may use it to look for signs of identity theft.

In many circles, the Dark Web has become synonymous with the Internet. It now plays host to a number of media organisations involved in investigative journalism, such as ProPublica and Intercept. WikiLeaks – the website that publishes classified official materials – and similar sites, also have a home on the Dark Web. Even Facebook maintains a presence there in order to make itself accessible in countries where it is censored by the government.

#### Cloud Services
A cloud service is an Internet service made available to users on demand instead of from a cloud computing provider’s servers. Cloud vulnerability will be one of the biggest cyber security challenges in the future as businesses are increasingly using cloud applications and storing sensitive data related to their employees and business operations on the cloud. This makes a tempting target for malicious hackers. Data breach, misconfiguration, insecure interfaces and APIs, account hijacking, malicious insider threats, and DDoS attacks are among the top cloud security threats that firms will be facing.

On top of this, cloud companies like Google and Amazon which store data belonging to other companies will be open to large-scale hacking and deep cyber intrusions like the 2016–2017 Cloud Hopper attack that accessed data stored on six major cloud service providers. Access management is one of the most common cloud computing security risks, because it is a key point of attack for hackers.

#### Deepfakes
Deepfakes are fake videos or audio recordings that look and sound just like the real thing. Once requiring the expertise and skills of Hollywood special effects studios or intelligence agencies like the CIA, today anyone can download deepfake software and create convincing fake videos in their spare time.

A key purpose of deepfakes is to create misinformation and fake news to undermine trust.

As well as all the ethical issues, fake news can also have a damaging effect on hardware too. 

In the business world, AI-generated fake videos or audio recordings can be used to impersonate CEOs, steal millions from enterprises, spread incorrect and misleading information about companies that can affect their stock price and interrupt business operations.

In the future, deepfakes are expected to evolve into a sophisticated type of forgery, making it a huge cyber security threat.

#### AI-Enhanced Cyberthreats
This is related to deepfakes. Artificial intelligence (AI) and machine learning have disrupted every industry and are finding their way into the business mainstream. However, AI is also a boon for cybercriminals, who are developing ways to use AI to create malicious software that can adapt to evade those who are trying to eliminate it.  

Hackers could integrate fuzzing techniques – the process of sending intentionally invalid data in the hopes of triggering an error condition or fault – with AI to create an automated tool that detects system vulnerabilities.

---
## Lesson 6: Understanding Social Engineering
**(Q21)**

Social engineering is an attack that involves manipulating people into breaking normal security procedures in order to gain access to systems, networks or physical locations, usually for financial gain.

The criminals who carry out social engineering exploit the one weakness that is found in every person and organisation: human psychology. Usually using phone calls, emails and text messages, these attackers trick people into handing over access to sensitive information.

There are several common attack types that social engineers use to target their victims. These include phishing, spear phishing, vishing, and smishing.

Criminals use social engineering tactics because it is usually easier to exploit people’s willingness to trust someone than it is to discover ways to hack software. For example, it is much easier to fool someone into giving out their password than it is to hack their password.

Security is all about knowing who and what to trust. It is important to know when and when not to take a person at their word, and when the person you are communicating with is who they say they are.

It doesn’t matter how many locks, alarm systems, floodlights, fences with barbed wire, and armed security personnel you have; if you trust the person at the gate who says they are delivering pizzas and let them in without first checking to see if they are legitimate, you are completely exposed to whatever risk they represent. The same is true for cyber security. It doesn’t matter how much anti-virus software you have if you then tell someone on the phone your password details.

---
## Lesson 7: Benefits of Using Social Engineering for Cyber Criminals
**(Q22a) (Q22b)**

Phishing is a low-investment, low-risk, high-reward strategy for cyber criminals. Some of the techniques used don’t even rely on people being particularly gullible, and can snare almost anyone. For example, pharming is a type of social engineering where the cyber criminal redirects web traffic from legitimate sites to malicious clones that can be indistinguishable from the real site. Users think they are on a real site, and put in their personal information or payment details without realising they are on a fake site.

As soon as one scam is detected by authorities, new scams can be easily thought up, many of which respond to rapidly-changing world events. When vaccinations for the coronavirus first became available in the UK, scammers sent out phishing emails within days, pretending to be from the NHS, and directing people to enter their personal details on a cloned website in order to be scheduled for a shot.

Social media is also an easy place to gather information, especially as people often let their guard down while on social sites. For example, angler phishing uses spoof customer service accounts on social media to get people to divulge personal details. Fear is another strong motivator. Scareware takes the form of a pop-up that warns that your security software is out of date or that malicious content has been detected on your machine – fooling victims into visiting malicious websites or buying worthless or non-existent products.

Because these attacks rely on human error, many people are too embarrassed by them to report them, or when they do, the money stolen may be refunded by banks, giving police little incentive to pursue the thieves.

---
## Lesson 8: Open Source Information
**(Q23a) (Q23b)**

Open source information may be defined as information which is publicly available and which anyone can lawfully obtain by request, purchase, or review. A lot of open source information is easily obtainable by criminals.

This information comes from a variety of sources, including the social media pages of a company and its staff, websites and google searches. These can be a goldmine of information, revealing details such as the names of key staff, design of ID badges, layout of the buildings and software used on internal systems. Many attackers will claim to be from someone in IT or senior management as a way of gaining access to the organisation. This type of information can be readily found on platforms such as LinkedIn or by looking at the company’s ‘about us’ webpage.

Often, seemingly innocent information shared through social networks and blogs can be used to develop highly convincing social engineering campaigns, which in turn are used to trick well-meaning users into handing over key information like passwords.

This is why using open source intelligence for security purposes is so important – it gives an organisation an opportunity to find and fix weaknesses in an organisation’s network and remove any sensitive information before a threat actor uses the same tools and techniques to exploit them.

---
## Lesson 9: Information a Cyber Criminal Could Obtain Through Open Sources
**(Q24a) (Q24b) (Q24c)**

All you need to do is consider how you use social media in order to realise that there is likely to be a huge amount of open source information already available about you. This could include information about your job history, education, hobbies, birth dates, family, and more. All of this information could help threat actors to tailor their phishing attacks.

Gathering open source information does not require hacking into systems or using private credentials to access data. Much of this information can be obtained by searching online for a person’s name, accessing their social media accounts or through a phone call, perhaps while pretending to be a government worker.

Company webpages can also be a good source of information on employee names, emails and job descriptions. Information like a person’s address and what they paid for their house are easily found on legitimate websites. Just having access to someone’s Facebook profile can show you information on their friends, relationship, personal interests, and family photos.

And there are many techniques for easily finding out more about someone online. For example, typing file type: PDF “john” “doe” into Google could result in John Doe’s resume – complete with contact information and employment history. And because most companies use a standard email format (e.g. first initial.lastname@company.com), once you know the work email address of one employee, you know them all.

If the person or organisation owns a website you can grab information about it, revealing the operating system being used, software version, personal contact info, and more. There are any number of free-to-use resources out there that will help people gather open source information.

---
## Lesson 10: Social Engineering Techniques
**(Q25a) (Q25b) (Q26a) (Q26b)**

Phishing is a very well-known way to grab information from an unwitting victim. Whilst it has been around for a while, it remains quite successful. The attacker typically sends an email or text to the target, seeking information that might help with a more significant crime. 

There are a huge number of different ways to conduct phishing attacks, including sending emails purporting to be from senior members of staff, targeting high-profile individuals like senior management or CEO (this is a type of phishing called ‘whaling’), rewriting unattended browser tabs with malware so the user thinks they are on a real site, when really they are on a fake site (this is called tab nabbing), etc.

In another form of phishing, known as spear phishing, the fraudster tries to target or ‘spear’ a specific person. The attacker will use social media to identify the name and email of, for example, a human resources person within a particular company. The attacker then sends the HR person an email that appears to come from a high-level company executive. 

Some recent cases involved an email request for employee payroll data, which includes names, home addresses, and National Insurance numbers. If the attacker is successful, the victim will unwittingly hand over information that could be used to steal the identities of dozens or even thousands of people.

### Email hacking and contact spamming
It’s in our nature to pay attention to messages from people we know. Some criminals try to take advantage of this by taking over email accounts and then sending out lots of emails spamming account contact lists. 

People are more likely to open emails from people they know, allowing scammers to gain access to insert malware or to pretend to be the sender and ask for money or information.

##### Example 1
If your friend sends you an email with the subject, “Check out this site I found, it’s totally cool,” you might not think twice before opening it. By taking over someone’s email account, a fraudster can make those on the contact list believe they’re receiving an email from someone they know. The primary objectives include spreading malware and tricking people out of their data.

### Quid pro quo
This scam involves an exchange – I give you this, and you give me that. Fraudsters make the victim believe it’s a fair exchange, but that’s far from the case, as the cheat always comes out on top.

##### Example 2
A scammer calls a target, pretending to be an IT support technician. The victim is persuaded to hand over the login credentials to their computer, thinking they’re receiving technical support. Instead, the scammer takes control of the victim’s computer, uploads malware or steals personal information from the computer.

A recent and prolific recent example was the ‘Microsoft tech support scam’. In this social engineering fraud, a fake representative calls victims, spoofing the caller ID so it looks like the phone call really is coming from the software giant. The scammer walks the victim through the process of installing applications that allow remote access to your computer. Or, the scammer may initiate contact by displaying fake pop-up messages on your screen that trick you into calling a fraudulent ‘support’ hotline. With both scams, the goal is to get victims to pay, in the form of a one-time fee or subscription, to fix the problem.

### Vishing
Vishing is the voice version of phishing. ‘V’ stands for voice, but otherwise, the scam is the same. One type of vishing attack involves sending recorded messages telling recipients their bank accounts have been compromised. Victims are then prompted to enter their details using their phone’s keypad, giving the scammers access to their accounts.

### Smishing
Smishing is any kind of phishing that involves a text message. Smishing is effective because people tend to be more likely to trust a text message than an email. Most people are aware of the security risks involved with clicking on links in emails. This is less true when it comes to text messages. The scammer will send text messages that purport to be from legitimate entities. This is often used in combination with other techniques to bypass 2FA (two-factor authentication). They might also direct victims to malicious websites on their phones, where malware may be downloaded. Another technique is to say that if the victim doesn’t enter their personal information they will be charged for use of a service.

---
## Summary
In this section, we have learned:

- different terms related to cyber security
- the differences between LAN and WAN
- organisational strategies in cyber security
- current and emerging challenges in cyber security
- how social engineering attacks work and the types of information that cyber criminals can gain
- how cyber criminals take advantage of open sources of information.