---
title: Legal and Ethical Aspects
Summary: Unit 3 - Understand Legal and Ethical Aspects of Cyber Security
Author: rvnt
Date: 2023-07-24
base_url: https://sanctum.spctr.uk/
---

## Lesson 1: Key UK Legislation Relating to Cyber Security
**(Q27)**

The UK has a number of different pieces of key legislation which cover various aspects of cyber security:
- The Computer Misuse Act 1990
- The Data Protection Act 2018/GDPR
- Official Secrets Act 1989
- The Privacy and Electronic Communications Regulations 2003.

#### Computer Misuse Act (1990)
The Computer Misuse Act 1990 protects personal data held by organisations from unauthorised access and modification. The four clauses cover a range of offences including hacking, computer fraud, blackmail and viruses.

The act makes the following illegal:

1. Unauthorised access to computer material. This refers to entering a computer system without permission (hacking).
1. Unauthorised access to computer materials with intent to commit a further crime. This refers to entering a computer system to steal data or destroy a device or network (such as planting a virus).
1. Unauthorised modification of data. This refers to modifying or deleting data, and also covers the introduction of malware or spyware onto a computer (electronic vandalism and theft of information).
1. Making, supplying or obtaining anything which can be used in computer misuse offences.

Failure to comply with the Computer Misuse Act can lead to fines and potentially imprisonment.

The Computer Misuse Act was first proposed when computers were a rarity in public life, and as a result, it used a fairly narrow definition of what was considered a malicious act. The rise of the digital age since the Act’s passage has led to the insertion of a number of new sections.

In 2015, the Computer Misuse Act was amended to create a new offence of unauthorised acts causing serious damage; to bring the EU Directive on Attacks against Information Systems into UK law; and to clarify the protections for law enforcement if they break into a computer in the course of a criminal investigation.

The new offence of unauthorised acts causing serious damage was made partly to increase the penalties for the kinds of attack that might by classed as cyber terrorism.

The changes made in regard to the EU Directive on Attacks Against Information Systems focused on making it easier to prosecute cyber criminals who were using the UK as a base (even if they weren’t physically located in the UK) as well as to allow the police and Crown Prosecution Service to pursue and prosecute UK residents for cyber crimes committed abroad.

The final provision was more controversial. Civil rights groups have argued that it gives an exemption under the law to police and spy agencies.

#### Data Protection Act (1998)
In the 1990s, as more organisations began using digital technology to store and process personal information, there was a growing danger this information could be misused. The Data Protection Act of 1998 was designed to tackle this issue. 

Data stored electronically is vulnerable as it is very easy to copy it to a removable drive or to email or transfer it via the Internet. Individuals who had data stored had several concerns:

- Who could access this information?
- How accurate was the information?
- Could it be easily copied?
- Was it possible to store information about a person without that individual’s knowledge or permission?

The Data Protection Act 1998 aimed to safeguard information held about an individual classified as personal (e.g. name, address, financial details) or sensitive (e.g. ethnicity, political opinion, religion). The act ensures data stored is processed fairly and lawfully. 

For example, there are strict rules as to who can access and alter health records. Regular checks are made to ensure that the rules of the Data Protection Act are being followed.

Principles of the Data Protection Act 1998:

1. Data must be collected and used fairly and inside the law.
1. Data must only be held and used for the reasons given to the Information Commissioner.
1. Data can only be used for registered purposes. You cannot give it away or sell it unless this was stated initially. For example, a school could not sell pupils’ data to a book or uniform supplier without permission.
1. The data held must be acceptable, appropriate and not beyond what is necessary when compared with the purpose for which the data is held.
1. Data must be accurate and be kept up to date. For example, making sure contact numbers are current.
1. Data must not be kept longer than is necessary. This rule means that it would be wrong to keep information about past customers longer than a few years at most.
1. Data must be kept safe and secure – for example, personal data should not be left open to be viewed by just anyone.
1. Data may not be transferred outside of the European Economic Area unless the country where the data is being sent has similar data protection laws. This part of the Data Protection Act has led to some countries passing compatible laws to allow computer data centres to be located in their jurisdiction.

Under the Data Protection Act 2018 (DPA), individuals have the right to find out what information the government and other organisations have on file. 

These include the right to:

- Be informed about how your data is being used
- Access personal data
- Have incorrect data updated
- Have data erased
- Stop or restrict the processing of your data
- Data portability (allowing you to access and reuse your data for different services)**
- Object to how your data is processed in certain circumstances.

The DPA 2018 updated and replaced the Data Protection Act 1998. It was amended again on 01 January 2021, to reflect the UK’s status outside the EU.

The amended DPA sits alongside and supplements the UK GDPR – for example, by providing exemptions. It also sets out separate data protection rules for law enforcement authorities, extends data protection to some other areas, such as national security and defence, and sets out the Information Commissioner’s functions and powers.

The law applies to anyone using information about people for any business or other non-household purpose and applies to most businesses and organisations, whatever their size. It does not apply to anyone using information for personal, family or household purposes – such as personal social media activity, private letters and emails, or use of your own household gadgets. For law enforcement and national security organisations, the updated DPA sets out the regulations for processing personal data.

#### General Data Protection Regulation (GDPR)
The General Data Protection Regulation or GDPR came into effect in 2018. Essentially, it is an EU regulation that’s designed to further strengthen data protection for everyone in the public, private and third sector, giving more control to individuals over how their data is used.

Some of the key privacy and data protection requirements of the GDPR include:

- Requiring the consent of subjects for data processing.
- Anonymising collected data to protect privacy.
- Providing data breach notifications.
- Safely handling the transfer of data across borders.
- Requiring certain companies to appoint a data protection officer to oversee GDPR compliance.

In effect, the GDPR set a basic level of standards for all companies and organisations that handle the data of EU citizens.

In the UK, the GDPR was brought into domestic law through the amended version of the DPA (2018) and the UK GDPR (2021). Post-Brexit, the main principles, rights and obligations of the GDPR remain the same. However, there are some new rules for the transfer of personal data between the UK and the EEA.

The GDPR requires that businesses keep personal data secure and only permit third parties access to the personal data if there are sufficient guarantees regarding security. Businesses must put in place safeguards that include physical measures (e.g. firewalls, anti-virus programs, perimeter scanning tools) and organisational measures (e.g. policies and procedures related to cyber security). Businesses are required to protect against unauthorised or unlawful use of personal data and against its loss, destruction and damage.

Failing to implement appropriate security measures to safeguard personal data can result in large fines (up to £20 million or 4% of annual global turnover, whichever is greater).

Enforcement action can be taken even when there has been no cyber attack or data breach.

Any company that markets goods or services to EU residents, regardless of where it is located, is subject to the GDPR regulations. As a result, the GDPR also affects global data protection requirements.

#### The Privacy and Electronic Communications Regulations (2003)
This directive works with the GDPR and sets out more specific privacy rights on electronic communications. It recognises that widespread public access to digital mobile networks and the Internet not only creates new opportunities for businesses and users, but also new risks to their privacy. It is mainly focused on the stopping of spam calls and unwanted marketing communications.

There are specific rules on:

- Marketing calls, emails, texts and faxes
- Cookies (and similar technologies)**
- Keeping communications services secure
- Customer privacy such as the telephone directory listings.

#### What is the Official Secrets Act?
The Official Secrets Act 1911–1989 are four legal documents protecting the UK against espionage and the leaking of sensitive government information. 

Offences covered by the acts include spying and sabotage, as well as the disclosure of sensitive information by employees and ex-employees of the security services. The penalty for breaching the Official Secrets Act is a maximum jail term of 14 years if the crime relates to spying or sabotage, and a maximum jail term of two years for offences under the 1989 Act.

In Scotland, the Scotland Act 1998 provides that members of the Scottish Executive and junior Scottish Ministers are to be considered as Crown servants for the purposes of the Official Secrets Act 1989. It also provides that people supplying goods or services for the purpose of office holders of the Scottish Administration are government contractors for the purposes of the 1989 Act.

Who has to sign it?

Employees and ex-employees of the security services, civil servants, law enforcement officers, judges, members of the armed forces and government contractors are among those subject to the Official Secrets Acts.

Even those who have not signed the act are bound by it, and government employees are usually informed they are subject to it in their contracts.

------
## Lesson 2: Ways to protect stored data
**(Q28)**

All the way through this online course we have looked at the importance of keeping data secure; now we will look at it from an IT system point of view.

Measures that can be taken to keep data secure include:

- Making regular backups of files. If the worst was to happen, a recently saved copy of the data could be used to bring all the systems back with minimal impact.
- Protecting yourself and your organisation against viruses by running anti-virus software.
- Using a system of secure passwords so that access to data is restricted.
- Only allowing authorised staff access to data storage systems.
- Turning terminals off at night and locking them down with passwords.
- Using data encryption techniques to code data so that it makes no apparent sense if intercepted.

#### Backup Early and Often
The single most important step in protecting data from loss is to back it up regularly.

Individuals can use the backup utility built into Windows and Apple operating systems to perform basic backups and even set this up to be performed automatically.

There are also numerous third-party backup programs that offer more sophisticated options for individuals and businesses. Whatever program you use, it’s important to store a copy of the backup offsite or in the cloud, in case of a disaster that can destroy an organisation’s servers or other physical backups.

#### Password Protection
For an organisation, all of the laptops, computers, tablets and smartphones in use will contain a lot of business-critical data, such as personal information on customers, and proprietary information. It is essential that this data is available only to authorised users.

Passwords, when set up correctly, are a free, easy and effective way to prevent unauthorised access. 

Some factors to consider include:
- Organisations should have a policy in place that requires everyone to use strong passwords.
- Passwords should not be left on notes by the device.
- Consider using a cloud-based password safe/vault.
- Strong passwords consist of a random string of letters (both capitals and lowercase), numbers and symbols. A series of random words is easier to remember, but they must be truly random.
- Passwords should be changed regularly.
- Never use the same password twice – use a unique password for every service, and don’t swap backwards and forwards between old and new passwords if a service demands that you input a brand-new password.
- Passwords should not be shorter than 12 to 14 characters in length.

#### The use of Anti-Malicious Software
Anti-virus or anti-malware secures data by protecting against many types of malware, including all types of viruses, as well as rootkits, ransomware and spyware. Anti-malware software can be installed on an individual computer, server or mobile device.

There are many types of anti-malicious software. The exact type you use will depend on the system you use and the level of security you need.

#### Performing Operating System Updates
Many malware attacks take advantage of software vulnerabilities in common applications, like operating systems and browsers. These are programs that require regular updates to remain safe and stable.

Operating system updates are some of the most essential steps you can take when it comes to protecting your information. In addition to making security fixes, software updates can also include new or enhanced features, allow compatibility with different devices or applications, improve the stability of your software and remove outdated features. You can schedule regular, automatic updates.

#### Share vs. File Level Security
These are different types of permission for accessing networks and files. Share permissions manage access to folders shared over a network. For example, they allow everyone on a project to access the documents used in that project. They don’t apply to users who log on locally, so any documents kept on an individual computer are not shared.


There are three types of share permissions – Full Control, Change and Read:
- Read – users can see what is in the file or folder, but cannot make changes.
- Change – users can read files, and can also add files, change the data in files, and delete files – but not folders.
- Full control – users can do everything in both Read and Change, and they can also change permissions and delete folders.

File or NTFS permissions determine the action users can take for a folder or file both across the network and locally. Unlike share permissions, NTFS permissions offer several other permissions besides Full Control, Change, and Read that can be set for groups or individually. Here you can change who has visibility of folders and who can access the specific folder by putting specific users into groups.

For example, senior managers may have full access to folders containing the company accounts across the network; however, you would not want the office administrator or receptionist to have access to these key documents unless authorised. To do this, the senior management team would be assigned access as a group through the NTFS permissions, whereas all other groups would be denied access.

#### Encryption
File and disk encryption is an important and necessary way to protect data stored on a computer or network.

Disk encryption doesn’t completely protect a computer. Hackers can still access the computer over an insecure network connection, or infect the computer with malware. However, encrypting a computer’s files or the entire hard disk greatly reduces the risk of data theft.

Encryption works like a form of digital cryptography. It uses mathematical algorithms to scramble messages, so that only individuals who have the digital key are able to decrypt and access the messages. Encrypted files can often be seen in a file listing, but they cannot be accessed and read without the key.

Many operating systems have built-in encryption.

For example, Windows supports an encryption system called the Encrypting File System (EFS). You can use this built-in certificate-based encryption method to protect individual files and folders stored on Windows machines. Encrypting a file or folder is as easy as checking a box. 

There are many products available that will allow you to encrypt an entire disk, for example, by locking down the entire contents of a disk drive/partition. Data is automatically encrypted when it’s written to the hard disk and automatically decrypted before being viewed.

Disk encryption products can also be used to encrypt removable USB drives, flash drives, etc. Some allow creation of a master password along with secondary passwords that allows you to give partial access to other users. There are two main methods of encryption: symmetric encryption and asymmetric encryption.

------
## Lesson 3: How to Encrypt Information
**(Q29) (Q30)**

The encrypting of data is vitally important as a measure to protect files. Two common encryption types are:

- Symmetric encryption (also called secret key encryption)**
- Asymmetric encryption (also called public key encryption).

The basic difference between these two types of encryption is that symmetric encryption uses one key for both encryption and decryption, and asymmetric encryption uses a public key for encryption and a private key for decryption.

Let’s explore each of these encryption methods separately to understand their differences better.

### Symmetric encryption
In symmetric-key encryption, each computer has a secret key that it can use to encrypt information before it is sent to another computer, where it is decoded. It is like a secret code that both computers must know in order to decrypt the information.

The code provides the key to decoding the message.

![Symmetric Encryption](img/SymmetricEncryption.png)**

The standard used for encryption is called the Advanced Encryption Standard (AES). This uses 128-, 192- or 256-bit keys. A 128-bit key, for instance, can have more than 300,000,000,000,000,000,000,000,000,000,000,000 key combinations, making it time consuming to break this type of encryption using brute force techniques (trying every combination).

This encryption method is best suited for encrypting files and drives. 

Because the key is often derived from a password, one weak spot with symmetric-key encryption is that it is only as strong as the password itself.

### Asymmetric encryption
Unlike symmetric-key encryption, asymmetric-key encryption uses two different keys at the same time – a public key and a private key. 

This type of encryption is commonly used for sending information between two devices.

Public-key encryption uses two different keys at once – a combination of a private key and a public key. The private key is known only to your computer, while the public key is given by your computer to any computer that wants to communicate securely with it.

![Asymmetric Encryption](img/AsymmetricEncryption.jpg)**

To decrypt an encrypted message, a receiving computer must use the public key, provided by the originating computer, and its own private key. Although a message sent from one computer to another won’t be secure, since the public key used for encryption is published and available to anyone, it can’t be read without the private key.

The key pair is based on very long prime numbers. This makes the system extremely secure, because there is essentially an infinite number of prime numbers available, meaning there are nearly infinite possibilities for keys. One very popular public-key encryption program is Pretty Good Privacy (PGP), which allows almost anything to be encrypted.

In order to use public-key encryption on a large scale, such as with a Web server, a different method is used – digital certificates. A digital certificate is basically a unique piece of code, or a very large number, that is authorised by an independent source known as a certificate authority. The certificate authority is a middle man that both computers trust.

The certificate confirms that each computer is in fact who it says it is, and then provides the public keys of each computer to the other.

The Secure Sockets Layer (SSL) is an Internet security protocol that makes use of a certificate and public-key encryption. It is part of a larger security protocol known as Transport Layer Security (TLS).

In your browser, you can tell when you are using a secure protocol because the ‘http’ in the address line is replaced with ‘https’, and a small padlock will appear in the status bar at the bottom of the browser window.

Once your browser requests a secure page and adds the ‘s’ onto ‘http’, it will send out the public key and the certificate, and check that the certificate is valid and comes from a trusted party, and that the certificate belongs with the site from which it’s coming.

### Advantages and disadvantages of encryption techniques
The advantages and disadvantages of the different encryption techniques are:
- Symmetric encryption is faster to run because the keys used are much shorter than those used in asymmetric cryptography. This allows symmetric encryption to handle a greater volume of data in a given time.
- Symmetric encryption is the preferred method for encrypting long messages or large files because it will not place a heavy load on processors, memory capacity or the batteries of portable devices.
- In symmetric encryption, the key needs to be transmitted between the communicating devices and it can be intercepted.
- Asymmetric encryption is more secure, because it uses two different keys.
- Asymmetric encryption is slower and takes more processing power and energy. 
- In asymmetric encryption, once your private key is identified by an attacker, then all of your messages can be accessed.

------
## Lesson 4: Movement of Data
**(Q31)**

### Advantages and disadvantages of encryption techniques
Data in transit refers to data that is moving from one location to another, such as across the Internet or through a private network.

Data protection in transit is the protection of this data while it’s travelling from network to network or being transferred from a local storage device to a cloud storage device – wherever data is moving, effective data protection measures for data in transit are critical, as data is often considered less secure while in motion.

Data at rest refers to data that is not moving from device to device or network to network, such as data stored on a hard drive, laptop, flash drive, or archived/stored in some other way. Protecting data at rest involves securing inactive data stored on a device or network. While data at rest may be less vulnerable than data in transit, it can also be a more valuable target than data in motion.

In either case, protecting sensitive data both in transit and at rest is important, as attackers find increasingly innovative ways to compromise systems and steal data.

#### The Role of Encryption in Data Protection in Transit and at Rest
Data can be exposed to attacks both in transit and at rest and requires protection at both times. There are a number of approaches to protecting data in transit and at rest. For example, encryption is a popular tool for securing data both in transit and at rest.

#### Best Practices for Data Protection in Transit and at Rest
While data in transit and data at rest may have slightly different risk profiles, the risk lies primarily in the sensitivity and value of your data; attackers will attempt to gain access to valuable data in whichever state is easiest to breach. That’s why a proactive approach is the safest and most effective way to protect sensitive data.

------
## Lesson 5: Security Checks Made by an Organisation
**(Q32)**

Data sharing usually means disclosing personal data to third parties outside of the organisation.

In the UK, organisations have to be extremely careful with how they share and distribute personal information.

Organisations are usually very familiar with protecting personal information they hold themselves, but establishing appropriate security in respect of shared information usually presents new challenges.

Before sharing information an organisation should:

- Review what personal data your organisation receives from other organisations, making sure you know where it came from.
- Review what personal data your organisation shares with other organisations, making sure you know who has access to it and what it will be used for.

These two areas are what an organisation would call a due diligence check. This means ensuring that all key security checks are complete before handing over personal information.

An organisation should also assess whether and how to share any data that is particularly sensitive.

Due diligence also involves identifying who has access to information that other organisations have shared with your organisation. You should avoid giving anyone access to shared information they do not need to carry out their job.

There should be robust security policies and procedures in place, and all staff members in the organisation who are involved in the sharing of information should be aware of these policies and procedures.

For both parties, it is good practice to have a data sharing agreement in place. This will set out the purpose of the data sharing, cover what is to happen to the data at each stage, set standards of data management and help all the parties to be clear about their roles. This will help you to demonstrate your accountability under the GDPR.

##### Example: Bounty UK fined £400,000 for sharing personal data unlawfully
In 2019, the Information Commissioner’s Office (ICO) fined Bounty (UK) Limited £400,000 for illegally sharing personal information belonging to more than 14 million people.

According to the ICO, an investigation found that “Bounty, a pregnancy and parenting club, collected personal information for the purpose of membership registration through its website and mobile app, merchandise pack claim cards and directly from new mothers at hospital bedsides.

But the company also operated as a data broking service until 30 April 2018, supplying data to third parties for the purpose of electronic direct marketing. Bounty breached the Data Protection Act 1998 by sharing personal information with a number of organisations without being fully clear with people that it might do so.

The company shared approximately 34.4 million records between June 2017 and April 2018 with credit reference and marketing agencies, including Acxiom, Equifax, Indicia and Sky.

These organisations represented the four largest recipients out of a total of 39 organisations which Bounty confirmed it shared personal data with.

The personal information shared was not only of potentially vulnerable new mothers or mothers-to-be, but also of very young children, and included the birth date and sex of the children.

Steve Eckersley, ICO’s Director of Investigations, said: “The number of personal records and people affected in this case is unprecedented in the history of the ICO’s investigations into data broking industry and organisations linked to this".

“Bounty were not open or transparent to the millions of people that their personal data may be passed on to such large number of organisations. Any consent given by these people was clearly not informed. Bounty’s actions appear to have been motivated by financial gain, given that data sharing was an integral part of their business model at the time".

“Such careless data sharing is likely to have caused distress to many people, since they did not know that their personal information was being shared multiple times with so many organisations, including information about their pregnancy status and their children.”

------
## Lesson 6: Ethical Conduct in Cyber Security
**(Q33) (Q34)**

### What are ethics in digital technology?
Ethics are “a system of principles and customs that affect how people lead their lives”. Although we do not have to follow these principles or ethics, it is generally in the best interests of everyone that we do. 

Ethics are different to legislation or laws that legally dictate what is right and wrong. Ethics are more about society’s opinions about what is right and wrong.

When we think about ethics in digital technology, we are considering how society treats the use of digital technology and the development of hardware and software.

For example:

- Should a company be allowed to sell your search history to third parties? 
- Should an employer be allowed to access your social media history? 
- How should you conduct yourself when online?

Because cyber security professionals are responsible for keeping data, computer systems and networks safe, they are also protecting the lives, livelihoods and happiness of those who depend upon them. 

For example, if you are a cyber security professional responsible for securing a hospital’s network and critical data, you are also involved in protecting sick patients, even though you are not a healthcare worker.

Patients’ privacy and health are in your hands. This means that ethical issues are at the core of cyber security practices. 

Given the increasing complexity and difficulty of securing data, the ethical responsibility that falls on cyber security professionals is growing.

### Examples of ethical conduct in cyber security
#### Following Company IT Policy
Organisational IT policies are written to ensure that personal customer and company information is protected, and that there is a balance between work efficiency and risk. 

Some basic organisational IT policies could include:

- Not visiting harmful sites from work devices.
- Not connecting personal devices to the organisation’s WiFi.
- Not accessing company servers from personal devices.

Not following these types of IT policies could leave companies and customers exposed to loss of information, hacking, and financial penalties.

#### Maintaining Confidentiality
In the course of their job, cyber security professionals often have access to confidential information, including personal information about employees, customers and clients, and information about the organisation they work for.

It is very important that they keep this information confidential, both to protect the people and organisations involved, and to protect the reputation of their employer.

#### Adherence to Applicble Laws
Cyber security professionals in the UK have both an ethical and a legal duty to adhere to various types of legislation, such as the GDPR. Failure to do so can have a negative effect on their employer, and on those whose information was compromised.

#### Promoting Information Security
Effective security awareness training is essential in order to identify and respond appropriately to the growing range of cyber security threats. All employees, at every level of the organisation, should receive training to ensure they have the skills required to identify an attack. 

Cyber security awareness training should be engaging and informative, to ensure that staff understand what is required of them and the importance of their role in safeguarding the organisation’s sensitive data.

Security awareness training should:

- Educate staff on the cyber threats faced.
- Raise awareness of the sensitivity of data on systems.
- Ensure procedures are followed correctly.
- Provide information on how to avoid phishing emails and other scam tactics.
- Reduce the number of data breaches.
- Build a culture of enhanced security compliance.

Organisations have an ethical duty to provide this training in order to protect their employees, clients and customers from data breaches.

#### Refraining from Conflicts of Interest
A conflict of interest arises when a person owes a duty, or believes they owe a duty, to two competing interests. For example, if a cyber professional is asked by an employer to grant someone access privileges that they think are inappropriate, or that place security at risk. In this case, the cyber security professional may feel they have a duty to both the employer and to those whose information may be at risk. This is an ethical conflict of interest.

It is not always possible to avoid conflicts of interest, but all organisations should have a policy in place that sets out what should be done in the case of a conflict of interest. The policy should clearly set out what a conflict of interest is, what steps should be taken, when a conflict must be reported, and who is responsible for making a decision.

### Unethical conduct within cyber security
Cyber security professionals are often in a position to engage in different types of unethical conduct. Some of this may seem, at the time, like something that will not cause any damage – such as looking up the credit score of a friend at their request – but which is still clearly unethical behaviour and violates a professional duty of cyber security practice. 

Some common types of unethical behaviour include:

- **Sabotage** – damaging equipment or files.
- **Misusing or disclosing confidential information** – looking up confidential information without a legal reason, selling or giving confidential information to others, or manipulating information.
- **Using information maliciousl**y – this could include to get revenge or to damage the reputation of a business or a person, posting confidential information on social media, or simply discussing confidential information with fellow employees who do not have access to that information.

It is not always completely clear what types of behaviour are unethical. For example, if your organisation is slow to report a data breach, at what point is the delay unethical? A week? A year? Or, how does your organisation respond to a request from a law enforcement agency to weaken your organisation’s encryption practices or to decrypt specific devices? This is why it is important that all organisations have a clear policy regarding ethical and unethical conduct and who is responsible for making decisions.

---
## Summary

In this section, we have learned:
- some of the key legislation related to cyber security and how it protects individuals, businesses, and nations
- ways of protecting stored data
- advantages and disadvantages of different data encryption techniques
- types of security checks that might be undertaken before releasing information
- what constitutes ethical and unethical conduct in cyber security.