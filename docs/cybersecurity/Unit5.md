---
title: Maintaining Cyber Security
Summary: Unit 5 - Methods of Maintaining Cyber Security
Author: rvnt
Date: 2023-07-24
base_url: https://sanctum.spctr.uk/
---

## Lesson 1: Routine Ways to Maintain Cyber Security
**(Q10)**

Every organisation is a potential victim of a cyber attack. All organisations have something of value that is worth something to others, even if that is simply money from ransomware. Organisations that do not take routine precautions will almost certainly experience some form of cyber attack.

### Common Elements of Routine Cyber Security
The following are a few of the precautions that are routinely used. Not every organisation will use every one of these.

- **Boundary Firewalls and Internet Gateways** – Establish network perimeter defences, such as web proxy, web filtering, content checking, and firewalls. These will help to detect and block executable downloads, block access to known malicious domains and prevent users’ computers from communicating directly with the Internet.
- **Malware Protection** – Set up and maintain malware software to detect and respond to known attacks. This could include installing security software and ad blockers, as well as restricting the use of personal email on work computers.
- **Patch Management and Software Updates** – Updating your software regularly with the latest version will ensure that new vulnerabilities and bugs are patched quickly, before they can be used by hackers.
- **Whitelisting and Execution Control** – This is a security feature that allows you to create lists of trusted IP addresses or IP ranges from which your users can access your domains. It also prevents unknown software from being able to run or install itself.
- **Secure Configuration** – This restricts the functionality of every device, operating system and application so they cannot be used in an unauthorised way.
- **Password Policy** – Ensure that an appropriate password policy is in place and followed. This should include the use of secure passwords, not writing down passwords or keeping them near the computer, etc.
- **User Access Control** – This includes limiting users’ execution permissions so that users only have permission to access certain files and folders, rather than every file and folder in the system.
- **Employee Education** – Make sure that every employee is fully trained in how to recognise and prevent cyber attacks. This should include a clear statement of what employees can and cannot do; procedures for handling a suspected data breach; encryption of all devices used for work, including BYOD devices; specifying the particular information and documents that should never leave the secure workplace; rules for online behaviour.
- **Back-Ups** – All data should be backed up regularly to an external drive or server, or to the cloud.
- **Policies and Procedures** – Have detailed and clear policies and procedures in place that set out what employees must and must not do and what to do should a cyber attack happen.

---
## Lesson 2: Vulnerability Testing 
**(Q11)**

A vulnerability test or assessment is the process of identifying and prioritising the vulnerabilities in a system. This type of test is performed in different kinds of systems, including IT systems, energy supply systems, communications systems, etc. The tests may be conducted for organisations ranging from small businesses up to large regional infrastructures. 

Vulnerability tests usually include cataloguing the assets and resources of the system; ranking the assets in terms of importance to the organisation; identifying potential threats to each asset or resource; finding the best ways of mitigating or eliminating the threats.

#### Vulnerability Assessment
A vulnerability assessment is a way to evaluate the security risks in the software system in order to reduce the probability of a threat. It is also called vulnerability testing. A vulnerability is any weakness in the IT system’s security procedures, design, or implementation that may result in systems becoming hacked.

The purpose of a vulnerability assessment is to reduce the possibility for intruders (hackers) to gain unauthorised access. To complete an assessment, a number of different tools are used. The first is a vulnerability scan, which is usually completed along with penetration testing.

#### Vulnerability Scanning
A vulnerability scan is an application that identifies and creates an inventory of all the systems (including servers, desktops, laptops, virtual machines, containers, firewalls, switches, and printers) currently connected to a network.

For each device that it identifies, it will also identify the operating system it runs and the software installed on it, along with other key information, such as open ports and user accounts.

Vulnerability scanners will also attempt to log in to systems using default or other credentials, in order to build a more detailed picture of the system.

Once it has built the inventory, the vulnerability scanner checks each item in the inventory against one or more databases of known vulnerabilities to see if any items are subject to any of these vulnerabilities. This identifies if any critical updates have not been applied. The result of a vulnerability scan is a list highlighting any systems that have known vulnerabilities that may need attention.

#### The Vulnerability Management Process
Once a scan has been completed, the information is then used to produce a detailed report. The report would include key information which is extremely useful to IT departments.

This includes:

 - Identification of vulnerabilities.
 - Evaluation of the risk posed by any vulnerabilities identified, e.g. how much risk is the organisation in currently.
 - The treatment of any identified vulnerabilities.
 - Reporting on vulnerabilities and how they have been handled, e.g. what has been done and what needs to be completed in the future.

---
## Lesson 3: What is Meant by Penetration Testing? 
**(Q12)**

A penetration test, also known as a pen test, is a simulated cyber attack against a computer system that checks for vulnerabilities that hackers can use. It is a way to test the security of an IT system by attempting to breach that system’s security, using the same tools and techniques a cyber attacker would use. 

It can be thought of as a type of White Hat attack.

Penetration tests are used to identify the amount of risk from software, hardware and social engineering vulnerabilities. In the UK, pen tests are often performed by qualified and experienced companies that have been certified under a particular certification scheme, such as CREST or the Cyber Scheme.

There are different types of penetration testing, including:

- **Whitebox Testing** – In this case, all of the information about the target software, hardware or employees is shared with the testers.
- **Blackbox Testing** – In this test, no information is shared with the testers about the target. This type of test is more accurate at identifying the risks from outside the organisation, but can also mean that some vulnerabilities remain undiscovered.

The following penetration tests can be run as either whitebox or blackbox tests:

- **Vulnerability Identification** – This is often used to test web applications. It gives feedback to web developers on coding practices to use to avoid any vulnerabilities that are found.
- **Scenario Driven Testing for Vulnerabilities** – In this case, the penetration testers explore a particular scenario to discover whether it leads to a vulnerability in your defences. Scenarios may include a lost laptop or an unauthorised device connected to the internal network, but there are many others. Organisations choose the scenarios they are most concerned about.
- **Scenario Driven Testing of Detection and Response Capability** – This type of test is also used with certain scenarios, such as a phishing attack, etc. But its aim is to find out how well the organisation can detect and respond to the vulnerability.

---
## Lesson 4: Security Updates and 'Patching' 
**(Q13) (Q14)**

Security updates are issued regularly by the software developer (the company that makes the software). They may be issued for many reasons. 

Often, security patches are included in software updates. Software updates can also include fixes for performance bugs, the addition of new features, compatibility fixes and may address any security issues that have been identified since the last update was deployed. Software vulnerabilities enable cybercriminals to access a person’s computer, allowing them to take control of the computer and access personal data, such as financial information and passwords. Updates sometimes run automatically in the background. Other times, updates come in the form of a free download. They may also be necessary for software to continue running when changes are made to the host operating system. 

### Patching
While a software update is designed to improve the performance of the software, a patch is a type of update that is designed to fix a particular security flaw, bug or vulnerability. The patch is a small piece of software that the software developer issues whenever a security flaw is uncovered. The patch covers the flaw, keeping hackers from further exploiting it. 

It is very important that all computer users update their software with the latest patches, as a delay can allow hackers to access their system. For example, the WannaCry ransomware infected more than 200,000 computers and networks before a cyber security expert found a way to stop it. The software developer Microsoft had actually issued a patch for the flaw that the attackers used, but many organisations and users had not installed the patch, so their systems were exposed to the malware. 

When cyber security experts or White Hat hackers discover a software flaw or bug, they alert the developer, and do not make the discovery public. This is to keep the flaw secret from malicious hackers who could take advantage of it to launch attacks. This also means that developers often release important patches before anyone knows about the problem. That’s why it’s very important to always update software on schedule – you could be eliminating a dangerous flaw. 

---
## Lesson 5: The Importance of Keeping Accurate and Up-To-Date Cyber Security Information and Records
**(Q15)**

We have already seen the risks involved with not keeping software updated and regularly patched. Cyber security is a constant cat and mouse game between organisations and attackers, with new vulnerabilities being discovered and closed all the time. 

This makes it very important to keep track of all maintenance and updates, to make sure that your organisation is always up-to-date with the latest fixes. This will also minimise any disruption to the business. 

For example, in 2017, the personal data of 147 million people was stolen from the US company Equifax. The cause of the breach was an application that hadn’t been updated, allowing hackers to access the data. 

However, a patch for this vulnerability was released months before the attack happened. This means that the attack could have been avoided by keeping up-to-date records to inform the IT managers that an update was needed.

Log management is another important record-keeping tool. Logs are the files that detail all the events that occur within a company’s systems and networks, including servers, firewalls, and other IT equipment. 

Each device, system, network, and application is called a log source. The event logs can show deviations from expected activity, alerting you to potential software, hardware, and security issues.

Logs provide data about a wide array of activities across the system, including log in failures, password changes, denial of service attacks, file name changes, exported data, new user logins, malware detection, etc. Tracking this information can help alert you to an unauthorised user or download, or a data breach.

Any systems connected to the Internet all require active monitoring. Any seriously suspicious behaviour or critical events will generate an alert that can then be checked and acted on by IT teams.

The logs will provide clear audit trails when investigating any incident and help identify where the attacker accessed the systems and outcome.

Maintenance records are also vital to keep systems and software safe.

The maintenance records need to be continually kept up to date so that IT professionals can ensure all the systems have the latest security patches installed. Records are used to identify which machines have which hardware and software installed on them so that when a vulnerability is identified, this can be addressed quickly. 

When IT teams are not sure of this key information, systems can be left vulnerable for a considerable amount of time, which gives hackers time to access the system.

---
## Lesson 6: User Access Control 
**(Q16)**

Access control is the way an organisation decides who should be allowed to see different types of data. For example, the company CEO may be able to see all of the data in the entire company; a division manager may be able to see the data for their division but not other divisions; an entry-level worker may be able to see only the data for their particular team. Each of these people has a different level of access.

The access control system verifies the identity of users and determines whether a particular user should be given access to particular data. Without appropriate access control, there is no data security. This is because giving people access to data they do not need can increase the chances of a data breach or data loss. It makes it easier for hackers to take advantage of uncontrolled administrative privileges to gain access. Failure to manage user access control can also lead to employees unknowingly or deliberately accessing and misusing data they shouldn’t be authorised to see.

The granting of high-level administration privileges should be carefully controlled and managed. This principle of only giving people access to the data they need is sometimes referred to as ‘least privilege’.

---
## Lesson 7: Methods of Restricting User Access 
**(Q17)**

### Establish Effective Account Management Processes
Have a system in place for managing user accounts, including keeping track of special account privileges; documenting user access permissions; and removing or disabling user accounts when no longer required. Unauthorised user accounts should be denied access to applications, computers and networks.

### Establish Policies and Standards for User Authentication and Access Control
A strong password and username policy should be developed that balances security and usability. Consider using two-factor authentication. This is a security system that requires two separate, distinct forms of identification in order to access something.

The first factor is a password and the second includes either something you have or something you are – such as a text with a code sent to your smartphone, or biometrics using your fingerprint, face, or retina.

### Limit User Privileges
Use the principle of ‘least privilege’ – granting users the permissions to access only the systems, services and information that they need to fulfil their business role.

### Limit the Number of Privileged Accounts
Control the number of people who have administrative or highly privileged access. Accounts with a high level of privileges should not be used for high risk or everyday activities, for example web browsing and email. 

Administrators should use normal accounts for standard business use.

### Monitor
Monitor user activity through the use of logs. Activity logs from network devices should be sent to a dedicated accounting and audit system that is separated from the core network.

### Have Training and Education for Employees
All users should be aware of the policies on proper account usage and their personal responsibility for following security policies.

---
## Lesson 8: How to Create a User Access Control System
**(Q18)**

To protect the privacy and integrity of information stored on a computer, it is important to control who can access the information.

Computer access is managed through user accounts. Each individual user of a computer signs in with their own account. Each user will also be assigned their own username and password. Usernames will often follow a set pattern. For example, firstname.last name@companyname.com. Assigning usernames and passwords is usually the first step in creating a user access control system.

There are different levels of user account, such as administrator or user. The access level depends on the rights or permissions that are assigned to that account.

The system actions that a user can perform are governed by the type of account they sign in with. An administrator account has higher-level permissions than a standard user account, which means that an administrator account owner can perform tasks on a computer that a standard user account owner cannot.

Standard user account credentials allow a user to do things that affect only their own account, including:

- Change or remove the password.
- Change the user account picture.
- Change the theme and desktop settings.
- View files stored in their personal folders and files in the public folders.

Administrator account credentials are necessary to do things such as:

- Create, change, and delete accounts.
- Change settings that affect all of the network’s users.
- Change security-related settings.
- Install and remove apps.
- Access system files and files in other user networks.

Tasks that require administrator permission are indicated in dialog boxes by a Windows security icon.

One of the key reasons for setting up an access control system is to restrict the files and folders a user can access and what they can do with the files. The Windows platform uses NTFS permissions to set up this file access.

NTFS permissions allow detailed control over the files and folders on the system. You can set NTFS permissions on a per file basis, as well as a per folder basis.

To set NTFS permission on a file, you should right click and go to properties then go to the security tab.

To edit the NTFS permissions for a User or Group, click on the edit button.

As you can see, there are a number of NTFS permissions, so let’s review them individually. First, let’s look at setting NTFS permissions on a **file**:

- Full Control allows you to read, write, modify, execute, change attributes, permissions, and take ownership of the file.
- Modify allows you to read, write, modify, execute, and change the file’s attributes.
- Read and Execute will allow you to display the file’s data, attributes, owner, and permissions, and run the file if it’s a program.
- Read will allow you to open the file, view its attributes, owner, and permissions.
- Write will allow you to write data to the file, append to the file, and read or change its attributes.

NTFS permissions for **folders** have slightly different options:

- Full Control allows you to read, write, modify, and execute files in the folder, change attributes, permissions, and take ownership of the folder or files within.
- Modify allows you to read, write, modify, and execute files in the folder, and change attributes of the folder or files within.
- Read & Execute will allow you to display the folder’s contents and display the data, attributes, owner, and permissions for files within the folder, and run files within the folder.
- List Folder Contents will allow you to display the folder’s contents and display the data, attributes, owner, and permissions for files within the folder.
- Read will allow you to display the file or folder’s data, attributes, owner, and permissions.
- Write will allow you to write data to the file or folder’s data and read or change its attributes.

---
## Lesson 9: Firewalls
**(Q19)**
A firewall is a network security device that monitors incoming and outgoing network traffic and allows or blocks specific traffic based on a defined set of security rules. Firewalls establish a barrier between secured and unsecured networks, such as between a company server and the Internet. By blocking access to unsecured sites, a firewall can prevent malware from being accidentally downloaded.

In protecting private information, a firewall is considered a first line of defence; firewalls are generally designed to protect network traffic and connections, and therefore do not attempt to authenticate individual users when determining who can access a particular computer or network.

#### Software Firewalls
Software, or host, firewalls include any type of firewall that is installed on a computer or server rather than a separate piece of hardware. The big benefit of a software firewall is that it’s highly useful for creating customised rules for what can be accessed.

#### Hardware Firewalls
Hardware, or network, firewalls act in a manner similar to a network router to intercept data packets and traffic requests before they’re connected to the network’s servers. 

Physical firewalls like this excel at perimeter security by making sure malicious traffic from outside the network is intercepted before the company’s network endpoints are exposed to risk.

The major weakness of a hardware-based firewall, however, is that it is often easy for insider attacks to bypass them. Also, the actual capabilities of a hardware firewall may vary depending on the manufacturer – some may have a more limited capacity to handle simultaneous connections than others, for example, and this can slow down network access.

#### Some Types of Firewalls
Most firewalls use one or more of the following methods to control traffic flowing in and out of the network:

- **Packet Filtering** – Packets are small chunks of data. The firewall software analyses the packets based on a particular set of filters. Packets that meet the criteria are sent through and those that don’t are discarded.
- **Proxy Service** – This is an early type of firewall that acts as a gateway between networks or between an internal and external network. Information requested from the Internet is retrieved by the firewall and then sent to the requesting system.
- **Stateful Inspection** – Instead of examining the contents of each packet, this type of firewall compares key parts of the packet to a database of trusted information. If the packet matches the trusted comparison, the information is allowed through. Otherwise it is discarded.
- **Application Gateway** – This is a security mechanism that is applied to specific applications, such as Telnet and FTP servers. It secures the server but can slowdown performance.
- **Next Generation Firewall (NGFW)** – A next generation firewall is a newer class of firewall. It filters Internet and network traffic based on the type of traffic and uses particular ports. NGFWs feature the basic functionalities of a standard firewall but can provide deeper and smarter inspection.

---
## Lesson 10: Functions of a Firewall 
**(Q20)**

An efficient firewall monitors both incoming and outgoing traffic. It also makes your computer ‘invisible’ when you’re online, helping prevent attempted intrusions in the first place. Most firewalls are capable of continuously updating the list of malicious applications. This way, your computer’s protection is always up-to-date. 

A firewall has a number of security functions, including:

- **In Packet Filtering** - The firewall acts as a type of gate keeper that determines what can pass through, based on the network’s rules.
- **Acting as an Application Proxy / Application Level Gateway** - In this case, the firewall can identify and stop malware.
- **Preventing the Loss of Valuable Information** – For example, a firewall can be installed to manage File Transfer Protocol (FTP), so that users cannot accidentally send confidential files or data to anyone outside the network.
- **Record User Activity** – every time a user accesses data, they will go through a firewall, which then records this in the log files.
- **Modifying Incoming Data Package** - Also known as NAT (Network Address Translation), this is a way to map multiple local private addresses to a public one before transferring the information. Organisations and home routers that want multiple devices to employ a single IP address use NAT. Using NAT allows information from a computer to the Internet to make it back to the computer using the router’s public address, not the computer’s private one.

---
## Lesson 11: Advantages and Disadvantages of a Firewall
**(Q21)**

## Advantages of using a firewall
The main advantage of having a firewall in place is that it provides an additional line of defence against attacks.

The main advantages of a firewall are that they:

- Monitor traffic – a firewall monitors all of the traffic entering your computer network.
- Detect malware – having a firewall can help keep malware out of your network.
- Control access – firewalls have an access policy as well. You can either use a default access policy or customise the settings to match your needs.

## Disadvantages of using a firewall
The main disadvantage is that they often require trained professionals to support their configuration and maintenance. If a firewall is not used properly, it could give a false impression that the network is safe. 

They also often cannot protect against an insider attack. Firewalls cannot protect against viruses once they are in the network, such as Trojans, worms and spyware that spread through flash drives and portable hard drives. They may restrict authorised users from accessing valuable services. They do not protect against backdoor attacks.

---
## Lesson 12: What is Network Traffic?
**(Q22)**

Network traffic is the amount of data that moves across a network at any point in time.

Examples of network traffic could be:

- Uploading a file to Dropbox
- Carrying out a webinar over Microsoft Teams or Zoom
- Browsing the Internet
- Downloading a file from a website
- Sending and receiving emails
- Uploading photos to Facebook.

### Network Traffic Monitoring
Network traffic monitoring, or network flow monitoring, is a process for tracking what devices are connected to a network, what kinds of data the devices are accessing, and how much bandwidth each device is using. It is essential for network security teams to detect zero-day threats, attacks, and other anomalies that need to be addressed.

Large companies are where network traffic monitoring tools are most often used. At this size, businesses can have hundreds or even thousands of devices joined to the same corporate network. It’s just too difficult to watch that many devices without help, and that’s where network monitoring tools come in. With bandwidth monitoring, automatic alerting and report generation, network administrators responsible for ensuring the network is running smoothly can stay on top of all the traffic on a company’s intranet with relative ease.

---
## Summary
In this section, we have learned:

- Different routine ways of maintaining cyber security, including vulnerability testing and penetration testing.
- The importance of security updates and patches.
- User access control and methods of restricting user access.
- How to set up a user access control system.
- The use of firewalls.