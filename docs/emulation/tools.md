# Tools

## Nintendo 3DS
### [3DSconv](https://github.com/ihaveamac/3dsconv)

3dsconv is a CLI tool that converts Nintendo 3DS CTR Cart Image files (CCI, ".cci", ".3ds") to the CTR Importable Archive format (CIA).

`boot9.bin` is required to be in the same location as `3dsconv.exe`.

`3dsconv.exe [options] game.3ds [game.3ds ...]`

#### Options

| **Command** | **Description** |
|-------------|-----------------|
| --output=<dir> |  Save converted files in specified directory; default is current directory or value of variable output-directory |
| --boot9=<file> | Path to dump of protected ARM9 bootROM |
| --overwrite | Overwrite existing converted files |
| --ignore-bad-hashes | Ignore invalid hashes and CCI files and convert anyway |
| --ignore-encryption | Ignore the encryption header value, assume the ROM as unencrypted |
| --verbose | Print more information |
| --dev-keys | Use developer-unit keys |


### [New Super Ultimate Injector 3DS](https://www.gamebrew.org/wiki/New_Super_Ultimate_Injector_3DS)

New Super Ultimate Injector 3DS (also known as NSUI) is an application that allows you to inject Nes, Snes, GameBoy, GameBoy Colour, GameBoy Advance, Mega Drive, Game Gear and TurbiGrafx 16 roms in to the 3DS, allowing roms of games from these popular classic consoles to be loaded from the 3DS home screen.

## Nintendo Switch
### [4NXCI](https://github.com/The-4n/4NXCI)

4NXCI is a tool for converting XCI (Ninetndo Switch Card Image) files to NSP.

`keys.dat` (aka [prod.keys](https://prodkeys.net/ryujinx-prod-keys/)) is required to be in the same location as `4nxci.exe` and `4nxci-GUI`.

`4nxci.exe [options...] <path_to_file.xci>`

#### Options

| **Command** | **Description** |
|-------------|-----------------|  
| -k, --keyset | Set keyset filepath, default filepath is ./keys.dat | 
| -h, --help | Display usage |
| -t, --tempdir | Set temporary directory path |  
| -o, --outdir | Set output directory path |
| -r, --rename | Use Titlename instead of Titleid in nsp name |
| --keepncaid | Keep current ncas ids |

## Management
### [Retool](https://github.com/unexpectedpanda/retool)

Retool is a filter utility for Redump and No-Intro DAT files. By customizing the DAT files before you load them into a ROM manager, you can more effectively trim, consolidate, and deduplicate your ROM sets.

### [dltool](https://github.com/kosmosnautti/dltool)

Tool to download contents of a DAT collection from Myrient. Used in combination with ReTool.

#### Options
| **Command** | **Description** | **Example** | **Status** |
|-------------|-----------------|-------------|------------|
| -i | DAT-file containing wanted ROMs | -i ModifiedNoIntro.dat | Required |
| -o | Output path for ROM files to be downloaded | -o /Emulation/Roms/System/ | Required |
| -c | Choose catalog manually, even if automatically found | | Optional |
| -s | Choose system collection manually, even if automatically found | | Optional |
| -l | List only ROMs that are not found in FTP-server (if any) | | Optional |
| -h | Display help message | | Optional |


### [RomVault](https://www.romvault.com/)

RomVault will take one or more DAT files and your set of ROM files, and clean up your files to match your DAT files. If you find more ROMs to add to your collection RomVault will scan and merge in these files, and if new DATs are released you can easily scan in these new DATs and update your ROM collections to match.

### [1G1R ROM Set Generator](https://github.com/andrebrait/1g1r-romset-generator)

A small utility written in Python that uses No-Intro DATs to generate 1G1R ROM sets.

For a comprehensive guide check the [wiki page](https://github.com/andrebrait/1g1r-romset-generator/wiki).

#### Options

| **Command** | **Description** | **Example** |
|-------------|-----------------|-------------|
| -r, --regions=REGIONS | A list of regions separated by commas. | -r USA,EUR,JPN |
| -l, --languages=LANGS | An optional list of languages separated by commas.<br/>This is a secondary prioritization criteria, not a filter. | -l en,es,ru |
| -d, --dat=DAT_FILE | The DAT file to be used. | -d snes.dat |
| -i, --input-dir=PATH | Provides an input directory. (i.e.: where your ROMs are) | -i "C:\Users\John\Downloads\Emulators\SNES\ROMs" |
| -o, --output-dir=PATH | ROMs will be copied to an output directory. | -o "C:\Users\John\Downloads\Emulators\SNES\ROMs\1G1R" |
| --move | ROMs will be moved, instead of copied, to the output directory. | |
| --symlink | ROMs will be symlinked (soft linked) to the output directory.<br/>*Newer versions of Windows 10+ require elevated privileges* |
| --relative | If set along with --symlink, will create relative symlinks instead of absolute. | |
| --group-by-first-letter | Groups ROMs on the output directory in subfolders according to the first letter in their name. | |

#### Step by step
1. Head to [DAT-o-MATIC](https://datomatic.no-intro.org/index.php?page=download&s=64&op=xml)
1. Select the 'P/C XML' option.
1. Select the desired system using the drop down box.
1. Check the "Default parent if there is no p/c info" box.
1. Click the 'Prepare' button.
1. Click the 'Download' button.
1. Extract the DAT file to the location of the 1g1r-romset-generator tool.
1. Run the following command to **preview** the set:

        python3 generate.py -r USA,EUR -d "Nintendo - Nintendo Entertainment System (Parent-Clone) (20200127-000518).dat" -i "/mnt/f/Emulators/Roms/*SystemName*"

1. Run the following command to **generate** the set:

        python3 generate.py -r USA,EUR -d "Nintendo - Nintendo Entertainment System (Parent-Clone) (20200127-000518).dat" -i "/mnt/f/Emulators/Roms/*SystemName*" -o "/mnt/d/Roms"

