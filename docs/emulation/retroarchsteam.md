# RetroArch (Steam)

[Notes](https://steamcommunity.com/sharedfiles/filedetails/?id=2929236360)

## Emulators
### Nintendo
#### Consoles
??? info "Super Nintendo Entertainment System"
    - RetroArch - [bsnes hd beta](https://docs.libretro.com/library/bsnes_accuracy/)
??? info "Nintendo 64"
    - RetroArch - [Mupen64Plus-Next](https://docs.libretro.com/library/mupen64plus/)
??? info "Gamecube"
    - RetroArch - [Doplhin](https://docs.libretro.com/library/dolphin/)
    - Standalone - [Dolphin](https://dolphin-emu.org/)
??? info "Wii"
    - RetroArch - [Doplhin](https://docs.libretro.com/library/dolphin/)
    - Standalone - [Dolphin](https://dolphin-emu.org/)
#### Handhelds
??? info "Gameboy"
    - RetroArch - [mGBA](https://docs.libretro.com/library/mgba/)
??? info "Gameboy Colour"
    - RetroArch - [mGBA](https://docs.libretro.com/library/mgba/)
??? info "Gameboy Advance"
    - RetroArch - [mGBA](https://docs.libretro.com/library/mgba/)
??? info "Nintendo DS"
    - RetroArch - [DeSmuME](https://docs.libretro.com/library/desmume/)

### Sega
#### Consoles
# Check these
??? info "Megadrive"
    - RetroArch - [PicoDrive](https://docs.libretro.com/library/picodrive/)
#### Handhelds
??? info "Game Gear"
    - RetroArch - [PicoDrive](https://docs.libretro.com/library/picodrive/)

### Sony
#### Consoles
??? info "Playstation"
    - RetroArch - [Beetle PSX HW](https://docs.libretro.com/library/beetle_psx_hw/)
??? info "Playstation 2"
    - RetroArch - [LRPS2 (LibRetro PCSX2)](https://docs.libretro.com/library/pcsx2/)
    - Standalone - [PCSX2](https://pcsx2.net/)
#### Handhelds
??? info "Playstation Portable"
    - RetroArch - [PPSSPP](https://docs.libretro.com/library/ppsspp/)
        - [System files](https://docs.libretro.com/library/ppsspp/#installing-from-the-core-system-files-downloader) required for operation.

## RetroArch (Steam)

[LibRetro Docs](https://docs.libretro.com/guides/install-windows/)

- Important directories are highlighted, with corresponding systems/cores not working without these directories and files.
- [Info files](https://github.com/libretro/libretro-core-info) required for non Steam cores to work on Steam.
- [BIOS files](https://github.com/Abdess/retroarch_system).

??? info "Directory Layout"
    <pre>
    RetroArch
        ├───assets
        ├───autoconfig
        ├───bearer
        ├───cheats
        ├───config
        ├───cores
        ├───database
        ├───downloads
        ├───filters
        ├───iconengines
        ├───imageformats
        ├───==info==
        ├───logs
        ├───mist
        ├───overlays
        ├───platforms
        ├───playlists
        ├───recordings
        ├───saves
        │   ├───fbneo
        │   ├───GBA
        │   ├───gbc
        │   ├───GC
        │   │   └───User
        │   │       └───Wii
        │   ├───MD
        │   ├───n64
        │   ├───NDS
        │   ├───pcsx2
        │   │   └───MemCard.ps2
        │   ├───PSP
        │   │   └───SAVEDATA
        │   │       ├───ULUS10002LUMINES
        │   │       └───ULUS10042001
        │   ├───PSX
        │   ├───Slot 1
        │   ├───Slot 2
        │   ├───SNES
        │   └───User
        │       ├───Cache
        │       │   ├───GameCovers
        │       │   └───Shaders
        │       ├───Config
        │       ├───Dump
        │       │   ├───Audio
        │       │   ├───DSP
        │       │   ├───Frames
        │       │   ├───Objects
        │       │   ├───SSL
        │       │   └───Textures
        │       ├───GameSettings
        │       ├───GC
        │       │   ├───EUR
        │       │   │   ├───Card A
        │       │   │   └───Card B
        │       │   ├───JAP
        │       │   │   ├───Card A
        │       │   │   └───Card B
        │       │   └───USA
        │       │       ├───Card A
        │       │       └───Card B
        │       ├───Load
        │       │   └───Textures
        │       ├───Logs
        │       │   └───Mail
        │       ├───Maps
        │       ├───ResourcePacks
        │       ├───ScreenShots
        │       ├───Shaders
        │       │   └───Anaglyph
        │       ├───StateSaves
        │       ├───Styles
        │       ├───Themes
        │       └───Wii
        │           ├───import
        │           ├───meta
        │           ├───shared1
        │           ├───shared2
        │           │   └───sys
        │           ├───sys
        │           ├───ticket
        │           ├───title
        │           ├───tmp
        │           └───wfs
        ├───screenshots
        ├───shaders
        ├───states
        ├───styles
        ├───system
        │   ├───dolphin-emu
        │   │   └───Sys
        │   ├───Mupen64plus
        │   │   ├───cache
        │   │   └───shaders
        │   ├───==pcsx2==
        │   │   ├───==bios==    | BIOS files must exist here for LRPS2 to work.
        │   │   ├───cheats
        │   │   └───cheats_ws
        │   └───==PPSSPP==
        │       ├───debugger
        │       ├───flash0
        │       ├───lang
        │       ├───mime
        │       ├───shaders
        │       ├───themes
        │       └───vfpu
        └───thumbnails
    </pre>