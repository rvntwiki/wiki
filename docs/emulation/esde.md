# ES-DE

[ES-DE](https://www.es-de.org/) is a frontend for browsing and launching games from your multi-platform collection. It comes preconfigured for use with a large selection of emulators, game engines, game managers and gaming services.

ES-DE can use either standalone emulators or RetroArch and its cores. This include RetroArch on [Steam](https://store.steampowered.com/app/1118310/RetroArch/).

---

## Settings
- `es_find_rules_installer.xml`: Examples of supported emulators.
- `es_find_rules.xml`: Emulator locations.
- `es_systems.xml`: Emulator launch and alternate emulators options.

---

### Locations
=== "ES-DE Portable"
    - `resources\systems\windows\es_find_rules.xml`
    - `resources\systems\windows\es_systems.xml`
=== "ES-DE Android"
    - `custom_systems\es_find_rules.xml`
    - `custom_systems\es_systems.xml`

### Examples
??? example "es_find_rules.xml"
    === "ES-DE Portable"
        ``` xml    
        <emulator name="DUCKSTATION">
            <!-- Sony PlayStation 1 emulator DuckStation -->
            <rule type="staticpath">
                <entry>%ESPATH%\Emulators\duckstation\duckstation-nogui-x64-ReleaseLTCG.exe</entry>
                <entry>%ESPATH%\Emulators\duckstation\duckstation-qt-x64-ReleaseLTCG.exe</entry>
                <entry>%ESPATH%\..\Emulators\duckstation\duckstation-nogui-x64-ReleaseLTCG.exe</entry>
                <entry>%ESPATH%\..\Emulators\duckstation\duckstation-qt-x64-ReleaseLTCG.exe</entry>
            </rule>
        </emulator>
        ```
    === "ES-DE Android"
        ``` xml
        <emulator name="DUCKSTATION">
            <!-- Sony PlayStation 1 emulator DuckStation -->
            <rule type="androidpackage">
                <entry>com.github.stenzek.duckstation/.EmulationActivity</entry>
            </rule>
        </emulator>
        ```

??? example "es_systems.xml"
    === "ES-DE Portable"
        ``` xml
        <system>
            <name>psx</name>
            <fullname>Sony PlayStation</fullname>
            <path>%ROMPATH%\psx</path>
            <extension>.bin .BIN .cbn .CBN .ccd .CCD .chd .CHD .cue .CUE .ecm .ECM .exe .EXE .img .IMG .iso .ISO .m3u .M3U .mdf .MDF .mds .MDS .minipsf .MINIPSF .pbp .PBP .psexe .PSEXE .psf .PSF .toc .TOC .z .Z .znx .ZNX .7z .7Z .zip .ZIP</extension>
            <command label="Beetle PSX">%EMULATOR_RETROARCH% -L %CORE_RETROARCH%\mednafen_psx_libretro.dll %ROM%</command>
            <command label="Beetle PSX HW">%EMULATOR_RETROARCH% -L %CORE_RETROARCH%\mednafen_psx_hw_libretro.dll %ROM%</command>
            <command label="PCSX ReARMed">%EMULATOR_RETROARCH% -L %CORE_RETROARCH%\pcsx_rearmed_libretro.dll %ROM%</command>
            <command label="SwanStation">%EMULATOR_RETROARCH% -L %CORE_RETROARCH%\swanstation_libretro.dll %ROM%</command>
            <command label="DuckStation (Standalone)">%EMULATOR_DUCKSTATION% -batch %ROM%</command>
            <command label="Mednafen (Standalone)">%EMULATOR_MEDNAFEN% -force_module psx %ROM%</command>
            <platform>psx</platform>
            <theme>psx</theme>
        </system>
        ```
    === "ES-DE Android"
        ``` xml
        <system>
            <name>psx</name>
            <fullname>Sony PlayStation</fullname>
            <path>%ROMPATH%/psx</path>
            <extension>.bin .BIN .cbn .CBN .ccd .CCD .chd .CHD .cue .CUE .ecm .ECM .exe .EXE .img .IMG .iso .ISO .m3u .M3U .mdf .MDF .mds .MDS .minipsf .MINIPSF .pbp .PBP .psexe .PSEXE .psf .PSF .toc .TOC .z .Z .znx .ZNX .7z .7Z .zip .ZIP</extension>
            <command label="Beetle PSX">%EMULATOR_RETROARCH% %EXTRA_CONFIGFILE%=/storage/emulated/0/Android/data/%ANDROIDPACKAGE%/files/retroarch.cfg %EXTRA_LIBRETRO%=mednafen_psx_libretro_android.so %EXTRA_ROM%=%ROM%</command>
            <command label="Beetle PSX HW">%EMULATOR_RETROARCH% %EXTRA_CONFIGFILE%=/storage/emulated/0/Android/data/%ANDROIDPACKAGE%/files/retroarch.cfg %EXTRA_LIBRETRO%=mednafen_psx_hw_libretro_android.so %EXTRA_ROM%=%ROM%</command>
            <command label="PCSX ReARMed">%EMULATOR_RETROARCH% %EXTRA_CONFIGFILE%=/storage/emulated/0/Android/data/%ANDROIDPACKAGE%/files/retroarch.cfg %EXTRA_LIBRETRO%=pcsx_rearmed_libretro_android.so %EXTRA_ROM%=%ROM%</command>
            <command label="SwanStation">%EMULATOR_RETROARCH% %EXTRA_CONFIGFILE%=/storage/emulated/0/Android/data/%ANDROIDPACKAGE%/files/retroarch.cfg %EXTRA_LIBRETRO%=swanstation_libretro_android.so %EXTRA_ROM%=%ROM%</command>
            <command label="DuckStation (Standalone)">%EMULATOR_DUCKSTATION% %EXTRABOOL_resumeState%=false %EXTRA_bootPath%=%ROMSAF%</command>
            <command label="ePSXe (Standalone)">%EMULATOR_EPSXE% %ACTION%=android.intent.action.MAIN %EXTRA_com.epsxe.ePSXe.isoName%=%ROMSAF%</command>
            <command label="FPseNG (Standalone)">%EMULATOR_FPSE-NG% %ACTION%=android.intent.action.VIEW %DATA%=%ROMPROVIDER%</command>
            <command label="FPse (Standalone)">%EMULATOR_FPSE% %ACTION%=android.intent.action.VIEW %DATA%=%ROMPROVIDER%</command>
            <platform>psx</platform>
            <theme>psx</theme>
        </system>
        ```

---

## Updating
=== "ES-DE Portable"
    1. Rename your old ES-DE directory. E.g. `ES-DE_OLD`.
    1. Extract the new version of ES-DE.
    1. Move your games from `ES-DE_OLD\ROMs\` `to ES-DE\ROMs\`. Contains Roms.
    1. Move your emulators from `ES-DE_OLD\Emulators\` to `ES-DE\Emulators\`. Contains Emulators.
    1. Move the contents of `ES-DE_OLD\ES-DE\` to `ES-DE\ES-DE\`. Contains settings and downloaded media.
    1. Update themes using the theme downloader to get support for all the latest systems and features.
=== "ES-DE Android"
    1. Download latest version from Patreon.
    1. Side load app.

## Emulators
### Commodore Business Machines
#### Consoles

##### Commodore 64
=== "ES-DE Portable"
    1. RetroArch - [VICE x64sc Accurate](https://docs.libretro.com/library/vice/)
        - BIOS Required: False
=== "ES-DE Android"
    1. RetroArch - [VICE x64sc Accurate](https://docs.libretro.com/library/vice/)
        - BIOS Required: False

---

### Microsoft
#### Consoles

##### Xbox
=== "ES-DE Portable"
    1. ?
=== "ES-DE Android"
    1. No emulator available.

##### Xbox 360
=== "ES-DE Portable"
    1. ?
=== "ES-DE Android"
    1. No emulator available.

##### Xbox One
=== "ES-DE Portable"
    1. ?
=== "ES-DE Android"
    1. No emulator available.

##### Xbox Series
=== "ES-DE Portable"
    1. ?
=== "ES-DE Android"
    1. No emulator available.

---

### Nintendo
#### Consoles

##### Nintendo Entertainment System
=== "ES-DE Portable"
    1. RetroArch - [Mesen](https://docs.libretro.com/library/mesen/)
        - BIOS Required: False
    1. RetroArch - [Mesen](https://docs.libretro.com/library/mesen/)
        - BIOS Required: False

##### Super Nintendo Entertainment System
=== "ES-DE Portable"
    - RetroArch - [bsnes hd beta](https://docs.libretro.com/library/bsnes_accuracy/)
        - BIOS Required: False
=== "ES-DE Android"
    - RetroArch - [bsnes hd beta](https://docs.libretro.com/library/bsnes_accuracy/)
        - BIOS Required: False

##### Nintendo 64
=== "ES-DE Portable"
    - RetroArch - [Mupen64Plus-Next](https://docs.libretro.com/library/mupen64plus/)
        - BIOS Required: False
=== "ES-DE Android"
    - RetroArch - [Mupen64Plus-Next](https://docs.libretro.com/library/mupen64plus/)
        - BIOS Required: False

##### Gamecube
=== "ES-DE Portable"
    - Standalone - [Dolphin](https://dolphin-emu.org/)
=== "ES-DE Android"
    - Standalone - [Dolphin](https://dolphin-emu.org/)
        - BIOS Required: False
        - Avoid Playstore version. Use F-Droid or direct from [website](https://dolphin-emu.org/).
        - Issues running as a stand alone

##### Wii
=== "ES-DE Portable"
    - Standalone - [Dolphin](https://dolphin-emu.org/)
=== "ES-DE Android"
    - Standalone - [Dolphin](https://dolphin-emu.org/)
        - BIOS Required: False
        - Issues running as a stand alone

##### Wii U
=== "ES-DE Portable"
    - Standalone - [CeMu](https://cemu.info/)
=== "ES-DE Android"
    - Not Used

##### Switch
=== "ES-DE Portable"
    - Standalone - [Ryujinx](https://ryujinx.org/)
=== "ES-DE Android"
    - Not Used

---

#### Handhelds
##### Gameboy
=== "ES-DE Portable"
    - RetroArch - [mGBA](https://docs.libretro.com/library/mgba/)
        - BIOS Required: False
=== "ES-DE Android"
    - RetroArch - [mGBA](https://docs.libretro.com/library/mgba/)
        - BIOS Required: False

##### Gameboy Colour
=== "ES-DE Portable"
    - RetroArch - [mGBA](https://docs.libretro.com/library/mgba/)
        - BIOS Required: False
=== "ES-DE Android"
    - RetroArch - [mGBA](https://docs.libretro.com/library/mgba/)
        - BIOS Required: False

##### Gameboy Advance
=== "ES-DE Portable"
    - RetroArch - [mGBA](https://docs.libretro.com/library/mgba/)
        - BIOS Required: False
=== "ES-DE Android"
    - RetroArch - [mGBA](https://docs.libretro.com/library/mgba/)
        - BIOS Required: False

##### Nintendo DS
=== "ES-DE Portable"
    - RetroArch - [DeSmuME](https://docs.libretro.com/library/desmume)
=== "ES-DE Android"
    - RetroArch - [DeSmuME](https://docs.libretro.com/library/desmume)
        - BIOS Required: False

##### Nintendo 3DS
=== "ES-DE Portable"
    - Standalone - [Lime3DS](https://github.com/Lime3DS/Lime3DS)
    - ~~Standalone - [Citra](https://docs.libretro.com/library/citra/)~~ [^1]    
    [^1]: Citra has been [discontinued](https://citra-emu.org/) as of March 2024
=== "ES-DE Android"
    - Standalone - [Lime3DS](https://github.com/Lime3DS/Lime3DS)
        - BIOS Required: False
        - Issues running as a stand alone.

---

### Sega
#### Consoles

##### Master System
=== "ES-DE Portable"
    - RetroArch - [Genesis Plus GX Wide](https://docs.libretro.com/library/genesis_plus_gx/)
=== "ES-DE Android"
    - RetroArch - [Genesis Plus GX Wide](https://docs.libretro.com/library/genesis_plus_gx/)
        - BIOS Required: False

---

##### Megadrive
=== "ES-DE Portable"
    - RetroArch - [Genesis Plus GX Wide](https://docs.libretro.com/library/genesis_plus_gx/)
    - Some roms may require [patching](https://www.libretro.com/index.php/genesis-plus-gx-wide-now-available-for-libretroretroarch/)
=== "ES-DE Android"
    - RetroArch - [Genesis Plus GX Wide](https://docs.libretro.com/library/genesis_plus_gx/)
        - BIOS Required: False

---

##### Mega CD
=== "ES-DE Portable"
    - RetroArch - [Genesis Plus GX Wide](https://docs.libretro.com/library/genesis_plus_gx/)
=== "ES-DE Android"
    - RetroArch - [Genesis Plus GX Wide](https://docs.libretro.com/library/genesis_plus_gx/)
        - BIOS Required: True

---

##### 32X
=== "ES-DE Portable"
    - RetroArch - [PicoDrive](https://docs.libretro.com/library/picodrive/)
=== "ES-DE Android"
    - RetroArch - [PicoDrive](https://docs.libretro.com/library/picodrive/)
        - BIOS Required: False

---

##### Saturn
=== "ES-DE Portable"
    - RetroArch - [Beetle Saturn](https://docs.libretro.com/library/beetle_saturn/)
=== "ES-DE Android"

---

##### Dreamcast
=== "ES-DE Portable"
    - RetroArch - [Flycast](https://docs.libretro.com/library/flycast/)

---

#### Handhelds
=== "ES-DE Portable"
    - RetroArch - [Genesis Plus GX Wide](https://docs.libretro.com/library/genesis_plus_gx/)


### Sony
#### Consoles

##### PlayStation
=== "ES-DE Portable"
    - RetroArch - [Beetle PSX HW](https://docs.libretro.com/library/beetle_psx_hw/)
=== "ES-DE Android"

---

##### PlayStation 2
=== "ES-DE Portable"
    - RetroArch - [LRPS2 (LibRetro PCSX2)](https://docs.libretro.com/library/pcsx2/)
    - Standalone - [PCSX2](https://pcsx2.net/)
        - Primarily used to enable HD texture packs which are located under `Emulation\Texture Packs`
        - Texture packs are region specific.

---

##### PlayStation 3
=== "ES-DE Portable"
    - Standalone - [RPCS3](https://rpcs3.net/)
    - File system directories located under `Emulation\File Systems`
    - Games must first be installed manually and then have a shortcut created and moved to `Roms/PlayStation3/shortcuts`
=== "ES-DE Android"
    - Not Used

---

##### PlayStation 4
=== "ES-DE Portable"
    ???
=== "ES-DE Android"
    - Not Used



#### Handhelds

##### PlayStation Portable
=== "ES-DE Portable"
    - RetroArch - [PPSSPP](https://docs.libretro.com/library/pcsx2/)
    - Current issue with internal fonts not rendering properly.

---

##### PlayStation Vita
=== "ES-DE Portable"
    - Standalone - [Vita3k]()
    - Require's games to be installed first. [Work.bin/rif keys](https://nopaystation.com/search)
    - Games must first be installed manually and then have a [shortcut](https://github.com/dragoonDorise/EmuDeck/wiki/vita3k#how-to-configure-vita3k-to-work-with-emulationstation-de) created and moved to `Roms/PlayStationVita/shortcuts`

---
