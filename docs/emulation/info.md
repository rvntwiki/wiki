# Information

## Preservation
- [/r/Roms Megathread](https://r-roms.github.io/)
- [Myrient](https://myrient.erista.me/)
- [Emulator Files](https://emulation.gametechwiki.com/index.php/Emulator_files)

## ROM Descriptors
| **Suffix** | **Description** |
|----------|-------------------|
| (USA)    | Originally released in  North American - NTSC &#124; 60Hz |
| (Europe) | Originally released in Europe - PAL &#124; 50Hz |
| (Wii...) | Released on the Wii Virtual Console |
| (Rev 1)  | First updated version of the game | 
| (Rev 2)  | Second updated version of the game |
| 1G1R     | [1 Game 1 Rom](https://www.romcenter.com/wiki/doku.php?id=1g1r) |

## Consoles
### Nintendo - Switch

#### System Files
`prod.keys` and `title.keys` are [required](https://prodkeys.net/ryujinx-prod-keys/) for emulators and various tools related to the Nintendo Switch.

Firmware is also [required](https://prodkeys.net/switch-firmwares/) by emulators.


## File Trees
### Frontends
```
/Emulators/
└── Frontends
    ├── EmulationStation-DE
    └── RetroArch
```

```
/Steam/steamapps/common/RetroArch/
├── assets
│   └── wallpapers
├── autoconfig
├── bearer
├── cheats
├── config
│   ├── record
│   └── remaps
├── cores
├── database
│   └── rdb
├── downloads
├── filters
│   ├── audio
│   └── video
├── iconengines
├── imageformats
├── info
├── logs
├── mist
├── overlays
│   └── keyboards
├── platforms
├── playlists
├── recordings
├── saves
├── screenshots
├── shaders
├── states
├── styles
├── system
└── thumbnails
```
### Live
```
/Emulation/
├── File Systems
│   ├── PlayStation 3
│   └── PlayStation Vita
├── Roms
│   ├── 32X
│   ├── 3DS
│   ├── C64
│   ├── Dreamcast
│   ├── GameBoy
│   ├── GameBoyAdvance
│   ├── GameBoyColour
│   ├── GameCube
│   ├── GameGear
│   ├── MasterSystem
│   ├── MegaCD
│   ├── MegaDrive
│   ├── N64
│   ├── NDS
│   ├── NES
│   ├── PlayStation
│   ├── PlayStation2
│   ├── PlayStation3
│   ├── PlayStationPortable
│   ├── PlayStationVita
│   ├── SNES
│   ├── Saturn
│   ├── Switch
│   ├── WD
│   ├── Wii
│   ├── WiiU
│   ├── Xbox
│   └── Xbox360
└── Texture Packs
    └── PS2
```

### Storage
```
/Emulators/
├── Artwork
│   ├── Logos
│   └── Posters
├── BIOS
│   ├── 3DO - 3DO
│   ├── Atari - 7800
│   ├── Atari - Jaguar
│   ├── Atari - Lynx
│   ├── Coleco - ColecoVision
│   ├── Commodore  - Commodore 64
│   ├── Fujitsu - FM Towns
│   ├── Microsoft - MSX
│   ├── Microsoft - Xbox
│   ├── Multisystem - LibRetro
│   ├── Multisystem - MESS
│   ├── Multisystem - Mednafen
│   ├── Multisystem - Mesen2
│   ├── Multisystem - OpenEmu
│   ├── Multisystem - PCem
│   ├── Multisytem - 86Box
│   ├── NEC - PC Engine - TurboGrafx
│   ├── NEC - PC-6000
│   ├── NEC - PC-8800
│   ├── NEC - PC-9800
│   ├── Nintendo - 3DS
│   ├── Nintendo - DS
│   ├── Nintendo - Gameboy
│   ├── Nintendo - Gameboy Advance
│   ├── Nintendo - Gameboy Colour
│   ├── Nintendo - Gameboy Pocket
│   ├── Nintendo - Gamecube - Wii
│   ├── Nintendo - Nintendo 64
│   ├── Nintendo - Super Nintendo Entertainment System
│   ├── Nintendo - Switch
│   ├── Nintendo - Wii U
│   ├── Sega - Dreamcast
│   ├── Sega - Master System
│   ├── Sega - Megadrive
│   ├── Sega - Saturn
│   ├── Sharp - X1
│   ├── Sharp - X68000
│   ├── Sony - PlayStation
│   ├── Sony - PlayStation 2
│   ├── Sony - PlayStation 3
│   ├── Sony - PlayStation Portable
│   ├── Sony - PlayStation Vita
│   └── Sony - PocketStation
├── Cache
│   └── ToSort
├── Dats
└── Roms
    ├── Commodore - Commodore 64
    ├── Microsoft - Xbox
    ├── Microsoft - Xbox (BIOS)
    ├── Microsoft - Xbox 360
    ├── Microsoft - Xbox 360 (XBLA)
    ├── Nintendo - 3DS (DLC and Updates)
    ├── Nintendo - Gameboy
    ├── Nintendo - Gameboy Advance
    ├── Nintendo - Gameboy Advance (Video)
    ├── Nintendo - Gameboy Color
    ├── Nintendo - Gamecube
    ├── Nintendo - Gamecube (BIOS)
    ├── Nintendo - New Nintendo 3DS (Encrypted)
    ├── Nintendo - Nintendo 3DS (Decrypted)
    ├── Nintendo - Nintendo 3DS (Encrypted)
    ├── Nintendo - Nintendo 64 (BigEndian)
    ├── Nintendo - Nintendo DS (Decrypted)
    ├── Nintendo - Nintendo DSi (Decrypted)
    ├── Nintendo - Nintendo Entertainment System (Headered)
    ├── Nintendo - Super Nintendo Entertainment System
    ├── Nintendo - Switch
    ├── Nintendo - Virtual Boy
    ├── Nintendo - Wii
    ├── Nintendo - Wii (Digital)
    ├── Nintendo - Wii U
    ├── Nintendo - Wii Ware
    ├── Nintendo - amiibo
    ├── Sega - 32X
    ├── Sega - Dreamcast
    ├── Sega - Game Gear
    ├── Sega - Master System
    ├── Sega - Mega Drive
    ├── Sega - SG-1000
    ├── Sony - PlayStation
    ├── Sony - PlayStation (BIOS)
    ├── Sony - PlayStation (SBI)
    ├── Sony - PlayStation 2
    ├── Sony - PlayStation 2 (BIOS)
    └── Sony - PlayStation Portable
```