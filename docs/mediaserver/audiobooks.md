---
title: Audiobooks
Summary: Tips for configuring the Apache2 web server
Author: rvnt
Date: 2023-06-14
base_url: https://sanctum.spctr.uk/
---

# Audiobooks
## Resources
[Seanap's Plex & Audiobook Guide](https://github.com/seanap/Plex-Audiobook-Guide)

## Workflow
1. Readarr will obtain the audiobooks.
1. beets will move, tag, rename, and organise audiobooks.
1. Audiobooks will be accessible from Plex and Audiobookshelf.

## Obtain
### Readarr
!!! warning
    Please note that seperate instances of Readarr are required for ebooks and audiobooks.

- Install [Readarr](https://github.com/Readarr/Readarr)
- Under 'Settings > General', set the 'Instance Name' to 'ReadarrAB'
- Configure as required.

## Prepare Beets
### beets-audible
A beets instance set up for Audiobooks is required.

- Install [beets](https://github.com/beetbox/beets)
- Install [beets-audible](https://github.com/Neurrone/beets-audible)
    1. `pip install beets-audible beets-copyartifacts3`
- Create a seperate config file for audiobooks.
    1. Navigate to ~/.config/beets
- Copy the existing or default config file
    1. `cp config.yaml configAudiobooks.yaml`
- Create an alias to simplify the import process.
    1. Open ~/.bash_aliases
    1. Add the line `alias beetAudiobooks='beet -c ~/.config/beets/configAudiobooks.yaml'`
- Populate config file appropriately.

    ??? info "beetsAudiobooks.yaml"
        ```
        # add audible to the list of plugins
        # copyartifacts is optional but recommended if you're manually specifying metadata via metadata.yml, see the "Importing non-audible content" section
        plugins: audible copyartifacts edit fromfilename scrub

        directory: /home/media/library/server/audiobooks/
        library: /home/user/.config/beets/audiobooklibrary.db

        # Place books in their own folders to be compatible with Booksonic and Audiobookshelf servers
        paths:
        # For books that belong to a series
        "albumtype:audiobook series_name::.+ series_position::.+": $albumartist/%ifdef{series_name}/%ifdef{series_position} - $album%aunique{}/$track - $title
        "albumtype:audiobook series_name::.+": $albumartist/%ifdef{series_name}/$album%aunique{}/$track - $title
        # Stand-alone books
        "albumtype:audiobook": $albumartist/$album%aunique{}/$track - $title
        default: $albumartist/$album%aunique{}/$track - $title
        singleton: Non-Album/$artist - $title
        comp: Compilations/$album%aunique{}/$track - $title
        albumtype_soundtrack: Soundtracks/$album/$track $title

        # disables musicbrainz lookup, as it doesn't help for audiobooks
        musicbrainz:
        enabled: no

        audible:
        # if the number of files in the book is the same as the number of chapters from Audible,
        # attempt to match each file to an audible chapter
        match_chapters: true
        source_weight: 0.0 # disable the source_weight penalty
        fetch_art: true # whether to retrieve cover art
        include_narrator_in_artists: true # include author and narrator in artist tag. Or just author
        keep_series_reference_in_title: true # set to false to remove ", Book X" from end of titles
        keep_series_reference_in_subtitle: true # set to false to remove subtitle if it contains the series name and the word book ex. "Book 1 in Great Series", "Great Series, Book 1"

        write_description_file: true # output desc.txt
        write_reader_file: true # output reader.txt

        copyartifacts:
        extensions: .yml # so that metadata.yml is copied, see below

        scrub:
        auto: yes # optional, enabling this is personal preference
        ```

- Add custom overrides as required

    ??? info "Custom Overrides"
        ```Car
        # Custom Overrides
        "albumartist:'writing as A. N. Roquelaure' albumtype:audiobook series_name::.+ series_position::.+": A. N. Roquelaure/%ifdef{series_name}/%ifdef{series_position} - $album%aunique{} ($year)/$track - $title
        "albumartist:'writing as Anne Rampling' albumtype:audiobook": Anne Rampling/$album%aunique{} ($year)/$track - $title
        "albumartist:'Hideyuki Kikuchi' albumtype:audiobook series_name::.+ series_position::.+": Hideyuki Kikuchi/%ifdef{series_name}/%ifdef{series_position} - $album%aunique{} ($year)/$track - $title
        "albumartist:'Bruce Campbell' albumtype:audiobook": Bruce Campbell/$album%aunique{} ($year)/$track - $title
        "albumartist:'Jim rey' albumtype:audiobook": Jim Carrey/$album%aunique{} ($year)/$track - $title
        "albumartist:'Neil deGrasse Tyson'": Neil deGrasse Tyson/$album%aunique{} ($year)/$track - $title
        # For books that belong to a series
        "albumtype:audiobook series_name::.+ series_position::.+": $albumartist/%ifdef{series_name}/%ifdef{series_position} - $album%aunique{} ($year)/$track - $title
        "albumtype:audiobook series_name::.+": $albumartist/%ifdef{series_name}/$album%aunique{} ($year)/$track - $title
        # Stand-alone books
        "albumtype:audiobook": $albumartist/$album%aunique{} ($year)/$track - $title
        default: $albumartist/$album%aunique{}/$track - $title
        singleton: Non-Album/$artist - $title
        comp: Compilations/$album%aunique{}/$track - $title
        albumtype_soundtrack: Soundtracks/$album/$track $title
        ```

## Prepare Audiobook
### Required files

??? info "metadata.yml"
    ```
    title: Audiobook Name
    authors: ["Author"]
    narrators:
    - "Narrator 1"
    - "Narrator 2"
    description: Synopsis / Blurb
    genres: ["Genre 1", "Genre 2"]
    releaseDate: YYYY-MM-DD
    publisher: Publisher

    # optional fields
    language: English
    series: Series Name
    seriesPosition: Series Number
    ```

- metadata.yml must be formatted correctly else import will fail

??? info "cover.jpg"
    The book cover must be included and be named "cover" with an extension of jpeg/jpg/png.

??? info "Chapters"
    Chapters can be added to m4b files.

    A chapters file must exist and contain chapter times in the following format:

    ```
    00:00.000 Chapter 01
    00:20.300 Chapter 02
    02:19.680 Chapter 03
    10:54.470 Chapter 04
    16:52.360 Chapter 05
    22:21.150 Chapter 06
    32:27.140 Chapter 07
    ```

    If the audiobooks file name is `BookTitle.m4b` then the chapters file would have to be named `BookTitle.chapters.txt`

    Move both files to Auto-M4b's merge directory to generate the chapters.

### Import
- Ensure the m4b, metadata.yml, and cover files are in the same directory.
- Import an audiobook: `beetAudiobooks im /path/to/audiobook`

## Consume
### Plex

- Install [AudNexus](https://github.com/djdembeck/Audnexus.bundle) metadata agent.
    1. Either extract the plugin: `/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Plug-ins/`.
    1. Or use [Plex Web Tools](https://github.com/ukdtom/WebTools.bundle).
    1. Or use [Plex Web Tools NG](https://github.com/WebTools-NG/WebTools-NG).
- From Plex, nagivate to the agents settings.
    1. `Settings > Agents`
- Under `Artists`, select the `Audnexus Agent` tab, and move the `Audnexus Agent` above `Local media Assets`.
    1. Optional: Click the gear icon and configure as needed.
- Under `Albums`, select the `Audnexus Agent` tab, and move the `Audnexus Agent` above `Local media Assets`.
    1. Optional: Click the gear icon and configure as needed.
- Create or modify a Audiobook Library.
    1. Set it as a music library.
    1. Add the location of the audiobook directory.
    1. Under advanced:    
        - Agent - **Audnexus Agent**.
        - Set the default region.
        - Album sorting - **By Name**.
        - Prefer Local Metadata - **Uncheck**.
        - Store track progress - **Check**.
        - Genres - **None**.
        - Album Art - **Local Files Only**.


### AudiobookShelf

- Install [AudioBookShelf](https://github.com/advplyr/audiobookshelf)
- Configure as required.
