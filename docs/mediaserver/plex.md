# Plex

## Album Types

| **ID3v2 Tag** | **Plex Album Type** |
| :-----------: | :-----------------: |
| album; compilation | Compilations |
| album; soundtrack | Soundtracks |
| album; remix | Remixes |
| album; live | Live Albums |
| album; demo | Demos |
| single | Singles & EPs |
| ep | Singles & EPs |

*https://forums.plex.tv/t/plex-media-server-1-27-2-5929-support-for-release-type-embedded-tags/799101/93