# Startup

## Mount Storage Drives
### Combined

    sshfs -o uid=1000,gid=1000 -o allow_other,reconnect jarvis@logan:/home/storage/logan/tv /home/storage/logan/tv && sshfs -o uid=1000,gid=1000 -o allow_other,reconnect jarvis@logan:/home/storage/logan/music /home/storage/logan/music && sshfs -o uid=1000,gid=1000 -o allow_other,reconnect jarvis@logan:/home/storage/logan/movies /home/storage/logan/movies && sshfs -o uid=1000,gid=1000 -o allow_other,reconnect jarvis@logan:/home/storage/logan/library /home/storage/logan/library && sshfs -o uid=1000,gid=1000 -o allow_other,reconnect jarvis@logan:/home/storage/logan/overflow /home/storage/logan/overflow

### Individual

    sshfs -o uid=1000,gid=1000 -o allow_other,reconnect jarvis@logan:/home/storage/logan/tv /home/storage/logan/tv
    sshfs -o uid=1000,gid=1000 -o allow_other,reconnect jarvis@logan:/home/storage/logan/music /home/storage/logan/music
    sshfs -o uid=1000,gid=1000 -o allow_other,reconnect jarvis@logan:/home/storage/logan/movies /home/storage/logan/movies
    sshfs -o uid=1000,gid=1000 -o allow_other,reconnect jarvis@logan:/home/storage/logan/library /home/storage/logan/library
    sshfs -o uid=1000,gid=1000 -o allow_other,reconnect jarvis@logan:/home/storage/logan/overflow /home/storage/logan/overflow

## Decrypt Mounted Partitions
### Combined

    encfs --public /home/storage/logan/music /home/media/music/logan && encfs --public /home/storage/logan/movies /home/media/movies/logan && encfs --public /home/storage/logan/tv /home/media/tv/logan && encfs --public /home/storage/logan/library /home/media/library/logan && encfs --public /home/storage/stark/music /home/media/music/stark && encfs --public /home/storage/stark/movies /home/media/movies/stark && encfs --public /home/storage/stark/tv /home/media/tv/stark && encfs --public /home/storage/stark/library /home/media/library/stark
    
### Individual

    encfs --public /home/storage/logan/music /home/media/music/logan
    encfs --public /home/storage/logan/movies /home/media/movies/logan
    encfs --public /home/storage/logan/tv /home/media/tv/logan
    encfs --public /home/storage/logan/library /home/media/library/logan
    encfs --public /home/storage/stark/music /home/media/music/stark
    encfs --public /home/storage/stark/movies /home/media/movies/stark
    encfs --public /home/storage/stark/tv /home/media/tv/stark

## Create Syslink for Overflow
### Combined

    ln -s /home/storage/logan/overflow/Plex/Media/var/lib/plexmediaserver/Library/Application\ Support/Plex\ Media\ Server/ && ln -s /home/storage/logan/overflow/Plex/Metadata /var/lib/plexmediaserver/Library/Application\ Support/Plex\ Media\ Server/

### Individual

    ln -s /home/storage/logan/overflow/Plex/Metadata /var/lib/plexmediaserver/Library/Application\ Support/Plex\ Media\ Server/
    ln -s /home/storage/logan/overflow/Plex/Media /var/lib/plexmediaserver/Library/Application\ Support/Plex\ Media\ Server/