---
title: Comics
Summary: Comic Protocol
Author: rvnt
Date: 2024-02-23
base_url: https://sanctum.spctr.uk/
---

# Comics

## Process Overview

Current process for sorting comics to be viewed by Kavita:

1. Obtain comics from [Kapowarr](https://github.com/Casvt/Kapowarr).
1. Convert to from cbr/pdf to cbz.
1. Process and tag comics using [Comic Tagger](https://github.com/comictagger/comictagger).
1. Add to [Kavita](https://github.com/Kareadita/Kavita).

## Detailed Process

1. Obtain the requested comics from [Kapowarr](https://github.com/Casvt/Kapowarr) or other trusted sources.
1. Convert all CBR, CBT, CBA, PDF, etc comics to CBZ using pdf2comic tool.
1. Import them into [Comic Tagger](https://github.com/comictagger/comictagger) and Identify or Auto-Tag the comics, being sure to use the ComicRack style of metadata.
1. All comics must at least have an issue number, series name, and volume number. This can be in done in batch with Comic Tagger `comictagger.exe -g -s -t CR,CBL -m "series=Series Name, volume=1" C:\Comics\Series\*.cbz`
1.  Import them back into Comic Tagger, ensure the read style is 'ComicRack' and the modify style is 'ComicBookLover', press ++ctrl+c++ to copy the metadata from 'ComicRack' to 'ComicBookLover'.
1. Press ++ctrl+n++ to rename. The naming convention is `{Series Name} - V{Volume Number} - #{Issue Number} - {Issue Title}.cbz`.
1. Import the into Kavita with the following [folder structure](https://wiki.kavitareader.com/en/guides/managing-your-files).
1. Remove comic from Kapowarr if all issues from series have been grabbed.

```
Comics
  ┖── {Series Name}
      ┖── {Series Name} - V{Volume Number} - #{Issue Number} - {Issue Title}.cbz
```

!!! warning
    Please note the following caveats for this method:

    - Kapowarr is immature software and lacks features compared to other similar software such as LazyLibrarian or Readarr.
    - However, it is used in the hopes it will become the defacto for comic book management.
    - ComicRack and ComicBookLover metadata should be kept identical where possible.

Up to date as of 23-Feb-2024