# Beets

[**A media library management system**](https://beets.readthedocs.io/en/stable/)

## Search

All available [values](https://beets.readthedocs.io/en/stable/reference/pathformat.html#available-values) for beet formats.

    beet ls -af '$album: $albumtotal' weeknd

## Modify

beet mod -a albumartist:"weekend" albumartist="Weeknd"


## Misc
### config.yaml

    directory: /home/media/music/stark
    library: /home/jarvis/.config/beets/library.db

    ignore_video_tracks: yes
    threaded: yes
    art_filename: cover

    import:
        move: yes
        copy: no
        write: yes

    replace:
        '[\\/]': _
        '^\.': _
        '[\x00-\x1f]': _
        '[<>:"\?\*\|]': _
        '\.$': _
        '\s+$': ''
        '^\s+': ''
        '^-': _

    paths:
        default: $albumartist/$album%aunique{albumartist album, year albumdisambig}/$disc-$track $title
        albumtype:soundtrack: 'Soundtracks/$album%aunique{albumartist album, year albumdisambig}/$disc-$track $title'
        label:'Monstercat': 'Monstercat/$album%aunique{albumartist album, year albumdisambig}/$disc-$track $title'
        label:'Ministry of Sound' ^albumartist:'DJ Fresh' ^albumartist:'Moby': 'Ministry of Sound/[$catalognum] $album%aunique{albumartist album, year albumdisambig}/$disc-$track $title'
        label:'NOW' ^label:'Boy Better Know' ^label:'Not Now Music Limited': 'NOW/$album%aunique{albumartist album, year albumdisambig}/$disc-$track $title'
        label:'Walt Disney Records' album:"Now That's What I Call Disney": 'NOW/$album%aunique{albumartist album, year albumdisambig}/$disc-$track $title'
        label:'Virgin TV' album:'Now Dance': 'NOW/$album%aunique{albumartist album, year albumdisambig}/$disc-$track $title'
        album:"Now That’s What I Call Music": 'NOW/$album%aunique{albumartist album, year albumdisambig}/$disc-$track $title'
        label:'Kontor Records': 'Kontor/$album%aunique{albumartist album, year albumdisambig}/$disc-$track $title'
        label:'Roadrunner Records' album:'Headbangers Ball': 'Headbangers Ball/$album%aunique{albumartist album, year albumdisambig}/$disc-$track $title'

    max_filename_length: 130
    per_disc_numbering: yes

    ui:
        color: yes

    match:
        ignored: missing_tracks unmatched_tracks
        strong_rec_thresh: 0.10
        ignored_media: ['Data CD', 'DVD', 'DVD-Video', 'Blu-ray', 'HD-DVD', 'VCD', 'SVCD', 'UMD', 'VHS']

    plugins: fetchart embedart ftintitle lastgenre plexupdate missing permissions lastimport mbsync absubmit convert
    fetchart:
        auto: yes
        cautious: true
        cover_names: cover art folder front
        sources: fanarttv coverart amazon albumart filesystem
        enforce_ratio: yes
        fanarttv_key: *FanartTVKey*
        auto: yes
        remove_art_file: yes
    ftintitle:
        auto: yes
        ftintitle: ft. {0}
    lastgenre:
        auto: yes
    plexupdate:
        host: localhost
        port: 32400
        token: *PlexToken*
    missing:
        format: $albumartist - $album - $title
        count: no
        total: no
    permissions:
        file: 644
        dir: 755
    lastfm:
        user: *LastFMUsername*
    convert:
        copy_album_art: yes

