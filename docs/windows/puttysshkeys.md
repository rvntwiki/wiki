---
title: PuTTY & SSH Keys
Summary: Setting up PuTTY and SSH keys
Author: rvnt
Date: 2021-04-12
base_url: https://sanctum.rvnt.uk/
---

Setting up SSH Keys
===

Generating Keys
---

1. On client machine run PuTTYgen.exe:

    ![PuTTYgen-Open](img/1-puttysshkeys.png)

1. Generate a 4096 bit RSA key:

    ![PuTTYgen-Setup](img/2-puttysshkeys.png)

1. Save public and private key and store securely.

Copy Public Key
---

1. Connect to server:

    Using PuTTY connect to the server where the public key will be installed.

1. Copy public key:

    Back in PuTTYgen, select the entire key. it will start with `ssh-rsa` and end in the custom key comment `**clientName-serverName-YYYYMMDD**`. e.g.
    
        ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAgEAhH0kJGPdwAQv3GH3HR1yJS3s5+A4LoasJVwU3q/n3DlGMS+CD8MQFV2FbWrwjXTZgzv7u8Ncd66nmPdjqNxvt36OUKF8V5cGJoypsgllBMCygIAv3nHPkYAsuh/dsyFqWCXpiMu/9UEQjm3ZVtimwye7D3BMDabJgfdoX1iUoPVqMhhew2yEELln/YJRf7GYpmwOmIqynOZvegGSkKsQt7VDjQ4w6SXM7rKVv7sefQXWuh9tO1y2987YX58sRK/oe7SS8rjkNeTJ/zynpc0zXnuf5oA4I/NOuVoOCOt6wSlcPQTDH5mtec3ruVmHHHNndHgJiR2muuqnVBBod3Y1HhurZ3rG5rlgolnAhxU+LmDQwinHKhS9ItgbGR3Blk6QGSApIxg8m/011sR/x9nZL00J+1yuxLuq4gI8xfn8c2LOpr936ZBMIib1/IJKh33PZ06Z5iTaZWDQ8gTWjtHTm06jlxTmysP0JhSD0k3VvzxwX/owhrlkj+OJl0adwC3VMA900fK5jaVvKyJW4FrxXIsBitCCmGKajnFr/Lq8Oeh+r/gNcx58LzAADI9LOzGjUvmHkwn/uacoVAUQl44C1ME9To/6xoDBMpZQ+kt7xLUz0VhvS1vJtiELWDaatWvnqPKq/kqQ9IcnVnZtYZ+RIp/YGfERwaOpDt6oN5iEv1s= **clientName-serverName-YYYYMMDD**

1. Edit authorized_keys:

    ``` shell
    nano /home/**userName**/.ssh/authorized_keys
    ```

1. Paste public key on next blank line:

    ++rbutton++

1. Save and exit:

    1. ++ctrl+o++
    1. ++enter++
    1. ++ctrl+x++

1. Reload SSH daemon.

    ``` shell
    sudo systemctl restart ssh
    ```

Connecting with keys
---

1. Define custom host:

    ``` shell
    nano ~/.ssh/config
    ```

1. Paste and modify:

    <!-- ssh-->
    ``` apache
    Host **hostName**
        User **userName**
        Hostname **serverAddress**
        IdentityFile ~/.ssh/**clientName-serverName-YYYYMMDD**
        TCPKeepAlive yes
        IdentitiesOnly yes
    ```

1. Save and exit:

    1. ++ctrl+o++
    1. ++enter++
    1. ++ctrl+x++

1. Verify it works:

    Open PuTTY, load server profile, and view `Auth` options. 
    Select the correct private key and save server profile.
    Open connection to server.

    ![PuTTYgen-Auth](img/3-puttysshkeys.png)

Disable Password Authentication
---

!!! warning
    With `PasswordAuthentication no` set, if ssh keys are lost or corrupted no remote access will be possible.

1. Using PuTTY open a connection to server and run:

    ``` shell
    sudo nano /etc/ssh/sshd_config
    ```

1. Find and update the variable:

    ``` shell
    PasswordAuthentication no
    ```

1. Save and exit:

    1. ++ctrl+o++
    1. ++enter++
    1. ++ctrl+x++

1. Reload SSHd daemon:

    ``` shell
    sudo systemctl restart sshd
    ```