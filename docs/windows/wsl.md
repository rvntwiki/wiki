---
title: WSL2
Summary: Windows Subsystem for Linux 2
Author: rvnt
Date: 2024-11-26
base_url: https://sanctum.spctr.uk/
---

# WSL2

## Mount External Drive

### Identify Drive
_The following command is a PowerShell command that must be run as administrator_

``` powershell
Get-CimInstance -Query "SELECT * from Win32_DiskDrive"
```
_Example output:_
``` powershell
\\.\PHYSICALDRIVE0  CT1000BX500SSD1  1  1000202273280   CT1000BX500SSD1
\\.\PHYSICALDRIVE1  TOSHIBA HDWG480  1  8001560609280   TOSHIBA HDWG480
```
Identify which drive is to me mounted in WSL2 by comparing manufacturer and model number. Make note of which `\\.\PHYSICALDRIVE*` it is.

### Mount **to** WSL
_The following command is a Command Prompt command_

``` batch
wsl --mount \\.\PHYSICALDRIVE1 --bare
```

- The --bare flag mounts the entire disk without any partitions, meaning the entire disk is accessible as a single block device in WSL2.

#### Automatic Mount
Window's Task Scheduler app can be used to automate the mounting process at boot time.

When creating the task the following options should be used:

- General: `Run whether user is logged on or not`
- General: `Run with highest privileges`
- Trigger: `Begin the task: At startup`
- Actions: `Action: start a program`
- Actions: `Program/script: C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe`
- Actions: `Add arguments (optional): options: -ExecutionPolicy Bypass -File "C:\path\to\your\script.ps1"`

### Mount **in** WSL
_The following command is a WSL2/Linux command_

#### Identify Device
``` bash
ls /dev/sd*
```

#### Automatic Mount
Open fstab file
``` bash
sudo nano /etc/fstab
```

Add the line
``` bash
/dev/sd** /home/media ext4 defaults 0 0
```

Ensure correct disk and parition is used. e.g. sdc1

## Firewall
A firewall rule controls the traffic that is allowed or blocked on a network interface. Firewall rules can be set to permit or deny specific types of network traffic based on criteria such as IP addresses, port numbers, and protocols. In the context of WSL and Hyper-V, a firewall rule would manage which types of network traffic are allowed to reach or leave the WSL instance.

_The following commands are PowerShell commands that must be run as administrator_

### Create Rule
``` powershell
New-NetFirewallHyperVRule -Name "WSL2 SSH Inbound" -DisplayName "WSL2 SSH Inbound" -Direction Inbound -VMCreatorId '{40E0AC32-46A5-438A-A0B2-2B479E8F2E90}' -Protocol TCP -LocalPorts 22
```

- Name and DisplayName label the rule
- Direction states which way connections can be made.
- Protocol states whether TCP, UDP, or Any can be used.
- LocalPorts states the port to open.
- The GUID {40E0AC32-46A5-438A-A0B2-2B479E8F2E90} is associated with Windows Subsystem for Linux (WSL) in the context of Hyper-V firewall settings

### Search Rule
``` powershell
Get-NetFirewallHyperVRule | Where-Object { $_.Name -like "WSL2 SSH Inbound" }
```

### Delete Rule
``` powershell
Remove-NetFirewallHyperVRule -Name "WSL2 SSH Inbound"
```

---

## Port Proxy
A port proxy, also known as port forwarding, redirects network traffic from one port or IP address to another. It effectively acts as an intermediary that forwards traffic from a client to a server. This is particularly useful for making services running on a private or restricted network accessible from another network or the internet.

_The following commands are PowerShell commands that must be run as administrator_

### Create Proxy
``` powershell
$wslIP = wsl hostname -I
netsh interface portproxy add v4tov4 listenaddress=0.0.0.0 listenport=22 connectaddress=$wslIP connectport=22
```

- $wslIP is a variable that will contain the current WSL IP address.
- netsh command will forward port 22 to the IPv4 address stored in $wslIP.

### List Proxies
``` powershell
netsh interface portproxy show all
```

- netsh command will list all active port proxies.

### Delete Proxy
``` powershell
netsh interface portproxy delete v4tov4 listenaddress=0.0.0.0 listenport=22
```

- netsh command will delete port proxy for port 22.

## Useful
### .wslconfig

```
[wsl2]
kernelCommandLine="ipv6.disable=1"
networkingMode=mirrored
```

- `kernelCommandLine="ipv6.disable=1"` - Disables IPv6 for WSL2.
- `networkingMode=mirrored` - The network interfaces of your Windows host are mirrored into the WSL2 environment.