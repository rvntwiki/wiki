---
title: Useful Commands
Summary: Useful powershell commands
Author: rvnt
Date: 2021-04-18
base_url: https://sanctum.rvnt.uk/
---

Find Files Recursively
---

``` powershell
Get-ChildItem -Path $(Get-Location) -File -Include *.mp3 -Recurse
```

To delete all found files

``` powershell
Get-ChildItem -Path $(Get-Location) -File -Include *.mp3 -Recurse | Remove-Item -Force -Verbose
```

Find Directories Last Modified x Days Ago
---

``` powershell
Get-ChildItem -Path $(Get-Location) -dir -Recurse | Where-Object {$_.LastWriteTime -lt (Get-Date).AddDays(-1000)}
```

Output to a file:

``` powershell
Get-ChildItem -Path $(Get-Location) -dir -Recurse | Where-Object {$_.LastWriteTime -lt (Get-Date).AddDays(-1000)} >> C:\Users\**username**\Desktop\**filename**.txt
```

Force delete directories:

``` powershell
Get-ChildItem -Path $(Get-Location) -dir -Recurse | Where-Object {$_.LastWriteTime -lt (Get-Date).AddDays(-1000)} | Remove-Item -Force -Verbose -Recurse
```