---
title: Bitwarden
Summary: Setting up Bitwarden
Author: rvnt
Date: 2021-04-13
base_url: https://sanctum.rvnt.uk/
---

Bitwarden
===

Setting up Bitwarden
---

1. Download a [Bitwarden Client](http://www.dereferer.org/?https://bitwarden.com/download/).

    Install the web browser and mobile apps. Desktop is optional.
    
    ![Bitwarden-Downloads](img/bitwarden/1-bitwarden.png)

1. From the [Bitwarden Website](http://www.dereferer.org/?https://bitwarden.com/) select 'Getting Started'.
    
    ![Bitwarden-Downloads](img/bitwarden/2-bitwarden.png)

!!! help "Master Password"

    Since Bitwarden will be used to secure all other passwords it is vital to use a strong yet memorable password or passphrase.

    An easy way to do this is to pick a memorable saying, book phrase, or lyric and replace characters with symbols.

    e.g. *ItsyBitsySpiderRanUpTheWaterSpout* would become *!t5yB!tsy$pid£rR@nUp7heW4terSp0ut*

    At least 16 characters, uses upper and lower case, uses numbers and symbols.

!!! warning

    Forgetting your master password may lead to permanent loss of access to your Bitwarden vault. **Keep it safe!**

1. Fill out the form to create and account and select 'Submit'.

    ![Bitwarden-SignUp](img/bitwarden/3-bitwarden.png)

1. Once created you can now log into your apps using your email and password.

1. Depending on the app used, once logged in you can start saving passwords.

    **The other apps will have similar screens for adding new items.**

    If you're using the browser app, click + in the top right of the Bitwarden menu.

    ![Bitwarden-Add](img/bitwarden/4-bitwarden.png)

    You can manually add existing username and passwords to save in Bitwarden.

    ![Bitwarden-NewLogin](img/bitwarden/5-bitwarden.png)

    Once done select 'Save' in the top right corner.

!!! tip "Other Items"

    You can save other useful items in your Bitwarden vault.

    ![Bitwarden-items](img/bitwarden/6-bitwarden.png)


Using Bitwarden
---

1. Once items have been added to your vault you can start using Bitwarden to autofill login and other web forms.

    If autofill fails you can right click / press and hold to open the Bitwarden menu to manually fill logins.

    ![Bitwarden-ManualFill](img/bitwarden/7-bitwarden.png)

Generating Passwords
---

1. You can use Bitwarden to generate passwords when signing up for new accounts.

    Since you only need to remember you master password you can generate complex passwords and let Bitwarden do the remembering.

    example:
    > GiWgh7&v5kwvCQ3da42A5hqe$8HLSPCt

    ![Bitwarden-GeneratingPasswords](img/bitwarden/8-bitwarden.png)

    [A 18 character password using uppercase, lowercase, numbers, and symbols would take 7,000,000,000,000,000 (seven quadrillion) years to crack](http://www.dereferer.org/?https://itigic.com/how-long-does-it-take-to-hack-or-crack-a-password/#This_is_the_time_to_crack_keys_between_12_and_18_characters) so using randomly generated 32 character passwords with the same requirement should take a tad longer, just to be safe.

1. You can also view all recently generated passwords and when they were generated if needed.

    ![Bitwarden-PasswordHistory](img/bitwarden/9-bitwarden.png)

