---
title: Excel
Summary: Useful Excel Tips
Author: rvnt
Date: 2022-01-25
base_url: https://sanctum.rvnt.uk/
---

# Microsoft Excel :material-microsoft-excel:

## Shortcuts

###  Duplicate the Cell Above

1. Select an empty located beneath the cell you want to duplicate.
1. Press ++ctrl+d++.

    ![Excel: Duplicate Cell Above](img/excel/DuplicateCellAbove-1.png){ #center }

---

### Duplicate Multiple Cells

1. Select a range of cells ensuring the top row contains data for each column.
1. Press ++ctrl+d++.

    ![Excel: Duplicate Multiple Cells](img/excel/DuplicateMultipleCells-1.png){ #center }

---

### Finding and Replacing Text

1. Press ++ctrl+h++.
1. Enter the text to find and replace accordingly.
1. Check the 'Match entire cell contents' option to prevent unintended changes.
    ![Excel: Finding and Replacing Text](img/excel/FindAndReplace-1.png){ #center }

1. Click 'Replace All'

    ![Excel: Finding and Replacing Text](img/excel/FindAndReplace-2.png){ #center }

---

## Formatting

###