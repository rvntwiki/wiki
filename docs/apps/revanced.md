---
title: ReVanced
Summary: Installing and using ReVanced app
Author: rvnt
Date: 2025-01-14
base_url: https://sanctum.spctr.uk/
---

# ReVanced

Customize your mobile experience through ReVanced by applying patches to your applications.

## Install
1. Visit the ReVanced website at [Revanced.app](https://revanced.app/download) and download the ReVanced Manager app.

![ReVanced: Download ReVanced Manager](img/revanced/1-Download.jpg){ .centerAndScaleImage }

---

## Patch
1. Open the ReVanced Manager app and open the 'Patcher' tab.
![ReVanced: Patcher](img/revanced/1-Patcher.jpg){ .centerAndScaleImage }

1. Select 'Select an app'.
![ReVanced: Select an app](img/revanced/2-SelectApp.jpg){ .centerAndScaleImage }

1. Under the app you want to patch, it will give a suggested app version number. Click this to search for that specific version of the app.
![ReVanced: App version](img/revanced/3-AppVersion.jpg){ .centerAndScaleImage }

1. Top result will typically be APK Mirror. Open this link and look for the 'nodpi' version of the app.
![ReVanced: Search results](img/revanced/4-SearchResults.jpg){ .centerAndScaleImage }

1. Follow directions on the website to download the APK.
![ReVanced: Locate APK](img/revanced/5-LocateAPK.jpg){ .centerAndScaleImage }
![ReVanced: Download APK](img/revanced/5-DownloadAPK.jpg){ .centerAndScaleImage }

1. Once downloaded, reopen the ReVanced Manager app and select 'Storage' in the bottom right corner.
![ReVanced: Storage](img/revanced/6-Storage.jpg){ .centerAndScaleImage }

1. Locate and select the app APK you just downloaded.
![ReVanced: Select APK](img/revanced/7-SelectAPK.jpg){ .centerAndScaleImage }

1. (Optional) Select 'Selected patches' to change the patches to be applies.
![ReVanced: Select patches](img/revanced/8-SelectPatches.jpg){ .centerAndScaleImage }

1. Select 'Patch' in the bottom right corner.
![ReVanced: Patch](img/revanced/9-Patch.jpg){ .centerAndScaleImage }

1. Wait for the patching to finish.
![ReVanced: Patching](img/revanced/10-Patching.jpg){ .centerAndScaleImage }

1. Select 'Install' in the bottom right corner and follow on screen instructions.
![ReVanced: Install](img/revanced/11-Install.jpg){ .centerAndScaleImage }
![ReVanced: Disable automatic patching](img/revanced/11-DisableAutoPatching.jpg){ .centerAndScaleImage }
![ReVanced: System install](img/revanced/11-SystemInstall.jpg){ .centerAndScaleImage }

1. Once installed select 'Open' from the bottom right corner.
![ReVanced: Open app](img/revanced/12-Open.jpg){ .centerAndScaleImage }

1. You can now close ReVanced Manager and use the selected app as normal.