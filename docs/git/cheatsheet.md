---
title: Cheat Sheet
Summary: Commonly used Git commands
Author: rvnt
Dataset: 2024-04-12
base_url: https://sanctum.spctr.uk/
---

# Git

## What is Git?

Git is a distributed version control system that tracks changes in any set of computer files1. It’s usually used for coordinating work among programmers who are collaboratively developing source code during software development. Git’s goals include speed, data integrity, and support for distributed, non-linear workflows.

---

### Key Points
- **Snapshots, Not Differences**: Unlike most other Version Control Systems (VCSs) that store information as a list of file-based changes, Git thinks of its data more like a series of snapshots of a miniature filesystem. Every time you commit, or save the state of your project, Git takes a picture of what all your files look like at that moment and stores a reference to that snapshot.
- **Nearly Every Operation Is Local**: Most operations in Git need only local files and resources to operate. This means you see the project history almost instantly. If you want to see the changes introduced between the current version of a file and the file a month ago, Git can look up the file a month ago and do a local difference calculation.
- **Mature and Actively Maintained**: Git is a mature, actively maintained open source project originally developed in 2005 by Linus Torvalds.
- **Most Popular VCS**: Git is the most popular distributed version control system. Git is commonly used for both open source and commercial software development, with significant benefits for individuals, teams, and businesses.

## Commands
---

### config

Command:

`git config` - Get and set repository or global options

Flags:

`--global`

- **Writing**: write to global `~/.gitconfig` file rather than the repository `.git/config`, write to `$XDG_CONFIG_HOME/git/config` file if this file exists and the `~/.gitconfig` file doesn’t.
- **Reading**: read only from global `~/.gitconfig` and from `$XDG_CONFIG_HOME/git/config` rather than from all available files.

`--system`

- **Writing**: write to system-wide `$(prefix)/etc/gitconfig` rather than the repository `.git/config`.
- **Reading**: read only from system-wide `$(prefix)/etc/gitconfig` rather than from all available files.

`--local`

- **Writing**: write to the repository `.git/config` file. This is the default behavior.
- **Reading**: read only from the repository `.git/config` rather than from all available files.

`-l`, `--list`

- List all variables set in config file, along with their values.

Examples:

**Returns a list of information about your git configuration:**

=== "Command"
    ```
    git config -l
    ```
=== "Output"
    ```
    user.name=Ironman
    user.email=tony@stark.com
    core.repositoryformatversion=0
    core.filemode=true
    core.bare=false
    core.logallrefupdates=true
    remote.origin.url=https://github.com/username/repository
    remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
    branch.master.remote=origin
    branch.master.merge=refs/heads/master
    ```

**Setup the user name:**

=== "Command"
    ```
    git config --global user.name "Ironman"
    ```

**Setup the user email address:**

=== "Command"
    ```
    git config --global user.email "tony@stark.com"
    ```

**Store login credentials in the cache**

=== "Command"
    ```
    git config --global credential.helper cache
    ```

---

### init

Command:

`git init` - Initialise a new Git repository

Examples:

**Initialise a new Git repo locally in your project root**

=== "Command"
    ```
    git init
    ```
=== "Output"
    ```
    Initialized empty Git repository in /path/of/repository/directory/.git/
    ```

---

### add

Command:

`git add` - Add files to the staging area

Examples:

**Add specific file to the staging area**

=== "Command"
    ```
    git add filename.ext
    ```

***Add certain files to the staging area**

=== "Command"
    ```
    git add filena*
    ```

**Add all files in current directory to the staging area**

=== "Command"
    ```
    git add .
    ```

---

### status

Command:

`git status` - Show the status of the current repository 

**Show the status of the current repository including staged, unstaged, and untracked files**

Examples:

=== "Command"
    ```
    git status
    ```
=== "Output"
    ```
    On branch master
    Changes to be committed:
    (use "git restore --staged <file>..." to unstage)
            new file:   file1.ext

    Untracked files:
    (use "git add <file>..." to include in what will be committed)
            file2.ext
    ```

---

### commit

Command:

`git commit` - Commit files within the staging area

Examples:

**Commit files within the staging area and be prompted for a commit message**

=== "Command"
    ```
    git commit
    ```
=== "Output"
    A text editor will open to prompt the user the enter a commit message. Once entered and saved, exit the text editor.
    ```
    [master 9ad7175] Commit Message
    1 file changed, 0 insertions(+), 0 deletions(-)
    create mode 100644 file3.ext
    ```

**Commit files within the staging area with included commit message**

=== "Command"
    ```
    git commit -m "Commit Message"
    ```
=== "Output"
    ```
    [master (root-commit) 3fc4e71] Commit Message 2
    1 file changed, 0 insertions(+), 0 deletions(-)
    create mode 100644 file4.ext
    ```

---

### log

Command:

`git log` - Show the commit history for the current repository

Flags:

`--oneline`

- This is designed to be as compact as possible.

Examples:

**Shows the commit history for the current repository**

=== "Command"
    ```
    git log
    ```
=== "Output"
    ```
    commit 9ad717599d4cb174eaf8913f8228978ba03215d0 (HEAD -> master)
    Author: Ironman <tony@stark.com>
    Date:   Sat Apr 13 16:37:29 2024 +0100

        Commit Message

    commit 3fc4e71a7aabd4fa2dcff8c6f137fb706b949b12
    Author: Ironman <tony@stark.com>
    Date:   Sat Apr 13 16:33:48 2024 +0100

        Commit Message 2
    ```

---

### diff

Command:

`git diff` - Show only unstaged changes made to files

Examples:

**Show only the unstaged changes that have been made to files**

=== "Command"
    ```
    git diff
    ```
=== "Output"
    ```
    diff --git a/file1.ext b/file2.ext
    index e69de29..9daeafb 100644
    --- a/file1.ext
    +++ b/file2.ext
    @@ -0,0 +1 @@
    +different_text
    ```

---

### rm

Command:

`git rm` - Remove items from the current working tree

Examples:

**Remove a specific file from the current working tree**

=== "Command"
    ```
    git rm file.ext
    ```
=== "Output"
    ```
    rm 'file.ext'
    ```

---

### mv

Command:

`git mv` - Move and rename files in the current working tree

Examples:

**Rename a file**

=== "Command"
    ```
    git mv file.ext new_file.ext
    ```

---

### checkout

Command:

`git checkout` - Switch branches or restore working tree files

Examples:

**Switch branch**

=== "Command"
    ```
    git checkout main
    ```
=== "Output"
    ```
    A       new_file.ext
    D       file2.ext
    M       file3.ext
    Switched to branch 'main'
    ```

**Revert unstaged changes**

=== "Command"
    ```
    git checkout file.ext
    ```
=== "Output"
    ```
    Updated 1 path from the index
    ```

---

### revert

Command:

`git revert` - Revert some existing commits

Examples:

**Rollback the latest commit**

=== "Command"
    ```
    git revert HEAD
    ```
=== "Output"
    ```
    Revert "Commit Message"
    1 file changed, 0 insertions(+), 0 deletions(-)
    rename new_file.txt => test.txt (100%)
    ```

**Rollback to a specfic commit**

=== "Command"
    ```
    git revert 7a7ed4f
    ```
=== "Output"
    ```
    On branch main
    Untracked files:
    (use "git add <file>..." to include in what will be committed)
            file.ext

    nothing added to commit but untracked files present (use "git add" to track)
    ```

---

### branch

Command:

`git branch` - List, create, or delete branches

Examples:

**List branches**

=== "Command"
    ```
    git branch
    ```
=== "Output"
    ```
    * main
    master
    feat/new-feature
    fix/bug-fix
    ```

**Delete branches**

=== "Command"
    ```
    git branch -d fix/bug-fix
    ```
=== "Output"
    ```
    * main
    master
    feat/new-feature
    fix/bug-fix
    ```

---

### merge

Command:

`git merge` - Join two or more development histories together

Examples:

**Merge a branch with the current branch**

=== "Command"
    ```
    git merge feat/new-feature
    ```
=== "Output"
    ```
    Updating a1b2c3d4
    Fast-forward
    file.txt | 3 +++
    1 files changed, 3 insertions(+), 2 deletions(-)
    ```
    
---

### remote

Command:

`git remote` - Manage set of tracked repositories

Examples:

**Add a remote repository**

=== "Command"
    ```
    git add remote https://<repo_host>.com/<username>/<repo>.git
    ```
=== "Output"
    ```
    * remote origin
    Fetch URL: https://<repo_host>.com/<username>/<repo>.git
    Push  URL: https://<repo_host>.com/<username>/<repo>.git
    HEAD branch: master
    Remote branch:
        master new (next fetch will store in remotes/origin)
    Local ref configured for 'git push':
        master pushes to master (up to date)
    ```

**View remote URLs**

=== "Command"
    ```
    git remote -v
    ```
=== "Output"
    ```
    origin  https://<repo_host>.com/<username>/<repo>.git (fetch)
    origin  https://<repo_host>.com/<username>/<repo>.git (push)
    ```

**View information about remote repository**

=== "Command"
    ```
    git remote show origin
    ```
=== "Output"
    ```
    * remote origin
    Fetch URL: https://<repo_host>.com/<username>/<repo>.git
    Push  URL: https://<repo_host>.com/<username>/<repo>.git
    HEAD branch: master
    Remote branch:
        master tracked
    Local branch configured for 'git pull':
        master merges with remote master
    Local ref configured for 'git push':
        master pushes to master (up to date)
    ```

---

### push

Command:

`git push` - Update remote refs along with associated objects

Examples:

**Push changes to a remote repository**

=== "Command"
    ```
    git push
    ```
=== "Output"
    ```
    Counting objects: 9, done.
    Delta compression using up to 8 threads.
    Compressing objects: 100% (5/5), done.
    Writing objects: 100% (5/5), 528 bytes | 528.00 KiB/s, done.
    Total 5 (delta 3), reused 0 (delta 0)
    To https://<repo_host>.com/<username>/<repo>.git
    a1b2c3d..e4f5g6h  master -> master
    ```

---

### pull

Command:

`git pull` - Fetch from and integrate with another repository or a local branc

Examples:

**Pull changes from a remote repository**

=== "Command"
    ```
    git pull
    ```
=== "Output"
    ```
    remote: Enumerating objects: 3, done.
    remote: Counting objects: 100% (3/3), done.
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 2 (delta 0), reused 0 (delta 0), pack-reused 0
    Unpacking objects: 100% (2/2), 663 bytes | 34.00 KiB/s, done.
    From https://<repo_host>.com/<username>/<repo>
    a1b2c3d..e4f5g6h  master     -> origin/master
    Updating a1b2c3d..e4f5g6h
    Fast-forward
    file1.txt | 2 +-
    1 file changed, 1 insertion(+), 1 deletion(-)
    ```

---

### getch

Command:

`git fetch` - Download objects and refs from another repository

Examples:

**Fetch remote repo changes without merging**

=== "Command"
    ```
    git fetch
    ```
=== "Output"
    ```
    $ git fetch origin
    remote: Enumerating objects: 143, done.
    remote: Compressing objects: 100% (143/143), done.
    remote: Total 143 (delta 118), reused 0 (delta 0)
    Receiving objects: 100% (143/143), 16.54 KiB | 1.65 MiB/s, done.
    From https://<repo_host>.com/<username>/<repo>
    a1b2c3d..e4f5g6h  master     -> origin/master

    ```

---

<!-- ###

Command:

`` -

Examples:

****

=== "Command"
    ```
    ```
=== "Output"
    ```
    ```

--- -->

### .gitignore

A .gitignore file is a text file that tells Git which files or directories to ignore in a project. It’s a way to avoid committing unnecessary files to the Git repository, such as:

- Log files
- Temporary files
- Build files and directories (like a /dist or /build directory)
- System files that may vary between different systems but are not needed for the project
- Dependencies that can be reinstalled with package managers (like node_modules in JavaScript projects)
- Personal IDE configuration files
- Files containing sensitive data (like a .env file containing API keys)

Each line in a .gitignore file specifies a pattern. When deciding whether to ignore a path, Git normally checks .gitignore patterns from multiple sources in the following order:

- Patterns read from the command line for those commands that support them.
- Patterns read from a .gitignore file in the same directory as the path, or in any parent directory, with patterns in the higher level files (up to the top-level of the work tree) being overridden by those in lower level files down to the directory containing the file.
- Patterns read from a file specified by the configuration variable core.excludesFile.

Example:

```
# ignore all .txt files
*.txt

# but do track lib.txt, even though you're ignoring .txt files above
!lib.txt

# ignore all files in the build/ directory
build/

# ignore doc/notes.txt, but not doc/server/arch.txt
doc/*.txt

# ignore all .pdf files in the doc/ directory and any of its subdirectories
doc/**/*.pdf
```

---

Credits: Man Pages &  https://www.freecodecamp.org/news/git-cheat-sheet/

---