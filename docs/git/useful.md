---
title: Useful
Summary: Useful tips for Git
Author: rvnt
Dataset: 2024-11-12
base_url: https://sanctum.spctr.uk/
---

# Useful

## Multiple Git Accounts

[Source](https://stackoverflow.com/questions/3225862/multiple-github-accounts-ssh-config)

### .ssh/config

Add the required keys to the ssh config file. Typically located within users home directory. e.g. `~/.ssh/config`

```
Host github.com
        Hostname github.com
        User git
        IdentityFile ~/.ssh/github-private-key-1
Host host.github.com
        Hostname github.com
        User git
        IdentityFile ~/.ssh/github-private-key-2
```
<p id=caption>Example .ssh/config file</p>


### .git/config

Update git config file as required. Typically located within each projects directory. e.g. `~/ProjectName/.git/config`

```
[core]
    repositoryformatversion = 0
    filemode = true
    bare = false
    logallrefupdates = true
[remote "origin"]
    url = https://github.com/AccountName/RepositoryName.git
    fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
    remote = origin
    merge = refs/heads/master
```
<p id=caption>Example .git/config file</p>

The url field would change from

`url = https://github.com/AccountName/RepositoryName.git`

to

`url = https://host.github.com/AccountName/RepositoryName.git`

Todo: _Confirm whether the custom URL can be made without manually editing .git/config file._

### Test

Test the ssh connection works.

`ssh -T git@host.github.com`

If successful the following message will be displayed

`Hi Username! You've successfully authenticated, but GitHub does not provide shell access.`